<?php
	$file = $_GET['file'];
	$filename = basename($file);
	$exp = strtolower(substr(strrchr($filename,"."),1));
	if($file && ($exp=='css' || $exp=='js' || $exp=='jpg' || $exp=='png' || $exp=='gif' || $exp=='swf')) {
		$buffer = file_get_contents($file);
		header("Connection: keep-alive");
		header("Cache-Control: max-age=604800, public");
		header("Expires: ".date("D, d M Y 00:00:00 GTM", time()+604800));
		if($exp == 'js') {
			header('Content-Type: application/javascript');
			$buffer = $buffer;
		}
		elseif($exp == 'css') {
			header("Content-type: text/css");
			$buffer = minifyCSS($buffer);
		}
		else {
			$ctype = '';
			switch($exp) {
				case "swf": $ctype="image/swf"; break;
				case "gif": $ctype="image/gif"; break;
				case "png": $ctype="image/png"; break;
				case "jpeg":
				case "jpg": $ctype="image/jpeg"; break;
				default:
			}
			if($ctype) header('Content-type: '.$ctype);
		}
		echo $buffer;
		exit;
	}
	
	function minifyCSS($buffer)
	{
		$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
		$buffer = str_replace(array("\r\n","\r","\n","\t",'  ','    ','     '), '', $buffer);
		$buffer = preg_replace(array('(( )+{)','({( )+)'), '{', $buffer);
		$buffer = preg_replace(array('(( )+})','(}( )+)','(;( )*})'), '}', $buffer);
		$buffer = preg_replace(array('(;( )+)','(( )+;)'), ';', $buffer);
		return $buffer;
	}
?>