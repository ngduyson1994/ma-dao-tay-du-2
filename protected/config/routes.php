<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = "main";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['home'] = "main";

/* Sitemap */
$route['sitemap.xml'] = "sitemap";
$route['sitemap.xsl'] = "sitemap/style";
$route['post-sitemap.xml'] = "sitemap/post";
$route['post-sitemap-(:any).xml'] = "sitemap/post/$1";
$route['sitemap.xsl'] = "sitemap/style";
$route['video-sitemap.xml'] = "sitemap/video";
$route['video-sitemap-(:any).xml'] = "sitemap/video/$1";
$route['page-sitemap.xml'] = "sitemap/page";
$route['page-sitemap-(:any).xml'] = "sitemap/page/$1";
$route['category-sitemap.xml'] = "sitemap/category";
$route['category-sitemap-(:any).xml'] = "sitemap/category/$1";
$route['tag-sitemap.xml'] = "sitemap/tag";
$route['tag-sitemap-(:any).xml'] = "sitemap/tag/$1";

/* Backend */
$route['manager'] = "manager";
$route['manager/(:any)'] = "manager/$1";
$route['manager/(:any)/(:any)'] = "manager/$1/$2";

/* Frontend */
$route['match/(:any)'] = "match";
$route['captcha.png'] = "captcha";
$route['channel/(:any)'] = "channels";
$route['kols/(:any)'] = "kols";
$route['live'] = "livestream";
$route['api'] = "api";
$route['api/(:any)'] = "api";
$route['api/(:any)/(:any)'] = "api";
$route['search'] = "search";
$route['all-channels'] = "all_channel";
$route['all-players'] = "all_player";
$route['video'] = "videos";
$route['news'] = "news";
$route['(:any)'] = "categories";
$route['(:any)/news'] = "categories";
$route['(:any)/video'] = "categories";
$route['video/(:any)'] = "single";
$route['tag/(:any)'] = "tags";
$route['tag/(:any)/(:any)'] = "tags";
$route['ajax/(:any)'] = "ajax/$1";
$route['(:any)/(:any)'] = "single";

