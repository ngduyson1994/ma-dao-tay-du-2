<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        exit;
    }

    public function tracking()
    {
        exit;
        $action = $this->input->get('action');
        $live_id = intval($this->input->get('live_id'));
        $this->load->model('online_model');
        $customer_id = get_cookie('customer_id');
        if ($action == 'exit') {
            if ($customer_id) $this->online_model->remove($customer_id, $live_id);
        } else {
            if (!$customer_id) {
                $customer_id = md5(rand(100000, 999999) . time() . getIP());
                set_cookie('customer_id', $customer_id, 86400 * 30);
            }
            $this->online_model->init($customer_id, $live_id);
            echo 'INIT-' . $customer_id . '-' . $live_id . '-' . time();
        }
    }

    public function live()
    {
        exit;
        if (!$this->input->is_ajax_request()) exit;
        $live_id = intval($this->input->get('live_id'));
        $this->load->model('online_model');
        echo number_format(intval($this->online_model->total($live_id)), 0, ',', '.');
    }

    public function searchPost()
    {
        $json = array();
        $search = strip_tags($this->input->get('search'));
        if ($search) {
            $parameter = array();
            $parameter['limit'] = 5;
            $parameter['arg'][] = array(array('title'), 'search', $search);
			$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');
            $parameter['with'] = array('relationships');
            $posts = $this->xcache->model('post_model', 'get_all', array($parameter));
            if (isset($posts) && is_array($posts)) {
                $i = 0;
                foreach ($posts as $post) {
                    $json['data'][$i]['id'] = $post['id'];
                    $json['data'][$i]['title'] = clearString($post['title']);
                    $json['data'][$i]['slug'] = $post['slug'];
                    $json['data'][$i]['thumb'] = $post['thumb'];
                    $json['data'][$i]['url'] = getPostURL($post);
                    $i++;
                }
                $origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
                $allowed_domains = [
                    'http://gametv.vn/',
                ];

                if (in_array($origin, $allowed_domains)) {
                    header('Access-Control-Allow-Origin: ' . $origin);
                }
                $this->output->set_header("Access-Control-Allow-Origin: *");
                $this->output->set_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin");
                $this->output->set_header("Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS");
                $this->output->set_header("Access-Control-Allow-Headers: Content-Type,X-CSRF-Token, XHR, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name");
                $this->output->set_header("Access-Control-Allow-Credentials: true");
                $this->output->set_status_header(200);
                $this->output->set_content_type('application/javascript; charset=utf-8');
                $this->output->set_header("Expires: 0");
                $this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
                $this->output->set_header("Pragma: no-cache");

                $json = json_encode($json);
                $json = isset($_GET['callback']) ? "{$_GET['callback']}($json)" : $json;
                $this->output->set_output($json);
                echo $json;
                exit;
            }
        }
    }

    public function set_Cookie()
    {
        $token = $this->input->get('token');
        $user_hash = $this->input->get('user_hash');
        $time = $this->input->get('time');
        set_cookie('token', $token, $time);
        set_cookie('user_hash', $user_hash, $time);
        $json = array('status' => true, 'msg' => 'Set Cookie thành công!');
        $this->output->set_header("Access-Control-Allow-Origin: *");
        $this->output->set_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin");
        $this->output->set_header("Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS");
        $this->output->set_header("Access-Control-Allow-Headers: Content-Type,X-CSRF-Token, XHR, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name");
        $this->output->set_header("Access-Control-Allow-Credentials: true");
        $this->output->set_status_header(200);
        $this->output->set_content_type('application/javascript; charset=utf-8');
        $json = json_encode($json);
        $json = isset($_GET['callback']) ? "{$_GET['callback']}($json)" : $json;
        echo $json;
        exit;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/Welcome.php */
