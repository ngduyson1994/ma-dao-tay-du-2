<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Api extends CI_Controller {
	private $token_key = 'gfOmNYHwF7Ps';
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$method = $this->uri->segment(2);
		$action = $this->uri->segment(3);
		$input = $this->input->get();
		$hash = '';
		if(count($input) > 1) {
			foreach($input as $key=>$value) {
				if($key != 'token') $hash .= $value;
			}
			$token = md5($hash.$this->token_key);
		} else $token = md5('NULL'.$this->token_key);
		/*if(!isset($input['token'])) $json = array('status'=>'false','message'=>'Vui lòng nhập token');
		if(!isset($json) && $token != $input['token']) $json = array('status'=>'false','message'=>'Token không hợp lệ');*/
		
		if(!isset($json)) {
			switch($method) {
				case 'getListSchedule':
					$json = $this->getListSchedule($action);
				break;
				case 'getFeatureHomeSchedule':
					$json = $this->getFeatureHomeSchedule($action);
				break;
				case 'getDetailSchedule':
					$json = $this->getDetailSchedule($action);
				break;
				case 'getListPosts':
					$json = $this->getListPosts($action);
				break;
			}
		}
		if(isset($json) && is_array($json)) {
            $this->output->set_header("Access-Control-Allow-Origin: *");
            $this->output->set_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin");
            $this->output->set_header("Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS");
            $this->output->set_header("Access-Control-Allow-Headers: Content-Type,X-CSRF-Token, XHR, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name");
            $this->output->set_header("Access-Control-Allow-Credentials: true");
            $this->output->set_status_header(200);
            $this->output->set_content_type('application/javascript; charset=utf-8');
			$json = json_encode($json);
            $json = isset($_GET['callback'])?"{$_GET['callback']}($json)":$json;
            $this->output->set_output($json);
		}
	}
	protected function getListPosts() {
		$page = intval($this->input->get('page'));
		$json = array();
		$parameter = array();
		$parameter['arg'][] = array('type', 'news');
		$parameter['arg'][] = array('status', 'publish');
		$parameter['arg'][] = array('is_slider', 0);
		$parameter['arg'][] = array('feature', 0);
		$parameter['with'] = array('relationships','admin');
//		$parameter['with'] = array('relationships','category');
		$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');
		$total = $data['posts'] = $this->xcache->model('post_model', 'count_rows', array($parameter));
		$parameter['paginate'] = array('rows' => 5, 'total' => $total);
		$posts = $this->xcache->model('post_model', 'paginate', array($parameter));
		
        $i=0;
		if(isset($posts) && is_array($posts)) {
			foreach($posts as $post) {
				$json['posts'][$i]['id'] = $post['id'];
				$json['posts'][$i]['title'] = $post['title'];
				$json['posts'][$i]['slug'] = $post['slug'];
				$json['posts'][$i]['type'] = $post['type'];
				$json['posts'][$i]['url'] = getPostURL($post);
				$json['posts'][$i]['thumb'] = getThumb($post, 380, 240);
				$json['posts'][$i]['summary'] = (isset($post['summary']) && $post['summary'])?cutOf(strip_tags($post['summary']),130):cutOf(strip_tags($post['content']),130);
				$json['posts'][$i]['category'] = (isset($post['category']['slug']) && $post['category']['slug'])?reset($post['category']['slug']):'GTV News';
				$json['posts'][$i]['user'] = (isset($post['admin']['name']) && $post['admin']['name'])?$post['admin']['name']:'GTV News';
				$json['posts'][$i]['published'] = showDate($post['published']);
				$i++;
			}
		}
		
		if(true) return array('status'=>true,'message'=>'', 'data'=>$json);
		else return array('status'=>false,'message'=>'Không có dữ liệu');
	}
	protected function getFeatureHomeSchedule() {
		$json = array();
		$parameter = array();
		$parameter['arg'][] = array('featured', 1);
		$parameter['with'] = array('home', 'away');
		$parameter['order'] = array('order_by'=>'start_time', 'order'=>'asc');
		$parameter['limit'] = 1;
		$schedule = $this->xcache->model('schedule_model', 'get_all', array($parameter));
		if(isset($schedule) && is_array($schedule)) {
			if($schedule){
				$json['id'] = $schedule[0]['id'];
				$json['name'] = $schedule[0]['name'];
				$json['status'] = $schedule[0]['status'];
				$json['start_time'] = date("H:i d/m/Y", $schedule[0]['start_time']);
				$stream_code = str_replace("\r\n", "\n", $schedule[0]['stream_code']);
				$stream_code = explode("\n", $stream_code);
				$json['stream_code'] = $stream_code;
			} else return array('status'=>false,'message'=>'Kênh đang offline');
		}
		if(true) return array('status'=>true,'message'=>'', 'data'=>$json);
		else return array('status'=>false,'message'=>'Không có dữ liệu');
	}
	
	protected function getListSchedule() {
		$type = $this->input->get('type');
		$page = intval($this->input->get('page'));
		$game = $this->input->get('game');
		$json = array();
		$parameter = array();
		if(isset($game) && $game) {
			$parameter['arg'][] = array('game', $game);
		}
		$parameter['with'] = array('home', 'away');
		$parameter['order'] = array('order_by'=>'start_time', 'order'=>'desc');
		$total = $data['posts'] = $this->xcache->model('schedule_model', 'count_rows', array($parameter));
		$parameter['paginate'] = array('rows' => 9, 'total' => $total);
		$schedules = $this->xcache->model('schedule_model', 'paginate', array($parameter));
        $i=0;
		if(isset($schedules) && is_array($schedules)) {
			foreach($schedules as $schedule) {
				$json['schedules'][$i]['id'] = $schedule['id'];
				$json['schedules'][$i]['name'] = $schedule['name'];
				$json['schedules'][$i]['description'] = $schedule['description'];
				$json['schedules'][$i]['avatar'] = $schedule['avatar'];
				$json['schedules'][$i]['status'] = $schedule['status'];
				$json['schedules'][$i]['game'] = $schedule['game'];
				$json['schedules'][$i]['start_time'] = date("H:i d/m/Y", $schedule['start_time']);
				$stream_code = str_replace("\r\n", "\n", $schedule['stream_code']);
				$stream_code = explode("\n", $stream_code);
				$json['schedules'][$i]['stream_code'] = $stream_code;
				$i++;
			}
		}
		
		if(true) return array('status'=>true,'message'=>'', 'data'=>$json);
		else return array('status'=>false,'message'=>'Không có dữ liệu');
	}
	protected function getDetailSchedule() {
		$json = array();
		$id = $this->input->get('id');
		if($id) {
			$this->load->model('schedule_model');
			$parameter['arg'] = $id;
			$schedule = $this->schedule_model->get($parameter);
			if(isset($schedule) && is_array($schedule)) {
				if($schedule){
					$json['id'] = $schedule['id'];
					$json['name'] = $schedule['name'];
					$json['status'] = $schedule['status'];
					$json['start_time'] = date("H:i d/m/Y", $schedule['start_time']);
					$stream_code = str_replace("\r\n", "\n", $schedule['stream_code']);
					$stream_code = explode("\n", $stream_code);
					$json['stream_code'] = $stream_code;
					return array('status'=>'success','message'=>'', 'data'=>$json);
				} else return array('status'=>false,'message'=>'Kênh không tồn tại');
			}
		} else return array('status'=>false,'message'=>'Kênh không tồn tại');
	}
}