<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Categories extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('post');
		$this->load->helper('frontend');
		$this->load->model('post_model');
		$this->load->model('category_model');
	}
	public function index()
	{
		/* Load Setting */
		$data = array();
		$data = init_data();
		$slug = $this->uri->segment(1);
		$type = $this->uri->segment(2);
		$parameter['arg'][] = array('slug', $slug);
		$data['category'] = $this->xcache->model('category_model', 'get', array($parameter));

		if(isset($data['category']['id'])) {
			/* Get List Category */
			$data['page'] = 'categories';

			/* Order */
			$orderby = $this->input->get('orderby');
			$orderby = ($orderby=='views' || $orderby=='title' || $orderby=='date' || $orderby=='rand')?$orderby:'id';
			if($orderby == 'rand') $orderby = 'rand()';
			if($orderby == 'date') $orderby = 'published';
			if($orderby == 'title') $order = '';
			else $order ='DESC';
			if($this->input->get('date-start')) $arg[] = array('published', '>', strtotime(str_replace('-', '/', $this->input->get('date-start'))));
			if($this->input->get('date-end')) $arg[] = array('published', '<', strtotime(str_replace('-', '/', '23:59 '.$this->input->get('date-end'))));
			/* Get Data */
			$arg[] = array('category', $data['category']['id']);
			$arg[] = array('status', 'publish');
			if($type=='video') {
				$data['type'] = 'video';
				$arg[] = array('type', 'video');
			} elseif($type=='news') {
				$data['type'] = 'news';
				$arg[] = array('type', 'news');
			} else {
				$data['type'] = 'news';
				$arg[] = array('type', 'news');
			}
			/* Pagination */
			$parameter['arg'] = $arg;
			$parameter['with'] = array('relationships','admin');
			$total = $data['posts'] = $this->xcache->model('post_model', 'count_rows', array($parameter));
			$parameter['paginate'] = array('rows' => 10, 'total' => $total);
			$parameter['order'] = array('order_by'=>$orderby, 'order'=>$order);
			/* Get List Post */
			$data['posts'] = $this->xcache->model('post_model', 'paginate', array($parameter));
			//print_r($data['posts']); die();
			/* Render Views */
			if(isset($data['category']['name'])) {
				$data['cat_id'] = $data['category']['id'];
				$data['cat_slug'] = $data['category']['slug'];
				$data['title'] = $data['category']['name'];
				$data['icon'] = $data['category']['icon'];
			}
			
			$data['categories'] = $this->category_model->getList();
			
			$template = getTemplate();
			if($template!='frontend' && $template!='mobile') $template='frontend';
			$this->load->view($template.'/header', $data);
			$this->load->view($template.'/archives', $data);
			$this->load->view($template.'/footer', $data);
		} else {
			$template = getTemplate();
			if($template!='frontend' && $template!='mobile') $template='frontend';
			$this->load->view($template.'/header', $data);
			$this->load->view($template.'/404', $data);
			$this->load->view($template.'/footer', $data);
		}
	}
}