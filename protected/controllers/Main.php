<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data = array();
		$data = init_data();
		$data['page'] = 'home';
		/* Render Views */
		$template = getTemplate();
		if($template!='frontend' && $template!='mobile') $template='frontend';
		$this->load->view($template.'/header', $data);
		$this->load->view($template.'/main', $data);
		$this->load->view($template.'/footer', $data);
	}
}