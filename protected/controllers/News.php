<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data = array();
		$data = init_data();
		$data['page'] = 'news';
		
		/* Render Views */
		$parameter = array();
        $parameter['arg'][] = array('type', 'news');
        $parameter['arg'][] = array('category', 1);
        $parameter['with'] = array('relationships');
        $parameter['order'] = array('order'=>'DESC', 'order_by'=>'id');
        $data['post_1'] = $this->xcache->model('post_model', 'get_all', array($parameter));
        /* Tin tức*/
        $parameter = array();
        $parameter['arg'][] = array('type', 'news');
        $parameter['arg'][] = array('category', 2);
        $parameter['with'] = array('relationships');
        $parameter['order'] = array('order'=>'DESC', 'order_by'=>'id');
        $data['post_2'] = $this->xcache->model('post_model', 'get_all', array($parameter));
        /* Tin tức*/
        $parameter = array();
        $parameter['arg'][] = array('type', 'news');
        $parameter['arg'][] = array('category', 3);
        $parameter['with'] = array('relationships');
        $parameter['order'] = array('order'=>'DESC', 'order_by'=>'id');
        $data['post_3'] = $this->xcache->model('post_model', 'get_all', array($parameter));

        /* Sự kiện*/
        $parameter = array();
        $parameter['arg'][] = array('type', 'news');
        $parameter['arg'][] = array('category', 4);
        $parameter['with'] = array('relationships');
        $parameter['order'] = array('order'=>'DESC', 'order_by'=>'id');
        $data['post_4'] = $this->xcache->model('post_model', 'get_all', array($parameter));

		$template = getTemplate();
		if($template!='frontend' && $template!='mobile') $template='frontend';
		$this->load->view($template.'/header', $data);
		$this->load->view($template.'/news', $data);
		$this->load->view($template.'/footer', $data);
	}
}