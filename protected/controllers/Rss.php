<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rss extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data['settings'] = $this->cache->model('setting', 'getList');
		$arg = array('limit'=>'20', 'orderby'=>'created');
		$data['posts'] = $this->cache->model('post', 'get', array($arg));
		$this->load->view('frontend/rss', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/Welcome.php */