<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->helper('post');
        $this->load->helper('frontend');
    }
    public function index()
    {
        /* Load Setting */
        $data = array();
        $data = init_data();
        $parameter['limit'] = 10;
        $s = strip_tags($this->input->get('s'));
        if($s) {
            $data['page'] = 'search';
            /* Order */
            $orderby = $this->input->get('orderby');
            $orderby = ($orderby=='views' || $orderby=='title' || $orderby=='comments' || $orderby=='rand')?$orderby:'id';
            if($orderby == 'rand') $orderby = 'rand()';
            if($orderby == 'title') $order = ''; else $order ='DESC';
            /* Get Data */
            $search = clearUTF($s);
            $arg[] = array(array('title','content','slug'), 'search', $search);
            $arg[] = array('status', 'publish');
            $arg[] = array('type', '<>', 'page');
            /* Pagination */
            $parameter['arg'] = $arg;
            $parameter['with'] = array('relationships');
            $total = $data['posts'] = $this->xcache->model('post_model', 'count_rows', array($parameter));
            $parameter['paginate'] = array('rows' => 12, 'total' => $total);
            $parameter['order'] = array('order_by'=>$orderby, 'order'=>$order);
            /* Get List Post */
            $data['posts'] = $this->xcache->model('post_model', 'paginate', array($parameter));
            /* Render Views */
            $data['title'] = ' : '.clearString($s);
            $template = getTemplate();
            if($template!='frontend' && $template!='mobile') $template='frontend';
            $this->load->view($template.'/header', $data);
            $this->load->view($template.'/search', $data);
            $this->load->view($template.'/footer', $data);
        } else redirect(URL);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/Welcome.php */