<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Single extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$uri = ($this->uri->uri_string())?URL.'/'.$this->uri->uri_string().'/':URL.'/';
		$this->session->set_userdata(array('ref'=>$uri));
		$slug = $this->uri->segment(2);
		$data = array();
		$cat = null;
		$data = init_data();
		$this->load->model('post_model');
		$parameter['arg'][] = array('slug', $slug);
        $parameter['with'] = array('relationships','admin','home','away');
		$data['post'] = $this->xcache->model('post_model', 'get', array($parameter));
		if(isset($data['post']['id'])) {
			$this->post_model->countViews($data['post']['id']);
			$data['page'] = 'posts';
			if(isset($data['post']['title'])) $data['title'] = $data['post']['title'].' | '.$data['settings']['site_title'];
			if(isset($data['post']['description']) && $data['post']['description'])
				$data['description'] = $data['post']['description'];
			if(isset($data['post']['keyword']) && $data['post']['keyword'])
				$data['keyword'] = $data['post']['keyword'];
			
			$template = getTemplate();
			if($template!='frontend' && $template!='mobile') $template='frontend';
			if($data['post']['type']=='video'){
				if(count($data['post']['categories']) > 0) $cat = reset($data['post']['categories']);
			
				$parameter = array();
				$parameter['arg'][] = array('type', 'video');
				$parameter['arg'][] = array('status', 'publish');
				if($cat['id']) {
					$parameter['arg'][] = array('category', $cat['id']);
				}
				$parameter['exclude'][] = $data['post']['id'];
				$parameter['limit'] = 10;
				$parameter['with'] = array('relationships');
				$parameter['order'] = array('order'=>'DESC', 'order_by'=>'id');
				$data['post_realted'] = $this->xcache->model('post_model', 'get_all', array($parameter));
				
				$parameter = array();
				$parameter['arg'][] = array('type', 'news');
				$parameter['arg'][] = array('status', 'publish');
				if($cat['id']) {
					$parameter['arg'][] = array('category', $cat['id']);
				}
				$parameter['exclude'][] = $data['post']['id'];
				$parameter['limit'] = 3;
				$parameter['with'] = array('relationships');
				$parameter['order'] = array('order'=>'DESC', 'order_by'=>'id');
				$data['post_realted_news'] = $this->xcache->model('post_model', 'get_all', array($parameter));
				
				$this->load->view($template.'/header', $data);
				$this->load->view($template.'/single_video', $data);
				$this->load->view($template.'/footer', $data);
			}
			if($data['post']['type']=='news'){
				if(count($data['post']['categories']) > 0) $cat = reset($data['post']['categories']);
			
				$parameter = array();
				$parameter['arg'][] = array('type', 'news');
				$parameter['arg'][] = array('status', 'publish');
				if($cat['id']) {
					$parameter['arg'][] = array('category', $cat['id']);
				}
				$parameter['exclude'][] = $data['post']['id'];
				$parameter['limit'] = 6;
				$parameter['with'] = array('relationships');
				$parameter['order'] = array('order'=>'DESC', 'order_by'=>'id');
				$data['post_realted'] = $this->xcache->model('post_model', 'get_all', array($parameter));
				
				$this->load->view($template.'/header', $data);
				$this->load->view($template.'/single_news', $data);
				$this->load->view($template.'/footer', $data);
			}
		} else {
			$template = getTemplate();
			if($template!='frontend' && $template!='mobile') $template='frontend';
			$this->load->view($template.'/header', $data);
			$this->load->view($template.'/404', $data);
			$this->load->view($template.'/footer', $data);
		}
	}
}