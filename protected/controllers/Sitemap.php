<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends CI_Controller {
	private $limit = 1000;
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data = array();
		$data = init_data();
		$parameter = array();
		$parameter['arg'][] = array('status', 'publish');
		$parameter['arg'][] = array('type', 'news');
		$data['count_post'] = $this->xcache->model('post_model', 'count_rows', array($parameter));
		$parameter['order'] = array('order_by'=>'published', 'order'=>'DESC');
		$parameter['limit'] = 1;
		$data['lastpost'] = $this->xcache->model('post_model', 'get', array($parameter));
		$parameter = array();
		$parameter['arg'][] = array('status', 'publish');
		$parameter['arg'][] = array('type', 'video');
		$data['count_video'] = $this->xcache->model('post_model', 'count_rows', array($parameter));
		$parameter['order'] = array('order_by'=>'published', 'order'=>'DESC');
		$parameter['limit'] = 1;
		$data['lastvideo'] = $this->xcache->model('post_model', 'get', array($parameter));
		$data['count_category'] = $this->xcache->model('category_model', 'count_rows');
		$data['count_tag'] = $this->xcache->model('tag_model', 'count_rows');
		$data['limit'] = $this->limit;
		header("Content-Type: text/xml");
		header("Pragma: no-cache");
		header("X-Robots-Tag: noindex, follow");
		$this->load->view('frontend/sitemap/index', $data);
	}
	public function post($page = 1)
	{
		$data = array();
		$data = init_data();
		$parameter['arg'][] = array('status', 'publish');
		$parameter['arg'][] = array('type', 'news');
		$parameter['order'] = array('order_by'=>'published', 'order'=>'DESC');
		$parameter['limit'] = $this->limit;
		$parameter['offset'] = ($page-1)*$this->limit;
		$data['posts'] = $this->xcache->model('post_model', 'get_all', array($parameter));
		$data['page'] = $page;
		header("Content-Type: text/xml");
		header("Pragma: no-cache");
		header("X-Robots-Tag: noindex, follow");
		$this->load->view('frontend/sitemap/post', $data);
	}
	public function video($page = 1)
	{
		$data = array();
		$data = init_data();
		$parameter['arg'][] = array('status', 'publish');
		$parameter['arg'][] = array('type', 'video');
		$parameter['order'] = array('order_by'=>'published', 'order'=>'DESC');
		$parameter['limit'] = $this->limit;
		$parameter['offset'] = ($page-1)*$this->limit;
		$data['posts'] = $this->xcache->model('post_model', 'get_all', array($parameter));
		$data['page'] = $page;
		header("Content-Type: text/xml");
		header("Pragma: no-cache");
		header("X-Robots-Tag: noindex, follow");
		$this->load->view('frontend/sitemap/post', $data);
	}
	public function page($page = 1)
	{
		$data = array();
		$data = init_data();
		$arg = array('status'=>'publish', 'type'=>'page', 'offset'=>($page-1)*$this->limit, 'limit'=>$this->limit, 'orderby'=>'published');
		$data['posts'] = $this->cache->model('post', 'get', array($arg));
		$data['page'] = $page;
		header("Content-Type: text/xml");
		header("Pragma: no-cache");
		header("X-Robots-Tag: noindex, follow");
		$this->load->view('frontend/sitemap/post', $data);
	}
	public function category($page = 1)
	{
		$data = array();
		$data = init_data();
		$parameter['limit'] = $this->limit;
		$parameter['offset'] = ($page-1)*$this->limit;
		$data['categories'] = $this->xcache->model('category_model', 'get_all', array($parameter));
		$data['page'] = $page;
		header("Content-Type: text/xml");
		header("Pragma: no-cache");
		header("X-Robots-Tag: noindex, follow");
		$this->load->view('frontend/sitemap/category', $data);
	}
	public function tag($page = 1)
	{
		$data = array();
		$data = init_data();
		$parameter['limit'] = $this->limit;
		$parameter['offset'] = ($page-1)*$this->limit;
		$data['tags'] = $this->xcache->model('tag_model', 'get_all', array($parameter));
		$data['page'] = $page;
		header("Content-Type: text/xml");
		header("Pragma: no-cache");
		header("X-Robots-Tag: noindex, follow");
		$this->load->view('frontend/sitemap/tag', $data);
	}
	public function style()
	{
		header("Content-Type: text/xml");
		header("Pragma: no-cache");
		header("X-Robots-Tag: noindex, follow");
		$this->load->view('frontend/sitemap/style', null);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/Welcome.php */
