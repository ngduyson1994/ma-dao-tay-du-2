<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('post');
		$this->load->helper('frontend');
		$this->load->model('post_model');
		$this->load->model('tag_model');
	}
	public function index()
	{	
		$s2 = $this->uri->segment(2);
		$type = $this->uri->segment(3);
		$data = array();
		$data = init_data();
		if($s2) {
			$parameter['arg'][] = array('slug', $s2);
			$data['tag'] = $this->xcache->model('tag_model', 'get', array($parameter));
			if(isset($data['tag']['id'])) {
				/* Get List Category */
				$data['page'] = 'categories';
				/* Order */
				$orderby = $this->input->get('orderby');
				$orderby = ($orderby=='views' || $orderby=='title' || $orderby=='comments' || $orderby=='rand')?$orderby:'id';
				if($orderby == 'rand') $orderby = 'rand()';
				if($orderby == 'title') $order = ''; else $order ='DESC';
				/* Get Data */
				$arg[] = array('tag', $data['tag']['id']);
				$arg[] = array('status', 'publish');
				
				if($type=='video') {
					$data['type'] = 'video';
					$arg[] = array('type', 'video');
				} elseif($type=='news') {
					$data['type'] = 'news';
					$arg[] = array('type', 'news');
				}
				/* Pagination */
				$parameter['arg'] = $arg;
				$total = $data['posts'] = $this->xcache->model('post_model', 'count_rows', array($parameter));
				$parameter['paginate'] = array('rows' => 12, 'total' => $total);
				$parameter['order'] = array('order_by'=>$orderby, 'order'=>$order);
				/* Get List Post */
				$data['posts'] = $this->xcache->model('post_model', 'paginate', array($parameter));
				if(isset($data['tag']['name'])) {
					$data['tag_slug'] = $data['tag']['slug'];
					$data['title'] = 'Tag: '.$data['tag']['name'];
				}
				$template = getTemplate();
				if($template!='frontend' && $template!='mobile') $template='frontend';
				$this->load->view($template.'/header', $data);
				$this->load->view($template.'/tags', $data);
				$this->load->view($template.'/footer', $data);
			} else redirect(URL);
		} else {
			$template = getTemplate();
			if($template!='frontend' && $template!='mobile') $template='frontend';
			$this->load->view($template.'/header', $data);
			$this->load->view($template.'/404', $data);
			$this->load->view($template.'/footer', $data);
		}
	}
}