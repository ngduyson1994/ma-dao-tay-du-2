<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Videos extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data = array();
		$data = init_data();
		$data['page'] = 'videos';
		
		/* Render Views */
		$orderby = $this->input->get('orderby');
		$orderby = ($orderby=='views' || $orderby=='title' || $orderby=='comments' || $orderby=='rand')?$orderby:'id';
		if($orderby == 'rand') $orderby = 'rand()';
		if($orderby == 'title') $order = ''; else $order ='DESC';
		
		$parameter = array();
		$parameter['arg'][] = array('type', 'video');
		$parameter['arg'][] = array('status', 'publish');
		//$parameter['limit'] = 12;
		$parameter['with'] = array('relationships');
		$total = $data['posts'] = $this->xcache->model('post_model', 'count_rows', array($parameter));
		$parameter['paginate'] = array('rows' => 12, 'total' => $total);
		$parameter['order'] = array('order_by'=>$orderby, 'order'=>$order);
		/* Get List Post */
		$data['posts'] = $this->xcache->model('post_model', 'paginate', array($parameter));
		$template = getTemplate();
		if($template!='frontend' && $template!='mobile') $template='frontend';
		$this->load->view($template.'/header', $data);
		$this->load->view($template.'/videos', $data);
		$this->load->view($template.'/footer', $data);
	}
}