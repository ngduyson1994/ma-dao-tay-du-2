<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adcodes extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		auth();
		$this->load->model('adcode_model');
	}
	public function index()
	{
		$data = loadData();
		$data['page'] = 'ad_codes';
		$arg = array();
		if($this->input->get('search')) {
			$q = $this->input->get('search');
			$arg[] = array(array('name'), 'search', $q);
		}
		$posts = $this->input->post();
		if(isset($posts['id'])) {
			$posts['code'] = base64_encode($posts['code']);
			if($posts['id']) $data['return'] = $this->adcode_model->update($posts, $posts['id']);
			else {
				$posts['created'] = time();
				$data['return'] = $this->adcode_model->insert($posts);
			}
		}
		$parameter['arg'] = $arg;
		$data['total'] = $this->adcode_model->count_rows($parameter);
		$parameter['paginate'] = array('rows' => 10, 'total' => $data['total']);
		$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');
		$data['adcodes'] = $this->adcode_model->paginate($parameter);
		/* Load views */
		$this->load->view('manager/header', $data);
		$this->load->view('manager/admanagers/adcode.php', $data);
		$this->load->view('manager/footer', $data);
	}
	public function ajax($action=null)
	{
		if($action == "get") {
			/* Lấy danh sách thành viên */
			$id = intval($this->input->post('id'));
			if($id>0) {
				$parameter['arg'] = $id;
				$data = $this->adcode_model->get($parameter);
				echo json_encode($data);
			}
		} else if($action == 'delete') {
			$id = intval($this->input->post('id'));
			if($id>0) {
				$this->adcode_model->delete($id);
				echo 'Deleted';
			}
		}
		exit;
	}
}