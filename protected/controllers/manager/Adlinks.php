<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adlinks extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('adzone_model');
		$this->load->model('adcode_model');
		$this->load->model('adlink_model');
		auth();
	}
	public function index()
	{
		
		$data = loadData();
		$data['page'] = 'ad_links';
		$posts = $this->input->post();
		if(isset($posts['id'])) {
			if(isset($posts['code']) && $posts['code']){
				$posts['code'] = base64_encode($posts['code']);
				if($posts['id']) $data['return'] = $this->adcode_model->update($posts, $posts['id']);
				else {
					$posts['created'] = time();
					$data['return'] = $this->adcode_model->insert($posts);
				}
			} else {
				if($posts['expired']) {
					$expired = str_replace('-', '/', $posts['expired']).' 00:00';
					$date = explode("/", $expired);
					$posts['expired'] = strtotime($date[1].'/'.$date[0].'/'.$date[2]);
				}
				else $posts['expired'] = 0;
				if($posts['id']) $data['return'] = $this->adlink_model->update($posts, $posts['id']);
				else $data['return'] = $this->adlink_model->insert($posts);
			}
			
		}
		
		$parameter['with'] = array('link');
		$data['adzones'] = $this->adzone_model->get_all($parameter);
		$data['adcodes'] = $this->adcode_model->get_all();
		foreach($data['adzones'] as $key1=>$adzone) {
			if(isset($adzone['link'])) {
				foreach($adzone['link'] as $key2=>$link) {
					$parameter = array();
					$arg = array('id'=>$link['cid']);
					$parameter['arg'] = $arg;
					$data['adzones'][$key1]['link'][$key2]['adcodes'] = $this->adcode_model->get($parameter);
				}
			}
		}
		/* Load views */
		$this->load->view('manager/header', $data);
		$this->load->view('manager/admanagers/adlink.php', $data);
		$this->load->view('manager/footer', $data);
	}
	public function ajax($action=null)
	{
		if($action == "get") {
			$id = intval($this->input->post('id'));
			if($id>0) {
				$parameter['arg'] = $id;
				$data = $this->adlink_model->get($parameter);
				echo json_encode($data);
			}
		} else if($action == 'delete') {
			$id = intval($this->input->post('id'));
			if($id>0) {
				$this->adlink_model->delete($id);
				echo 'Deleted';
			}
		}
		exit;
	}
}