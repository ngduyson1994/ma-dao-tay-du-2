<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admins extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		auth();
		/* Load Model */
		$this->load->model('admin_model');
		$this->load->model('permission_model');
		$this->load->model('admin_group_model');
		$this->load->model('log_model');
	}
	public function index()
	{
		$data = loadData();
		$data['page'] = 'admins';
		$arg = null;
		if($this->input->get('search')) {
			$q = $this->input->get('search');
			$arg[] = array(array('username','name','email','ipaddress'), 'search', $q);
		}
		if($this->input->get('group')) $arg[] = array('gid', $this->input->get('group'));
		if($this->input->get('status')) $arg[] = array('status', $this->input->get('status'));
		$parameter['arg'] = $arg;
		$data['total'] = $this->admin_model->count_rows($parameter);
		$parameter['paginate'] = array('rows' => 15, 'total' => $data['total']);
		$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');
		$parameter['with'] = array('group');
		$data['admins'] = $this->admin_model->paginate($parameter);
		$data['groups'] = $this->admin_group_model->get_all();
		/* Load views */
		$this->load->view('manager/header', $data);
		$this->load->view('manager/admins/admins', $data);
		$this->load->view('manager/footer', $data);
	}
	public function item($id=null)
	{
		$data = loadData();
		if($id) $data['page'] = 'admin_edit';
		else $data['page'] = 'admin_add';
		$id = intval($id);
		$posts = $this->input->post();
		if(isset($posts['id'])) {
			$posts = array_filter($posts);
			$log = array('model'=>get_class($this), 'item'=>$posts['username']);
			if($id) {
				$data['return'] = $this->admin_model->update($posts, $id);
				$log['item_id'] = $id;
				$log['function'] = 'update';
			}
			else {
				$data['return'] = $this->admin_model->insert($posts);
				$log['item_id'] = $data['return']['id'];
				$log['function'] = 'insert';
				if(isset($data['return']['id'])) redirect(URL.'/manager/admins/item/'.$data['return']['id']);
			}
			$this->log_model->insert($log);
		}
		$parameter['arg'] = $id;
		if($id>0) $data['data'] = $this->admin_model->get($parameter);
		$data['groups'] = $this->admin_group_model->get_all();
		/* Load views */
		$this->load->helper('form');
		$this->load->library('form_builder');
		$this->load->view('manager/header', $data);
		$this->load->view('manager/admins/admin', $data);
		$this->load->view('manager/footer', $data);
	}
	public function account()
	{
		$id = intval($this->session->userdata('uID'));
		$data['user'] = $this->admin_model->get($id);
		if(!isset($data['user']['id'])) redirect(URL.'/login/');
		if($this->input->post('submit')) {
			if($id>0) {
				$arg = array(
					'name' => $this->input->post('name'),
					'email' => $this->input->post('email'),
					'password' => $this->input->post('password')
				);
				$data['return'] = $this->admin_model->update($arg, $id);
				$data['user'] = $this->admin_model->get($id);
			}
		}
		/* Load views */
		$this->load->view('manager/header', $data);
		$this->load->view('manager/admins/account', $data);
		$this->load->view('manager/footer', $data);
	}
	public function group()
	{
		$data = loadData();
		$data['page'] = 'admin_group';
		$posts = $this->input->post();
		if(isset($posts['id'])) {
			$posts = array_filter($posts);
			if(isset($posts['id'])) $data['return'] = $this->admin_group_model->update($posts, $posts['id']);
			else $data['return'] = $this->admin_group_model->insert($posts);
		}
		$data['groups'] = $this->admin_group_model->get_all();
		/* Load views */
		$this->load->helper('form');
		$this->load->library('form_builder');
		$this->load->view('manager/header', $data);
		$this->load->view('manager/admins/groups', $data);
		$this->load->view('manager/footer', $data);
	}
	public function permission()
	{
		$data = loadData();
		$data['page'] = 'admin_permission';
		$posts = $this->input->post();
		if(isset($posts['id'])) {
			$posts = array_filter($posts);
			if(isset($posts['id'])) $data['return'] = $this->permission_model->update($posts, $posts['id']);
			else $data['return'] = $this->permission_model->insert($posts);
		}
		$data['permissions'] = $this->permission_model->with_data()->order_by('group')->get_all();
		$data['groups'] = $this->admin_group_model->get_all();
		$this->load->helper('form');
		$this->load->library('form_builder');
		/* Load views */
		$this->load->view('manager/header', $data);
		$this->load->view('manager/admins/permission.php', $data);
		$this->load->view('manager/footer', $data);
	}
	public function ajax($action=null)
	{
		if($action == 'del') {
			if(permission('users')) {
				$id = intval($this->input->post('id'));
				$user = $this->admin_model->get($id);
				if(isset($user['id'])) {
					$this->admin_model->delete($id);
					$log = array('model'=>get_class($this), 'item'=>$post['username'], 'item_id'=>$id, 'function'=>'delete');
					echo 'Deleted';
				}
			}
		} else if($action == 'getGroup') {
			if(permission('users')) {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$parameter['arg'] = $id;
					$data = $this->admin_group_model->get($parameter);
					echo json_encode($data);
				}
			}
		} else if($action == 'delGroup') {
			if(permission('users')) {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$data = $this->admin_group_model->delete($id);
					echo json_encode($data);
				}
			}
		} else if($action == 'permission') {
			if(permission('permission')) {
				$pid = intval($this->input->post('pid'));
				$gid = intval($this->input->post('gid'));
				$allow = intval($this->input->post('allow'));
				if($gid && $pid) {
					$parameter['arg'] = array(array('pid', $pid), array('gid', $gid));
					$per = $this->permission_group_model->get($parameter);
					$arg = array('pid' => $pid, 'gid'=> $gid, 'allow' => $allow);
					if(isset($per['pid'])) {
						$update = $this->permission_group_model->update($arg, $per['id']);
						if($update['status'] == 'success') echo 'Updated';
					} else {
						$insert = $this->permission_group_model->insert($arg);
						if($insert['status'] == 'success') echo 'Inserted';
					}
				}
			}
		} elseif($action == 'getPermission') {
			if(permission('users')) {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$parameter['arg'] = $id;
					$data = $this->permission_model->get($parameter);
					echo json_encode($data);
				}
			}
		} elseif($action == 'delPermission') {
			if(permission('users')) {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$data = $this->permission_model->delete($id);
					echo json_encode($data);
				}
			}
		}
		exit;
	}
}