<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Adzones extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		auth();
		$this->load->model('adzone_model');
	}
	public function index($id=0)
	{
		$data = loadData();
		$data['page'] = 'ad_zones';
		$arg = array();
		if($this->input->get('search')) {
			$q = $this->input->get('search');
			$arg[] = array(array('name'), 'search', $q);
		}
		$posts = $this->input->post();
		if(isset($posts['id'])) {
			if($posts['id']) $data['return'] = $this->adzone_model->update($posts, $posts['id']);
			else $data['return'] = $this->adzone_model->insert($posts);
		}
		$parameter['arg'] = $arg;
		$data['total'] = $this->adzone_model->count_rows($parameter);
		$parameter['paginate'] = array('rows' => 10, 'total' => $data['total']);
		$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');
		$data['adzones'] = $this->adzone_model->paginate($parameter);
		
		$this->load->view('manager/header', $data);
		$this->load->view('manager/admanagers/adzone.php', $data);
		$this->load->view('manager/footer', $data);
	}
	public function ajax($action=null)
	{
		if($action == "get") {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$parameter['arg'] = $id;
					$data = $this->adzone_model->get($parameter);
					echo json_encode($data);
				}
		} else if($action == 'delete') {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$this->adzone_model->delete($id);
					echo 'Deleted';
				}
		}
		exit;
	}
}