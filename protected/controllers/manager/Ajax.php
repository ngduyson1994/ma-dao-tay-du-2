<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
	function __construct() {
		parent::__construct();
		auth();
	}
	public function index() {
	}
	/* Turn off payment */
	public function payment_status($action=null)
	{
        $this->load->model('setting_model');
		if($action == "status") {
			$parameter['arg'][] = array('name', 'payment_status');
			$setting = $this->setting_model->get($parameter);
			echo intval($setting['value']);
		} else if($action == 'update') {
			if(permission('payment_status')) {
				$parameter['arg'][] = array('name', 'payment_status');
				$setting = $this->setting_model->get($parameter);
				$value = intval($this->input->post('value'));
				$this->setting_model->update(array('value'=>$value), $setting['id']);
				echo $value;
			} else echo 'Only Admin';
		}
		exit;
	}
	/* Đăng xuất */
	public function clear_cache() {
		$this->xcache->delete_all();
		echo 'Cleared';
		exit;
	}
	/* Upload images */
	public function logout() {
		$this->auth->logout();
		redirect(URL.'/manager/login');
		exit;
	}
}