<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Categories extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('category_model');
		auth();
	}
	public function index($id=0)
	{
		$data = loadData();
		$data['page'] = 'categories';
		$posts = $this->input->post();
		if(isset($posts['id'])) {
			$posts = array_filter($posts);
			if(isset($posts['id'])) $data['return'] = $this->category_model->update($posts, $posts['id']);
			elseif(isset($posts['name'])) $data['return'] = $this->category_model->insert($posts);
		}
		$data['categories'] = $this->category_model->getList();
		/* Load views */
		$this->load->helper('views');
		$this->load->view('manager/header', $data);
		$this->load->view('manager/posts/categories', $data);
		$this->load->view('manager/footer', $data);
	}
	public function ajax($action=null)
	{
		if(permission('categories')) {
			if($action == "get") {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$arg = array('id'=>$id);
					$parameter['arg'] = $arg;
					$category = $this->category_model->get($parameter);
					echo json_encode($category);
				}
			} else if($action == 'update') {
				$data = $this->input->post('data');
				$data = json_decode($data);
				$this->updateCat($data, 0);
			} else if($action == 'delete') {
				if(permission('categories')) {
					$id = intval($this->input->post('id'));
					if($id>0) {
						$this->category_model->delete($id);
						echo 'Deleted';
					}
				}
			}
			else if($action == 'slug') {
				echo clearUTF(trim($this->input->post('slug')));
			}
		}
		exit;
	}
	protected function updateCat($data, $parent=0) {
		$i=0;
		if(is_array($data) && count($data))
		foreach ($data as $key => $item) {
			$items = get_object_vars($item);
			$arg = array('id'=>$items['id'], 'parent'=>$parent,'order'=>$key);
			$this->category_model->update($arg, $arg['id']);
			if(isset($items['children']) && $items['children']) $this->updateCat($items['children'], $items['id']);
			
			$i++;
		}
	}
}