<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Game extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('game_model');
        auth();
    }

    public function index()
    {
        /* Khai báo dữ liệu views */
        $data = loadData();
        $data['page'] = 'game';
        $game = $this->input->post();
        $arg = array();
        if ($this->input->get('search')) {
            $q = $this->input->get('search');
            $arg[] = array(array('name'), 'search', $q);
        }
        if (isset($game['id'])) {
            $game = array_filter($game);
            if (isset($game['id'])) $data['return'] = $this->game_model->update($game, $game['id']);
            elseif (isset($game['name'])) $data['return'] = $this->game_model->insert($game);
            $posts['created'] = time();
        }
        $parameter['arg'] = $arg;

        $data['total'] = $this->game_model->count_rows($parameter);
        $parameter['paginate'] = array('rows' => 30, 'total' => $data['total']);
        $parameter['order'] = array('order_by' => 'rank', 'order' => 'ASC');
        $data['game'] = $this->game_model->paginate($parameter);
        /* Load views */
        $this->load->helper('views');
        $this->load->view('manager/header', $data);
        $this->load->view('manager/game/game', $data);
        $this->load->view('manager/footer', $data);
    }

    public function ajax($action = null)
    {
        if ($action == "get") {
            $id = intval($this->input->post('id'));
            if ($id > 0) {
                $parameter['arg'] = $id;
                $data = $this->game_model->get($parameter);
                echo json_encode($data);
            }
        } else if ($action == 'delete') {
            if (permission('game')) {
                $id = intval($this->input->post('id'));
                if ($id > 0) {
                    $this->game_model->delete($id);
                    echo 'Deleted';
                }
            }
        } else if ($action == 'feature') {
            if (permission('game')) {
                $id = intval($this->input->post('id'));
                $value = intval($this->input->post('value'));
                $this->game_model->update(array('feature' => $value), $id);
                $this->xcache->delete_group('post_');
            }
            exit;
        }
    }
}