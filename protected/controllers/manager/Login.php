<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
	}
	public function index()
	{
		$data['page'] = 'users';
		$ref = $this->session->userdata('backend_ref');
		$ref = ($ref)?$ref:URL.'/manager';
		if(intval($this->session->userdata('aID'))) redirect($ref);
		$user = $this->input->post('username');
		$pass = $this->input->post('password');
		if($user && $pass) {
			$data['login'] = $this->login($user, $pass);
			if($data['login']['status'] == 'success') redirect($ref);
		}
		$this->load->view('manager/login', $data);
	}
	protected function login($identity, $password) {
		if(empty($identity) || empty($password)) return array('status'=>'error', 'message'=>'Input empty');
		$parameter['arg'] = array(array('username', $identity), array('email', $identity, null, true));
		$admin = $this->admin_model->get($parameter);
		if(isset($admin['id'])) {
			//if($this->is_time_locked_out($admin['id'])) return array('status'=>'error', 'message'=>'IP locked temp');
			if($admin['status'] == 'banned') return array('status'=>'error', 'message'=>'Your account is banned');
			if($admin['password'] == md5(md5($password).$admin['salt'])) {
				/* Login success */
				//$this->clearLoginAttempts($admin['id']);
				$this->session->set_userdata(array('aID' => $admin['id']));
				$this->session->set_userdata(array('aGroup' => $admin['gid']));
				return array('status'=>'success', 'message'=>'Login successed');
			} else {
				//$this->increaseLoginAttempts($identity);
				return array('status'=>'error', 'message'=>'Wrong password');
			}
		} else {
			//$this->increaseLoginAttempts($identity);
			return array('status'=>'error', 'message'=>'Account '.$identity.' not exist');
		}
	}
}