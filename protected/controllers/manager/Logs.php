<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Logs extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('log_model');
		auth();
	}
	public function index($id=0)
	{
		$data = loadData();
		$data['page'] = 'logs';
		$arg = array();
		if($this->input->get('search')) {
			$q = $this->input->get('search');
			$arg[] = array(array('ipaddress','item'), 'search', $q);
		}
		if($this->input->get('model')) $arg[] = array('model', $this->input->get('model'));
		if($this->input->get('aid')) $arg[] = array('aid', $this->input->get('aid'));
		if($this->input->get('function')) $arg[] = array('function', $this->input->get('function'));
		$parameter['arg'] = $arg;
		$data['total'] = $this->log_model->count_rows($parameter);
		$parameter['paginate'] = array('rows' => 15, 'total' => $data['total']);
		$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');
		$parameter['with'] = array('admin'=>'fields:username,name,gid', 'group'=>'fields:name,style');
		$data['logs'] = $this->log_model->paginate($parameter);
		$data['models'] = $this->log_model->getModel();
		$this->load->model('admin_group_model');
		$groups = $this->admin_group_model->get_all();
		foreach($groups as $key=>$value) {
			$data['groups'][$value['id']] = $value;
		}
		/* Load views */
		$this->load->library('user_agent');
		$this->load->view('manager/header', $data);
		$this->load->view('manager/logs/logs.php', $data);
		$this->load->view('manager/footer', $data);
	}
	public function payment()
	{
		$data = loadData();
		$data['page'] = 'payment_logs';
		$id = 0;
		$this->load->model('payment_transactions_model');
		$arg[] = array('method', array('add_money','sub_money', 'receive_money', 'send_money', 'buy_channel', 'mobile_card', 'bank_transfer'));
		if($this->input->get('search')) {
			$q = $this->input->get('search');
			$arg[] = array('user', $q);
		}
		if($this->input->get('method')) $arg[] = array('method', $this->input->get('method'));
		if($this->input->get('date-start')) $arg[] = array('created', '>', strtotime('00:00 '.$this->input->get('date-start')));
		if($this->input->get('date-end')) $arg[] = array('created', '<', strtotime('23:59 '.$this->input->get('date-end')));
		
		$parameter['arg'] = $arg;
		$parameter['with'] = array('admin' => 'fields:username', 'user' => 'fields:username', 'other' => 'fields:username', 'method' => 'fields:name');
		$parameter['fields'] = 'id, admin_id, user_id, other_id, method, money, real_money, note, set_type, created';
		$data['total'] = $this->payment_transactions_model->count_rows($parameter);
		$parameter['paginate'] = array('rows' => 15, 'total' => $data['total']);
		$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');		
		$data['logs'] = $this->payment_transactions_model->paginate($parameter);
		//print_r($data['logs']);die();
		$parameter = array();
		$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');
		$data['payment_methods'] = $this->payment_method_model->get_all($parameter);
		/* Load views */
		$this->load->view('manager/header', $data);
		$this->load->view('manager/logs/log_payment_transactions.php', $data);
		$this->load->view('manager/footer', $data);
	}
	
	public function betting()
	{
		$data = loadData();
		$data['page'] = 'payment_betting_logs';
		$id = 0;
		$this->load->model('payment_transactions_model');
		$arg[] = array('method', array('pay_betting','user_betting', 'cancel_betting', 'create_betting', 'rollback_betting', 'refund_betting'));
		if($this->input->get('search')) {
			$q = $this->input->get('search');
			$arg[] = array('user', $q);
		}
		if($this->input->get('method')) $arg[] = array('method', $this->input->get('method'));
		if($this->input->get('date-start')) $arg[] = array('created', '>', strtotime('00:00 '.$this->input->get('date-start')));
		if($this->input->get('date-end')) $arg[] = array('created', '<', strtotime('23:59 '.$this->input->get('date-end')));
		
		$parameter['arg'] = $arg;
		$parameter['with'] = array('user' => 'fields:username', 'other' => 'fields:username', 'method' => 'fields:name');
		$parameter['fields'] = 'id, user_id, other_id, method, money, real_money, note, set_type, created';
		$data['total'] = $this->payment_transactions_model->count_rows($parameter);
		$parameter['paginate'] = array('rows' => 15, 'total' => $data['total']);
		$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');		
		$data['logs'] = $this->payment_transactions_model->paginate($parameter);
		
		$parameter = array();
		$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');
		$data['payment_methods'] = $this->payment_method_model->get_all($parameter);
		/* Load views */
		$this->load->view('manager/header', $data);
		$this->load->view('manager/logs/log_betting_transactions.php', $data);
		$this->load->view('manager/footer', $data);
	}
	public function money_change()
	{
		$data = loadData();
		$data['page'] = 'payment_money_change';
		$id = 0;
		$this->load->model('payment_transactions_model');
		$arg[] = array('method', array('pay_betting','user_betting', 'cancel_betting', 'create_betting', 'rollback_betting', 'refund_betting','add_money','sub_money', 'receive_money', 'send_money', 'buy_channel', 'mobile_card', 'bank_transfer'));
		if($this->input->get('search')) {
			$q = $this->input->get('search');
			$arg[] = array('user', $q);
		}
		if($this->input->get('method')) $arg[] = array('method', $this->input->get('method'));
		if($this->input->get('date-start')) $arg[] = array('created', '>', strtotime('00:00 '.$this->input->get('date-start')));
		if($this->input->get('date-end')) $arg[] = array('created', '<', strtotime('23:59 '.$this->input->get('date-end')));
		
		$parameter['arg'] = $arg;
		$parameter['with'] = array('admin' => 'fields:username', 'user' => 'fields:username', 'other' => 'fields:username', 'method' => 'fields:name');
		$parameter['fields'] = 'id, admin_id, user_id, other_id, method, money, real_money, note, description, set_type, created';
		$data['total'] = $this->payment_transactions_model->count_rows($parameter);
		$parameter['paginate'] = array('rows' => 15, 'total' => $data['total']);
		$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');		
		$data['logs'] = $this->payment_transactions_model->paginate($parameter);
		
		$parameter = array();
		$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');
		$data['payment_methods'] = $this->payment_method_model->get_all($parameter);
		/* Load views */
		$this->load->view('manager/header', $data);
		$this->load->view('manager/logs/log_money_change', $data);
		$this->load->view('manager/footer', $data);
	}
	
	public function payment_mobile()
	{
		$data = loadData();
		$data['page'] = 'payment_mobiles';
		$this->load->model('payment_mobile_model');
		$arg = array();
		if($this->input->get('search')) {
			$q = $this->input->get('search');
			$arg[] = array('user_id', $q);
		}
		if($this->input->get('date-start')) $arg[] = array('created', '>', strtotime('00:00 '.$this->input->get('date-start')));
		if($this->input->get('date-end')) $arg[] = array('created', '<', strtotime('23:59 '.$this->input->get('date-end')));
		
		$parameter['arg'] = $arg;
		$parameter['with'] = array('user' => 'fields:username');
		$data['total'] = $this->payment_mobile_model->count_rows($parameter);
		$parameter['paginate'] = array('rows' => 15, 'total' => $data['total']);
		$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');		
		$data['logs'] = $this->payment_mobile_model->paginate($parameter);
		
		/* Load views */
		$this->load->view('manager/header', $data);
		$this->load->view('manager/logs/log_payment_mobile.php', $data);
		$this->load->view('manager/footer', $data);
	}
	
	public function ajax($action=null)
	{
		if($action == "get") {
			/* Lấy danh sách logs */
			$id = intval($this->input->post('id'));
			if($id>0) {
				$log = $this->logger->getByID($id);
				echo json_encode($log);
			}
		} else if($action == 'delete') {
			if(permission('logs')) {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$this->logger->delete($id);
					echo 'Deleted';
				}
			}
		}
		exit;
	}
}