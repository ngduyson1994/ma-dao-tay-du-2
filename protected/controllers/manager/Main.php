<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		auth();
	}
	public function index()
	{
		$data = loadData();
		$data['page'] = 'main';
		$this->load->model('post_model');
		
		$this->load->view('manager/header', $data);
		$this->load->view('manager/index', $data);
		$this->load->view('manager/footer', $data);
	}
	public function logout() {
		$session_data = array('aID' => null, 'aGroup' => null);
		$this->session->unset_userdata($session_data);
		$this->session->sess_destroy();
		$this->session->sess_regenerate(true);
		redirect(URL.'/manager/login');
		exit;
	}
}
