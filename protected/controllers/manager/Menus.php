<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menus extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		auth();
		$this->load->model('menu_model');
		$this->load->model('menu_data_model');
		$this->load->model('category_model');
	}
	public function index()
	{
		$data = loadData();
		$data['page'] = 'menus';
		$data['menus'] = $this->menu_model->get_all();
		$mid = intval($this->input->get('mid'));
		if($mid==0) $mid = $data['menus'][0]['id'];
		$posts = $this->input->post();
		if(isset($posts['id'])) {
			if($posts['type'] == 'category') $posts['value'] = $posts['category'];
			elseif($posts['link']) $posts['value'] = $posts['link'];
			if($posts['id']) $data['return'] = $this->menu_data_model->update($posts, $posts['id']);
			else $data['return'] = $this->menu_data_model->insert($posts);
		}
		$data['menu_items'] = $this->menu_data_model->getList($mid);
		$data['categories'] = $this->category_model->getList();
		/* Load views */
		$this->load->helper('views');
		$this->load->view('manager/header', $data);
		$this->load->view('manager/menus', $data);
		$this->load->view('manager/footer', $data);
	}
	public function ajax($action=null)
	{
		if(permission('menu')) {
			if($action == "get") {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$parameter['arg'] = $id;
					$menu = $this->menu_data_model->get($parameter);
					$menu['title'] = clearString($menu['title'], false);
					$menu['icon'] = clearString($menu['icon'], false);
					echo json_encode($menu);
				}
			} else if($action == 'update') {
				if(permission('menu')) {
					$data = $this->input->post('data');
					$data = json_decode($data);
					$this->updateMenu($data, 0);
				}
			} else if($action == 'delete') {
				if(permission('menu')) {
					$id = intval($this->input->post('id'));
					if($id>0) {
						$this->menu_data_model->delete($id);
						echo 'Deleted';
					}
				}
			}
		}
		exit;
	}
	protected function updateMenu($data, $parent=0) {
		if(is_array($data) && count($data))
		foreach ($data as $key => $item) {
			$item = get_object_vars($item);
			$arg = array('parent'=>$parent,'order'=>$key);
			$this->menu_data_model->update($arg, $item['id']);
			if(isset($item['children'])) $this->updateMenu($item['children'], $item['id']);
		}
	}
}