<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		auth();
		$this->load->model('post_model');
	}
	public function index()
	{
		$data = loadData();
		$data['page'] = 'pages';
		$arg = array();
		$arg[] = array('type', 'page');
		if($this->input->get('search')) {
			$q = $this->input->get('search');
			$arg[] = array(array('title','slug'), 'search', $q);
		}
		if($this->input->get('sort-start')) $arg[] = array('published', '>', strtotime(str_replace('-', '/', $this->input->get('sort-start'))));
		if($this->input->get('sort-end')) $arg[] = array('published', '<', strtotime(str_replace('-', '/', '23:59 '.$this->input->get('sort-end'))));
		$arg[] = ($this->input->get('status'))?array('status', $this->input->get('status')):array('status', 'publish');
		$gid = intval($this->session->userdata('uGroup'));
		if($gid==3) $arg['uid'] = intval($this->session->userdata('uID'));
		
		if($this->input->get('trash')) $parameter['with_trashed'] = true;
		$parameter['arg'] = $arg;
		$data['total'] = $this->post_model->count_rows($parameter);
		$parameter['paginate'] = array('rows' => 20, 'total' => $data['total']);
		$parameter['order'] = array('order_by'=>'published', 'order'=>'DESC');
		$parameter['with'] = array('admin');
		$data['posts'] = $this->post_model->paginate($parameter);
		$this->load->model('admin_group_model');
		$groups = $this->admin_group_model->get_all();
		foreach($groups as $key=>$value) {
			$data['groups'][$value['id']] = $value;
		}
		$this->load->helper('views');
		$this->load->view('manager/header', $data);
		$this->load->view('manager/posts/pages', $data);
		$this->load->view('manager/footer', $data);
	}
	
	public function item($id=null)
	{
		$data = loadData();
		$id = intval($id);
		if($id) $data['page'] = 'page_edit';
		else $data['page'] = 'page_add';
		$this->load->helper('form');
		$this->load->library('form_builder');
		$posts = $this->input->post();
		$gid = intval($this->session->userdata('aGroup'));
		$admin_id = intval($this->session->userdata('aID'));
		$condition = false;
		if($gid>0 && $gid<3) $condition = true;
		elseif($gid==3 && $posts['admin_id']==$admin_id) $condition = true;
		if(isset($posts['id']) && $condition) {
			if($_FILES['uploadthumb']['name']) {
				$this->load->helper('uploader');
				$upload = upload('thumb', 'uploadthumb');
				if($upload['status']=='success') $posts['thumb'] = $upload['thumb'];
			}
			if(isset($posts['remote_image']) && $posts['remote_image']) {
				$this->load->helper('images');
				//Thumb
				if(!strpos($posts['thumb'], 'blogspot.com')) $posts['thumb'] = picasa($posts['thumb']);
				//Content
				preg_match_all("/<img(.*)>/U", $posts['content'], $match);
				foreach($match[1] as $image) {
					$image = str_replace("'", '"', $image);
					$image = getString($image, 'src="', '"');
					if($image && !strpos($image, 'blogspot.com')) $posts['content'] = str_replace($image, picasa($image), $posts['content']);
				}
			}
			if(!permission('post_publish')) $posts['status'] = 'pending';
			$posts['published'] = strtotime(str_replace('-', '/', $posts['date']).' '.$posts['hour'].':'.$posts['min']);
			$posts = array_filter($posts);
			$posts['feature'] = intval($this->input->post('feature'));
			$posts['content'] = $_POST['content'];
			$log = array('model'=>get_class($this), 'item'=>$posts['title']);
			if($id) {
				$data['return'] = $this->post_model->update($posts, $id);
				$log['item_id'] = $id;
				$log['function'] = 'update';
			}
			else {
				$data['return'] = $this->post_model->insert($posts);
				$log['item_id'] = $data['return']['id'];
				$log['function'] = 'insert';
				if(isset($data['return']['id'])) redirect(URL.'/manager/pages/item/'.$data['return']['id']);
			}
		}
		if($id>0) {
			$parameter['arg'] = $id;
			$parameter['with'] = array('admin', 'relationships');
			$data['data'] = $this->post_model->get($parameter);
		}
		/* Load views */
		$this->load->helper('post');
		$this->load->view('manager/header', $data);
		$this->load->view('manager/posts/page', $data);
		$this->load->view('manager/footer', $data);
	}
	
	public function ajax($action=null)
	{
		if($action == "get") {
			$id = intval($this->input->post('id'));
			if($id>0) {
				$post = $this->post->getByID($id);
				echo json_encode($post);
			}
		} else if($action == 'delete') {
			if(permission('post_delete')) {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$this->post->delete($id);
					echo 'Deleted';
				}
			}
		} else if($action == 'slug') {
			$slug = clearUTF(trim($this->input->post('slug')));
			$id = intval($this->input->post('id'));
			$post = $this->post_model->get(array('slug'=>$slug));
			$i=0;
			while(isset($post['id'])) {
				if($id == $post['id']) break; $i++;
				$post = $this->post_model->get(array('slug'=>$slug.'.'.$i));
			}
			if($i>0) echo $slug.'.'.$i;
			else echo $slug;
		} else if($action == 'feature') {
			if(permission('pages')) {
				$id = intval($this->input->post('id'));
				$value = intval($this->input->post('value'));
				$this->post_model->update(array('feature'=>$value), $id);
				$this->xcache->delete_group('post_');
			}
		} else if($action == 'upload') {
			if(permission('pages')) {
				if(isset($_FILES['files'])) {
					$this->load->helper('uploader');
					$pid = intval($this->input->post('pid'));
					$thumb = upload_picasa('tmp', 'files', $pid);
					if($thumb['status']=='success') {
						$json = array('status'=>'success','img'=>$thumb['thumb']);
					} else {
						$json = array('status'=>'error','msg'=>$thumb['msg']);
					}
					echo json_encode($json);
				}
			}
		}
		exit;
	}
}