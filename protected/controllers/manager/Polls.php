<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Polls extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		auth();
		$this->load->model('polls_model');
	}
	public function index()
	{
		$data = loadData();
		$data['page'] = 'polls';
		$arg = array();
		if($this->input->get('search')) {
			$q = $this->input->get('search');
			$arg[] = array(array('name'), 'search', $q);
		}
		$posts = $this->input->post();
		$answers = $this->input->post('answers');
		if(isset($posts['title'])) {
			print_r($posts);exit;
			$answers = str_replace("\r\n", "\n", $answers);
			$answers = explode("\n", $answers);
			$json = array();
			$i = 0;
			foreach($answers as $answer) {
				if(strpos($answer, '|')) {
					$item = explode("|", $answer);
					$json[$i]['name'] = $item[0];
					$json[$i]['vote'] = intval($item[1]);
				} else {
					$json[$i]['name'] = $answer;
					$json[$i]['vote'] = 0;
				}
				$i++;
			}
			$answers = json_encode($json);
			if($answers) {
				$data = array(
					'name'=> $this->input->post('name'),
					'question'=> $this->input->post('question'),
					'answers'=> $answers
				);
			}
			if($posts['id']) $data['return'] = $this->polls_model->update($data, $posts['id']);
			else $data['return'] = $this->polls_model->insert($data);
		}

		$parameter['arg'] = $arg;
		$data['total'] = $this->polls_model->count_rows($parameter);
		$parameter['paginate'] = array('rows' => 30, 'total' => $data['total']);
		$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');

		$data['polls'] = $this->polls_model->paginate($parameter);
		//print_r($data['polls']); die();
		/* Load views */
		$this->load->view('manager/header', $data);
		$this->load->view('manager/polls/index', $data);
		$this->load->view('manager/footer', $data);
	}

	public function ajax($action=null)
	{
		if($action == "get") {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$parameter['arg'] = $id;
					$data = $this->poll_model->get($parameter);
					//print_r($data['answers']); die();
					$list = json_decode($data['answers'], true);
					foreach($list as $key=>$item) {
						$list[$key] = implode("|", $item);
					}
					$data['answers'] = implode("\r\n", $list);
					echo json_encode($data);
				}
		} else if($action == 'delete') {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$this->poll_model->delete($id);
					echo 'Deleted';
				}
		}
		exit;
	}

}
