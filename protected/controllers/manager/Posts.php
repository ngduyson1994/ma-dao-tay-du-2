<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Posts extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		/* Load Model */
		$this->load->model('post_model');
		$this->load->model('category_model');
		$this->load->model('tag_model');
		$this->load->model('log_model');
		auth();
	}
	public function index()
	{
		$data = loadData();
		$data['page'] = 'posts';
		$arg = array();
		if($this->input->get('search')) {
			$q = $this->input->get('search');
			$arg[] = array(array('title','slug'), 'search', $q);
		}
		if($this->input->get('user')) $arg[] = array('user_id', $this->input->get('user'));
		if($this->input->get('category')) $arg[] = array('category', $this->input->get('category'));
		if($this->input->get('sort-start')) $arg[] = array('published', '>', strtotime(str_replace('-', '/', $this->input->get('sort-start'))));
		if($this->input->get('sort-end')) $arg[] = array('published', '<', strtotime(str_replace('-', '/', '23:59 '.$this->input->get('sort-end'))));
		$arg[] = ($this->input->get('status'))?array('status', $this->input->get('status')):array('status', 'publish');
		$arg[] = ($this->input->get('type'))?array('type', $this->input->get('type')):array('type', '!=', 'page');
		$gid = intval($this->session->userdata('aGroup'));
		if($gid==3) $arg[] = array('user_id', intval($this->session->userdata('aID')));
		$data['categories'] = $this->category_model->get_all();
		if($this->input->get('trash')) $parameter['only_trashed'] = true;
		$parameter['arg'] = $arg;
		$data['total'] = $this->post_model->count_rows($parameter);
		$parameter['paginate'] = array('rows' => 20, 'total' => $data['total']);
		$parameter['order'] = array('order_by'=>'id', 'order'=>'DESC');
		$parameter['with'] = array('admin');
		$data['posts'] = $this->post_model->paginate($parameter);
		$this->load->model('admin_group_model');
		$groups = $this->admin_group_model->get_all();
		foreach($groups as $key=>$value) {
			$data['groups'][$value['id']] = $value;
		}

		/* Load views */
		$this->load->helper('views');
		$this->load->view('manager/header', $data);
		$this->load->view('manager/posts/posts', $data);
		$this->load->view('manager/footer', $data);
	}
	public function checkviews()
    {
        $data = loadData();
        $data['page'] = 'posts';
        $parameter = array();
        $parameter['arg'][] =array('published', '>=', strtotime(str_replace('-', '/', $this->input->get('sort-start'))));
        $parameter['arg'][] =array('published', '<=', strtotime(str_replace('-', '/', '23:59 '.$this->input->get('sort-end'))));
        $parameter['fields'] = 'SUM(views) as sum_views';
        $result = $this->post_model->get($parameter);
        $data['sum_views'] = intval($result['sum_views']);

        $this->load->helper('views');
        $this->load->view('manager/header', $data);
        $this->load->view('manager/posts/check_view', $data);
        $this->load->view('manager/footer', $data);
    }
	public function item($id=null)
	{
		$data = loadData();
		$id = intval($id);
		if($id) $data['page'] = 'post_edit';
		else $data['page'] = 'post_add';
		$this->load->helper('form');
		$this->load->library('form_builder');
		$posts = $this->input->post();
		$gid = intval($this->session->userdata('aGroup'));
		$admin_id = intval($this->session->userdata('aID'));
		$condition = false;
		if(isset($posts['id'])) {
			if(!$posts['thumb']) $posts['thumb'] = getThumb($posts);
			if(!permission('post_publish')) $posts['status'] = 'pending';
			$posts['published'] = strtotime(str_replace('-', '/', $posts['date']).' '.$posts['hour'].':'.$posts['min']);
			$posts = array_filter($posts);
			$posts['feature'] = intval($this->input->post('feature'));
			$posts['is_slider'] = intval($this->input->post('is_slider'));
			$posts['user_id'] = $admin_id;
			if($posts['type']=='video') {
				$posts['content'] = $_POST['text'];
			} else {
				$posts['content'] = $_POST['content'];
			}
			$log = array('model'=>get_class($this), 'item'=>$posts['title']);
			
			if($id) {
				$data['return'] = $this->post_model->update($posts, $id);
				$log['item_id'] = $id;
				$log['function'] = 'update';
			}
			else {
				$data['return'] = $this->post_model->insert($posts);
				$log['item_id'] = $data['return']['id'];
				$log['function'] = 'insert';
				if(isset($data['return']['id'])) redirect(URL.'/manager/posts/item/'.$data['return']['id']);
			}
			$this->log_model->insert($log);
		}
		if($id>0) {
			$parameter['arg'] = $id;
			$parameter['with'] = array('user', 'relationships');
			$data['data'] = $this->post_model->get($parameter);
		}
		$parameter = array();
		$parameter['fields'] = 'id,name,slug,type,facebook,thumb,cover,count';
		$data['tags'] = $this->tag_model->get_all($parameter);
        $parameter = array();
        $parameter['fields'] = 'id,name';
        $data['home'] = $this->player_model->get_all($parameter);
		$parameter = array();
		$parameter['order'] = array('order_by'=>'id', 'order'=>'ASC');
		$data['categories'] = $this->category_model->getList();
		/* Load views */
		$this->load->helper('post');
		$this->load->view('manager/header', $data);
		$this->load->view('manager/posts/post', $data);
		$this->load->view('manager/footer', $data);
	}
	public function ajax($action=null)
	{
		if($action == "get") {
			$id = intval($this->input->post('id'));
			if($id>0) {
				$parameter['arg'] = $id;
				$post = $this->post_model->get($parameter);
				echo json_encode($post);
			}
		} else if($action == 'delete') {
			if(permission('posts')) {
				$id = intval($this->input->post('id'));
				$parameter['arg'] = $id;
				$post = $this->post_model->get($parameter);
				if($post['id']>0) {
					$this->post_model->delete($id);
					$this->xcache->delete_group('post_model');
					$log = array('model'=>get_class($this), 'item'=>$post['title'], 'item_id'=>$id, 'function'=>'delete');
					$this->log_model->insert($log);
					echo 'Success';
				}
			}
		} else if($action == 'force_delete') {
			if(permission('post_force_delete')) {
				$id = intval($this->input->post('id'));
				$parameter['arg'] = $id;
				$post = $this->post_model->get($parameter);
				if($post['id']>0) {
					$this->post_model->force_delete($id);
					$log = array('model'=>get_class($this), 'item'=>$post['title'], 'item_id'=>$id, 'function'=>'force_delete');
					$this->log_model->insert($log);
					$this->xcache->delete_group('post_model');
					echo 'Success';
				}
			}
		} else if($action == 'restore') {
			if(permission('post_force_delete')) {
				$id = intval($this->input->post('id'));
				$parameter['arg'] = $id;
				$post = $this->post_model->get($id);
				if($post['id']>0) {
					$this->post_model->restore($id);
					$log = array('model'=>get_class($this), 'item'=>$post['title'], 'item_id'=>$id, 'function'=>'restore');
					$this->log_model->insert($log);
					$this->xcache->delete_group('post_');
					echo 'Success';
				}
			}
		} else if($action == 'slug') {
			$slug = clearUTF(trim($this->input->post('slug')));
			$id = intval($this->input->post('id'));
			$parameter['arg'][] = array('slug', $slug);
			$post = $this->post_model->get($parameter);
			$i=0;
			while(isset($post['id'])) {
				if($id == $post['id']) break; $i++;
				$parameter['arg'][] = array('slug', $slug.'.'.$i);
				$post = $this->post_model->get($parameter);
			}
			if($i>0) echo $slug.'.'.$i;
			else echo $slug;
		} else if($action == 'is_slider') {
			if(permission('posts')) {
				$id = intval($this->input->post('id'));
				$value = intval($this->input->post('value'));
				$this->post_model->update(array('is_slider'=>$value), $id);
				$this->xcache->delete_group('post_');
			}
		}  else if($action == 'feature') {
			if(permission('posts')) {
				$id = intval($this->input->post('id'));
				$value = intval($this->input->post('value'));
				$this->post_model->update(array('feature'=>$value), $id);
				$this->xcache->delete_group('post_');
			}
		} else if($action == 'thumb') {
			$this->load->helper('images');
			$id = intval($this->input->post('id'));
			$post = $this->post_model->get($id);
			if(isset($post['id'])) {
				$thumb = getThumb($post);
				if(!strpos($thumb, 'cdn8.net')) {
					$data = array();
					$data['thumb'] = picasa($thumb);
					if(strpos($data['thumb'], 'cdn8.net')) {
						$this->post_model->update($data, $id);
						echo 'success';
					} else echo 'Error';
				} else echo 'success';
			}
		} else if($action == 'upload') {
			if(permission('posts')) {
				if(isset($_FILES['files'])) {
					$this->load->helper('uploader');
					$pid = intval($this->input->post('pid'));
					$thumb = upload_picasa('tmp', 'files', $pid);
					if($thumb['status']=='success') {
						$json = array('status'=>'success','img'=>$thumb['thumb']);
					} else {
						$json = array('status'=>'error','msg'=>$thumb['msg']);
					}
					echo json_encode($json);
				}
			}
		}
		exit;
	}
}
