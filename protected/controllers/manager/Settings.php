<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Settings extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		auth();
		$this->load->model('setting_model');
	}
	public function index($id=0)
	{
		$data = loadData();
		$data['page'] = 'settings';
		$posts = $this->input->post();
		if(isset($posts['value']) && is_array($posts['value'])) {
			foreach($posts['value'] as $id=>$value) {
				$this->setting_model->update(array('value'=>$value), $id);
			}
		}
		$parameter['order'] = array('order_by'=>'id', 'order'=>'ASC');
		$data['settings'] = $this->setting_model->get_all($parameter);
		$this->load->view('manager/header', $data);
		$this->load->view('manager/settings.php', $data);
		$this->load->view('manager/footer', $data);
	}
	public function ajax($action=null)
	{
		if($action == "get") {
			$id = intval($this->input->post('id'));
			if($id>0) {
				$parameter['arg'] = $id;
				$data = $this->adlink_model->get($parameter);
				echo json_encode($data);
			}
		}
		exit;
	}
}