<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sliders extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('slider_model');
        auth();
    }

    public function index($id = 0) {
        $data = loadData();
		$data['page'] = 'sliders';
		$posts = $this->input->post();
		$arg = array();
		if($this->input->get('search')) {
			$q = $this->input->get('search');
			$arg[] = array(array('name'), 'search', $q);
		}
		if(isset($posts['id'])) {
			$posts = array_filter($posts);
			if(!isset($posts['status'])) $posts['status'] = 0;
			if(isset($posts['id'])) $data['return'] = $this->slider_model->update($posts, $posts['id']);
			elseif(isset($posts['name'])) $data['return'] = $this->slider_model->insert($posts);
		}
		$parameter['arg'] = $arg;
		$data['total'] = $this->slider_model->count_rows($parameter);
		$parameter['paginate'] = array('rows' => 10, 'total' => $data['total']);
		$parameter['order'] = array('order_by'=>'id', 'order'=>'ASC');
		$data['sliders'] = $this->slider_model->paginate($parameter);

        /* Load views */
        $this->load->helper('views');
        $this->load->view('manager/header', $data);
        $this->load->view('manager/posts/sliders', $data);
        $this->load->view('manager/footer', $data);
    }

    public function ajax($action = null) {
        if ($action == "get") {
            $id = intval($this->input->post('id'));
            if ($id > 0) {
                $parameter['arg'] = $id;
                $data = $this->slider_model->get($parameter);
                echo json_encode($data);
            }
        } else if ($action == 'delete') {
            if (permission('sliders')) {
                $id = intval($this->input->post('id'));
                if ($id > 0) {
                    $this->slider_model->delete($id);
                    echo 'Deleted';
                }
            }
        } else if($action == 'upload') {
			if(permission('sliders')) {
				if(isset($_FILES['files'])) {
					$name = $_FILES['files']['name'];
					$this->load->helper('uploader');
					$pid = intval($this->input->post('pid'));
					$thumb = upload_cdn('tmp', 'files', $pid);
					if($thumb['status']=='success') {
						$json = array('status'=>'success','img'=>$thumb['thumb']);
					} else {
						$json = array('status'=>'error','msg'=>$thumb['msg']);
					}
					echo json_encode($json);
				}
			}
		}
        exit;
    }

}
