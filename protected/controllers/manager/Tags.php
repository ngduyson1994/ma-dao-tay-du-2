<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('tag_model');
        auth();
    }

    public function index($id = 0)
    {
        /* Khai báo dữ liệu views */
        $data = loadData();
        $data['page'] = 'tags';
        $posts = $this->input->post();
        $arg = array();
        if ($this->input->get('search')) {
            $q = $this->input->get('search');
            $arg[] = array(array('name'), 'search', $q);
        }
        if (isset($posts['id'])) {
            $posts = array_filter($posts);
            if (isset($posts['id'])) $data['return'] = $this->tag_model->update($posts, $posts['id']);
            elseif (isset($posts['name'])) $data['return'] = $this->tag_model->insert($posts);
        }
        $parameter['arg'] = $arg;
        $parameter['arg'][] = array('type', array('tag', 'team', 'com', 'category'));
        $data['total'] = $this->tag_model->count_rows($parameter);
        $parameter['paginate'] = array('rows' => 30, 'total' => $data['total']);
        $parameter['order'] = array('order_by' => 'id', 'order' => 'DESC');
        $data['tags'] = $this->tag_model->paginate($parameter);
        /* Load views */
        $this->load->helper('views');
        $this->load->view('manager/header', $data);
        $this->load->view('manager/posts/tags', $data);
        $this->load->view('manager/footer', $data);
    }

    public function ajax($action = null)
    {
        if ($action == "get") {
            $id = intval($this->input->post('id'));
            if ($id > 0) {
                $parameter['arg'] = $id;
                $data = $this->tag_model->get($parameter);
                echo json_encode($data);
            }
        } else if ($action == 'delete') {
            if (permission('tags')) {
                $id = intval($this->input->post('id'));
                if ($id > 0) {
                    $this->tag_model->delete($id);
                    echo 'Deleted';
                }
            }
        } else if ($action == 'slug') {
            echo clearUTF(trim($this->input->post('slug')));
        }
        exit;
    }
}