<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		auth();
		/* Load Model */
		$this->load->model('user_model');
		$this->load->model('admin_model');
		$this->load->model('permission_model');
		$this->load->model('admin_group_model');
		$this->load->model('log_model');
	}
	public function index()
	{
		$data = loadData();
		$data['page'] = 'users';
		$posts = $this->input->post();
		if(isset($posts['id'])) {
			if(isset($posts['action']) && $posts['action'] == 'set_money') {
				if(permission('set_money')) {
					$this->load->helper('user');
					$method_code = ($posts['type']=='add')?'add_money':'sub_money';
					$money = str_replace('.', '', $posts['money']);
					$data['return'] = admin_payment($this->session->userdata('aID'), $posts['id'], doubleval($money), $method_code, 0, $posts['type'], $posts['note'], $posts['set_type']);
					if($data['return']['status'] == 'success') {
						header("Refresh:0"); exit;
					}
				}
				//print_r($posts);exit;
			} elseif(isset($posts['action']) && $posts['action'] == 'ban_user') {
				$this->load->model('user_ban_model');
				if($posts['ban_time'] = 'day') $expried = time()+86400;
				elseif($posts['ban_time'] = 'week') $expried = time()+86400*7;
				elseif($posts['ban_time'] = 'month') $expried = time()+86400*30;
				else $expried = 0;
				$is_ban = ($posts['is_ban']=='1')?'1':'0';
				$arg = array(
					'admin_id'=>intval($this->session->userdata('aID')),
					'user_id'=>$posts['id'],
					'is_ban'=>$is_ban,
					'note'=>$posts['ban_note'],
					'expried'=>$expried
				);
				$this->user_ban_model->insert($arg);
				if($is_ban==1){
					$arg = array('status' => 'banned', 'token' => '');
				} else{
					$arg = array('status' => 'valid', 'token' => '');
				}
				$this->user_model->update($arg, $posts['id']);
				$data['return']['message'] = 'Thành công';
			} else {
				if(isset($posts['username']) && $posts['username']) {
					$log = array('model'=>get_class($this), 'item'=>$posts['username']);
					unset($posts['money']);
					unset($posts['vip_money']);
					if($posts['id']) {
						$data['return'] = $this->user_model->update($posts, $posts['id']);
						$log['item_id'] = $posts['id'];
						$log['function'] = 'update';
					}
					else {
						$data['return'] = $this->user_model->insert($posts);
						$log['item_id'] = $data['return']['id'];
						$log['function'] = 'insert';
					}
					$this->log_model->insert($log);
				}
			}
		}
		$arg = null;
		if($this->input->get('search')) {
			$q = $this->input->get('search');
			$arg[] = array(array('username','name','email','ipaddress'), 'search', $q);
		}
		if($this->input->get('status')) $arg[] = array('status', $this->input->get('status'));		
		if($this->input->get('date-start')) $arg[] = array('created', '>', strtotime('00:00 '.$this->input->get('date-start')));
		if($this->input->get('date-end')) $arg[] = array('created', '<', strtotime('23:59 '.$this->input->get('date-end')));
		$parameter['arg'] = $arg;
		$data['total'] = $this->user_model->count_rows($parameter);
		$parameter['paginate'] = array('rows' => 15, 'total' => $data['total']);
		if($this->input->get('order_by')) {
			$order_by = $this->input->get('order_by');
			$order = strtolower($this->input->get('order'));
			if($order_by!='money' && $order_by!='last_activity') $order_by = 'id';
			if($order!='desc' && $order!='asc') $order = 'DESC';
			else $order = strtoupper($order);
		} else {
			$order_by = 'id';
			$order = 'DESC';
		}
		$parameter['order'] = array('order_by'=>$order_by, 'order'=>$order);
		$data['users'] = $this->user_model->paginate($parameter);
		$parameter = array();
		$parameter['arg'][] = array('last_activity', '>', time()-1800);
		$parameter['arg'][] = array('token', '!=', '');
		$parameter['arg'][] = array('status', 'valid');
		$data['users_online'] = $this->user_model->count_rows($parameter);
		/* Load views */
		$this->load->view('manager/header', $data);
		$this->load->view('manager/users/users', $data);
		$this->load->view('manager/footer', $data);
	}
	public function item($id=null)
	{
		$data = loadData();
		if($id) $data['page'] = 'user_edit';
		else $data['page'] = 'user_add';
		$id = intval($id);
		$this->load->helper('form');
		$this->load->library('form_builder');
		$posts = $this->input->post();
		$parameter['arg'] = $id;
		if($id>0) $data['data'] = $this->user_model->get($parameter);
		/* Load views */
		$this->load->helper('form');
		$this->load->library('form_builder');
		$this->load->view('manager/header', $data);
		$this->load->view('manager/users/user_detail', $data);
		$this->load->view('manager/footer', $data);
	}
	public function account()
	{
		$id = intval($this->session->userdata('aID'));
		$data['admin'] = $this->admin_model->get($id);
		if(!isset($data['admin']['id'])) redirect(URL.'/login/');
		$posts = $this->input->post();
		if(isset($posts['id'])) {
			if($id>0) {
				if($this->input->post('password')!='') {
					$arg = array(
						'name' => $this->input->post('name'),
						'email' => $this->input->post('email'),
						'password' => $this->input->post('password')
					);
				} else {
					$arg = array(
						'name' => $this->input->post('name'),
						'email' => $this->input->post('email'),
					);
				}
				$data['return'] = $this->admin_model->update($arg, $id);
				$data['admin'] = $this->admin_model->get($id);
			}
		}
		/* Load views */
		$this->load->view('manager/header', $data);
		$this->load->view('manager/admins/account', $data);
		$this->load->view('manager/footer', $data);
	}
	public function group()
	{
		$data = loadData();
		$data['page'] = 'user_group';
		$posts = $this->input->post();
		if(isset($posts['id'])) {
			$posts = array_filter($posts);
			if(isset($posts['id'])) $data['return'] = $this->admin_group_model->update($posts, $posts['id']);
			else $data['return'] = $this->admin_group_model->insert($posts);
		}
		$data['groups'] = $this->admin_group_model->get_all();
		/* Load views */
		$this->load->helper('form');
		$this->load->library('form_builder');
		$this->load->view('manager/header', $data);
		$this->load->view('manager/users/groups', $data);
		$this->load->view('manager/footer', $data);
	}
	public function permission()
	{
		$data = loadData();
		$data['page'] = 'user_permission';
		$posts = $this->input->post();
		if(isset($posts['id'])) {
			$posts = array_filter($posts);
			if(isset($posts['id'])) $data['return'] = $this->permission_model->update($posts, $posts['id']);
			else $data['return'] = $this->permission_model->insert($posts);
		}
		$data['permissions'] = $this->permission_model->with_data()->order_by('group')->get_all();
		$data['groups'] = $this->admin_group_model->get_all();
		$this->load->helper('form');
		$this->load->library('form_builder');
		/* Load views */
		$this->load->view('manager/header', $data);
		$this->load->view('manager/users/permission', $data);
		$this->load->view('manager/footer', $data);
	}
	public function ajax($action=null)
	{
		if($action == 'del') {
			if(permission('users')) {
				$id = intval($this->input->post('id'));
				$user = $this->user_model->get($id);
				if(isset($user['id'])) {
					$this->user_model->delete($id);
					$log = array('model'=>get_class($this), 'item'=>$post['username'], 'item_id'=>$id, 'function'=>'delete');
					echo 'Deleted';
				}
			}
		} elseif($action == 'get') {
			if(permission('users')) {
				$id = intval($this->input->post('id'));
				$user = $this->user_model->get($id);
				if(isset($user['id'])) {
					$parameter['arg'] = $id;
					$data = $this->user_model->get($parameter);
					echo json_encode($data);
				}
			}
		} else if($action == 'getGroup') {
			if(permission('users')) {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$parameter['arg'] = $id;
					$data = $this->admin_group_model->get($parameter);
					echo json_encode($data);
				}
			}
		} else if($action == 'delGroup') {
			if(permission('users')) {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$data = $this->admin_group_model->delete($id);
					echo json_encode($data);
				}
			}
		} else if($action == 'permission') {
			if(permission('permission')) {
				$pid = intval($this->input->post('pid'));
				$gid = intval($this->input->post('gid'));
				$allow = intval($this->input->post('allow'));
				if($gid && $pid) {
					$parameter['arg'] = array(array('pid', $pid), array('gid', $gid));
					$per = $this->permission_group_model->get($parameter);
					$arg = array('pid' => $pid, 'gid'=> $gid, 'allow' => $allow);
					if(isset($per['pid'])) {
						$update = $this->permission_group_model->update($arg, $per['id']);
						if($update['status'] == 'success') echo 'Updated';
					} else {
						$insert = $this->permission_group_model->insert($arg);
						if($insert['status'] == 'success') echo 'Inserted';
					}
				}
			}
		} elseif($action == 'getPermission') {
			if(permission('users')) {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$parameter['arg'] = $id;
					$data = $this->permission_model->get($parameter);
					echo json_encode($data);
				}
			}
		} elseif($action == 'delPermission') {
			if(permission('users')) {
				$id = intval($this->input->post('id'));
				if($id>0) {
					$data = $this->permission_model->delete($id);
					echo json_encode($data);
				}
			}
		}
		exit;
	}
}