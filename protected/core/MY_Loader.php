<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
/* load the MX_Loader class */
define('EXT', '.php');
class MY_Loader extends CI_Loader {
    function widget($widget = '', $params = NULL, $object_name = NULL) {
        if ($widget == '') return FALSE;
        if (!is_null($params) AND !is_array($params)) $params = NULL;
        if (is_array($widget)) {
            foreach ($widget as $class) {
                $this->_ci_load_widget($class, $params, $object_name);
            }
        } else {
            $this->_ci_load_widget($widget, $params, $object_name);
        }
    }
    function _ci_load_widget($class, $params = NULL, $object_name = NULL) {
		$this->_ci_loaded_files = array();
        $subdir = '';
        if(strpos($class, '/') !== FALSE) {
            $temp = explode('/', $class);
            $class = end($temp);
            unset($temp[count($temp) - 1]);
            $subdir = implode($temp, '/') . '/';
        }
        $widget_class = APPPATH.'widgets/'.$subdir.$class.'/index'.EXT;
        if (in_array($widget_class, $this->_ci_loaded_files)) {
            if (!is_null($object_name)) {
                $CI = & get_instance();
                if (!isset($CI->$object_name)) {
                    return $this->_ci_init_library($class, '', $params, $object_name);
                }
            }
            $is_duplicate = TRUE;
            return;
        }
		if(file_exists($widget_class)) {
			include_once($widget_class);
			$this->_ci_loaded_files[] = $widget_class;
			return $this->_ci_init_library($class, '', $params, $object_name);
		} else return;
    }
	public function controller($file_name){
        $CI = & get_instance();
        $file_path = APPPATH.'controllers/'.$file_name.'.php';
        $object_name = $file_name;
        $class_name = ucfirst($file_name);
        if(file_exists($file_path)){
            require $file_path;
            $CI->$object_name = new $class_name();
        }
        else{
            show_error("Unable to load the requested controller class: ".$class_name);
        }
    }
}