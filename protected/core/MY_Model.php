<?php defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Model extends CI_Model
{
    /**
     * Select the database connection from the group names defined inside the database.php configuration file or an
     * array.
     */
    protected $_database_connection = NULL;
    /** @var
     * This one will hold the database connection object
     */
    protected $_database;
    /** @var null
     * Sets table name
     */
    public $table = NULL;
    /**
     * @var null
     * Sets PRIMARY KEY
     */
    public $primary_key = 'id';
    /**
     * @var array
     * You can establish the fields of the table. If you won't these fields will be filled by MY_Model (with one query)
     */
    public $table_fields = array();
    /**
     * @var array
     * Sets fillable fields
     */
    public $fillable = array();
    /**
     * @var array
     * Sets unique fields
     */
    public $unique = array();
    /**
     * @var array
     * Sets protected fields
     */
    public $protected = array();
    private $_can_be_filled = NULL;
    /** @var bool | array
     * Enables created_at and updated_at fields
     */
    protected $timestamps = TRUE;
    protected $timestamps_format = 'timestamp';
    protected $_created_at_field;
    protected $_updated_at_field;
    protected $_deleted_at_field;
    /** @var bool
     * Enables soft_deletes
     */
    protected $soft_deletes = FALSE;
    /** relationships variables */
    private $_relationships = array();
    public $has_one = array();
    public $has_many = array();
    public $has_many_pivot = array();
    public $separate_subqueries = TRUE;
    private $_requested = array();
    /** end relationships variables */
    /*pagination*/
    public $next_page;
    public $previous_page;
    public $all_pages;
    public $array_pages;
    public $pagination_open;
    public $pagination_delimiters;
    public $pagination_arrows;
    /* cache */
    private $cache;
    /* validation */
    private $validated = TRUE;
    private $row_fields_to_update = array();
    /**
     * The various callbacks available to the model. Each are
     * simple lists of method names (methods will be run on $this).
     */
    protected $before_create = array();
    protected $after_create = array();
    protected $before_update = array();
    protected $after_update = array();
    protected $before_get = array();
    protected $after_get = array();
    protected $before_delete = array();
    protected $after_delete = array();
    protected $before_soft_delete = array();
    protected $after_soft_delete = array();

    protected $callback_parameters = array();

    protected $return_as = 'array';
    protected $return_as_dropdown = NULL;
    protected $_dropdown_field = '';

    private $_trashed = 'without';

    private $_select = '*';


    public function __construct()
    {
        parent::__construct();
        $this->load->helper('inflector');
        $this->_set_connection();
        $this->_set_timestamps();
        $this->_fetch_table();
        $this->pagination_tags = array('<ul class="pagination">','</ul>');
        $this->pagination_delimiters = (isset($this->pagination_delimiters)) ? $this->pagination_delimiters : array('<li>','</li>','<li class="active">');
        $this->pagination_delimiters = (isset($this->pagination_delimiters)) ? $this->pagination_delimiters : array('<li>','</li>','<li class="active">');
        $this->pagination_arrows = (isset($this->pagination_arrows)) ? $this->pagination_arrows : array('‹','›');
		$this->cache = null;
        /* These below are implementation examples for before_create and before_update triggers.
        Their respective functions - add_creator() and add_updater() - can be found at the end of the model.
        They add user id on create and update. If you comment this out don't forget to do the same for the methods()
        $this->before_create[]='add_creator';
        $this->before_create[]='add_updater';
        */
    }
    public function cache($key)
    {
		$this->cache = $key;
		$this->load->library('xcache');
		return $this;
    }
    public function _get_table_fields()
    {
        if(empty($this->table_fields)) $this->table_fields = $this->_database->list_fields($this->table);
        return TRUE;
    }
    public function fillable_fields()
    {
        if(!isset($this->_can_be_filled))
        {
            $this->_get_table_fields();
            $no_protection = array();
            foreach ($this->table_fields as $field) {
                if (!in_array($field, $this->protected)) {
                    $no_protection[] = $field;
                }
            }
            if (!empty($this->fillable)) {
                $can_fill = array();
                foreach ($this->fillable as $field) {
                    if (in_array($field, $no_protection)) {
                        $can_fill[] = $field;
                    }
                }
                $this->_can_be_filled = $can_fill;
            } else {
                $this->_can_be_filled = $no_protection;
            }
        }
        return TRUE;
    }
    public function _prep_before_write($data, $id = null)
    {
        $this->fillable_fields();
        // We make sure we have the fields that can be filled
        $can_fill = $this->_can_be_filled;
        // Let's make sure we receive an array...
        $data_as_array = (is_object($data)) ? (array)$data : $data;
        $new_data = array();
		foreach ($data_as_array as $field => $value)
		{
			if(in_array($field, $this->unique)) {
				if($this->checkExist($field, $value, $id)) return array('status'=>'error', 'message'=>ucfirst($field).' '.$value.' is exist');
			}
			if (in_array($field, $can_fill)) {
				$new_data[$field] = $value;
			}
		}
        return array('status'=>'success','data'=>$new_data);
    }
    public function _prep_after_write()
    {
        return TRUE;
    }
    public function _prep_before_read() {}
    public function _prep_after_read($data, $multi = TRUE)
    {
        $data = $this->join_temporary_results($data);
        $this->_database->reset_query();
        if(isset($this->return_as_dropdown) && $this->return_as_dropdown == 'dropdown')
        {
            foreach($data as $row)
            {
                $dropdown[$row[$this->primary_key]] = $row[$this->_dropdown_field];
            }
            $data = $dropdown;
            $this->return_as_dropdown = NULL;
        }
        elseif($this->return_as == 'object')
        {
            $data = json_decode(json_encode($data), FALSE);
        }
        if(isset($this->_select))
        {
            $this->_select = '*';
        }
        return $data;
    }
    /**
     * public function from_form($rules = NULL,$additional_values = array(), $row_fields_to_update = array())
     * Gets data from form, after validating it and waits for an insert() or update() method in the query chain
     * @param null $rules Gets the validation rules. If nothing is passed (NULL), will look for the validation rules
     * inside the model $rules public property
     * @param array $additional_values Accepts additional fields to be filled, fields that are not to be found inside
     * the form. The values are inserted as an array with "field_name" => "field_value"
     * @param array $row_fields_to_update You can mention the fields from the form that can be used to identify
     * the row when doing an update
     * @return $this
     */
    public function from_form($rules = NULL,$additional_values = NULL, $row_fields_to_update = array())
    {
        $this->_get_table_fields();
        $this->load->library('form_validation');
        if(!isset($rules))
        {
            if(empty($row_fields_to_update))
            {
                $rules = $this->rules['insert'];
            }
            else
            {
                $rules = $this->rules['update'];
            }
        }
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run())
        {
            $this->fillable_fields();
            $this->validated = array();
            foreach($rules as $rule)
            {
                if(in_array($rule['field'],$this->_can_be_filled))
                {
                    $this->validated[$rule['field']] = $this->input->post($rule['field']);
                }
            }
            if(isset($additional_values) && is_array($additional_values) && !empty($additional_values))
            {
                foreach($additional_values as $field => $value)
                {
                    if(in_array($field, $this->_can_be_filled))
                    {
                        $this->validated[$field] = $value;
                    }
                }
            }

            if(!empty($row_fields_to_update))
            {
                foreach ($row_fields_to_update as $key => $field) {
                    if (in_array($field, $this->table_fields)) {
                        $this->row_fields_to_update[$field] = $this->input->post($field);
                    }
                    else if (in_array($key, $this->table_fields)){
                        $this->row_fields_to_update[$key] = $field;
                    }
                    else {
                        continue;
                    }
                }
            }
            return $this;
        }
        else
        {
            $this->validated = FALSE;
            return $this;
        }

    }
    public function checkExist($field, $value, $column_name_where=null) {
		if($column_name_where)
		{
			if (is_array($column_name_where))
			{
				$item = $this->get(array($field=>$value));
				$this->where($column_name_where);
				$raw = $this->get(array($field=>$value));
				if(isset($item['id']) && isset($raw['id']) && $item['id']!=$raw['id']) return true;
				else return false;
			} elseif (is_numeric($column_name_where)) {
				$this->_database->where($this->primary_key.' !=', $column_name_where);
			} else {
				$column_value = (is_object($data)) ? $data->{$column_name_where} : $data[$column_name_where];
				$this->_database->where($column_name_where.' !=', $column_value);
			}
		}
		$parameter['arg'] = array($field=>$value);
		$item = $this->get($parameter);
		if(isset($item['id'])) return true;
		else return false;
	}
    /**
     * public function insert($data)
     * Inserts data into table. Can receive an array or a multidimensional array depending on what kind of insert we're talking about.
     * @param $data
     * @return array Returns id, status, error
     */
    public function insert($data = NULL)
    {
        if(!isset($data) && $this->validated!=FALSE)
        {
            $data = $this->validated;
            $this->validated = FALSE;
        }
        elseif(!isset($data)) return array('status' => 'error', 'message' => 'Input not valid');
		$data = $this->trigger('before_create', $data);
        $data = $this->_prep_before_write($data);
		if($data['status'] == 'success') $data = $data['data'];
		else return $data;
        //now let's see if the array is a multidimensional one (multiple rows insert)
		if($this->timestamps !== FALSE)
		{
			$data[$this->_created_at_field] = $this->_the_timestamp();
		}
		if($this->_database->insert($this->table, $data))
		{
			$this->_prep_after_write();
			$id = $this->_database->insert_id();
			$return = $this->trigger('after_create', array('status'=>'success','message'=>'Insert is successfully','id'=>$id));
			return $return;
		}
		return array('status' => false, 'message' => 'Error');
        return false;
    }
    /**
     * public function update($data)
     * Updates data into table. Can receive an array or a multidimensional array depending on what kind of update we're talking about.
     * @param array $data
     * @param array|int $column_name_where
     * @param bool $escape should the values be escaped or not - defaults to true
     * @return str/array Returns id/ids of inserted rows
     */
    public function update($data = NULL, $column_name_where = NULL, $escape = TRUE)
    {
        if(!isset($data) && $this->validated!=FALSE)
        {
            $data = $this->validated;
            $this->validated = FALSE;
        }
        elseif(!isset($data))
        {
            $this->_database->reset_query();
            return array('status' => false);
        }
        // Prepare the data...
		$data = $this->trigger('before_update', $data);
        $data = $this->_prep_before_write($data, $column_name_where);
		if($data['status'] == 'success') $data = $data['data'];
		else return $data;
		if($this->timestamps !== FALSE)
		{
			$data[$this->_updated_at_field] = $this->_the_timestamp();
		}
		if($this->validated === FALSE && count($this->row_fields_to_update))
		{
			$this->where($this->row_fields_to_update);
			$this->row_fields_to_update = array();
		}
		if(isset($column_name_where))
		{
			if (is_array($column_name_where))
			{
				$this->where($column_name_where);
			} elseif (is_numeric($column_name_where)) {
				$this->_database->where($this->primary_key, $column_name_where);
			} else {
				$column_value = (is_object($data)) ? $data->{$column_name_where} : $data[$column_name_where];
				$this->_database->where($column_name_where, $column_value);
			}
		}
		if($escape)
		{
			if($this->_database->update($this->table, $data))
			{
				$this->_prep_after_write();
				$affected = $this->_database->affected_rows();
				$return = $this->trigger('after_update', array('status'=>'success','message'=>'Update is successfully','id'=>$column_name_where,'affected'=>$affected));
				return $return;
			}
		}
		else
		{
			if($this->_database->set($data, null, FALSE)->update($this->table))
			{
				$this->_prep_after_write();
				$affected = $this->_database->affected_rows();
				$return = $this->trigger('after_update', array('status'=>'success','message'=>'Update is successfully','id'=>$column_name_where,'affected'=>$affected));
				return $return;
			}
		}
		return array('status' => false);
        return false;
    }

    /**
     * public function where($field_or_array = NULL, $operator_or_value = NULL, $value = NULL, $with_or = FALSE, $with_not = FALSE, $custom_string = FALSE)
     * Sets a where method for the $this object
     * @param null $field_or_array - can receive a field name or an array with more wheres...
     * @param null $operator_or_value - can receive a database operator or, if it has a field, the value to equal with
     * @param null $value - a value if it received a field name and an operator
     * @param bool $with_or - if set to true will create a or_where query type pr a or_like query type, depending on the operator
     * @param bool $with_not - if set to true will also add "NOT" in the where
     * @param bool $custom_string - if set to true, will simply assume that $field_or_array is actually a string and pass it to the where query
     * @return $this
     */
    public function where($field_or_array = NULL, $operator_or_value = NULL, $value = NULL, $with_or = FALSE, $with_not = FALSE, $custom_string = FALSE)
    {
		$field_or_array = $this->trigger('before_get', $field_or_array);
        if(is_array($field_or_array))
        {
            $multi = $this->is_multidimensional($field_or_array);
            if($multi === TRUE)
            {
				$field_or_array = $this->trigger('before_get', $field_or_array);
                foreach ($field_or_array as $where)
                {
                    $field = $where[0];
                    $operator_or_value = isset($where[1]) ? $where[1] : NULL;
                    $value = isset($where[2]) ? $where[2] : NULL;
                    $with_or = (isset($where[3])) ? TRUE : FALSE;
                    $with_not = (isset($where[4])) ? TRUE : FALSE;
                    $this->where($field, $operator_or_value, $value, $with_or,$with_not);
                }
                return $this;
            }
        }
		//Multi search
        if(is_array($field_or_array) && $operator_or_value=='search' && $value)
        {
			foreach ($field_or_array as $key=>$field)
			{
                if($key==0) $this->_database->like($field, $value);
                else $this->_database->or_like($field, $value);
			}
			return $this;
        }
        if($with_or === TRUE) $where_or = 'or_where'; else $where_or = 'where';
        if($with_not === TRUE) $not = '_not'; else $not = '';
        if($custom_string === TRUE)
        {
            $this->_database->{$where_or}($field_or_array, NULL, FALSE);
        }
        elseif(is_numeric($field_or_array))
        {
            $this->_database->{$where_or}(array($this->table.'.'.$this->primary_key => $field_or_array));
        }
        elseif(is_array($field_or_array) && !isset($operator_or_value))
        {
            $this->_database->where($field_or_array);
        }
        elseif(!isset($value) && isset($field_or_array) && isset($operator_or_value) && !is_array($operator_or_value))
        {
            $this->_database->{$where_or}(array($this->table.'.'.$field_or_array => $operator_or_value));
        }
        elseif(!isset($value) && isset($field_or_array) && isset($operator_or_value) && is_array($operator_or_value) && !is_array($field_or_array))
        {
            $this->_database->{$where_or.$not.'_in'}($this->table.'.'.$field_or_array, $operator_or_value);
        }
        elseif(isset($field_or_array) && isset($operator_or_value) && isset($value))
        {
            if(strtolower($operator_or_value) == 'like') {
                if($with_not === TRUE)
                {
                    $like = 'not_like';
                }
                else
                {
                    $like = 'like';
                }
                if ($with_or === TRUE)
                {
                    $like = 'or_'.$like;
                }
                $this->_database->{$like}($field_or_array, $value);
            }
            else
            {
                $this->_database->{$where_or}($field_or_array.' '.$operator_or_value, $value);
            }
        }
        return $this;
    }

    /*
     * public function is_multidimensional($array)
     * Verifies if an array is multidimensional or not;
     * @param array $array
     * @return bool return TRUE if the array is a multidimensional one
     */
    public function is_multidimensional($array)
    {
        if(is_array($array))
        {
            foreach($array as $element)
            {
                if(is_array($element))
                {
                    return TRUE;
                }
            }
        }
        return false;
    }
    /**
     * public function limit($limit, $offset = 0)
     * Sets a rows limit to the query
     * @param $limit
     * @param int $offset
     * @return $this
     */
    public function limit($limit, $offset = 0)
    {
        $this->_database->limit($limit, $offset);
        return $this;
    }

    /**
     * public function group_by($grouping_by)
     * A wrapper to $this->_database->group_by()
     * @param $grouping_by
     * @return $this
     */
    public function group_by($grouping_by)
    {
        $this->_database->group_by($grouping_by);
        return $this;
    }
    /**
     * public function delete($where)
     * Deletes data from table.
     * @param $where primary_key(s) Can receive the primary key value or a list of primary keys as array()
     * @return Returns affected rows or false on failure
     */
    public function delete($parameter = NULL)
    {
        if(!empty($this->before_delete) || !empty($this->before_soft_delete) || !empty($this->after_delete) || !empty($this->after_soft_delete) || ($this->soft_deletes === TRUE))
        {
            $to_update = array();
            $this->parse_data($parameter);
            $query = $this->_database->get($this->table);
            foreach($query->result() as $row)
            {
                $to_update[] = array($this->primary_key => $row->{$this->primary_key});
            }
            if(!empty($this->before_soft_delete))
            {
                foreach($to_update as &$row)
                {
                    $row = $this->trigger('before_soft_delete',$row);
                }
            }
            if(!empty($this->before_delete))
            {
                foreach($to_update as &$row)
                {
                    $row = $this->trigger('before_delete',$row);
                }
            }
        }
        $this->parse_data($parameter);
        $affected_rows = 0;
        if($this->soft_deletes === TRUE)
        {
            if(isset($to_update)&& count($to_update) > 0)
            {

                foreach($to_update as &$row)
                {
                    //$row = $this->trigger('before_soft_delete',$row);
                    $row[$this->_deleted_at_field] = $this->_the_timestamp();
                }
                $affected_rows = $this->_database->update_batch($this->table, $to_update, $this->primary_key);
                $to_update['affected_rows'] = $affected_rows;
                $this->_prep_after_write();
                $this->trigger('after_soft_delete',$to_update);
            }
            return $affected_rows;
        }
        else
        {
            if($this->_database->delete($this->table))
            {
                $affected_rows = $this->_database->affected_rows();
                if(!empty($this->after_delete))
                {
                    $to_update['affected_rows'] = $affected_rows;
                    $to_update = $this->trigger('after_delete',$to_update);
                    $affected_rows = $to_update;
                }
                $this->_prep_after_write();
                return $affected_rows;
            }
        }
        return false;
    }

    /**
     * public function force_delete($where = NULL)
     * Forces the delete of a row if soft_deletes is enabled
     * @param null $where
     * @return bool
     */
    public function force_delete($parameter = NULL)
    {
        $this->parse_data($parameter);
        if($this->_database->delete($this->table))
        {
            $this->_prep_after_write();
            return $this->_database->affected_rows();
        }
        return false;
    }

    /**
     * public function restore($where = NULL)
     * "Un-deletes" a row
     * @param null $where
     * @return bool
     */
    public function restore($parameter = NULL)
    {
        $this->with_trashed();
        $this->parse_data($parameter);
        if($affected_rows = $this->_database->update($this->table,array($this->_deleted_at_field=>NULL)))
        {
            $this->_prep_after_write();
            return $affected_rows;
        }
        return false;
    }

    /**
     * public function trashed($where = NULL)
     * Verifies if a record (row) is soft_deleted or not
     * @param null $where
     * @return bool
     */
    public function trashed($parameter = NULL)
    {
        $this->only_trashed();
        $this->parse_data($parameter);
        $this->limit(1);
        $query = $this->_database->get($this->table);
        if($query->num_rows() == 1)
        {
            return TRUE;
        }
        return false;
    }


    /**
     * public function get()
     * Retrieves one row from table.
     * @param null $where
     * @return mixed
     */
    public function get($parameter = NULL)
    {
		$this->parse_data($parameter);
		if($this->_select) $this->_database->select($this->_select);
		$this->limit(1);
		$query = $this->_database->get($this->table);
		if ($query->num_rows() == 1)
		{
			$row = $query->row_array();
			$row =  $this->_prep_after_read(array($row),FALSE);
			$row = $this->trigger('after_get', $row);
			$row = $row[0];
			return $row;
		}
		else return false;
    }

    /**
     * public function get_all()
     * Retrieves rows from table.
     * @param null $where
     * @return mixed
     */
    public function get_all($parameter = NULL)
    {
		$this->parse_data($parameter);
		if($this->soft_deletes===TRUE) $this->_where_trashed();
		if(isset($this->_select)) $this->_database->select($this->_select);
		if(!empty($this->_requested))
		{
			foreach($this->_requested as $requested)
			{
				$this->_database->select($this->_relationships[$requested['request']]['local_key']);
			}
		}
		$query = $this->_database->get($this->table);
		if($query->num_rows() > 0)
		{
			$data = $query->result_array();
			$data = $this->_prep_after_read($data, TRUE);
			$data = $this->trigger('after_get', $data);
			return $data;
		}
		else return false;
    }

    /**
     * public function count_rows()
     * Retrieves number of rows from table.
     * @param null $where
     * @return integer
     */
    public function count_rows($parameter = NULL)
    {
		$this->parse_data($parameter);
		if($this->soft_deletes===TRUE) $this->_where_trashed();
        $this->_database->from($this->table);
        $number_rows = $this->_database->count_all_results();
        return $number_rows;
    }

    /** RELATIONSHIPS */

    /**
     * public function with($requests)
     * allows the user to retrieve records from other interconnected tables depending on the relations defined before the constructor
     * @param string $request
     * @param array $arguments
     * @return $this
     */
    public function parse_data($parameter) {
		if(is_numeric($parameter)) $this->where($parameter);
		else {
			if(isset($parameter['as_array']) && $parameter['as_array']==true) $this->as_array();
			elseif(isset($parameter['as_object']) && $parameter['as_object']==true) $this->as_object();
			elseif(isset($parameter['as_dropdown']) && $parameter['as_dropdown']==true) $this->as_dropdown();
			if(isset($parameter['with_trashed']) && $parameter['with_trashed']==true) $this->with_trashed();
			if(isset($parameter['only_trashed']) && $parameter['only_trashed']==true) $this->only_trashed();
			if(isset($parameter['arg'])) $this->where($parameter['arg']);
			if(isset($parameter['exclude'])) {
				if(!is_array($parameter['exclude'])) $parameter['exclude'] = array(intval($parameter['exclude']));
				$this->where('id', $parameter['exclude'], null, false, true);
			}
			if(isset($parameter['fields'])) $this->fields($parameter['fields']);
			if(isset($parameter['limit'])) {
			    if(isset($parameter['offset'])) $this->limit($parameter['limit'], $parameter['offset']);
				else $this->limit($parameter['limit']);
			}
			if(isset($parameter['with'])) {
				foreach($parameter['with'] as $key=>$value) {
					if(is_numeric($key)) $this->with($value);
					else {
						if(!is_array($value)) $value = array($value);
						$this->with($key, $value);
					}
				}
			}
			if(isset($parameter['order'])) {
				if(isset($parameter['order']['order_by']) && isset($parameter['order']['order']))
					$this->order_by($parameter['order']['order_by'], $parameter['order']['order']);
				else $this->order_by($parameter['order']);
			}
		}
	}
    public function with($request, $arguments = array())
    {
        $this->_set_relationships();
        if (array_key_exists($request, $this->_relationships))
        {
            $this->_requested[$request] = array('request'=>$request);
            $parameters = array();
            if(isset($arguments))
            {
                foreach($arguments as $argument)
                {
                    if(is_array($argument))
                    {
                        foreach($argument as $k => $v)
                        {
                            $parameters[$k] = $v;
                        }
                    }
                    else
                    {
                        $requested_operations = explode('|',$argument);
                        foreach($requested_operations as $operation)
                        {
                            $elements = explode(':', $operation, 2);
                            if (sizeof($elements) == 2) {
                                $parameters[$elements[0]] = $elements[1];
                            } else {
                                show_error('MY_Model: Parameters for with_*() method must be of the form: "...->with_*(\'where:...|fields:...\')"');
                            }
                        }
                    }
                }
            }
            $this->_requested[$request]['parameters'] = $parameters;
        }


        /*
        if($separate_subqueries === FALSE)
        {
            $this->separate_subqueries = FALSE;
            foreach($this->_requested as $request)
            {
                if($this->_relationships[$request]['relation'] == 'has_one') $this->_has_one($request);
            }
        }
        else
        {
            $this->after_get[] = 'join_temporary_results';
        }
        */
        return $this;
    }

    /**
     * protected function join_temporary_results($data)
     * Joins the subquery results to the main $data
     * @param $data
     * @return mixed
     */
    protected function join_temporary_results($data)
    {
        $order_by = array();
        $order_inside_array = array();
        //$order_inside = '';
        foreach($this->_requested as $requested_key => $request)
        {
            $pivot_table = NULL;
            $relation = $this->_relationships[$request['request']];
            $this->load->model($relation['foreign_model']);
            $foreign_key = $relation['foreign_key'];
            $local_key = $relation['local_key'];
            $foreign_table = $relation['foreign_table'];
            $type = $relation['relation'];
            $relation_key = $relation['relation_key'];
            if($type=='has_many_pivot')
            {
                $pivot_table = $relation['pivot_table'];
                $pivot_local_key = $relation['pivot_local_key'];
                $pivot_foreign_key = $relation['pivot_foreign_key'];
                $get_relate = $relation['get_relate'];
            }
            if(array_key_exists('order_inside',$request['parameters']))
            {
                //$order_inside = $request['parameters']['order_inside'];
                $elements = explode(',', $request['parameters']['order_inside']);
                foreach($elements as $element)
                {
                    $order = explode(' ',$element);
                    if(sizeof($order)==2)
                    {
                        $order_inside_array[] = array(trim($order[0]), trim($order[1]));
                    }
                    else
                    {
                        $order_inside_array[] = array(trim($order[0]), 'desc');
                    }
                }

            }
            $local_key_values = array();
            foreach($data as $key => $element)
            {
                if(isset($element[$local_key]) and !empty($element[$local_key]))
                {
                    $id = $element[$local_key];
                    $local_key_values[$key] = $id;
                }
            }
            if(!$local_key_values)
            {
                $data[$key][$relation_key] = NULL;
                continue;
            }
            if(!isset($pivot_table))
            {
                $sub_results = $this->{$relation['foreign_model']};
                $select = array();
                $select[] = '`'.$foreign_table.'`.`'.$foreign_key.'`';
                if(!empty($request['parameters']))
                {
                    if(array_key_exists('fields',$request['parameters']))
                    {
                        if($request['parameters']['fields'] == '*count*')
                        {
                            $the_select = '*count*';
                            $sub_results = (isset($the_select)) ? $sub_results->fields($the_select) : $sub_results;
                            $sub_results = $sub_results->fields($foreign_key);
                        }
                        else
                        {
                            $fields = explode(',', $request['parameters']['fields']);
                            foreach ($fields as $field)
                            {
                                $select[] = (strpos($field,'.')===FALSE) ? '`' . $foreign_table . '`.`' . trim($field) . '`' : trim($field);
                            }
                            $the_select = implode(',', $select);
                            $sub_results = (isset($the_select)) ? $sub_results->fields($the_select) : $sub_results;
                        }

                    }
                    if(array_key_exists('fields',$request['parameters']) && ($request['parameters']['fields']=='*count*'))
                    {
                        $sub_results->group_by('`' . $foreign_table . '`.`' . $foreign_key . '`');
                    }
                    if(array_key_exists('where',$request['parameters']) || array_key_exists('non_exclusive_where',$request['parameters']))
                    {
                        $the_where = array_key_exists('where', $request['parameters']) ? 'where' : 'non_exclusive_where';
                    }
                    $sub_results = isset($the_where) ? $sub_results->where($request['parameters'][$the_where],NULL,NULL,FALSE,FALSE,TRUE) : $sub_results;

                    if(isset($order_inside_array))
                    {
                        foreach($order_inside_array as $order_by_inside)
                        {
                            $sub_results = $sub_results->order_by($order_by_inside[0],$order_by_inside[1]);
                        }
                    }

                    //Add nested relation
                    if(array_key_exists('with',$request['parameters']))
                    {
                        // Do we have many nested relation
                        if(is_array($request['parameters']['with']) && isset($request['parameters']['with'][0]))
                        {
                            foreach ($request['parameters']['with'] as $with)
                            {
                                $with_relation = array_shift($with);
                                $sub_results->with($with_relation, array($with));
                            }
                        }
                        else // single nested relation
                        {
                            $with_relation = array_shift($request['parameters']['with']);
                            $sub_results->with($with_relation,array($request['parameters']['with']));
                        }
                    }
                }
                $sub_results = $sub_results->where($foreign_key, $local_key_values)->get_all();
            }
            else
            {
                $this->_database->join($pivot_table, $foreign_table.'.'.$foreign_key.' = '.$pivot_table.'.'.$pivot_foreign_key, 'left');
                $this->_database->join($this->table, $pivot_table.'.'.$pivot_local_key.' = '.$this->table.'.'.$local_key,'left');
                $this->_database->select($foreign_table.'.'.$foreign_key);
                $this->_database->select($pivot_table.'.'.$pivot_local_key);
                if(!empty($request['parameters']))
                {
                    if(array_key_exists('fields',$request['parameters']))
                    {
                        if($request['parameters']['fields'] == '*count*')
                        {
                            $this->_database->select('COUNT(`'.$foreign_table.'`*) as counted_rows, `' . $foreign_table . '`.`' . $foreign_key . '`', FALSE);
                        }
                        else
                        {
                            $fields = explode(',', $request['parameters']['fields']);
                            $select = array();
                            foreach ($fields as $field) {
                                $select[] = (strpos($field,'.')===FALSE) ? '`' . $foreign_table . '`.`' . trim($field) . '`' : trim($field);
                            }
                            $the_select = implode(',', $select);
                            $this->_database->select($the_select);
                        }
                    }

                    if(array_key_exists('where',$request['parameters']) || array_key_exists('non_exclusive_where',$request['parameters']))
                    {
                        $the_where = array_key_exists('where',$request['parameters']) ? 'where' : 'non_exclusive_where';

                        $this->_database->where($request['parameters'][$the_where],NULL,NULL,FALSE,FALSE,TRUE);
                    }
                }
                $this->_database->where_in($pivot_table.'.'.$pivot_local_key,$local_key_values);

                if(!empty($order_inside_array))
                {
                    $order_inside_str = '';
                    foreach($order_inside_array as $order_by_inside)
                    {
                        $order_inside_str .= (strpos($order_by_inside[0],',')=== false) ? '`'.$foreign_table.'`.`'.$order_by_inside[0].' '.$order_by_inside[1] : $order_by_inside[0].' '.$order_by_inside[1];
                        $order_inside_str .= ',';
                    }
                    $order_inside_str = rtrim($order_inside_str, ",");
                    $this->_database->order_by(rtrim($order_inside_str,","));
                }
                $sub_results = $this->_database->get($foreign_table)->result_array();
                $this->_database->reset_query();
            }

            if(isset($sub_results) && !empty($sub_results)) {
                $subs = array();

                foreach ($sub_results as $result) {
                    $result_array = (array)$result;
                    $the_foreign_key = $result_array[$foreign_key];
                    if(isset($pivot_table))
                    {
                        $the_local_key = $result_array[$pivot_local_key];
                        if(isset($get_relate) and $get_relate === TRUE)
                        {
                            $subs[$the_local_key][$the_foreign_key] = $this->{$relation['foreign_model']}->where($local_key, $result[$local_key])->get();
                        }
                        else
                        {
                            $subs[$the_local_key][$the_foreign_key] = $result;
                        }
                    }
                    else
                    {
                        if ($type == 'has_one') {
                            $subs[$the_foreign_key] = $result;
                        } else {
                            $subs[$the_foreign_key][] = $result;
                        }
                    }


                }
                $sub_results = $subs;

                foreach($local_key_values as $key => $value)
                {
                    if(array_key_exists($value,$sub_results))
                    {
                        $data[$key][$relation_key] = $sub_results[$value];
                    }
                    else
                    {
                        if(array_key_exists('where',$request['parameters']))
                        {
                            unset($data[$key]);
                        }
                    }
                }
            }
            else
            {
                $data[$key][$relation_key] = NULL;
            }
            if(array_key_exists('order_by',$request['parameters']))
            {
                $elements = explode(',', $request['parameters']['order_by']);
                if(sizeof($elements)==2)
                {
                    $order_by[$relation_key] = array(trim($elements[0]), trim($elements[1]));
                }
                else
                {
                    $order_by[$relation_key] = array(trim($elements[0]), 'desc');
                }
            }
            unset($this->_requested[$requested_key]);
        }
        if(!empty($order_by))
        {
            foreach($order_by as $field => $row)
            {
                list($key, $value) = $row;
                $data = $this->_build_sorter($data, $field, $key, $value);
            }
        }
        return $data;
    }


    /**
     * private function _has_one($request)
     *
     * returns a joining of two tables depending on the $request relationship established in the constructor
     * @param $request
     * @return $this
     */
    private function _has_one($request)
    {
        $relation = $this->_relationships[$request];
        $this->_database->join($relation['foreign_table'], $relation['foreign_table'].'.'.$relation['foreign_key'].' = '.$this->table.'.'.$relation['local_key'], 'left');
        return TRUE;
    }

    /**
     * private function _set_relationships()
     *
     * Called by the public method with() it will set the relationships between the current model and other models
     */
    private function _set_relationships()
    {
        if(empty($this->_relationships))
        {
            $options = array('has_one','has_many','has_many_pivot');
            foreach($options as $option)
            {
                if(isset($this->{$option}) && !empty($this->{$option}))
                {
                    foreach($this->{$option} as $key => $relation)
                    {
                        if(!is_array($relation))
                        {
                            $foreign_model = $relation;
                            $foreign_model_name = strtolower($foreign_model);
                            $this->load->model($foreign_model_name);
                            $foreign_table = $this->{$foreign_model_name}->table;
                            $foreign_key = $this->{$foreign_model_name}->primary_key;
                            $local_key = $this->primary_key;
                            $pivot_local_key = $this->table.'_'.$local_key;
                            $pivot_foreign_key = $foreign_table.'_'.$foreign_key;
                            $get_relate = FALSE;

                        }
                        else
                        {
                            if($this->_is_assoc($relation))
                            {
                                $foreign_model = $relation['foreign_model'];
                                if(array_key_exists('foreign_table',$relation))
                                {
                                    $foreign_table = $relation['foreign_table'];
                                }
                                else
                                {
                                    $foreign_model_name = strtolower($foreign_model);
                                    $this->load->model($foreign_model_name);
                                    $foreign_table = $this->{$foreign_model_name}->table;
                                }
                                $foreign_key = $relation['foreign_key'];
                                $local_key = $relation['local_key'];
                                if($option=='has_many_pivot')
                                {
                                    $pivot_table = $relation['pivot_table'];
                                    $pivot_local_key = (array_key_exists('pivot_local_key',$relation)) ? $relation['pivot_local_key'] : $this->table.'_'.$this->primary_key;
                                    $pivot_foreign_key = (array_key_exists('pivot_foreign_key',$relation)) ? $relation['pivot_foreign_key'] : $foreign_table.'_'.$foreign_key;
                                    $get_relate = (array_key_exists('get_relate',$relation) && ($relation['get_relate']===TRUE)) ? TRUE : FALSE;
                                }
                            }
                            else
                            {
                                $foreign_model = $relation[0];
                                $foreign_model_name = strtolower($foreign_model);
                                $this->load->model($foreign_model_name);
                                $foreign_table = $this->{$foreign_model_name}->table;
                                $foreign_key = $relation[1];
                                $local_key = $relation[2];
                                if($option=='has_many_pivot')
                                {
                                    $pivot_local_key = $this->table.'_'.$this->primary_key;
                                    $pivot_foreign_key = $foreign_table.'_'.$foreign_key;
                                    $get_relate = (isset($relation[3]) && ($relation[3]===TRUE())) ? TRUE : FALSE;
                                }
                            }

                        }

                        if($option=='has_many_pivot' && !isset($pivot_table))
                        {
                            $tables = array($this->table, $foreign_table);
                            sort($tables);
                            $pivot_table = $tables[0].'_'.$tables[1];
                        }

                        $this->_relationships[$key] = array('relation' => $option, 'relation_key' => $key, 'foreign_model' => strtolower($foreign_model), 'foreign_table' => $foreign_table, 'foreign_key' => $foreign_key, 'local_key' => $local_key);
                        if($option == 'has_many_pivot')
                        {
                            $this->_relationships[$key]['pivot_table'] = $pivot_table;
                            $this->_relationships[$key]['pivot_local_key'] = $pivot_local_key;
                            $this->_relationships[$key]['pivot_foreign_key'] = $pivot_foreign_key;
                            $this->_relationships[$key]['get_relate'] = $get_relate;
                        }
                    }
                }
            }
        }
    }

    /** END RELATIONSHIPS */

    /**
     * public function on($connection_group = NULL)
     * Sets a different connection to use for a query
     * @param $connection_group = NULL - connection group in database setup
     * @return obj
     */
    public function on($connection_group = NULL)
    {
        if(isset($connection_group))
        {
            $this->_database->close();
            $this->load->database($connection_group);
            $this->_database = $this->db;
        }
        return $this;
    }

    /**
     * public function reset_connection($connection_group = NULL)
     * Resets the connection to the default used for all the model
     * @return obj
     */
    public function reset_connection()
    {
        if(isset($connection_group))
        {
            $this->_database->close();
            $this->_set_connection();
        }
        return $this;
    }

    /**
     * Trigger an event and call its observers. Pass through the event name
     * (which looks for an instance variable $this->event_name), an array of
     * parameters to pass through and an optional 'last in interation' boolean
     */
    public function trigger($event, $data = array(), $last = TRUE)
    {
        if (isset($this->$event) && is_array($this->$event))
        {
            foreach ($this->$event as $method)
            {
                if (strpos($method, '('))
                {
                    preg_match('/([a-zA-Z0-9\_\-]+)(\(([a-zA-Z0-9\_\-\., ]+)\))?/', $method, $matches);
                    $method = $matches[1];
                    $this->callback_parameters = explode(',', $matches[3]);
                }
                $data = call_user_func_array(array($this, $method), array($data, $last));
            }
        }
        return $data;
    }


    /**
     * public function with_trashed()
     * Sets $_trashed to TRUE
     */
    public function with_trashed()
    {
        $this->_trashed = 'with';
        return $this;
    }

    /**
     * public function with_trashed()
     * Sets $_trashed to TRUE
     */
    public function only_trashed()
    {
        $this->_trashed = 'only';
        return $this;
    }

    private function _where_trashed()
    {
		if($this->soft_deletes) {
			switch($this->_trashed)
			{
				case 'only' :
					$this->_database->where($this->_deleted_at_field.' IS NOT NULL', NULL, FALSE);
					break;
				case 'without' :
					$this->_database->where($this->_deleted_at_field.' IS NULL', NULL, FALSE);
					break;
				case 'with' :
					break;
			}
			$this->_trashed = 'without';
        }
        return $this;
    }

    /**
     * public function fields($fields)
     * does a select() of the $fields
     * @param $fields the fields needed
     * @return $this
     */
    public function fields($fields = NULL)
    {
        if(isset($fields))
        {
            if($fields == '*count*')
            {
                $this->_select = '';
                $this->_database->select('COUNT(*) AS counted_rows',FALSE);
            }
            else
            {
                $this->_select = array();
                $fields = (!is_array($fields)) ? explode(',', $fields) : $fields;
                if (!empty($fields))
                {
                    foreach ($fields as &$field)
                    {
                        $exploded = explode('.', $field);
                        if (sizeof($exploded) < 2)
                        {
							if(strstr($field, 'SUM(')) {
								$field = preg_replace("/SUM\((.*)/", "SUM(".$this->table.".$1", $field);
							}
							else $field = $this->table . '.' . $field;
                        }
                    }
                }
                $this->_select = $fields;
            }
        }
        else
        {
            $this->_select = NULL;
        }
        return $this;
    }

    /**
     * public function order_by($criteria, $order = 'ASC'
     * A wrapper to $this->_database->order_by()
     * @param $criteria
     * @param string $order
     * @return $this
     */
    public function order_by($criteria, $order = 'ASC')
    {
        if(is_array($criteria))
        {
            foreach ($criteria as $key=>$value)
            {
                $this->_database->order_by($key, $value);
            }
        }
        else
        {
            $this->_database->order_by($criteria, $order);
        }
        return $this;
    }

    /**
     * Return the next call as an array rather than an object
     */
    public function as_array()
    {
        $this->return_as = 'array';
        return $this;
    }

    /**
     * Return the next call as an object rather than an array
     */
    public function as_object()
    {
        $this->return_as = 'object';
        return $this;
    }

    public function as_dropdown($field = NULL)
    {
        if(!isset($field))
        {
            show_error('MY_Model: You must set a field to be set as value for the key: ...->as_dropdown(\'field\')->...');
            exit;
        }
        $this->return_as_dropdown = 'dropdown';
        $this->_dropdown_field = $field;
        $this->_select = array($this->primary_key, $field);
        return $this;
    }

    /**
     * private function _set_timestamps()
     *
     * Sets the fields for the created_at, updated_at and deleted_at timestamps
     * @return bool
     */
    private function _set_timestamps()
    {
        if($this->timestamps !== FALSE)
        {
            $this->_created_at_field = (is_array($this->timestamps) && isset($this->timestamps[0])) ? $this->timestamps[0] : 'created';
            $this->_updated_at_field = (is_array($this->timestamps) && isset($this->timestamps[1])) ? $this->timestamps[1] : 'updated';
            $this->_deleted_at_field = (is_array($this->timestamps) && isset($this->timestamps[2])) ? $this->timestamps[2] : 'deleted';
        }
        return TRUE;
    }

    /**
     * private function _the_timestamp()
     *
     * returns a value representing the date/time depending on the timestamp format choosed
     * @return string
     */
    private function _the_timestamp()
    {
        if($this->timestamps_format=='timestamp')
        {
            return time();
        }
        else
        {
            return date($this->timestamps_format);
        }
    }

    /**
     * private function _set_connection()
     *
     * Sets the connection to database
     */
    private function _set_connection()
    {
        if(isset($this->_database_connection))
        {
            $this->_database = $this->load->database($this->_database_connection,TRUE);
        }
        else
        {
            $this->load->database();
            $this->_database =$this->db;
        }
        // This may not be required
        return $this;
    }

    /*
     * HELPER FUNCTIONS
     */
    public function paginate($parameter, $page_number = 1)
    {
		$rows_per_page = $parameter['paginate']['rows'];
		$total_rows = $parameter['paginate']['total'];
        $this->load->helper('url');
        $segments = $this->uri->total_segments();
        $uri_array = $this->uri->segment_array();
        $uri_string = implode('/',$uri_array);
        $page = $this->input->get('page');
		$query = str_replace('&page=', 'page=', $_SERVER['QUERY_STRING']);
		$query = preg_replace('/page=([0-9]*)/', '', $query);
		if($query && $query[0] == '&') $query = substr($query, 1);
        if(is_numeric($page)) $page_number = $page;
        $next_page = $page_number+1;
        $previous_page = $page_number-1;
		$number_of_pages = ceil($total_rows/$rows_per_page);
        if($number_of_pages>1) {
            if($page_number != 1)
            {
                if($query) $temp_query = '?page=1&'.$query;
                else $temp_query = '?page=1';
                $this->previous_page = $this->pagination_delimiters[0].anchor(URL.'/'.$uri_string.$temp_query,'«').$this->pagination_delimiters[1];
				$first = array('title'=>'«', 'page'=>1);
                if($query) $temp_query = '?page='.$previous_page.'&'.$query;
                else $temp_query = '?page='.$previous_page;
                $this->previous_page .= $this->pagination_delimiters[0].anchor(URL.'/'.$uri_string.$temp_query,$this->pagination_arrows[0]).$this->pagination_delimiters[1];
				$previous = array('title'=>$this->pagination_arrows[0], 'page'=>$previous_page);
            }
            if(isset($total_rows) && ($number_of_pages > $page_number))
            {
                if($query) $temp_query = '?page='.$next_page.'&'.$query;
                else $temp_query = '?page='.$next_page;
                $this->next_page = $this->pagination_delimiters[0].anchor(URL.'/'.$uri_string.$temp_query, $this->pagination_arrows[1]).$this->pagination_delimiters[1];
				$next = array('title'=>$this->pagination_arrows[1], 'page'=>$next_page);
                if($query) $temp_query = '?page='.$number_of_pages.'&'.$query;
                else $temp_query = '?page='.$number_of_pages;
                $this->next_page .= $this->pagination_delimiters[0].anchor(URL.'/'.$uri_string.$temp_query, '»').$this->pagination_delimiters[1];
				$last = array('title'=>'»', 'page'=>$number_of_pages);
            }
            $rows_per_page = (is_numeric($rows_per_page)) ? $rows_per_page : 10;
            if(isset($total_rows))
            {
                if($total_rows!=0)
                {
					if($page_number > 2) $start = $page_number - 2;
					else $start = 1;
					if($number_of_pages - $page_number > 2) $end = $page_number + 2;
					else $end = $number_of_pages;
                    $links = $this->previous_page;
					if(isset($first)) $this->array_pages[] = $first;
					if(isset($previous)) $this->array_pages[] = $previous;
                    for ($i = $start; $i <= $end; $i++) {
                        unset($uri_array[$segments]);
    					if(($page_number == $i)) {
    						$links .= $this->pagination_delimiters[2];
    						$links .= '<a href="javascript:;">'.$i.'</a>';
    						$links .= $this->pagination_delimiters[1];
							$this->array_pages[] = array('title'=>$i, 'page'=>'');
    					} else {
                            if($query) $temp_query = '?page='.$i.'&'.$query;
                            else $temp_query = '?page='.$i;
    						$links .= $this->pagination_delimiters[0];
    						$links .= anchor(URL.'/'.$uri_string.$temp_query, $i);
    						$links .= $this->pagination_delimiters[1];
							$this->array_pages[] = array('title'=>$i, 'page'=>$i);
    					}
                    }
                    $links .= $this->next_page;
					if(isset($next)) $this->array_pages[] = $next;
					if(isset($last)) $this->array_pages[] = $last;
                    $this->all_pages = $this->pagination_tags[0].$links.$this->pagination_tags[1];
                }
                else
                {
                    $this->all_pages =  $this->pagination_tags[0].$this->pagination_delimiters[0].$this->pagination_delimiters[1]. $this->pagination_tags[1];
                }
            }
        }
		$this->parse_data($parameter);
		$this->limit($rows_per_page, (($page_number-1)*$rows_per_page));
		$data = $this->get_all();
		if($data) return $data;
		else return false;
    }
    public function set_pagination_delimiters($delimiters)
    {
        if(is_array($delimiters) && sizeof($delimiters)==2) $this->pagination_delimiters = $delimiters;
        return $this;
    }
    public function set_pagination_arrows($arrows)
    {
        if(is_array($arrows) && sizeof($arrows)==2) $this->pagination_arrows = $arrows;
        return $this;
    }
    private function _fetch_table()
    {
        if(!isset($this->table)) $this->table = $this->_get_table_name(get_class($this));
        return TRUE;
    }
    private function _get_table_name($model_name)
    {
        $table_name = plural(preg_replace('/(_m|_model)?$/', '', strtolower($model_name)));
        return $table_name;
    }
    public function __call($method, $arguments)
    {
        if(substr($method,0,6) == 'where_')
        {
            $column = substr($method,6);
            $this->where($column, $arguments);
            return $this;
        }
        if(($method!='with_trashed') && (substr($method,0,5) == 'with_'))
        {
            $relation = substr($method,5);
            $this->with($relation,$arguments);
            return $this;
        }
        $parent_class = get_parent_class($this);
        if ($parent_class !== FALSE && !method_exists($parent_class, $method) && !method_exists($this,$method))
        {
            echo 'No method with that name ('.$method.') in MY_Model or CI_Model.';
        }
    }

    private function _build_sorter($data, $field, $order_by, $sort_by = 'DESC')
    {
        usort($data, function($a, $b) use ($field, $order_by, $sort_by) {
            return strtoupper($sort_by) ==  "DESC" ? ($a[$field][$order_by] < $b[$field][$order_by]) : ($a[$field][$order_by] > $b[$field][$order_by]);
        });

        return $data;
    }


    /**
     * Verifies if an array is associative or not
     * @param array $array
     * @return bool
     */
    private function _is_assoc(array $array) {
        return (bool)count(array_filter(array_keys($array), 'is_string'));
    }

    /*
    public function add_creator($data)
    {
    	$data['created_by'] = $_SESSION['user_id'];
    	return $data;
    }
    */

    /*
    public function add_updater($data)
    {
	    $data['updated_by'] = $_SESSION['user_id'];
	    return $data;
    }
    */
}
