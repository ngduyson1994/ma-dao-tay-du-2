<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
function loadData() {
    $CI = & get_instance();
    $data = array();
    $aid = intval($CI->session->userdata('aID'));
    if ($aid) {
        $CI->load->model('admin_model');
        $parameter['arg'] = $aid;
        $data['admin'] = $CI->admin_model->get($parameter);
    }
    return $data;
}
function permission($function = null) {
    if ($function) {
        $CI = & get_instance();
        $CI->load->model('permission_model');
        $CI->load->model('permission_group_model');
        if (!intval($CI->session->userdata('aID'))) {
            $uri = ($CI->uri->uri_string())?URL.'/'.$CI->uri->uri_string().'/':URL.'/';
            $CI->session->set_userdata(array('backend_ref'=>$uri));
            redirect(URL . '/manager/login');
        }
        $permission = $CI->permission_model->where_function($function)->get();
        if($permission && intval($CI->session->userdata('aID')) > 0 && intval($CI->session->userdata('aGroup')) > 0) {
            $pid = $permission['id'];
            $gid = intval($CI->session->userdata('aGroup'));
            $parameter['arg'] = array(array('pid', $pid), array('gid', $gid));
            $return = $CI->permission_group_model->get($parameter);
            if(isset($return['allow'])) return $return['allow'];
            else return false;
        }
    }
    return false;
}
function auth() {
    /*
    $valid_passwords = array("vec" => "41b3145deb94dd68f422c1fa6dfad5cd");
    $valid_users = array_keys($valid_passwords);
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) && (strlen($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) > 0)) {
            list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':', base64_decode(substr($_SERVER['REDIRECT_HTTP_AUTHORIZATION'], 6)));
            if (strlen($_SERVER['PHP_AUTH_USER']) == 0 || strlen($_SERVER['PHP_AUTH_PW']) == 0) {
                unset($_SERVER['PHP_AUTH_USER']);
                unset($_SERVER['PHP_AUTH_PW']);
            }
        }
    }
    $user = (isset($_SERVER['PHP_AUTH_USER'])) ? $_SERVER['PHP_AUTH_USER'] : '';
    $pass = (isset($_SERVER['PHP_AUTH_PW'])) ? md5($_SERVER['PHP_AUTH_PW']) : '';
    $validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);
    if (!$validated) {
        header('WWW-Authenticate: Basic realm="Authentication System"');
        header('HTTP/1.0 401 Unauthorized');
        echo "You must enter a valid login ID and password to access this resource<br />";
        die("Not authorized");
    }
    */
    $CI = get_instance();
    if(!intval($CI->session->userdata('aID'))) {
        $uri = ($CI->uri->uri_string())?URL.'/'.$CI->uri->uri_string().'/':URL.'/';
        $CI->session->set_userdata(array('backend_ref'=>$uri));
        redirect(URL.'/manager/login/');
    }
}