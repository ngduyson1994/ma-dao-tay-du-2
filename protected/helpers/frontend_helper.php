<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
function init_data() {
    $CI = get_instance();
    $data = array();
    //$uri = ($CI->uri->uri_string())?URL.'/'.$CI->uri->uri_string().'/':URL.'/';
    if(isset($_SERVER["HTTP_REFERER"]) && $_SERVER["HTTP_REFERER"]) $uri = $_SERVER["HTTP_REFERER"];
    else $uri = URL;
    $CI->session->set_userdata(array('ref' => $uri));
    $CI->load->library('auth');
    $CI->load->database();
    if($CI->auth->isLogged()) $data['user'] = $CI->auth->data;
    $data['settings'] = $CI->xcache->model('setting_model', 'getList');
    return $data;
}
function widget($name = '', $arg = null) {
    if(strpos($name, '/')) {
        $class = explode('/', $name);
        $class = end($class);
    }
    else $class = $name;
    $CI = & get_instance();
    $CI->load->widget($name);
    if (isset($CI->$class)) echo $CI->$class->run($arg);
}
function getTemplate() {
    return 'frontend';
    $CI = & get_instance();
    $CI->load->library('user_agent');
    $platform = $CI->input->get('platform');
    if ($platform == 'mobile' || $platform == 'desktop')
        $CI->session->set_userdata(array('platform' => $platform));
    $platform = $CI->session->userdata('platform');
    if ($CI->agent->is_mobile() || $platform == 'mobile')
        $template = 'mobile';
    else
        $template = 'frontend';
    return $template;
}