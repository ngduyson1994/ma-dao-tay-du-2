<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
function genString($count, $with_number=true) {
    $number = '0123456789';
    $possible = 'abcdfghjkmnpqrstvwxyzABCDFGHJKMNPQRSTVWXYZ';
    if($with_number == true) $possible .= $number;
    $code = '';
    $i = 0;
    while ($i < $count) {
        $code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
        $i++;
    }
    return $code;
}
function timeFormat($time) {
    $now = time();
    if (($now - $time) < 3500)
        echo 'vài phút trước';
    if (($now - $time) < 86400)
        echo round(($now - $time) / 3600) . ' giờ trước';
    elseif (($now - $time) < (86400 * 6))
        echo round(($now - $time) / 86400) . ' ngày trước';
    else
        echo date('d/m/Y', $time);
}
function getString($string, $start, $end) {
    $string = " " . $string;
    $ini = strpos($string, $start);
    if ($ini == 0)
        return "";
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}
function clearString($string, $quot = true) {
    $string = str_replace('\'', "'", $string);
    $string = str_replace("\'", "'", $string);
    $string = str_replace('\"', '"', $string);
    $string = str_replace('\r\n', "", $string);
    $string = str_replace('\n', "", $string);
    if ($quot) {
        $string = str_replace('"', "&quot;", $string);
        $string = str_replace("'", "&#039;", $string);
    }
    return $string;
}
function sendmail($to, $from, $reply, $name, $subject, $message, $type = 1) {
    if (function_exists('imap_open') && $type == 0) {
        $message = stripslashes($message);
        $subject = stripslashes($subject);
        $uid = strtoupper(md5(uniqid(time())));
        $to = trim($to);
        $header = "From: " . $name . " <" . $from . ">\r\nReply-To: " . $reply . "\r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: text/html\r\n";
        $header .= "Content-Transfer-Encoding: 8bit\r\n\r\n";
        $header .= "$message\r\n";
    } else {
        $smpt = array('host'=>'smtp.pepipost.com', 'username'=>'Y3liZXJ6', 'password'=>'Wm9uZUAxMjM=', 'port'=>'2525');
        $smpt = array('host'=>'smtp.sparkpostmail.com', 'username'=>'SMTP_Injection', 'password'=>'2992fd49afdabaacf90a2752f654cbeacd28ba58', 'port'=>'587');
        require_once APPPATH . '/third_party/mailer/PHPMailerAutoload.php';
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();
        $mail->SMTPSecure = "tls";
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = $smpt['host'];
        $mail->Port = $smpt['port'];
        $mail->SMTPAuth = true;
        $mail->Username = ($smpt['username']);
        $mail->Password = ($smpt['password']);
        $mail->setFrom($from, $name);
        $mail->addReplyTo($reply, $name);
        $mail->addAddress($to);
        $mail->Subject = $subject;
        $mail->msgHTML($message);
        return $mail->send();
    }
}
function cutOf($string, $len, $type = false) {
    if (strlen($string) > $len) {
        if ($type == false) {
            $t1 = substr($string, 0, $len);
            $t2 = substr($string, $len, strlen($string));
            $t2 = str_replace(strstr($t2, " "), "...", $t2);
            return $t1 . $t2;
        } else {
            $t1 = substr($string, 0, $len);
            return $t1 . "...";
        }
    }
    else
        return $string;
}
function getIP() {
    if(isset($_SERVER['HTTP_CF_CONNECTING_IP'])) $ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
    elseif(isset($_SERVER['HTTP_INCAP_CLIENT_IP'])) $ip = $_SERVER['HTTP_INCAP_CLIENT_IP'];
    elseif(isset($_SERVER['HTTP_CLIENT_IP'])) $ip = $_SERVER['HTTP_CLIENT_IP'];
    elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else $ip = $_SERVER['REMOTE_ADDR'];
    return $ip;
}
function getInfoIP($ip) {
    require_once APPPATH . '/third_party/GeoIP/MaxMind/Db/Reader.php';
    try {
        $reader = new Reader(APPPATH . '/third_party/GeoIP/GeoLite2.mmdb');
        $records = $reader->get($ip);
        $reader->close();
    } catch (Exception $e) {
        return array('ip'=>$ip,'country_code'=>'XX','country'=>'XX');
    }
    if($records) {
        $info['ip'] = $ip;
        $info['country_code'] = $records['country']['iso_code'];
        $info['country'] = $records['country']['names']['en'];
        return $info;
    }
    return false;
}
function getCountryName($code) {
    $countries = array('AF' => 'Afghanistan','AL' => 'Albania','DZ' => 'Algeria','AS' => 'American Samoa','AD' => 'Andorra','AO' => 'Angola','AI' => 'Anguilla','AQ' => 'Antarctica','AG' => 'Antigua and Barbuda','AR' => 'Argentina','AM' => 'Armenia','AW' => 'Aruba','AU' => 'Australia','AT' => 'Austria','AZ' => 'Azerbaijan','BS' => 'Bahamas','BH' => 'Bahrain','BD' => 'Bangladesh','BB' => 'Barbados','BY' => 'Belarus','BE' => 'Belgium','BZ' => 'Belize','BJ' => 'Benin','BM' => 'Bermuda','BT' => 'Bhutan','BO' => 'Bolivia','BQ' => 'Bonaire','BA' => 'Bosnia and Herzegovina','BW' => 'Botswana','BV' => 'Bouvet Island','BR' => 'Brazil','IO' => 'British Indian Ocean Territory','BN' => 'Brunei Darussalam','BG' => 'Bulgaria','BF' => 'Burkina Faso','BI' => 'Burundi','KH' => 'Cambodia','CM' => 'Cameroon','CA' => 'Canada','CV' => 'Cape Verde','KY' => 'Cayman Islands','CF' => 'Central African Republic','TD' => 'Chad','CL' => 'Chile','CN' => 'China','CX' => 'Christmas Island','CC' => 'Cocos (Keeling) Islands','CO' => 'Colombia','KM' => 'Comoros','CG' => 'Congo','CD' => 'Congo','CK' => 'Cook Islands','CR' => 'Costa Rica','HR' => 'Croatia','CU' => 'Cuba','CW' => 'CuraÃ§ao','CY' => 'Cyprus','CZ' => 'Czech Republic','CI' => 'CÃ´te d\'Ivoire','DK' => 'Denmark','DJ' => 'Djibouti','DM' => 'Dominica','DO' => 'Dominican Republic','EC' => 'Ecuador','EG' => 'Egypt','SV' => 'El Salvador','GQ' => 'Equatorial Guinea','ER' => 'Eritrea','EE' => 'Estonia','ET' => 'Ethiopia','FK' => 'Falkland Islands (Malvinas)','FO' => 'Faroe Islands','FJ' => 'Fiji','FI' => 'Finland','FR' => 'France','GF' => 'French Guiana','PF' => 'French Polynesia','TF' => 'French Southern Territories','GA' => 'Gabon','GM' => 'Gambia','GE' => 'Georgia','DE' => 'Germany','GH' => 'Ghana','GI' => 'Gibraltar','GR' => 'Greece','GL' => 'Greenland','GD' => 'Grenada','GP' => 'Guadeloupe','GU' => 'Guam','GT' => 'Guatemala','GG' => 'Guernsey','GN' => 'Guinea','GW' => 'Guinea-Bissau','GY' => 'Guyana','HT' => 'Haiti','HM' => 'Heard Island and McDonald Mcdonald Islands','VA' => 'Holy See (Vatican City State)','HN' => 'Honduras','HK' => 'Hong Kong','HU' => 'Hungary','IS' => 'Iceland','IN' => 'India','ID' => 'Indonesia','IR' => 'Iran','IQ' => 'Iraq','IE' => 'Ireland','IM' => 'Isle of Man','IL' => 'Israel','IT' => 'Italy','JM' => 'Jamaica','JP' => 'Japan','JE' => 'Jersey','JO' => 'Jordan','KZ' => 'Kazakhstan','KE' => 'Kenya','KI' => 'Kiribati','KP' => 'North Korea','KR' => 'Korea','KW' => 'Kuwait','KG' => 'Kyrgyzstan','LA' => 'Laos','LV' => 'Latvia','LB' => 'Lebanon','LS' => 'Lesotho','LR' => 'Liberia','LY' => 'Libya','LI' => 'Liechtenstein','LT' => 'Lithuania','LU' => 'Luxembourg','MO' => 'Macao','MK' => 'Macedonia, the Former Yugoslav Republic of','MG' => 'Madagascar','MW' => 'Malawi','MY' => 'Malaysia','MV' => 'Maldives','ML' => 'Mali','MT' => 'Malta','MH' => 'Marshall Islands','MQ' => 'Martinique','MR' => 'Mauritania','MU' => 'Mauritius','YT' => 'Mayotte','MX' => 'Mexico','FM' => 'Micronesia, Federated States of','MD' => 'Moldova, Republic of','MC' => 'Monaco','MN' => 'Mongolia','ME' => 'Montenegro','MS' => 'Montserrat','MA' => 'Morocco','MZ' => 'Mozambique','MM' => 'Myanmar','NA' => 'Namibia','NR' => 'Nauru','NP' => 'Nepal','NL' => 'Netherlands','NC' => 'New Caledonia','NZ' => 'New Zealand','NI' => 'Nicaragua','NE' => 'Niger','NG' => 'Nigeria','NU' => 'Niue','NF' => 'Norfolk Island','MP' => 'Northern Mariana Islands','NO' => 'Norway','OM' => 'Oman','PK' => 'Pakistan','PW' => 'Palau','PS' => 'Palestine, State of','PA' => 'Panama','PG' => 'Papua New Guinea','PY' => 'Paraguay','PE' => 'Peru','PH' => 'Philippines','PN' => 'Pitcairn','PL' => 'Poland','PT' => 'Portugal','PR' => 'Puerto Rico','QA' => 'Qatar','RO' => 'Romania','RU' => 'Russian Federation','RW' => 'Rwanda','RE' => 'Reunion','BL' => 'Saint Barthalemy','SH' => 'Saint Helena','KN' => 'Saint Kitts and Nevis','LC' => 'Saint Lucia','MF' => 'Saint Martin (French part)','PM' => 'Saint Pierre and Miquelon','VC' => 'Saint Vincent and the Grenadines','WS' => 'Samoa','SM' => 'San Marino','ST' => 'Sao Tome and Principe','SA' => 'Saudi Arabia','SN' => 'Senegal','RS' => 'Serbia','SC' => 'Seychelles','SL' => 'Sierra Leone','SG' => 'Singapore','SX' => 'Sint Maarten (Dutch part)','SK' => 'Slovakia','SI' => 'Slovenia','SB' => 'Solomon Islands','SO' => 'Somalia','ZA' => 'South Africa','GS' => 'South Georgia and the South Sandwich Islands','SS' => 'South Sudan','ES' => 'Spain','LK' => 'Sri Lanka','SD' => 'Sudan','SR' => 'Suriname','SJ' => 'Svalbard and Jan Mayen','SZ' => 'Swaziland','SE' => 'Sweden','CH' => 'Switzerland','SY' => 'Syrian','TW' => 'Taiwan','TJ' => 'Tajikistan','TZ' => 'United Republic of Tanzania','TH' => 'Thailand','TL' => 'Timor-Leste','TG' => 'Togo','TK' => 'Tokelau','TO' => 'Tonga','TT' => 'Trinidad and Tobago','TN' => 'Tunisia','TR' => 'Turkey','TM' => 'Turkmenistan','TC' => 'Turks and Caicos Islands','TV' => 'Tuvalu','UG' => 'Uganda','UA' => 'Ukraine','AE' => 'United Arab Emirates','GB' => 'United Kingdom','US' => 'United States','UM' => 'United States Minor Outlying Islands','UY' => 'Uruguay','UZ' => 'Uzbekistan','VU' => 'Vanuatu','VE' => 'Venezuela','VN' => 'Viet Nam','VG' => 'British Virgin Islands','VI' => 'US Virgin Islands','WF' => 'Wallis and Futuna','EH' => 'Western Sahara','YE' => 'Yemen','ZM' => 'Zambia','ZW' => 'Zimbabwe','AX' => 'Aland Islands', 'XX' => 'Unknow');
    if(isset($countries[$code])) return $countries[$code];
    else return $code;
}
function getCountryCode($country) {
    $countries = array('Afghanistan' => 'AF','Albania' => 'AL','Algeria' => 'DZ','American Samoa' => 'AS','Andorra' => 'AD','Angola' => 'AO','Anguilla' => 'AI','Antarctica' => 'AQ','Antigua and Barbuda' => 'AG','Argentina' => 'AR','Armenia' => 'AM','Aruba' => 'AW','Australia' => 'AU','Austria' => 'AT','Azerbaijan' => 'AZ','Bahamas' => 'BS','Bahrain' => 'BH','Bangladesh' => 'BD','Barbados' => 'BB','Belarus' => 'BY','Belgium' => 'BE','Belize' => 'BZ','Benin' => 'BJ','Bermuda' => 'BM','Bhutan' => 'BT','Bolivia' => 'BO','Bonaire' => 'BQ','Bosnia and Herzegovina' => 'BA','Botswana' => 'BW','Bouvet Island' => 'BV','Brazil' => 'BR','British Indian Ocean Territory' => 'IO','Brunei' => 'BN','Bulgaria' => 'BG','Burkina Faso' => 'BF','Burundi' => 'BI','Cambodia' => 'KH','Cameroon' => 'CM','Canada' => 'CA','Cape Verde' => 'CV','Cayman Islands' => 'KY','Central African Republic' => 'CF','Chad' => 'TD','Chile' => 'CL','China' => 'CN','Christmas Island' => 'CX','Cocos (Keeling) Islands' => 'CC','Colombia' => 'CO','Comoros' => 'KM','Congo' => 'CG','Congo' => 'CD','Cook Islands' => 'CK','Costa Rica' => 'CR','Croatia' => 'HR','Cuba' => 'CU','CuraÃƒÂ§ao' => 'CW','Cyprus' => 'CY','Czech Republic' => 'CZ','CÃƒÂ´te d\'Ivoire' => 'CI','Denmark' => 'DK','Djibouti' => 'DJ','Dominica' => 'DM','Dominican Republic' => 'DO','Ecuador' => 'EC','Egypt' => 'EG','El Salvador' => 'SV','Equatorial Guinea' => 'GQ','Eritrea' => 'ER','Estonia' => 'EE','Ethiopia' => 'ET','Falkland Islands (Malvinas)' => 'FK','Faroe Islands' => 'FO','Fiji' => 'FJ','Finland' => 'FI','France' => 'FR','French Guiana' => 'GF','French Polynesia' => 'PF','French Southern Territories' => 'TF','Gabon' => 'GA','Gambia' => 'GM','Georgia' => 'GE','Germany' => 'DE','Ghana' => 'GH','Gibraltar' => 'GI','Greece' => 'GR','Greenland' => 'GL','Grenada' => 'GD','Guadeloupe' => 'GP','Guam' => 'GU','Guatemala' => 'GT','Guernsey' => 'GG','Guinea' => 'GN','Guinea-Bissau' => 'GW','Guyana' => 'GY','Haiti' => 'HT','Heard Island and McDonald Mcdonald Islands' => 'HM','Holy See (Vatican City State)' => 'VA','Honduras' => 'HN','Hong Kong' => 'HK','Hungary' => 'HU','Iceland' => 'IS','India' => 'IN','Indonesia' => 'ID','Iran' => 'IR','Iraq' => 'IQ','Ireland' => 'IE','Isle of Man' => 'IM','Israel' => 'IL','Italy' => 'IT','Jamaica' => 'JM','Japan' => 'JP','Jersey' => 'JE','Jordan' => 'JO','Kazakhstan' => 'KZ','Kenya' => 'KE','Kiribati' => 'KI','North Korea' => 'KR','South Korea' => 'KR','Kuwait' => 'KW','Kyrgyzstan' => 'KG','Laos' => 'LA','Latvia' => 'LV','Lebanon' => 'LB','Lesotho' => 'LS','Liberia' => 'LR','Libya' => 'LY','Liechtenstein' => 'LI','Lithuania' => 'LT','Luxembourg' => 'LU','Macao' => 'MO','Macedonia, the Former Yugoslav Republic of' => 'MK','Madagascar' => 'MG','Malawi' => 'MW','Malaysia' => 'MY','Maldives' => 'MV','Mali' => 'ML','Malta' => 'MT','Marshall Islands' => 'MH','Martinique' => 'MQ','Mauritania' => 'MR','Mauritius' => 'MU','Mayotte' => 'YT','Mexico' => 'MX','Micronesia, Federated States of' => 'FM','Moldova, Republic of' => 'MD','Monaco' => 'MC','Mongolia' => 'MN','Montenegro' => 'ME','Montserrat' => 'MS','Morocco' => 'MA','Mozambique' => 'MZ','Myanmar' => 'MM','Namibia' => 'NA','Nauru' => 'NR','Nepal' => 'NP','Netherlands' => 'NL','New Caledonia' => 'NC','New Zealand' => 'NZ','Nicaragua' => 'NI','Niger' => 'NE','Nigeria' => 'NG','Niue' => 'NU','Norfolk Island' => 'NF','Northern Mariana Islands' => 'MP','Norway' => 'NO','Oman' => 'OM','Pakistan' => 'PK','Palau' => 'PW','Palestine, State of' => 'PS','Panama' => 'PA','Papua New Guinea' => 'PG','Paraguay' => 'PY','Peru' => 'PE','Philippines' => 'PH','Pitcairn' => 'PN','Poland' => 'PL','Portugal' => 'PT','Puerto Rico' => 'PR','Qatar' => 'QA','Romania' => 'RO','Russia' => 'RU','Rwanda' => 'RW','Reunion' => 'RE','Saint Barthalemy' => 'BL','Saint Helena' => 'SH','Saint Kitts and Nevis' => 'KN','Saint Lucia' => 'LC','Saint Martin (French part)' => 'MF','Saint Pierre and Miquelon' => 'PM','Saint Vincent and the Grenadines' => 'VC','Samoa' => 'WS','San Marino' => 'SM','Sao Tome and Principe' => 'ST','Saudi Arabia' => 'SA','Senegal' => 'SN','Serbia' => 'RS','Seychelles' => 'SC','Sierra Leone' => 'SL','Singapore' => 'SG','Sint Maarten (Dutch part)' => 'SX','Slovakia' => 'SK','Slovenia' => 'SI','Solomon Islands' => 'SB','Somalia' => 'SO','South Africa' => 'ZA','South Georgia and the South Sandwich Islands' => 'GS','South Sudan' => 'SS','Spain' => 'ES','Sri Lanka' => 'LK','Sudan' => 'SD','Suriname' => 'SR','Svalbard and Jan Mayen' => 'SJ','Swaziland' => 'SZ','Sweden' => 'SE','Switzerland' => 'CH','Syrian' => 'SY','Taiwan' => 'TW','Tajikistan' => 'TJ','United Republic of Tanzania' => 'TZ','Thailand' => 'TH','Timor-Leste' => 'TL','Togo' => 'TG','Tokelau' => 'TK','Tonga' => 'TO','Trinidad and Tobago' => 'TT','Tunisia' => 'TN','Turkey' => 'TR','Turkmenistan' => 'TM','Turks and Caicos Islands' => 'TC','Tuvalu' => 'TV','Uganda' => 'UG','Ukraine' => 'UA','United Arab Emirates' => 'AE','United Kingdom' => 'GB','United States' => 'US','United States Minor Outlying Islands' => 'UM','Uruguay' => 'UY','Uzbekistan' => 'UZ','Vanuatu' => 'VU','Venezuela' => 'VE','Viet Nam' => 'VN', 'Vietnam' => 'VN','British Virgin Islands' => 'VG','US Virgin Islands' => 'VI','Wallis and Futuna' => 'WF','Western Sahara' => 'EH','Yemen' => 'YE','Zambia' => 'ZM','Zimbabwe' => 'ZW','Aland Islands' => 'AX');
    if(isset($countries[$country])) return $countries[$country];
    else return 'XX';
}
function getOS() {
    $user_agent = $user_agent = $_SERVER['HTTP_USER_AGENT'];;
    $os_platform    =   "unknow";
    $os_array       =   array(
        '/windows nt 10/i'     =>  'Windows 10',
        '/windows nt 6.3/i'     =>  'Windows 8.1',
        '/windows nt 6.2/i'     =>  'Windows 8',
        '/windows nt 6.1/i'     =>  'Windows 7',
        '/windows nt 6.0/i'     =>  'Windows Vista',
        '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
        '/windows nt 5.1/i'     =>  'Windows XP',
        '/windows xp/i'         =>  'Windows XP',
        '/windows nt 5.0/i'     =>  'Windows 2000',
        '/windows me/i'         =>  'Windows ME',
        '/win98/i'              =>  'Windows 98',
        '/win95/i'              =>  'Windows 95',
        '/win16/i'              =>  'Windows 3.11',
        '/macintosh|mac os x/i' =>  'Mac OS X',
        '/mac_powerpc/i'        =>  'Mac OS 9',
        '/linux/i'              =>  'Linux',
        '/ubuntu/i'             =>  'Ubuntu',
        '/iphone/i'             =>  'iPhone',
        '/ipod/i'               =>  'iPod',
        '/ipad/i'               =>  'iPad',
        '/android/i'            =>  'Android',
        '/blackberry/i'         =>  'BlackBerry',
        '/webos/i'              =>  'Mobile'
    );
    foreach ($os_array as $regex => $value) {
        if (preg_match($regex, $user_agent)) {
            $os_platform    =   $value;
        }
    }
    return $os_platform;
}
function getOSType() {
    $user_agent = $user_agent = $_SERVER['HTTP_USER_AGENT'];;
    $os_platform = "unknow";
    $os_array = array('/windows nt 10/i'     =>  'windows', '/windows nt 6.3/i'     =>  'windows', '/windows nt 6.2/i'     =>  'windows', '/windows nt 6.1/i'     =>  'windows', '/windows nt 6.0/i'     =>  'windows', '/windows nt 5.2/i'     =>  'windows', '/windows nt 5.1/i'     =>  'windows', '/windows xp/i'         =>  'windows', '/windows nt 5.0/i'     =>  'windows', '/windows me/i'         =>  'windows', '/win98/i'              =>  'windows', '/win95/i'              =>  'windows', '/win16/i'              =>  'windows', '/macintosh|mac os x/i' =>  'macos', '/mac_powerpc/i'        =>  'macos', '/linux/i'              =>  'linux', '/ubuntu/i'             =>  'linux', '/iphone/i'             =>  'mobile', '/ipod/i'               =>  'mobile', '/ipad/i'               =>  'mobile', '/android/i'            =>  'mobile', '/blackberry/i'         =>  'mobile','/webos/i'              =>  'mobile');
    foreach ($os_array as $regex => $value) {
        if (preg_match($regex, $user_agent)) {
            $os_platform = $value;
        }
    }
    return $os_platform;
}
function getBrowser() {
    $user_agent = $user_agent = $_SERVER['HTTP_USER_AGENT'];;
    $browser        =   "unknow";
    $browser_array  =   array(
        '/msie/i'       =>  'Internet Explorer',
        '/firefox/i'    =>  'Firefox',
        '/safari/i'     =>  'Safari',
        '/chrome/i'     =>  'Chrome',
        '/opera/i'      =>  'Opera',
        '/netscape/i'   =>  'Netscape',
        '/maxthon/i'    =>  'Maxthon',
        '/konqueror/i'  =>  'Konqueror',
        '/mobile/i'     =>  'Handheld Browser'
    );
    foreach ($browser_array as $regex => $value) {
        if (preg_match($regex, $user_agent)) {
            $browser    =   $value;
        }
    }
    return $browser;
}
function clearUTF($string, $link = true, $lower = true) {
   $chars = array('a' => array('ấ', 'ầ', 'ẩ', 'ẫ', 'ậ', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ', 'á', 'à', 'ả', 'ã', 'ạ', 'â', 'ă'),'A' => array('Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ', 'Á', 'À', 'Ả', 'Ã', 'Ạ', 'Â', 'Ă'),'e' => array('ế', 'ề', 'ể', 'ễ', 'ệ', 'é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê'),'E' => array('Ế', 'Ề', 'Ể', 'Ễ', 'Ệ', 'É', 'È', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê'),'i' => array('í', 'ì', 'ỉ', 'ĩ', 'ị'),'I' => array('Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị'),'o' => array('ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ', 'ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ơ'),'O' => array('Ố', 'Ồ', 'Ổ', 'Ô', 'Ộ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ', 'Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ơ'),'u' => array('ứ', 'ừ', 'ử', 'ữ', 'ự', 'ú', 'ù', 'ủ', 'ũ', 'ụ', 'ư'),'U' => array('Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự', 'Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ', 'Ư'),'y' => array('ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ'),'Y' => array('Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ'),'d' => array('đ'),'D' => array('Đ'));
   foreach ($chars as $key => $arr)
       foreach ($arr as $val)
           $string = str_replace($val, $key, $string);
   $string = preg_replace('[^a-zA-Z0-9_-]', '', $string);
   $string = str_replace(',', '', $string);
   if($link) $string = str_replace(' ', '-', $string);
   if($lower) $string = strtolower($string);
   if($link)
       while (strstr($string, '--'))
           $string = str_replace('--', '-', $string);
            $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
   return $string;
}
function ping() {
    $google = 'http://www.google.com/webmasters/tools/ping?sitemap=' . URL . '/sitemap_index.xml';
    $bing = 'http://www.bing.com/webmaster/ping.aspx?sitemap=' . URL . '/sitemap_index.xml';
    //$yandex = 'http://blogs.yandex.ru/pings/?status=success&url='.URL.'/sitemap_index.xml';
    //$ask = 'http://submissions.ask.com/ping?sitemap='.URL.'/sitemap_index.xml';
    //$yahoo = 'http://search.yahooapis.com/SiteExplorerService/V1/updateNotification?appid=3usdTDLV34HbjQpIBuzMM1UkECFl5KDN7fogidABihmHBfqaebDuZk1vpLDR64I-&url='.URL.'/sitemap_index.xml';
    curl($google);
    curl($bing);
}
function curl($url, $post = false, $cookie = false, $header = false, $socks5 = false, $auth = false, $head = 0) {
    $curl = ($url);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    if ($post != "") {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }
    if (is_array($header))
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    if ($auth != "")
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
    if ($socks5 != "") {
        curl_setopt($ch, CURLOPT_PROXY, $socks5);
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
    }
    curl_setopt($ch, CURLOPT_TIMEOUT_MS, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
    if ($head) curl_setopt($ch, CURLOPT_HEADER, 1);
    //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 3);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
function ads_content( $insertion, $paragraph_id, $content ) {
    $closing_p = '</p>';
    $paragraphs = explode( $closing_p, $content );
    foreach ($paragraphs as $index => $paragraph) {
        if ( trim( $paragraph ) ) {
            $paragraphs[$index] .= $closing_p;
        }
 
        if ( $paragraph_id == $index + 1 ) {
            $paragraphs[$index] .= $insertion;
        }
    }
    return implode( '', $paragraphs );
}