<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
function getPostURL($post) {
    if ($post['type'] == 'video') $type = 'video';
    else $type = 'news';

    return URL . '/' . $type . '/' . $post['slug'] . '/';
}
function getCatURL($slug) {
    $url = URL . '/' . $slug . '/';
    return $url;
}
function get_http_response_code($url) {
    $headers = get_headers($url);
    return substr($headers[0], 9, 3);
}
function getThumb($post, $width=null, $height=null) {
    if (isset($post['id'])) {
        $thumb = $post['thumb'];
        if (!strpos($thumb, 'ttp')) {
            if(isset($post['content'])) {
                $youtube = getYoutubeID(clearString($post['content']));
                if($youtube) {
                    $thumb = 'http://img.youtube.com/vi/' . $youtube . '/maxresdefault.jpg';
                    if(get_http_response_code($thumb) == 404) $thumb = 'http://img.youtube.com/vi/' . $youtube . '/mqdefault.jpg';
                }
                elseif(strpos($post['content'], 'streamable.com')) {
                    $streamable = getStreamableID($post['content']);
                    $json = @file_get_contents('https://api.streamable.com/videos/'.$streamable);
                    if($json) $json = json_decode($json);
                    if(isset($json->thumbnail_url)) $thumb = 'http:'.$json->thumbnail_url;
                }
            }
        }
        if (strpos($thumb, 'youtube.com')) {
            $thumb = str_replace(array('mqdefault.jpg', '0.jpg'),'maxresdefault.jpg', $thumb);
        }
        if (!strpos($thumb, 'ttp')) {
            if(isset($post['content'])) {
                $img = getString(clearString($post['content']), '<img', '>');
                $img = str_replace(array('"', "'", "\\", '&quot;'), "", $img);
                $thumb = getString($img, 'src=', ' ');
            }
        }
        if($width && $height && (strpos($thumb, 'blogspot.com') || strpos($thumb, 'cdn8.net'))) $thumb = str_replace('/s0/', '/w'.$width.'-h'.$height.'-c/', $thumb);
        if ($thumb) return $thumb;
        else return CDN . '/images/no-thumbnail.jpg';
    } return false;
}
function getThumbPlayer($post, $width=null, $height=null) {
    if (isset($post['id'])) {
        $thumb = $post['thumb'];
        if (!strpos($thumb, 'ttp')) {
            if(isset($post['content'])) {
                $youtube = getYoutubeID(clearString($post['content']));
                if($youtube) {
                    $thumb = 'http://img.youtube.com/vi/' . $youtube . '/maxresdefault.jpg';
                    if(get_http_response_code($thumb) == 404) $thumb = 'http://img.youtube.com/vi/' . $youtube . '/mqdefault.jpg';
                }
                elseif(strpos($post['content'], 'streamable.com')) {
                    $streamable = getStreamableID($post['content']);
                    $json = @file_get_contents('https://api.streamable.com/videos/'.$streamable);
                    if($json) $json = json_decode($json);
                    if(isset($json->thumbnail_url)) $thumb = 'http:'.$json->thumbnail_url;
                }
            }
        }
        if (strpos($thumb, 'youtube.com')) {
            $thumb = str_replace(array('mqdefault.jpg', '0.jpg'),'maxresdefault.jpg', $thumb);
        }
        if (!strpos($thumb, 'ttp')) {
            if(isset($post['content'])) {
                $img = getString(clearString($post['content']), '<img', '>');
                $img = str_replace(array('"', "'", "\\", '&quot;'), "", $img);
                $thumb = getString($img, 'src=', ' ');
            }
        }
        if($width && $height && (strpos($thumb, 'blogspot.com') || strpos($thumb, 'cdn8.net'))) $thumb = str_replace('/s0/', '/w'.$width.'-h'.$height.'-c/', $thumb);
        if ($thumb) return $thumb;
        else return CDN . '/core/images/no-avatar-player.jpg';
    } return false;
}
function getThumbs($post, $width=null, $height=null) {
    if (isset($post['id'])) {
        $thumb = $post['thumb'];
        if (!strpos($thumb, 'ttp')) {
            $youtube = getYoutubeID(clearString($post['content']));
            if ($youtube) $thumb = 'http://img.youtube.com/vi/' . $youtube . '/0.jpg';
        }
        if (!strpos($thumb, 'ttp')) {
            $kenh1 = getKenh1ID(clearString($post['content']));
            $res = @file_get_contents('http://kenh1.vn/watch/'.$kenh1.'/');
            $thumb = getString($res, 'Logo GTV.png" content="', '"');
        }
        if (!strpos($thumb, 'ttp')) {
            $img = getString(clearString($post['content']), '<img', '>');
            $img = str_replace(array('"', "'", "\\", '&quot;'), "", $img);
            $thumb = getString($img, 'src=', ' ');
        }
        if($width && $height && strpos($thumb, 'cdn8.net')) $thumb = str_replace('/s0/', '/w'.$width.'-h'.$height.'-c/', $thumb);
        if ($thumb) return $thumb;
        else
            return CDN . '/images/no-thumbnail.png';
    }
    return false;
}
function resizeThumb($img, $width=null, $height=null) {
    return str_replace('/s0/', '/w'.$width.'-h'.$height.'-c/', $img);
}
function showImage($link, $width, $height) {
    if (strpos($link, 'cdn8.net') || strpos($link, 'blogspot.com')) {
        return str_replace('/s0/', '/w' . $width . '-h' . $height . '-c/', $link);
    }
    else return $link;
}
function numberFormat($number) {
    $len = strlen($number);
    $num = 0;
    $ext = '';
    if ($len > 9) {
        $num = substr($number, 0, $len - 9);
        $ext = 'B';
    } elseif ($len > 6) {
        $num = substr($number, 0, $len - 6);
        $ext = 'M';
    } elseif ($len > 3) {
        $num = substr($number, 0, $len - 3);
        $ext = 'K';
    }
    else
        $num = $number;
    return $num . $ext;
}
function showDate($time) {
    $day = '\\n\g\à\y';
    $string = date("H:i - D, $day d/m/Y", $time);
    $arr = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
    $arr2 = array('Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'Chủ Nhật');
    $string = str_replace($arr, $arr2, $string);
    return $string;
}
function showTime($time) {
    if (time() - $time < 3)
        $string = 'vài giây trước';
    elseif (time() - $time < 60)
        $string = round(time() - $time).' giây trước';
    elseif (time() - $time < 3600)
        $string = round((time() - $time) / 60) . ' phút trước';
    elseif (time() - $time < 86400)
        $string = round((time() - $time) / 3600) . ' giờ trước';
    elseif (time() - $time < 172800)
        $string = date("G:i", $time) . ' hôm qua';
    else {
        $string = date("G:i d/m/Y", $time);
    }
    return $string;
}
function getYoutubeID($markup) {
    $markup = str_replace(" ", "\n", $markup);
    preg_match('#<object[^>]+>.+?https?://www\.youtube(?:\-nocookie)?\.com/[ve]/([A-Za-z0-9\-_]+).+?</object>#s', $markup, $matches);
    if (!isset($matches[1]))
        preg_match('#https?://www\.youtube(?:\-nocookie)?\.com/[ve]/([A-Za-z0-9\-_]+)#', $markup, $matches);
    if (!isset($matches[1]))
        preg_match('#https?://www\.youtube(?:\-nocookie)?\.com/embed/([A-Za-z0-9\-_]+)#', $markup, $matches);
    if (!isset($matches[1]))
        preg_match('#(?:https?(?:a|vh?)?://)?(?:www\.)?youtube(?:\-nocookie)?\.com/watch\?.*v=([A-Za-z0-9\-_]+)#', $markup, $matches);
    if (!isset($matches[1]))
        preg_match('#(?:https?(?:a|vh?)?://)?youtu\.be/([A-Za-z0-9\-_]+)#', $markup, $matches);
    if (!isset($matches[1]))
        preg_match('#youtube(?:\-nocookie)?\.com/[ve]/([A-Za-z0-9\-_]+)#', $markup, $matches);
    if (!isset($matches[1]))
        preg_match('#youtube(?:\-nocookie)?\.com/embed/([A-Za-z0-9\-_]+)#', $markup, $matches);
    if (!isset($matches[1]) && function_exists('lyte_parse'))
        preg_match('#<div class="lyte" id="([A-Za-z0-9\-_]+)"#', $markup, $matches);
    if (isset($matches[1]))
        return trim($matches[1]);
    else
        return false;
}
function getStreamableID($markup) {
    $markup = strip_tags($markup);
    $markup = str_replace(" ", "\n", $markup);
    preg_match('#https://streamable.com/(.*)#s', $markup, $matches);
    if (isset($matches[1])) return trim($matches[1]);
    else return false;
}
function getCountCommentFacebook($url) {
    $sRequestUri = 'http://api.facebook.com/restserver.php?method=links.getStats&urls=' . $url;
    $sFileSource = @file_get_contents($sRequestUri);
    $sContentFile = htmlentities($sFileSource);
    $sOpenTag = '<commentsbox_count>';
    $sEndTag = '</commentsbox_count>';
    $posOpenTag = strpos($sContentFile, htmlentities($sOpenTag));
    $posEndTag = strpos($sContentFile, htmlentities($sEndTag));
    $iFullCount = strlen($sContentFile);
    $iStart = $iFullCount - $posEndTag;
    $iCount = intval(substr($sContentFile, $posOpenTag + 25, -$iStart));
    return $iCount;
}
function getCountLikeFacebook($url) {
    $sRequestUri = 'http://api.facebook.com/restserver.php?method=links.getStats&urls=' . $url;
    $sFileSource = @file_get_contents($sRequestUri);
    $sContentFile = htmlentities($sFileSource);
    $sOpenTag = '<like_count>';
    $sEndTag = '</like_count>';
    $posOpenTag = strpos($sContentFile, htmlentities($sOpenTag));
    $posEndTag = strpos($sContentFile, htmlentities($sEndTag));
    $iFullCount = strlen($sContentFile);
    $iStart = $iFullCount - $posEndTag;
    $iCount = intval(substr($sContentFile, $posOpenTag + 18, -$iStart));
    return $iCount;
}