<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function upload($path, $field, $filename = null, $config = array()) {
    $CI = get_instance();
    $temp = realpath("./") . '/uploads/' . $path;
    $config['upload_path'] = $temp;
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '1024';
    $config['file_name'] = ($filename) ? $filename : md5(time() . rand(1000, 9999));
    $CI->load->library('upload', $config);
    if ($CI->upload->do_upload($field)) {
        $upload = $CI->upload->data();
        $CI->load->helper('images');
        $path = URL . '/uploads/' . $path . '/' . $upload['file_name'];
        return array('status' => 'success', 'msg' => '', 'thumb' => $path);
    } else {
        $error = $CI->upload->display_errors();
        return array('status' => 'error', 'msg' => $error);
    }
}

function cdn8($img_link, $filename) {
    $url = 'https://cdn8.net/api/upload';
    $data = array(
        'name' => $filename,
        'api_token' => '5B7D7FFF100509D3334D54AA1160333A',
        'data' => $img_link
    );
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 3);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}

function upload_cdn($path = "tmp", $field) {
    $CI = get_instance();
    $temp = realpath("./") . '/uploads/' . $path;
    $config['upload_path'] = $temp;
    $config['allowed_types'] = 'gif|jpg|png';
    $CI->load->library('upload', $config);
    if ($CI->upload->do_upload($field)) {
        $upload = $CI->upload->data();
        $CI->load->helper('images');
        $url = URL . '/uploads/' . $path . '/' . $upload['file_name'];
        $filePath = realpath("./") . '/uploads/' . $path . '/' . $upload['file_name'];
        if (!is_image($filePath))
            return array('status' => 'error', 'msg' => 'Image is invalid format');
        if ($filePath) {
            $type = pathinfo($filePath, PATHINFO_EXTENSION);
            $data = file_get_contents($filePath);
            $base64 = base64_encode($data);
            $result = json_decode(cdn8($base64, $upload['file_name']));
            if (file_exists($filePath))
                @unlink($filePath);
            return array('status' => 'success', 'msg' => '', 'thumb' => $result->url);
        }
    } else {
        $error = $CI->upload->display_errors();
        return array('status' => 'error', 'msg' => $error);
    }
}

function upload_picasa($path = "tmp", $field) {
    $CI = get_instance();
    $temp = realpath("./") . '/uploads/' . $path;
    $config['upload_path'] = $temp;
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '2000';
    $config['max_width'] = '1024';
    //$config['max_height']  = '768';
    $CI->load->library('upload', $config);
    if ($CI->upload->do_upload($field)) {
        $upload = $CI->upload->data();
        $CI->load->helper('images');
        $url = URL . '/uploads/' . $path . '/' . $upload['file_name'];
        $filePath = realpath("./") . '/uploads/' . $path . '/' . $upload['file_name'];
        /* Upload to Picasa */
        $sitename = 'CyberService_';
        $dir = APPPATH . 'third_party/ImageService';
        $ext = pathinfo(basename($upload['file_name']), PATHINFO_EXTENSION);
        if (strpos($ext, '?')) {
            $ext = explode('?', $ext);
            $ext = $ext[0];
        }
        if (!is_image($filePath))
            return array('status' => 'error', 'msg' => 'Image is invalid format');
        require_once($dir . '/Client.php');
        require_once($dir . '/Abstract.php');
        require_once($dir . '/Picasa.php');
        $picasa = new Picasa();
        $clientID = "830767186327-8lkj730jj52cbogfivrg9tr20i798dd8.apps.googleusercontent.com";
        $secret = "LYv_P3NHVvpUDwhos8B3JoJI";
        $album = array('6237072089557787825', '6237072094326360401', '6237072099888173265', '6237072105337995713', '6237072107902766049', '6237072114272324433', '6237072118762229969', '6237072119298823553', '6237072126838587105', '6237072130632443393', '6237072134156491873', '6237072143055436641', '6237072147726478161', '6237072149531855841', '6237072154513919121', '6237072159765513873', '6237072165933072385', '6237072173994063841', '6237072173694371537', '6237072177386218993', '6237072182040119905', '6237072187262914993', '6237072188926289601', '6237072196154049569', '6237072199564262081', '6237072201721539153', '6237072209220742049', '6237072212508104689', '6237072217661267601', '6237072089557787825', '6237072094326360401', '6237072099888173265', '6237072105337995713', '6237072107902766049', '6237072114272324433', '6237072118762229969', '6237072119298823553', '6237072126838587105', '6237072130632443393', '6237072134156491873', '6237072143055436641', '6237072147726478161', '6237072149531855841', '6237072154513919121', '6237072159765513873', '6237072165933072385', '6237072173994063841', '6237072173694371537', '6237072177386218993', '6237072182040119905', '6237072187262914993', '6237072188926289601', '6237072196154049569', '6237072199564262081', '6237072201721539153', '6237072209220742049', '6237072212508104689', '6237072217661267601');
        $picasa->login('zone.vne@gmail.com', null);
        $picasa->setApi($clientID);
        $picasa->setSecret($secret);
        $picasa->setToken(json_decode(base64_decode('eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5LkF3STB0WW5VajRBVlo5NHE4VHhWb2FnRk8tLUxnU00wOGY4U0xIaERFRVkwaGhuS2VjRGpnTXNkLWtLbXVBNzdEdDVwIiwidG9rZW5fdHlwZSI6IkJlYXJlciIsImV4cGlyZXNfaW4iOjM2MDAsInJlZnJlc2hfdG9rZW4iOiIxXC9IbVZYT0ZLNzU5a1NqUkJhM25LOVZVaTQxRXdSQlhwZ1p4RnpEUDFYQmhoSWdPckpEdGR1bjZ6SzZYaUFUQ0tUIiwiY3JlYXRlZCI6MTQ0NDAxNDU3MX0'), true));
        $picasa->setAlbumId($album[rand(0, count($album) - 1)]);
        $contentUrl = $picasa->upload($filePath);
        if (file_exists($filePath))
            @unlink($filePath);
        $url = preg_replace("/https:\/\/(.*)googleusercontent.com(.*)$sitename(.*)/i", "http://3.bp.blogspot.com$2s0/$sitename$3", $contentUrl);
        return array('status' => 'success', 'msg' => '', 'thumb' => $url);
    } else {
        $error = $CI->upload->display_errors();
        return array('status' => 'error', 'msg' => $error);
    }
}

function uploadFPT($data) {
    $CI = get_instance();
    $CI->load->library('ftp');
    $config['hostname'] = '113.164.14.119';
    $config['username'] = 'img.dev.upload';
    $config['password'] = 'HnBTHd6HZjYidLW';
    $config['port'] = 21;
    $config['passive'] = FALSE;
    $config['debug'] = TRUE;
    $CI->ftp->connect($config);
    $dir = date("/Y/m/d", time());
    $dirs = explode("/", $dir);
    $temp = "/";
    for ($i = 1; $i <= 3; $i++) {
        $list = $CI->ftp->list_files($temp);
        $temp .= $dirs[$i] . '/';
        if (!in_array($dirs[$i], $list))
            $CI->ftp->mkdir($temp, '0777');
    }
    $path = $dir . '/' . md5(rand(10000, 99999) . time()) . $data['file_ext'];
    $CI->ftp->upload($data['full_path'], $path);
    $CI->ftp->close();
    unlink($data['full_path']);
    return $path;
}

function list_files($directory = '.') {
    if ($directory != '.')
        $directory = rtrim($directory, '/') . '/';
    if ($handle = opendir($directory)) {
        $arr = array();
        while (false !== ($file = readdir($handle))) {
            if ($file != '.' && $file != '..')
                $arr[] = $file;
        }
        closedir($handle);
        return $arr;
    }
}


