<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
	- method []
	- type ['add','sub']
	- set_type ['other','delay','agent','event']
*/
function admin_payment($admin_id = 0, $user_id = 0, $money, $method, $log_id, $type = 'add', $note = NULL, $set_type = 'other') {
    $CI = &get_instance();
    $real_money = $money;
    $CI->load->model('user_model');
    $CI->load->model('payment_transactions_model');
    $CI->load->model('payment_method_model');
    if($method=='add_money' || $method=='sub_money') {
        if($user_id) {
            $parameters['arg'] = $user_id;
            $user = $CI->user_model->get($parameters);
            if(isset($user['id'])) {
                $parameters = array('arg' => array(array('code', $method)));
                $method_data = $CI->payment_method_model->get($parameters);
                if(isset($method_data['id'])) {
                    if($type=='sub' && $user['money']<$money) $money = $user['money'];
                    $rate = ($method_data['rate'])?$method_data['rate']:1;
                    $real_money = ($type=='sub')?$money*-1:$money*$rate;
                    $description = json_encode(array('begin'=>$user['money'], 'change'=>$real_money, 'end'=>$user['money']+$real_money));
                    $payment_data = array(
                        'admin_id' => $admin_id,
                        'user_id' => $user['id'],
                        'method' => $method,
                        'money' => $money,
                        'real_money' => $real_money,
                        'note' => $note,
                        'description' => $description,
                        'set_type' => $set_type,
                    );
                    $new_money = $user['money'] + $real_money;
                    $CI->user_model->update(array('money' => $new_money), $user['id']);
                    $payment = $CI->payment_transactions_model->insert($payment_data);
                    return array('status' => 'success', 'message' => 'Chuyển tiền thành công', 'id' => $payment['id']);
                } else return array('status' => 'error', 'error_code' => 'method_not_valid', 'message' => 'Phương thức chuyển tiền không tồn tại');
            } else return array('status' => 'error', 'error_code' => 'receiver_not_valid', 'message' => 'Người nhận tiền không tồn tại');
        } else return array('status' => 'error', 'error_code' => 'input_not_valid', 'message' => 'Dữ liệu đầu vào không hợp lệ');
    }
}
function process_payment($user_id = 0, $other_id = 0, $money, $type = 'sub', $method=null, $log_id=null, $note = null) {
    $CI = &get_instance();
    $real_money = $money;
    $CI->load->model('user_model');
    $CI->load->model('payment_transactions_model');
    $CI->load->model('payment_method_model');
    $CI->load->model('payment_rating_model');
    if($user_id) {
        $parameters['arg'] = $user_id;
        $user = $CI->user_model->get($parameters);
        $parameters['arg'] = $other_id;
        $other = $CI->user_model->get($parameters);
        if(isset($user['id']) || isset($other['id'])) {
            $parameters = array('arg' => array(array('code', $method)));
            $method_data = $CI->payment_method_model->get($parameters);
            if(!isset($method_data['id']) && $method != null) {
                return array('status' => 'error', 'error_code' => 'method_not_valid', 'message' => 'Phương thức chuyển tiền không tồn tại');
            }
            if($method == null) $rate = 1;
            else $rate = ($method_data['rate'])?$method_data['rate']:1;
            if($user_id && $method != null) {
                $parameters = array('arg' => array(array('user_id', $user_id)));
                $rating = $CI->payment_rating_model->get($parameters);
                if(isset($rating['id']) && ($rating['direction'] == 'both' || $rating['direction'] == 'out')) $rate = $rating['rate'];
            }
            if($other_id && $method == 'send_money') {
                $parameters = array('arg' => array(array('user_id', $other_id)));
                $rating = $CI->payment_rating_model->get($parameters);
                if(isset($rating['id']) && ($rating['direction'] == 'both' || $rating['direction'] == 'in')) $rate = $rating['rate'];
            }
            $real_money = ($type=='sub')?$money*$rate*-1:$money*$rate;
            if($type=='sub' && $user['money'] < $money*$rate && $method != 'rollback_betting')
                return array('status' => 'error', 'error_code' => 'not_enough_money', 'message' => 'Tiền trong tài khoản không đủ để thực hiện giao dịch');
            $description = json_encode(array('begin'=>$user['money'], 'change'=>$real_money, 'end'=>$user['money']+$real_money));
            $payment_data = array(
                'user_id' => $user_id,
                'other_id' => $other_id,
                'method' => $method,
                'money' => $money,
                'real_money' => $real_money,
                'log_id' => $log_id,
                'note' => $note,
                'description' => $description,
            );
            $new_money = $user['money'] + $real_money;
            $CI->user_model->update(array('money' => $new_money), $user['id']);
            $payment = $CI->payment_transactions_model->insert($payment_data);
            if($method=='send_money') {
                $receive = process_payment($other_id, $user_id, $money, 'add', 'receive_money');
                if($receive['status']=='success') return array('status' => 'success', 'message' => 'Chuyển tiền thành công');
                else {
                    $new_money = $user['money'];
                    $CI->user_model->update(array('money' => $new_money), $user['id']);
                    return array('status' => 'error', 'error_code' => 'receive_payment_error', 'message' => 'Chuyển tiền không thành công');
                }
            } else return array('status' => 'success', 'message' => 'Chuyển tiền thành công', 'id' => $payment['id']);
        } else return array('status' => 'error', 'error_code' => 'user_not_valid', 'message' => 'Có lỗi xảy ra vui lòng liên hệ Admin');
    } else return array('status' => 'error', 'error_code' => 'input_not_valid', 'message' => 'Dữ liệu đầu vào không hợp lệ');
}
function update_payment_log($payment_id, $log_id) {
    $CI = &get_instance();
    $CI->load->model('payment_transactions_model');
    $CI->payment_transactions_model->update(array('log_id'=>$log_id), $payment_id);
}
function message($title, $content, $user_id, $from=0) {
    $CI = &get_instance();
    $CI->load->model('message_model');
    $user_id = intval($user_id);
    if($user_id && $title) {
        $arg = array(
            'user_id'=>$user_id,
            'from_id'=>$from,
            'title'=>$title,
            'content'=>$content,
        );
        $CI->message_model->insert($arg);
    } else return array('status' => 'error', 'error_code' => 'input_not_valid', 'message' => 'Dữ liệu đầu vào không hợp lệ');
}