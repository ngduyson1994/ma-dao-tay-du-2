<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function showButtonSub($id, $slug) {
    $CI = get_instance();
    $CI->load->model('channel');
    if(intval($CI->session->userdata('uID')) > 0) if(!$CI->channel->isFollow($id)) echo '<button class="follow" value="'.$slug.'"><i class="fa fa-check-circle"></i><span class="follow-text">Theo dõi</span></button>';
}
function menuSelectBox($menus, $echo = false) {
    $return = menuSelectItem($menus);
    if($echo) echo $return;
    else return $return;
}
function menuSelectItem($menus, $parent=0, $level=0) {
    $return = '';
    foreach($menus as $menu) {
        $style = $icon = '';
        if($level>0) {
            $icon = 'data-icon="fa-long-arrow-right"';
            $style = 'style="padding-left: '. 20*($level) .'px"';
        }
        $return .= '<option '.$icon.' value="'.$menu['id'].'" '.$style.'>'.$menu['title'].'</option>';
        if(isset($menu['child'])) $return .= menuSelectItem($menu['child'], $menu['id'], $level+1);
    }
    return $return;
}
function menuListBox($menus, $echo = false) {
    $return .= catListItem($menus);
    if($echo) echo $return;
    else return $return;
}
function catSelectBox($categories, $echo = false) {
    $return = catSelectItem($categories);
    if($echo) echo $return;
    else return $return;
}
function catSelectItem($categories, $parent=0, $level=0) {
    $return = '';
    $CI = get_instance();
    $cid = intval($CI->input->get('category'));
    if(is_array($categories))
        foreach($categories as $cat) {
            $style = $icon = '';
            if($level>0) {
                $icon = 'data-icon="fa-long-arrow-right"';
                $style = 'style="padding-left: '. 20*($level) .'px"';
            }
            if($cid==$cat['id']) $select = 'selected ';
            else $select='';
            $return .= '<option '.$select.$icon.' value="'.$cat['id'].'" '.$style.'>'.$cat['name'].'</option>';
            if(isset($cat['child'])) $return .= catSelectItem($cat['child'], $cat['id'], $level+1);
        }
    return $return;
}
function catPostBox($categories, $list_checked, $echo = false) {
    $lists = array();
    if(is_array($list_checked)) foreach($list_checked as $category['id']=>$list_id) $lists[] = $category['id'];
    $return = catPostItem($categories, $lists);
    if($echo) echo $return;
    else return $return;
}
function catPostItem($categories, $list_id, $parent=0, $level=0) {
    $return = '';
    foreach($categories as $category) {
        $icon = '';
        if($level>0) {
            $icon = '<i style="margin-left: '. 20*($level-1) .'px" class="fa fa-long-arrow-right" style="font-size: 13pt"></i> ';
        } else $icon = '';
        if($level==1) {
            $icon = '<i style="margin-left: '. 20*($level-1) .'px" class="fa fa-plus" style="font-size: 13pt"></i> ';
        }
        if($level==3) {
            $icon = '<i style="margin-left: '. 30*($level-1) .'px" style="font-size: 13pt"></i> ';
        }
        if(in_array($category['id'], $list_id)) $checked = ' checked';
        else $checked='';
        $return .= $icon.'<input type="checkbox" id="categories['.$category['id'].']" name="categories['.$category['id'].']" value="'.$category['id'].'"'.$checked.'>&nbsp;<label style="line-height: 16px;margin: 0;" for="categories['.$category['id'].']">'.$category['name'].'</label><br />';
        if(isset($category['child'])) $return .= catPostItem($category['child'], $list_id, $category['id'], $level+1);
    }
    return $return;
}

function SubCatListBox($categories, $echo = false) {
    $return = SubCatListItem($categories);
    if($echo) echo $return;
    else return $return;
}
function SubCatListItem($categories, $parent=0, $level=0) {
    $return = '';
    foreach($categories as $category['id']=>$category) {
        $icon = '';
        if($level>0) {
            $icon = '<i style="margin-left: '. 20*($level) .'px" class="fa fa-long-arrow-right"></i>';
        } else $icon = '';
        if($level==0) {
            $class='class="category_sub_link"';
        }
        if($level==1) {
            $icon = '<i style="margin-left: '. 20*($level) .'px" class="fa fa-plus sub_cat_icon_1"></i> ';
            $class='class="category_sub_link_1"';
        }
        if($level==2) {
            $icon = '<i style="margin-left: '. 20*($level) .'px" class="fa fa-long-arrow-right sub_cat_icon_2"></i> ';
            $class='class="category_sub_link_2"';
        }
        $return .= '
				<li>
					<a href="'.URL.'/'.$category['slug'].'">
						'.$icon.' <p '.$class.'>'.$category['name'].'</p>
					</a>
				</li>
			';
        if(isset($category['child'])) $return .= SubCatListItem($category['child'], $category['id'], $level+1);
    }
    return $return;
}
?>