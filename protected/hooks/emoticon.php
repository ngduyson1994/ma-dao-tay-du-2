<?php if (!defined('BASEPATH')) exit('No direct script access allowed');  
class Emoticon {
    function index()  
    {
        global $OUT;
        $this->CI =& get_instance();
        $output = $this->CI->output->get_output();
		$this->CI->load->model('emoticons');
		$emoticons = $this->CI->emoticons->get();
		foreach($emoticons as $emo) {
			if(strpos($emo['short'], ',')) $short = explode(',', $emo['short']);
			else $short = array($emo['short']);
			foreach($short as $key => $value) $short[$key] = '@'.$value;
			$output = str_replace($short, '<span class="emoticon fb_'.$emo['class'].'"></span>', $output);
		}
        $OUT->_display($output);
    }
}