<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth
{
	public function __construct()
	{
		$this->load->model('user_model');
	}
	public function __get($var)
	{
		return get_instance()->$var;
	}
	public $data;
	/* Login facebook */
	public function fb_auth($token) {
		$this->load->model('log_login_model');
		require APPPATH.'/third_party/facebook/facebook.php';
		$facebook = new Facebook(array(
			'appId'  => '1805821963058476',
			'secret' => '0082f01f3567533910ad2d18dc9727dd',
		));
		$facebook->setAccessToken($token);
		if ($facebook->getUser()) {
			$user = $facebook->api('/me');
			if(isset($user['id'])) {
				if(!isset($user['email']) && !isset($user['phone'])) return array('status'=>'error', 'message'=>'Your account facebook don\'t have email');
				if(!$user['verified']) return array('status'=>'error', 'message'=>'Your facebook account is unverified');
				if(!isset($user['email']) && isset($user['phone'])) $user['email'] = $user['phone'];
				$parameter['arg'] = array(array('email', $user['email']));
				$parameter['with'] = array('banned');
				$u = $this->user_model->get($parameter);
				if($u['deleted']) return array('status'=>'error', 'message'=>'Your account has been deleted! Pls contact admin');
				if (isset($u['id'])) {
					if($u['status']=='banned') {
						$is_banned = true;
						$note = '';
						$banned = end($u['banned']);
						if(isset($banned)) {
							if($banned['is_ban']==0 || ($banned['is_ban']==1&& $banned['expried'] > 0 && $banned['expried']<time())) {
								$arg = array('status' => 'valid');
								$this->db->update('users', $arg, 'id='.intval($u['id']));
								$is_banned = false;
							} else {
								if($banned['expried'] != 0) $note .= ' đến '.date("G:i d/m/Y", $banned['expried']);
								else $note .= ' vĩnh viễn';
								if($banned['note']) $note .= '. Nguyên nhân: '.$banned['note'];
							}
						}
						if($is_banned) {
							$log_login = array('user_id' => $u['id'], 'status' => 'error','type' => 'user', 'note'=>'Tài khoản bị khoá');
							$this->log_login_model->insert($log_login);
							return array('status'=>'error', 'message'=>'Tài khoản bị khóa'.$note);
						}
					}
					$sync = json_decode($u['sync'], true);
					if($sync['facebook']['sync_id'] != $u['id']) {
						$sync['facebook'] = array('sync_id'=>$u['id'], 'sync_token' => $token);
						$this->user_model->update(array('sync' => json_encode($sync)), $u['id']);
					}
					$this->setSession($u['id']);
					return array('status'=>'success', 'message'=>'Đăng nhập thành công');
				} else {
					if(!isset($user['username'])) {
						if(strpos($user['email'], '@')) {
							$mail = explode('@', $user['email']);
							$user['username'] = 'fb_'.$mail[0];
						} else $user['username'] = $user['email'];
					}
					$sync['facebook'] = array('sync_id'=>$user['id'], 'sync_token' => $token);
					$data = array(
						'username' => $user['username'],
						'name' => (isset($user['name']))?$user['name']:'',
						'email' => $user['email'],
						'gender' => (isset($user['gender']))?$user['gender']:'',
						'avatar' => 'https://graph.facebook.com/'.$user['id'].'/picture?type=large',
						'status' => 'valid',
						'ipaddress' => getIP(),
						'sync' => json_encode($sync),
					);
					$return = $this->user_model->insert($data);
					if(isset($return['id'])) {
						$this->setSession($return['id']);
						redirect(URL."/account/");
						return array('status'=>'success', 'message'=>'Đăng nhập thành công');
					} else return array('status'=>'error', 'message'=>$return['msg']);
				}
			} else return array('status'=>'error', 'message'=>'Không thể kết nối tài khoản Facebook của bạn');
		}
	}
	/* Login */
	public function login($identity, $password) {
		$this->load->model('log_login_model');
		if(empty($identity) || empty($password)) return array('status'=>'error', 'message'=>'Vui lòng điền đầy đủ thông tin');
		$parameter['arg'] = array(array('username', $identity), array('email', $identity, null, true));
		$parameter['with'] = array('banned');
		$user = $this->user_model->get($parameter);
		if(isset($user['id'])) {
			if($user['status']=='banned') {
				$is_banned = true;
				$note = '';
				$banned = end($user['banned']);
				if(isset($banned)) {
					if($banned['is_ban']==0 || ($banned['is_ban']==1&& $banned['expried'] > 0 && $banned['expried']<time())) {
						$arg = array('status' => 'valid');
						$this->db->update('users', $arg, 'id='.intval($user['id']));
						$is_banned = false;
					} else {
						if($banned['expried'] != 0) $note .= ' đến '.date("G:i d/m/Y", $banned['expried']);
						else $note .= ' vĩnh viễn';
						if($banned['note']) $note .= '. Nguyên nhân: '.$banned['note'];
					}
				}
				if($is_banned) {
					$log_login = array('user_id' => $user['id'], 'status' => 'error','type' => 'user', 'note'=>'Tài khoản bị khoá');
					$this->log_login_model->insert($log_login);
					return array('status'=>'error', 'message'=>'Tài khoản bị khóa'.$note);
				}
			} elseif($user['status']=='pending') {
				return array('status'=>'error', 'message'=>'Tài khoản chưa được kích hoạt vui lòng kiểm tra email đăng ký để xác nhận');
			}
			if($this->is_time_locked_out($user['id'])) return array('status'=>'error', 'message'=>'IP đang bị tạm khóa');
			if($user['password'] == md5(md5($password).$user['salt'])) {
				if($this->check_member($username))
					$this->init_member($user['username'], $password, $user['email'], $user['username']);
				/* Login success */
				$this->clearLoginAttempts($user['id']);
				$this->setSession($user['id']);
				$log_login = array('user_id' => $user['id'], 'status' => 'success','type' => 'user', 'note'=>'');
				$this->log_login_model->insert($log_login);
				return array('status'=>'success', 'message'=>'Đăng nhập thành công');
			} else {
				$this->increaseLoginAttempts($identity);
				$log_login = array('user_id' => $user['id'], 'status' => 'error','type' => 'user', 'note'=>'Sai mật khẩu');
				$this->log_login_model->insert($log_login);
				return array('status'=>'error', 'message'=>'Sai mật khẩu');
			}
		} else {
			$this->increaseLoginAttempts($identity);
			return array('status'=>'error', 'message'=>'Tài khoản '.$identity.' không tồn tại');
		}
	}
	/* Create Session */
	public function setSession($uid) {
		if(isset($uid) && intval($uid) > 0) {
			$last_activity = time();
			$token = md5($uid.getIP().$this->input->user_agent().$last_activity);
			$this->session->set_userdata(array('user_id' => $uid));
			$this->session->set_userdata(array('token' => $token));
			$arg = array('token' => $token, 'last_activity' => $last_activity);
			$this->db->update('users', $arg, 'id='.intval($uid));
			return true;
		} return false;
	}
	public function isLogged() {
		$token = $this->session->userdata('token');
		if($token) $parameter['arg']['token'] = $token;
		else {
			$this->session->set_userdata(array('user_id' => null));
			$this->session->set_userdata(array('token' => null));
			return false;
		}
		$user = $this->user_model->get($parameter);
		if(isset($user['id'])) {
			$last_activity = time();
			$token = md5($user['id'].getIP().$this->input->user_agent().$last_activity);
			$this->session->set_userdata(array('token' => $token));
			$arg = array('token'=>$token, 'last_activity'=>$last_activity);
			$this->db->update('users', $arg, 'id='.$user['id']);
			$this->load->model('message_model');
			$parameter = array();
			$parameter['arg'][] = array('user_id', $this->session->userdata('user_id'));
			$parameter['arg'][] = array('readed', 0);
			$message = $this->message_model->count_rows($parameter);
			$this->data = array(
				'id' => $user['id'],
				'username' => $user['username'],
				'name' => $user['name'],
				'money' => $user['money'],
				'vip_money' => $user['vip_money'],
				'chatroom_color' => $user['chatroom_color'],
				'disable_chatroom' => $user['disable_chatroom'],
				'message' => $message,
			);
			return $this->data;
		} else {
			$this->session->set_userdata(array('user_id' => null));
			$this->session->set_userdata(array('token' => null));
			return false;
		}
	}
	public function isMaxLoginAttemptsExceeded($identity) {
		if ($this->config->item('track_login_attempts')) {
			$max_attempts = $this->config->item('maximum_login_attempts');
			if ($max_attempts > 0) {
				$attempts = $this->getAttemptsNum($identity);
				return $attempts >= $max_attempts;
			}
		}
		return false;
	}
	function getAttemptsNum($identity) {
        if ($this->config->item('track_login_attempts')) {
            $ip_address = getIP();
            $this->db->select('1', false);
            if ($this->config->item('track_login_ip_address')) $this->db->where('ip_address', $ip_address);
            else if (strlen($identity) > 0) $this->db->or_where('identity', $identity);
            $qres = $this->db->get('login_attempts');
            return $qres->num_rows();
        }
        return 0;
	}
	public function is_time_locked_out($identity) {
		return $this->isMaxLoginAttemptsExceeded($identity) && $this->getLastAttemptTime($identity) > time() - $this->config->item('lockout_time');
	}
	public function getLastAttemptTime($identity) {
		if ($this->config->item('track_login_attempts')) {
			$ip_address = getIP();
			$this->db->select_max('time');
            if ($this->config->item('track_login_ip_address')) $this->db->where('ip_address', $ip_address);
			else if ($identity > 0) $this->db->or_where('identity', $identity);
			$qres = $this->db->get('login_attempts', 1);
			if($qres->num_rows() > 0) return $qres->row()->time;
		}
		return 0;
	}
	public function increaseLoginAttempts($identity) {
		if ($this->config->item('track_login_attempts')) {
			$ip_address = getIP();
			return $this->db->insert('login_attempts', array('ip_address' => $ip_address, 'identity' => $identity, 'referer' => $this->agent->referrer(), 'time' => time()));
		}
		return false;
	}
	public function clearLoginAttempts($identity, $expire_period = 86400) {
		if ($this->config->item('track_login_attempts')) {
			$ip_address = getIP();
			$this->db->where(array('ip_address' => $ip_address, 'identity' => $identity));
			$this->db->or_where('time <', time() - $expire_period, false);
			return $this->db->delete('login_attempts');
		}
		return false;
	}
	public function forgotten_password($identity)    //changed $email to $identity
	{
		if ( $this->user->forgotten_password($identity) )   //changed
		{
			// Get user information
      $identifier = $this->user->identity_column; // use model identity column, so it can be overridden in a controller
      $user = $this->where($identifier, $identity)->where('active', 1)->users()->row();  //changed to get_user_by_identity from email

			if ($user)
			{
				$data = array(
					'identity'		=> $user->{$this->config->item('identity', 'ion_auth')},
					'forgotten_password_code' => $user->forgotten_password_code
				);

				if(!$this->config->item('use_ci_email', 'ion_auth'))
				{
					$this->set_message('forgot_password_successful');
					return $data;
				}
				else
				{
					$message = $this->load->view($this->config->item('email_templates', 'ion_auth').$this->config->item('email_forgot_password', 'ion_auth'), $data, true);
					$this->email->clear();
					$this->email->from($this->config->item('admin_email', 'ion_auth'), $this->config->item('site_title', 'ion_auth'));
					$this->email->to($user->email);
					$this->email->subject($this->config->item('site_title', 'ion_auth') . ' - ' . $this->lang->line('email_forgotten_password_subject'));
					$this->email->message($message);

					if ($this->email->send())
					{
						$this->set_message('forgot_password_successful');
						return true;
					}
					else
					{
						$this->set_error('forgot_password_unsuccessful');
						return false;
					}
				}
			}
			else
			{
				$this->set_error('forgot_password_unsuccessful');
				return false;
			}
		}
		else
		{
			$this->set_error('forgot_password_unsuccessful');
			return false;
		}
	}
	public function forgotten_password_complete($code)
	{
		$this->user->trigger_events('pre_password_change');

		$identity = $this->config->item('identity', 'ion_auth');
		$profile  = $this->where('forgotten_password_code', $code)->users()->row(); //pass the code to profile

		if (!$profile)
		{
			$this->user->trigger_events(array('post_password_change', 'password_change_unsuccessful'));
			$this->set_error('password_change_unsuccessful');
			return false;
		}

		$new_password = $this->user->forgotten_password_complete($code, $profile->salt);

		if ($new_password)
		{
			$data = array(
				'identity'     => $profile->{$identity},
				'new_password' => $new_password
			);
			if(!$this->config->item('use_ci_email', 'ion_auth'))
			{
				$this->set_message('password_change_successful');
				$this->user->trigger_events(array('post_password_change', 'password_change_successful'));
					return $data;
			}
			else
			{
				$message = $this->load->view($this->config->item('email_templates', 'ion_auth').$this->config->item('email_forgot_password_complete', 'ion_auth'), $data, true);

				$this->email->clear();
				$this->email->from($this->config->item('admin_email', 'ion_auth'), $this->config->item('site_title', 'ion_auth'));
				$this->email->to($profile->email);
				$this->email->subject($this->config->item('site_title', 'ion_auth') . ' - ' . $this->lang->line('email_new_password_subject'));
				$this->email->message($message);

				if ($this->email->send())
				{
					$this->set_message('password_change_successful');
					$this->user->trigger_events(array('post_password_change', 'password_change_successful'));
					return true;
				}
				else
				{
					$this->set_error('password_change_unsuccessful');
					$this->user->trigger_events(array('post_password_change', 'password_change_unsuccessful'));
					return false;
				}

			}
		}

		$this->user->trigger_events(array('post_password_change', 'password_change_unsuccessful'));
		return false;
	}
	public function forgotten_password_check($code)
	{
		$profile = $this->where('forgotten_password_code', $code)->users()->row(); //pass the code to profile

		if (!is_object($profile))
		{
			$this->set_error('password_change_unsuccessful');
			return false;
		}
		else
		{
			if ($this->config->item('forgot_password_expiration', 'ion_auth') > 0) {
				//Make sure it isn't expired
				$expiration = $this->config->item('forgot_password_expiration', 'ion_auth');
				if (time() - $profile->forgotten_password_time > $expiration) {
					//it has expired
					$this->clear_forgotten_password_code($code);
					$this->set_error('password_change_unsuccessful');
					return false;
				}
			}
			return $profile;
		}
	}
	public function register($username, $name, $password, $confirm_password, $password_second, $email, $phone)
	{
		if(strlen($username) < 4) return array('status'=>'error', 'message'=>'Tài khoản phải có tối thiểu 4 ký tự');
		elseif(strlen($username) > 15) return array('status'=>'error', 'message'=>'Tài khoản tối đa được 15 ký tự');
		if(!preg_match("/^([-a-z0-9_ ])+$/i", $username)) return array('status'=>'error', 'message'=>'Tài khoản chỉ chấp nhập ký latin tự và số');
		if(strlen($password)<6) return array('status'=>'error', 'message'=>$password.'Mật khẩu phải lớn hơn 6 ký tự');
		//if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{6,24}$/', $password)) return array('status'=>'error', 'message'=>'Mật khẩu phải có cả chữ hoặc số');
		//if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{6,24}$/', $password_second)) return array('status'=>'error', 'message'=>'Mật khẩu 2 lớp phải có cả chữ hoặc số');
		if($confirm_password!=$password) return array('status'=>'error', 'message'=>'Xác nhận mật khẩu không khớp');
		//if(strlen($password_second)<6) return array('status'=>'error', 'message'=>'Mật khẩu 2 lớp phải lớn hơn 6 ký tự');
		//if($password==$password_second) return array('status'=>'error', 'message'=>'Mật khẩu lớp 2 không được trùng với mật khẩu đăng nhập');
		if(!(preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email))) return array('status'=>'error', 'message'=>'Email không hợp lệ');
		$phone = str_replace(array('+', ' ', '-', '.'), '', $phone);
		//if(strlen($phone) < 9) return array('status'=>'error', 'message'=>'Số điện thoại phải có 10 số trở lên');
		//elseif(strlen($phone) > 12) return array('status'=>'error', 'message'=>'Số điện thoại không quá 12 số');
		//if(!is_numeric($phone)) return array('status'=>'error', 'message'=>'Số điện thoại phải là chữ số');
		$activate = md5($username.time());
		$data = array(
			'username' => $username,
			'name' => $name,
			'password' => $password,
			'password_second' =>$password_second,
			'email' =>$email,
			'phone' =>$phone,
			'status' => 'valid',
			'activate' => $activate,
			'ipaddress' => getIP(),
		);
		$this->load->library('vinamine');
		$status = $this->vinamine->init_member($username, $password, $email, $username);
		if($status) {
			$return = $this->user_model->insert($data);
			if(isset($return['id'])) {
				$html = '<h2>Vui lòng kích hoạt tài khoản Vào chơi</h2><br />
				Bạn cần phải xác nhận email '.$email.' của bạn để kích hoạt tài khoản của bạn. Kích hoạt tài khoản của bạn sẽ kích hoạt đầy đủ tính năng để hoạt động trên Vào chơi.<br /><br />
				Vui lòng click vào link truy cập dưới đây để kích hoạt<br />
				<a href="http://vaochoi.net/?action=confirm&email='.$email.'&code='.$activate.'">http://vaochoi.net/?action=confirm&email='.$email.'&code='.$activate.'</a><br />
				© Vào chơi 2016';
				//sendmail($email, 'no-reply@vaochoi.net', 'no-reply@vaochoi.net', 'Hệ thống Vào chơi', 'Kích hoạt tài khoản', $html);
				/* Sync member to game */
				return array('status'=>'success', 'message'=>'Đăng ký thành công');
			} else return array('status'=>'error', 'message'=>$return['message']);
		} else return array('status'=>'error', 'message'=>'Đăng ký gặp vấn đề. Vui lòng thử lại');
	}
	public function logout()
	{
		$uid = intval($this->session->userdata('user_id'));
		$this->user_model->update(array('token' => ''), $uid);
		$session_data = array('user_id' => null);
		$this->session->set_userdata($session_data);
		$session_data = array('token' => null);
		$this->session->set_userdata($session_data);
	}
}
