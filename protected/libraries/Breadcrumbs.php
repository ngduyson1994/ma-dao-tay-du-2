<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Breadcrumbs {
	private $breadcrumbs = array();
	public function __construct()
	{	
		$this->ci =& get_instance();
		// Get breadcrumbs display options
		$this->tag_open = '<ul class="breadcrumb">';
		$this->tag_close = '</ul>';
		$this->crumb_open = '<li>';
		$this->crumb_close = '</li>';
		$this->crumb_last_open = '<li class="active">';
		$this->crumb_divider = '<span class="divider"></span>';
	}
	function push($page, $href)
	{
		if (!$page OR !$href) return;
		$this->breadcrumbs[$href] = array('page' => $page, 'href' => $href);
	}
	function unshift($page, $href)
	{
		if (!$page OR !$href) return;
		array_unshift($this->breadcrumbs, array('page' => $page, 'href' => $href));
	}
	function show()
	{
		if ($this->breadcrumbs) {
			$output = $this->tag_open;
			foreach ($this->breadcrumbs as $key => $crumb) {
				$output .= $this->crumb_open.'<a href="' . $crumb['href'] . '">' . $crumb['page'] . '</a> '.$this->crumb_divider.$this->crumb_close;
			}
			return $output . $this->tag_close . PHP_EOL;
		}
		return '';
	}
}