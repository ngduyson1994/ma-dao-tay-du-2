<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Vinamine
{
	private $game_db;
	public function __construct()
	{
		$this->game_db = $this->load->database('game_db', true);
	}
	public function __get($var)
	{
		return get_instance()->$var;
	}
	public function init_member($username, $password, $email, $realname) {
		$username = $this->game_db->escape_str($username);
		$realname = $this->game_db->escape_str($realname);
		$email = $this->game_db->escape_str($email);
		if($this->check_member($username)) {
			$data = array(
				'username' => $username,
				'realname' => $realname,
				'password' => md5($password),
				'email' => $email,
				'ip' => getIP()
			);
			$this->game_db->insert('authme', $data);
			return ($this->game_db->insert_id());
		} else return false;
	}
	public function update_member($username, $arg) {
		if(!$this->check_member($username) && $arg) {
			$data = array();
			if(isset($arg['password'])) $data['password'] = md5($arg['password']);
			if(isset($arg['email'])) $data['email'] = $this->game_db->escape_str($arg['email']);
			if(isset($arg['realname'])) $data['realname'] = $this->game_db->escape_str($arg['realname']);
			return $this->game_db->update('authme', $data, 'username="'.$username.'"');
		}
		return false;
	}
	public function get_money($username) {
		if(!$this->check_member($username)) {
			$uuid = $this->getUUID($username);
			$this->game_db->select("points");
			$this->game_db->where('playername', $uuid);
			$query = $this->game_db->get('playerpoints');
			$return = $query->row_array();
			if($return) return $return['points'];
			else {
				$data = array(
					'playername' => $uuid,
					'points' => 0,
				);
				$this->game_db->insert('playerpoints', $data);
				return 0;
			}
		}
		return false;
	}
	public function change_money($username, $money) {
		$money = intval($money);
		if(!$this->check_member($username) && $money) {
			$uuid = $this->getUUID($username);
			/* GET current money */
			$this->game_db->select("points");
			$this->game_db->where('playername', $uuid);
			$query = $this->game_db->get('playerpoints');
			$return = $query->row_array();
			$current = $this->get_money($username);
			/* Update money */
			$data = array('points' => $current+$money);
			return $this->game_db->update('playerpoints', $data, 'playername="'.$uuid.'"');
		}
		return false;
	}
	private function getUUID($username) {
		$username = md5("OfflinePlayer:".$username, true);
		$byte = array_values(unpack('C16', $username));
		$tLo = ($byte[0] << 24) | ($byte[1] << 16) | ($byte[2] << 8) | $byte[3];
		$tMi = ($byte[4] << 8) | $byte[5];
		$tHi = ($byte[6] << 8) | $byte[7];
		$csLo = $byte[9];
		$csHi = $byte[8] & 0x3f | (1 << 7);
		if (pack('L', 0x6162797A) == pack('N', 0x6162797A)) {
			$tLo = (($tLo & 0x000000ff) << 24) | (($tLo & 0x0000ff00) << 8) | (($tLo & 0x00ff0000) >> 8) | (($tLo & 0xff000000) >> 24);
			$tMi = (($tMi & 0x00ff) << 8) | (($tMi & 0xff00) >> 8);
			$tHi = (($tHi & 0x00ff) << 8) | (($tHi & 0xff00) >> 8);
		}
		$tHi &= 0x0fff;
		$tHi |= (3 << 12);
		$uuid = sprintf(
			'%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x',
			$tLo, $tMi, $tHi, $csHi, $csLo,
			$byte[10], $byte[11], $byte[12], $byte[13], $byte[14], $byte[15]
		);
		return $uuid;
	}
	public function check_member($username) {
		$this->game_db->select("username");
		$this->game_db->where('username', $username);
		$query = $this->game_db->get('authme');
		$return = $query->row_array();
		return ($return)?false:true;
	}
}