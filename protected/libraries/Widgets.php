<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Widgets {
    private $_ci;
    protected $parser_enable = FALSE;
    function __construct() {
        $this->widgets();
    }
    function widgets(){
        $this->_ci =& get_instance();
    }
    function build($view, $data=array()) {
        $widget_view = $view.'/views'.EXT;
        if(file_exists($widget_view)){
            $widget_view = str_replace(APPPATH, '../', $widget_view);
            if($this->parser_enable) {
                $this->_ci->load->library('parser');
                return $this->_ci->parser->parse($widget_view, $data,TRUE);
            }
            else{
                return $this->_ci->load->view($widget_view, $data,TRUE);
            }
        }
        return FALSE;
    }
    function __get($var) {
        static $ci;
        isset($ci) OR $ci = get_instance();
        return $ci->$var;
    }
}
?>