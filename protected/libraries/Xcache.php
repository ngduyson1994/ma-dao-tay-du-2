<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class XCache
{
	private $_ci;
	private $_path;
	private $_contents;
	private $_filename;
	private $_expires;
	private $_default_expires;
	private $_prefix;
	private $_created;
	private $_dependencies;
	private $adapter;
	function __construct()
	{
		//log_message('debug', "Cache Class Initialized.");
		$this->_ci =& get_instance();
		$this->_reset();
		$this->_ci->load->config('cache');
		$this->_path = $this->_ci->config->item('cache_dir');
		$this->_prefix = $this->_ci->config->item('cache_prefix');
		$this->_default_expires = $this->_ci->config->item('cache_default_expires');
		
		$apc = new apc();
		$memcached = new mem();
		if($memcached->is_supported()) $this->adapter = $memcached;
		elseif($apc->is_supported()) $this->adapter = $apc;
		else $this->adapter = false;
		$this->adapter = false;

		if(!is_dir($this->_path))
		{
			show_error("Cache Path not found: $this->_path");
		}
	}
	private function _reset()
	{
		$this->_contents = NULL;
		$this->_filename = NULL;
		$this->_expires = NULL;
		$this->_created = NULL;
		$this->_dependencies = array();
	}
	public function library($library, $method, $arguments = array(), $expires = NULL)
	{
		if(!class_exists(ucfirst($library))) $this->_ci->load->library($library);
		return $this->_call($library, $method, $arguments, $expires);
	}
	public function model($model, $method, $arguments = array(), $expires = NULL)
	{
		if(!class_exists(ucfirst($model))) $this->_ci->load->model($model);
		return $this->_call($model, $method, $arguments, $expires);
	}
	private function _call($property, $method, $arguments = array(), $expires = NULL)
	{
		$this->_ci->load->helper('security');
		if(! is_array($arguments)) $arguments = (array) $arguments;
		$arguments = array_values($arguments);
		$cache_file = $property.DIRECTORY_SEPARATOR.do_hash($method.serialize($arguments).URL, 'sha1');
		// See if we have this cached or delete if $expires is negative
		if($expires >= 0)
		{
			$cached_response = $this->get($cache_file);
		}
		else
		{
			$this->delete($cache_file); return;
		}

		// Not FALSE? Return it
		if($cached_response !== FALSE && $cached_response !== NULL)
		{
			return $cached_response;
		}
		else
		{
			// Call the model or library with the method provided and the same arguments
			$new_response = call_user_func_array(array($this->_ci->$property, $method), $arguments);
			$this->write($new_response, $cache_file, $expires);
			return $new_response;
		}
	}
	function set_dependencies($dependencies)
	{
		if (is_array($dependencies))
		{
			$this->_dependencies = $dependencies;
		}
		else
		{
			$this->_dependencies = array($dependencies);
		}

		return $this;
	}
	function add_dependencies($dependencies)
	{
		if (is_array($dependencies))
		{
			$this->_dependencies = array_merge($this->_dependencies, $dependencies);
		}
		else
		{
			$this->_dependencies[] = $dependencies;
		}

		return $this;
	}
	function get_dependencies() { return $this->_dependencies; }
	function get_created($created) { return $this->_created; }
	function get($filename = NULL, $use_expires = true)
	{
		if($this->adapter) {
			$content = $this->adapter->get($filename);
			$this->_reset();
			return $content;
		}
		if ($filename !== NULL)
		{
			$this->_reset();
			$this->_filename = $filename;
		}

		if(!is_dir($this->_path) OR ! is_really_writable($this->_path)) return FALSE;
		$filepath = $this->_path.$this->_prefix.$this->_filename. md5(URL).'.cache';
		if(!@file_exists($filepath)) return FALSE;
		if(!$fp = @fopen($filepath, FOPEN_READ)) return FALSE;

		flock($fp, LOCK_SH);
		if (filesize($filepath) > 0) $this->_contents = unserialize(fread($fp, filesize($filepath)));
		else $this->_contents = NULL;
		flock($fp, LOCK_UN);
		fclose($fp);

		// Check cache expiration, delete and return FALSE when expired
		if ($use_expires && ! empty($this->_contents['__cache_expires']) && $this->_contents['__cache_expires'] < time())
		{
			$this->delete($filename);
			return FALSE;
		}
		if(isset($this->_contents['__cache_dependencies']))
		{
			foreach ($this->_contents['__cache_dependencies'] as $dep)
			{
				$cache_created = filemtime($this->_path.$this->_prefix.$this->_filename. md5(URL).'.cache');
				if (! file_exists($this->_path.$this->_prefix.$dep. md5(URL).'.cache') or filemtime($this->_path.$this->_prefix.$dep. md5(URL).'.cache') > $cache_created)
				{
					$this->delete($filename);
					return FALSE;
				}
			}
		}

		$this->_expires		= isset($this->_contents['__cache_expires']) ? $this->_contents['__cache_expires'] : NULL;
		$this->_dependencies = isset($this->_contents['__cache_dependencies']) ? $this->_contents['__cache_dependencies'] : NULL;
		$this->_created		= isset($this->_contents['__cache_created']) ? $this->_contents['__cache_created'] : NULL;
		$this->_contents = @$this->_contents['__cache_contents'];
		//log_message('debug', "Cache retrieved: ".$filename);
		return $this->_contents;
	}
	function write($contents = NULL, $filename = NULL, $expires = NULL, $dependencies = array())
	{
		if($this->adapter) {
			$content = $this->adapter->save($filename, $contents, $expires);
			$this->_reset();
			return $content;
		}
		if ($contents !== NULL)
		{
			$this->_reset();
			$this->_contents = $contents;
			$this->_filename = $filename;
			$this->_expires = $expires;
			$this->_dependencies = $dependencies;
		}
		$this->_contents = array('__cache_contents' => $this->_contents);
		if(!is_dir($this->_path) OR ! is_really_writable($this->_path)) return;

		// check if filename contains dirs
		$subdirs = explode(DIRECTORY_SEPARATOR, $this->_filename);
		if (count($subdirs) > 1)
		{
			array_pop($subdirs);
			$test_path = $this->_path.implode(DIRECTORY_SEPARATOR, $subdirs);

			// check if specified subdir exists
			if(!@file_exists($test_path))
			{
				// create non existing dirs, asumes PHP5
				if(!@mkdir($test_path, DIR_WRITE_MODE, TRUE)) return FALSE;
			}
		}

		// Set the path to the cachefile which is to be created
		$cache_path = $this->_path.$this->_prefix.$this->_filename. md5(URL).'.cache';

		// Open the file and log if an error occures
		if(!$fp = @fopen($cache_path, FOPEN_WRITE_CREATE_DESTRUCTIVE))
		{
			//log_message('error', "Unable to write Cache file: ".$cache_path);
			return;
		}

		// Meta variables
		$this->_contents['__cache_created'] = time();
		$this->_contents['__cache_dependencies'] = $this->_dependencies;

		// Add expires variable if its set...
		if (! empty($this->_expires))
		{
			$this->_contents['__cache_expires'] = $this->_expires + time();
		}
		// ...or add default expiration if its set
		elseif (! empty($this->_default_expires) )
		{
			$this->_contents['__cache_expires'] = $this->_default_expires + time();
		}

		// Lock the file before writing or log an error if it failes
		if (flock($fp, LOCK_EX))
		{
			fwrite($fp, serialize($this->_contents));
			flock($fp, LOCK_UN);
		}
		else
		{
			//log_message('error', "Cache was unable to secure a file lock for file at: ".$cache_path);
			return;
		}
		fclose($fp);
		@chmod($cache_path, DIR_WRITE_MODE);
		//log_message('debug', "Cache file written: ".$cache_path);
		$this->_reset();
	}
	function delete($filename = NULL)
	{
		if($this->adapter) {
			$this->adapter->delete($filename);
			$this->_reset();
			return;
		}
		if ($filename !== NULL) $this->_filename = $filename;
		$file_path = $this->_path.$this->_prefix.$this->_filename. md5(URL).'.cache';
		if (file_exists($file_path)) @unlink($file_path);
		$this->_reset();
	}
	public function delete_group($group = null)
	{
		if($this->adapter) {
			return;
		}
		if ($group === null) return FALSE;
		$this->_ci->load->helper('directory');
		$map = directory_map($this->_path, TRUE);
		foreach ($map AS $file)
		{
			if (strpos($file, $group)  !== FALSE)
			{
				@unlink($this->_path.$file);
			}
		}
		$this->_reset();
	}
	function delete_all($dirname = '')
	{
		if($this->adapter) {
			$this->adapter->clean();
			$this->_reset();
			return;
		}
		if (empty($this->_path)) return FALSE;
		$this->_ci->load->helper('file');
		if (file_exists($this->_path.$dirname)) delete_files($this->_path.$dirname, TRUE);
		$this->_reset();
	}
}
class apc {
	public function get($id)
	{
		$success = FALSE;
		$data = apc_fetch($id, $success);
		if ($success === TRUE) return is_array($data)?unserialize($data[0]):$data;
		return FALSE;
	}
	public function save($id, $data, $ttl = 60, $raw = FALSE)
	{
		$ttl = (int) $ttl;
		return apc_store($id, ($raw === TRUE ? $data : array(serialize($data), time(), $ttl)), $ttl);
	}
	public function delete($id)
	{
		return apc_delete($id);
	}
	public function increment($id, $offset = 1)
	{
		return apc_inc($id, $offset);
	}
	public function decrement($id, $offset = 1)
	{
		return apc_dec($id, $offset);
	}
	public function clean()
	{
		return apc_clear_cache('user');
	}
	 public function cache_info($type = NULL)
	 {
		 return apc_cache_info($type);
	 }
	public function get_metadata($id)
	{
		$success = FALSE;
		$stored = apc_fetch($id, $success);
		if ($success === FALSE OR count($stored) !== 3) return FALSE;
		list($data, $time, $ttl) = $stored;
		return array(
			'expire'	=> $time + $ttl,
			'mtime'		=> $time,
			'data'		=> unserialize($data)
		);
	}
	public function is_supported()
	{
		if (!extension_loaded('apc') OR ! ini_get('apc.enabled'))
		{
			//log_message('debug', 'The APC PHP extension must be loaded to use APC Cache.');
			return FALSE;
		}
		return TRUE;
	}
}
class mem {
	protected $_memcached;
	protected $_memcache_conf = array(
		'default' => array(
			'host'		=> '127.0.0.1',
			'port'		=> 11211,
			'weight'	=> 1
		)
	);
	public function get($id)
	{
		$data = $this->_memcached->get($id);
		return is_array($data) ? $data[0] : $data;
	}
	public function save($id, $data, $ttl = 60, $raw = FALSE)
	{
		if ($raw !== TRUE) $data = array($data, time(), $ttl);
		if (get_class($this->_memcached) === 'Memcached') return $this->_memcached->set($id, $data, $ttl);
		elseif (get_class($this->_memcached) === 'Memcache') return $this->_memcached->set($id, $data, 0, $ttl);
		return FALSE;
	}
	public function delete($id)
	{
		return $this->_memcached->delete($id);
	}
	public function increment($id, $offset = 1)
	{
		return $this->_memcached->increment($id, $offset);
	}
	public function decrement($id, $offset = 1)
	{
		return $this->_memcached->decrement($id, $offset);
	}
	public function clean()
	{
		return $this->_memcached->flush();
	}
	public function cache_info()
	{
		return $this->_memcached->getStats();
	}
	public function get_metadata($id)
	{
		$stored = $this->_memcached->get($id);
		if (count($stored) !== 3) return FALSE;
		list($data, $time, $ttl) = $stored;
		return array(
			'expire'	=> $time + $ttl,
			'mtime'		=> $time,
			'data'		=> $data
		);
	}
	protected function _setup_memcached()
	{
		$CI =& get_instance();
		$defaults = $this->_memcache_conf['default'];
		if ($CI->config->load('memcached', TRUE, TRUE))
		{
			if (is_array($CI->config->config['memcached']))
			{
				$this->_memcache_conf = array();

				foreach ($CI->config->config['memcached'] as $name => $conf)
				{
					$this->_memcache_conf[$name] = $conf;
				}
			}
		}
		if (class_exists('Memcached', FALSE))
		{
			$this->_memcached = new Memcached();
		}
		elseif (class_exists('Memcache', FALSE))
		{
			$this->_memcached = new Memcache();
		}
		else
		{
			//log_message('error', 'Failed to create object for Memcached Cache; extension not loaded?');
			return FALSE;
		}
		foreach ($this->_memcache_conf as $cache_server)
		{
			isset($cache_server['hostname']) OR $cache_server['hostname'] = $defaults['host'];
			isset($cache_server['port']) OR $cache_server['port'] = $defaults['port'];
			isset($cache_server['weight']) OR $cache_server['weight'] = $defaults['weight'];
			if (get_class($this->_memcached) === 'Memcache')
			{
				$this->_memcached->addServer(
					$cache_server['hostname'],
					$cache_server['port'],
					TRUE,
					$cache_server['weight']
				);
			}
			else
			{
				$this->_memcached->addServer(
					$cache_server['hostname'],
					$cache_server['port'],
					$cache_server['weight']
				);
			}
		}
		return TRUE;
	}
	public function is_supported()
	{
		if(!extension_loaded('memcached') && ! extension_loaded('memcache'))
		{
			//log_message('debug', 'The Memcached Extension must be loaded to use Memcached Cache.');
			return FALSE;
		}
		return $this->_setup_memcached();
	}
}