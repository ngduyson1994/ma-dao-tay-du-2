<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Adlink_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'adlink';
        $this->primary_key = 'id';
		$this->has_one['zone'] = array('adzone_model','id','zid');
		$this->has_one['code'] = array('adcode_model','id','cid');
        $this->soft_deletes = false;
        $this->timestamps = false;
		parent::__construct();
	}
}