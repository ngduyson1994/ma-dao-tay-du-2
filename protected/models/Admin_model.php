<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Admin_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'admins';
        $this->primary_key = 'id';
        $this->unique = array('username', 'email');
        $this->soft_deletes = true;
		$this->has_one['group'] = array('admin_group_model','id','gid');
        $this->before_create[] = 'proccess_data';
        $this->before_update[] = 'proccess_data';
		parent::__construct();
	}
    protected function proccess_data($data)
    {
		if(isset($data['birthday']) && $data['birthday']) {
			$data['birthday'] = date("Y-m-d", strtotime($data['birthday']));
		}
		if(isset($data['password']) && $data['password']) {
			$salt = substr(md5(rand(0, 9999999999)), rand(0, 10), 10);
			$password = md5(md5($data['password']).$salt);
			$data['password'] = $password;
			$data['salt'] = $salt;
		}
        return $data;
    }
}