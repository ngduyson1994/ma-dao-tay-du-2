<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Adzone_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'adzone';
        $this->primary_key = 'id';
		$this->has_many['link'] = array('adlink_model','zid','id');
        $this->soft_deletes = false;
        $this->timestamps = false;
		parent::__construct();
	}
}