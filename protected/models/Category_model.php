<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'categories';
        $this->primary_key = 'id';
        $this->unique = array('name', 'slug');
//        $this->has_many['relationships'] = array('relationship_model','foreign_key','id');
        $this->soft_deletes = false;
        $this->timestamps = false;
		parent::__construct();
	}
    public $before_create = array('proccess_data');
    public $before_update = array('proccess_data');
    protected function proccess_data($data)
    {
		if(!isset($data['slug']) && isset($data['name'])) {
			$data['slug'] = clearUTF(trim($data['name']));
		}
		
        return $data;
    }
	/* Get data in list */
	public function getList($parent=0) {
		$parameter = array();
		$parameter['arg'][] = array('parent', $parent);
		$parameter['fields'] = '*';
		$parameter['order'] = array('order_by' => 'order', 'order' => 'ASC');
		$data = $this->get_all($parameter);
		if(is_array($data)) {
			foreach($data as $i=>$value) {
				if(isset($value['id'])) {
					$parent = $value['id'];
					$data[$i]['child'] = $this->getList($parent);
                    
				}
			}
			
			return $data;
		}
	}
	/* Get parent  */
	public function getParentByID($id) {
		$id = intval($id);
		if($id) {
			$cat = $this->get($id);
			$parent = intval($cat['parent']);
			if($parent) {
				$return = $this->get($parent);
				if(isset($return['id'])) return $return;
			}
		}
		return false;
	}
}