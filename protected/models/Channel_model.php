<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Channel_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'channels';
        $this->primary_key = 'id';
        $this->soft_deletes = false;
        $this->timestamps = false;
		parent::__construct();
	}
}