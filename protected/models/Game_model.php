<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Game_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'game';
        $this->primary_key = 'id';
        $this->unique = array('name', 'rank');
        $this->soft_deletes = false;
		parent::__construct();
	}

}