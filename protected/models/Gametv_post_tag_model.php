<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gametv_post_tag_model extends MY_Model
{
	public function __construct()
	{
        $this->_database_connection = 'gametv';
        $this->table = 'post_tag';
        $this->primary_key = 'id';
        $this->soft_deletes = false;
        $this->timestamps = false;
		parent::__construct();
	}
}