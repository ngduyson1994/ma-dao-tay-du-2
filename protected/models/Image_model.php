<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Image_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'images';
        $this->primary_key = 'id';
        $this->unique = array('name', 'link');
        $this->soft_deletes = false;
		parent::__construct();
	}
}