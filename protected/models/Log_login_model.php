<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Log_login_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'log_login';
        $this->primary_key = 'id';
        $this->soft_deletes = false;
		$this->has_one['user'] = array('user_model','id','user_id');
        $this->before_create[] = 'before_create';
		parent::__construct();
	}
    protected function before_create($data)
    {
    	$this->load->library('user_agent');
	    $data['ipaddress'] = getIP();
	    $data['user_agent'] = $this->agent->agent_string();
        return $data;
	}
}