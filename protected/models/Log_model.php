<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Log_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'logs';
        $this->primary_key = 'id';
        $this->soft_deletes = false;
		$this->has_one['admin'] = array('admin_model','id','aid');
        $this->before_create[] = 'before_create';
		parent::__construct();
	}
	public function getModel() {
		$this->db->select("DISTINCT(model)");
		/* Get data */
		$query = $this->db->get('logs');
		$return = $query->result_array();
		return $return;
	}
    protected function before_create($data)
    {
    	$this->load->library('user_agent');
	    $data['ipaddress'] = getIP();
	    $data['user_agent'] = $this->agent->agent_string();
	    $data['referrer'] = $this->agent->referrer();
		$data['aid'] = intval($this->session->userdata('aID'));
        return $data;
	}
}