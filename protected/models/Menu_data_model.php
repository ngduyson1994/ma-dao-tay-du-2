<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menu_data_model extends MY_Model
{
	public function __construct()
	{
		$this->table = 'menu_item';
        $this->primary_key = 'id';
		$this->has_many['menu'] = array('menu_model','id','mid');
        $this->unique = array('name', 'slug');
        $this->soft_deletes = false;
        $this->timestamps = false;
        $this->after_get[] = 'proccess_result';
		parent::__construct();
	}
	/* Get data in list */
	public function getList($mid, $parent=0) {
		$parameter = array();
		$parameter['arg'][] = array('mid', $mid);
		$parameter['arg'][] = array('parent', $parent);
		$parameter['order'] = array('order_by' => 'order', 'order' => 'ASC');
		$data = $this->get_all($parameter);
		if(is_array($data)) {
			foreach($data as $i=>$value) {
				$parent = $value['id'];
				$data[$i]['child'] = $this->getList($mid, $parent);
			}
			return $data;
		}
	}
    protected function proccess_result($result)
    {
		if($result) {
			foreach($result as $key=>$value) {
				if(isset($value['type']) && $value['type']=='category') {
					$this->load->model('category_model');
					$parameter = array();
					$parameter['arg'] = $value['value']; 
					$parameter['fields'] = 'slug';
					$category = $this->category_model->get($parameter);
					$result[$key]['slug'] = $category['slug'];
				}
			}
		}
        return $result;
    }
}
