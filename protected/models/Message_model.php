<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Message_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'user_message';
        $this->primary_key = 'id';
        $this->soft_deletes = false;
		$this->has_one['user'] = array('user_model','id','user_id');
		$this->has_one['from'] = array('user_model','id','from_user');
		parent::__construct();
	}
}