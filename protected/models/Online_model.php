<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Online_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'onlines';
        $this->primary_key = 'id';
        $this->soft_deletes = false;
        $this->timestamps = false;
		parent::__construct();
	}
	public function init($customer_id, $live_id) {
		$agent = $_SERVER['HTTP_USER_AGENT'];
		$parameter = array();
		$parameter['arg'][] = array('expried', '<', time()-3600);
		$this->delete($parameter);
		$parameter = array();
		$parameter['arg'][] = array('customer_id', $customer_id);
		$parameter['arg'][] = array('live_id', $live_id);
		$check = $this->get($parameter);
		if(!isset($check['id'])) {
			$arg = array(
				'customer_id' => $customer_id,
				'live_id' => $live_id,
				'ip' => getIP(),
				'user_hash' => $agent,
				'expried' => time()+3600,
			);
			$this->insert($arg);
		}
	}
	public function total($live_id) {
		$parameter = array();
		$parameter['arg'][] = array('id', $live_id);
		$this->load->model('schedule_model');
		$live = $this->schedule_model->get($parameter);
		if(isset($live['id'])) {
			$parameter = array();
			$parameter['arg'][] = array('live_id', $live_id);
			$parameter['fields'] = '*count*';
			$onlines = $this->get($parameter);
			$online = intval($onlines['counted_rows']);
			if($live['max_ccu'] < $online) {
				$this->schedule_model->update(array('max_ccu'=>$online), $live_id);
			}
			return $online;
		}
		return 0;
	}
	public function remove($customer_id, $live_id) {
		$agent = $_SERVER['HTTP_USER_AGENT'];
		$parameter = array();
		$parameter['arg'][] = array('customer_id', $customer_id);
		$parameter['arg'][] = array('live_id', $live_id);
		$check = $this->delete($parameter);
	}
}