<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Permission_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'permission';
        $this->primary_key = 'id';
        $this->soft_deletes = false;
        $this->timestamps = false;
		$this->has_many['data'] = array('permission_group_model','pid','id');
		parent::__construct();
	}
}