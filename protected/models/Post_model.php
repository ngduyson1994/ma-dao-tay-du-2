<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Post_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'posts';
        $this->primary_key = 'id';
        $this->unique = array('slug');
        $this->soft_deletes = true;
        $this->has_one['admin'] = array('admin_model','id','user_id'); 
       	$this->has_one['category'] = array('category_model','id','foreign_key');
		$this->has_many['relationships'] = array('relationship_model','candidate_key','id');
        $this->before_create[] = 'before_create';
        $this->before_update[] = 'before_update';
        $this->after_create[] = 'after_create';
        $this->after_update[] = 'after_update';
        $this->before_get[] = 'before_get';
        $this->after_get[] = 'after_get';
		parent::__construct();
	}
    protected $categories;
    protected $tags;
	public function countViews($id) {
		$this->_database->query("UPDATE ".$this->table." SET views = views + 1 WHERE id = ".$id);
	}
    protected function before_create($data)
    {
		$data = $this->before_update($data);
		$data['published'] = time();
        return $data;
	}
    protected function before_update($data) {
		if(isset($data['content'])) $data['content'] = bzcompress($data['content'], 9);
		if(isset($data['categories']) && $data['categories']) {
			$this->categories = $data['categories'];
			unset($data['categories']);
		}
		if(isset($data['tags']) && $data['tags']) {
			$this->tags = explode(',', $data['tags']);
			unset($data['tags']);
		}
        return $data;
    }
    protected function after_create($data) {
		return $this->after_update($data);
	}
    protected function after_update($data) {
		$id = intval($data['id']);
		if($id && ($this->categories || $this->tags)) {
			$this->load->model('relationship_model');
			$this->load->model('tag_model');
			$parameter['arg'][] = array('candidate_table', 'posts');
			$parameter['arg'][] = array('candidate_key', $id);
			$this->relationship_model->delete($parameter);
			if($this->categories) {
				foreach($this->categories as $cid=>$no) {
					if($cid) $this->relationship_model->insert(array('candidate_table'=>'posts','candidate_key'=>$id,'foreign_table'=>'categories','foreign_key'=>$cid));
				}
				unset($this->categories);
			}
			if($this->tags) {
				foreach($this->tags as $tagid) {
					if(!is_numeric($tagid)) {
						$arg = array('name'=>$tagid, 'slug'=>clearUTF($tagid));
						$tag = $this->tag_model->insert($arg);
						if(isset($tag['id'])) $tagid = $tag['id'];
					}
					if($tagid > 0) {
						$this->relationship_model->insert(array('candidate_table'=>'posts','candidate_key'=>$id,'foreign_table'=>'tags','foreign_key'=>$tagid));
						$parameter = array();
						$parameter['arg'][] = array('foreign_table', 'tags');
						$parameter['arg'][] = array('foreign_key', $tagid);
						$count = $this->relationship_model->count_rows($parameter);
						$this->tag_model->update(array('count'=>$count), $tagid);
					}
				}
				unset($this->tags);
			}
		}
		return $data;
    }
    protected function before_get($where) {
		if(is_array($where)) {
			foreach($where as $key=>$value) {
				//Get by category
				if($value[0] == 'category') {
					$in = array();
					if($value[1]) {
						$this->load->model('relationship_model');
						$arg = array(
							array('candidate_table', 'posts'),
							array('foreign_table', 'categories'),
							array('foreign_key', $value[1])
						);
						$parameter['arg'] = $arg;
						$temp = $this->relationship_model->get_all($parameter);
						if(is_array($temp)) foreach($temp as $relation) $in[] = $relation['candidate_key'];
					}
					$in = array_unique($in);
					if(count($in)) $where[$key] = array('id', $in);
					else $where[$key] = array('id', array(0));
				}
				//Get by tag
				if($value[0] == 'tag') {
					$in = array();
					if($value[1]) {
						$this->load->model('relationship_model');
						$arg = array(
							array('candidate_table', 'posts'),
							array('foreign_table', 'tags'),
							array('foreign_key', $value[1])
						);
						$parameter['arg'] = $arg;
						$temp = $this->relationship_model->get_all($parameter);
						if(is_array($temp)) foreach($temp as $relation) $in[] = $relation['candidate_key'];
					}
					$in = array_unique($in);
					if(count($in)) $where[$key] = array('id', $in);
					else $where[$key] = array('id', array(0));
				}
			}
		}
		return $where;
    }
    protected function after_get($result) {
		if($result) {
			foreach($result as $key=>$value) {
				$tags = array();
				$categories = array();
				if(isset($value['relationships']) && $value['relationships']) {
					$this->load->model('tag_model');
					$this->load->model('category_model');
					foreach($value['relationships'] as $tag) {
						$parameter['arg'] = $tag['foreign_key'];
						$parameter['fields'] = 'id,name,slug';
						if($tag['foreign_table'] == 'tags') {
							$t = $this->tag_model->get($parameter);
							if(isset($t['id']) && $t['id']) $tags[$tag['foreign_key']] = $t;
						} elseif($tag['foreign_table'] == 'categories') {
							$categories[$tag['foreign_key']] = $this->category_model->get($parameter);
						}
					}
					unset($result[$key]['relationships']);
				}
				$result[$key]['tags'] = $tags;
				$result[$key]['categories'] = $categories;
				$content = (isset($result[$key]['content']))?bzdecompress($result[$key]['content']):'';
				if(!is_numeric($content)) $result[$key]['content'] = $content;
			}
		}
        return $result;
    }
}