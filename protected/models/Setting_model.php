<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Setting_model extends MY_Model
{
	/* Model xử lý dữ liệu Tag */
	public function __construct()
	{
		$this->table = 'settings';
        $this->primary_key = 'id';
        $this->soft_deletes = false;
        $this->timestamps = false;
		parent::__construct();
	}
	public function getList() {
		$return = $this->get_all();
		$temp = array();
		foreach($return as $setting) $temp[$setting['name']] = $setting['value'];
		return $temp;
	}
}
