<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tag_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'tags';
        $this->primary_key = 'id';
        $this->unique = array('name', 'slug');
		$this->has_one['game_1'] = array('game_model','id','game_id_1');
		$this->has_one['game_2'] = array('game_model','id','game_id_2'); 
        $this->soft_deletes = false;
		parent::__construct();
	}
    public $before_create = array('proccess_data');
    public $before_update = array('proccess_data');
    protected function proccess_data($data)
    {
		if(!isset($data['slug']) && isset($data['name'])) {
			$data['slug'] = clearUTF(trim($data['name']));
		}
        return $data;
    }
}