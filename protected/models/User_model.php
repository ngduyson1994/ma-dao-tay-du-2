<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends MY_Model
{
	public function __construct()
	{
        $this->table = 'users';
        $this->primary_key = 'id';
        $this->unique = array('username', 'email');
        $this->soft_deletes = true;
		$this->has_many['banned'] = array('user_ban_model','user_id','id');
		$this->has_many['log_login'] = array('log_login_model','user_id','id');
        $this->before_create[] = 'proccess_data';
        $this->before_update[] = 'proccess_data';
				parent::__construct();
	}
    protected function proccess_data($data)
    {
		if(isset($data['birthday']) && $data['birthday']) {
			$data['birthday'] = date("Y-m-d", strtotime($data['birthday']));
		}
		if(isset($data['password']) && $data['password']) {
			$salt = substr(md5(rand(0, 9999999999)), rand(0, 10), 10);
			$password = md5(md5($data['password']).$salt);
			$data['password'] = $password;
			$data['salt'] = $salt;
		} else if(isset($data['password'])) unset($data['password']);
		if(isset($data['password_second']) && $data['password_second']) {
			$salt = $this->config->item('encryption_key');
			$password_second = md5(md5($data['password_second']).$salt);
			$data['password_second'] = $password_second;
		} else if(isset($data['password_second'])) unset($data['password_second']);
        return $data;
    }
}
