<?php
require_once 'MaxMind/Db/Reader.php';
$reader = new Reader('GeoLite2.mmdb');
$record = $reader->get('42.113.193.137');
$reader->close();
print($record['country']['iso_code'] . "\n");
print($record['country']['names']['en'] . "\n");
print_r($record);