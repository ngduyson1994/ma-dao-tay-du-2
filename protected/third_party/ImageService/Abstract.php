<?php
abstract class Class_Abstract
{
    protected $useAccount = false;
    protected $username;
    protected $password;
    protected $apiKey;
    protected $file;
    protected $url;
    protected $useCurl = false;
    protected $client;

    final public function __construct()
    {
        $this->client = $this->createHttpClient();
    }
    public function createHttpClient()
    {
        $httpClient = new Http_Client();
        $httpClient->useCurl($this->useCurl);
        return $httpClient;
    }
    public function useCurl($useCurl)
    {
        $this->useCurl = $useCurl;
    }
    protected function resetHttpClient()
    {
        $this->client->reset();
        $this->client->useCurl($this->useCurl);

        return $this->client;
    }
    final public function login($username, $password)
    {
        $this->username   = $username;
        $this->password   = $password;
        $this->useAccount = true;
        return $this->doLogin($username, $password);
    }
    final public function setApi($keys)
    {
        $keys = (array) $keys;

        $this->apiKey = $keys[array_rand($keys, 1)];
    }
    public function upload($file)
    {
        if (!$filepath = realpath($file)) {
            $this->throwException('%s: File "%s" is not exists.', __METHOD__, $file);
        }
        if (!getimagesize($filepath)) {
            $this->throwException('%: The file "%s" is not an image.', __METHOD__, $file);
        }
        $this->file = $filepath;
        return $this->doUpload();
    }
    final public function transload($url)
    {
        $this->url = trim($url);
        return $this->doTransload();
    }
    final public function getIdentifier()
    {
        return substr(md5($this->getName().$this->username.$this->password), 0, 5);
    }
    final public function getName()
    {
        return get_class($this);
    }
    abstract protected function doLogin();
    abstract protected function doUpload();
    abstract protected function doTransload();
    protected function getMatch($regex, $text, $number = 1, $default = null)
    {
        if (preg_match($regex, $text, $match) && isset($match[$number])) {
            return $match[$number];
        }

        return $default;
    }
    protected function getElement($array, $keys, $default = null)
    {
        foreach (explode('.', $keys) as $key) {
            if (isset($array[$key])) {
                $array = $array[$key];
            } else {
                return $default;
            }
        }

        return $array;
    }
    protected function throwException()
    {
        $arguments = $this->trimBaseClassName(func_get_args());
        $msg = call_user_func_array('sprintf', array_slice($arguments, 0, 10));
        echo $msg;
        exit;
    }
    protected function checkHttpClientErrors($method)
    {
        if ($this->client->errors) {
            $this->throwRequestError($method);
        }
    }
    protected function throwRequestError($method)
    {
        $method = $this->trimBaseClassName($method);
        return $this->throwException('%s: %s', $method, implode(', ', $this->client->errors));
    }
    protected function trimBaseClassName($value)
    {
        if (is_array($value)) {
            foreach ($value as &$val) {
                $val = $this->trimBaseClassName($val);
            }
        }
        return $value;
    }
}
