<?php
class Picasa extends Class_Abstract
{
    const OAUTH_ENDPOINT = 'https://accounts.google.com/o/oauth2/auth';
    const OAUTH_TOKEN_ENDPOINT = 'https://www.googleapis.com/oauth2/v3/token';
    const OAUTH_SCOPE_PICASA = 'https://picasaweb.google.com/data/';

    const USER_FEED_ENDPOINT = 'https://picasaweb.google.com/data/feed/api/user/%s';
    const ALBUM_FEED_ENPOINT = 'https://picasaweb.google.com/data/feed/api/user/%s/albumid/%s';

    const OAUTH_TOKEN = 'oauth_token';
    const OAUTH_TOKEN_EXPIRES_AT = '__expires_at';

    protected $albumId = 'default';
    protected $secret;
    protected $token;
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }
    public function setAlbumId($albumIds)
    {
        if (empty($albumIds)) {
            $albumIds = 'default';
        }
        $albumIds = (array) $albumIds;
        $this->albumId = $albumIds[array_rand($albumIds, 1)];
    }
    public function getOAuthToken($callback)
    {
        if (empty($_GET['code'])) {
            return $this->requestToken($callback);
        }
        return $this->getOAuthAccessToken($_GET['code'], $callback);
    }
    public function refreshToken(array $token)
    {
        $client = $this->createHttpClient();
        $client->setParameters(array(
            'refresh_token' => $token['refresh_token'],
            'client_id'     => $this->apiKey,
            'client_secret' => $this->secret,
            'grant_type'    => 'refresh_token',
        ))->execute(self::OAUTH_TOKEN_ENDPOINT, 'POST');
        $result = json_decode($client, true);
        if (isset($result['error'])) {
            $this->throwException('REFRESH_TOKEN_PROBLEM: %s -> %s', $result['error'], $result['error_description']);
        }
        $result['refresh_token'] = $token['refresh_token'];
        $this->setToken($result);
    }

    /**
     * Determine if token still valid.
     *
     * @return bool
     */
    public function hasValidToken()
    {
        $token = $this->getToken();

        return !empty($token) && !$this->isTokenExpired($token);
    }

    /**
     * Determine if token is expired.
     *
     * @param array $token
     *
     * @return bool
     */
    public function isTokenExpired(array $token)
    {
        return empty($token[self::OAUTH_TOKEN_EXPIRES_AT])
            || $token[self::OAUTH_TOKEN_EXPIRES_AT] < time();
    }

    /**
     * Gets current token.
     *
     * @return array
     */
    public function getToken()
    {
        return $this->token;
    }
    public function setToken(array $token)
    {
        $this->token = $token;
    }
    protected function getOAuthAccessToken($code, $callback)
    {
        $client = $this->createHttpClient();
        $client->setParameters(array(
            'code'          => $code,
            'client_id'     => $this->apiKey,
            'client_secret' => $this->secret,
            'redirect_uri'  => $callback,
            'grant_type'    => 'authorization_code',
        ))->execute(self::OAUTH_TOKEN_ENDPOINT, 'POST');
        $result = json_decode($client, true);
        if (isset($result['error'])) $this->throwException('GET_TOKEN_PROBLEM: %s -> %s', $result['error'], $result['error_description']);
        $this->setToken($result);
        return $result;
    }
    protected function requestToken($callback)
    {
        $url = $this->getRequestTokenUrl($callback);
        if (headers_sent()) {
            echo '<meta http-equiv="refresh" content="0; url='.$url.'">'
                .'<script type="text/javascript">window.location.href = "'.$url.'";</script>';
        } else {
            header('Location: '.$url);
        }
        exit;
    }
    public function getRequestTokenUrl($callback)
    {
        $params = array(
            'response_type'          => 'code',
            'client_id'              => $this->apiKey,
            'redirect_uri'           => $callback,
            'scope'                  => self::OAUTH_SCOPE_PICASA,
            'state'                  => 'request_token',
            'approval_prompt'        => 'force',
            'access_type'            => 'offline',
            'include_granted_scopes' => 'true',
        );
        return self::OAUTH_ENDPOINT.'?'.http_build_query($params);
    }
    protected function doLogin()
    {
        $this->username = preg_replace('#@gmail\.com#i', '', $this->username);
        return true;
    }
    protected function doUpload()
    {
        $this->checkValidToken(__METHOD__);
        $endpoint = sprintf(self::ALBUM_FEED_ENPOINT, $this->username, $this->albumId).'?alt=json';
        $this->resetHttpClient()
            ->setHeaders($this->getGeneralHeaders())
            ->setSubmitNormal('image/jpeg')
            ->setHeaders('Slug', basename($this->file))
            ->setRawPostFile($this->file)
        ->execute($endpoint);
        $this->checkHttpClientErrors(__METHOD__);
        $result = json_decode($this->client, true);
        if ($this->client->getResponseStatus() != 201
            || empty($result['entry']['media$group']['media$content'][0])
        ) {
            $this->throwException('%s: Upload failed. %s', __METHOD__, $this->client);
        }
        // url, width, height, type
        extract($result['entry']['media$group']['media$content'][0]);
        $url = preg_replace('#/(s\d+/)?([^/]+)$#', '/s0/'.basename($this->file), $url);
        $url = preg_replace("/https:\/\/(.*)googleusercontent.com(.*)\/s0\/(.*)/i", "http://i2.cdn8.net$2/s0/$3", $url);
        return $url;
    }
    public function addAlbum($title, $access = 'public', $description = '')
    {
        $this->checkValidToken(__METHOD__);
        $endpoint = sprintf(self::USER_FEED_ENDPOINT, $this->username);
        $this->resetHttpClient()
            ->setHeaders($this->getGeneralHeaders())
            ->setSubmitNormal('application/atom+xml')
            ->setRawPost(sprintf("<entry xmlns='http://www.w3.org/2005/Atom' "
                    ."xmlns:media='http://search.yahoo.com/mrss/' "
                    ."xmlns:gphoto='http://schemas.google.com/photos/2007'>"
                ."<title type='text'>%s</title>"
                ."<summary type='text'>%s</summary>"
                .'<gphoto:access>%s</gphoto:access>'
                ."<category scheme='http://schemas.google.com/g/2005#kind' "
                    ."term='http://schemas.google.com/photos/2007#album'></category>"
                .'</entry>', $title, $description, $access))
        ->execute($endpoint);
        $this->checkHttpClientErrors(__METHOD__);
        return $this->getMatch('#<id>.+?albumid/(.+?)</id>#i', $this->client, 1, false);
    }
    protected function doTransload()
    {
        $this->throwException('%s: Currently, this plugin does not support transload image.', __METHOD__);
    }
    private function checkValidToken($method)
    {
        if (!$token = $this->getToken()) {
            $this->throwException('%s: You must have a valid token to perform this action.', __METHOD__);
        }
        if ($this->isTokenExpired($token)) {
            $this->refreshToken($token);
        }
    }
    public function getAuthorizationHeader()
    {
        $token = $this->getToken();
        return sprintf('%s %s', $token['token_type'], $token['access_token']);
    }
    private function getGeneralHeaders()
    {
        return array(
            'Authorization' => $this->getAuthorizationHeader(),
            'GData-Version' => '2',
            'MIME-version'  => '1.0',
        );
    }
}
