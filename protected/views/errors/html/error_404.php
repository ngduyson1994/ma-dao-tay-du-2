<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Basic Page Needs ================================================== -->
	<title>Không tìm thấy trang</title>
	<meta charset="utf-8">
	<!-- Favicons ================================================== -->
	<link rel="shortcut icon" href="<?=CDN?>/core/images/favicon.png">
	<!-- Mobile Specific Metas ================================================== -->
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
	<!-- Google Web Fonts ================================================== -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CSource+Sans+Pro:400,700" rel="stylesheet">
	<!-- CSS ================================================== -->
	<!-- Preloader CSS -->
	<!--link href="css/preloader.css" rel="stylesheet"-->
	<!-- Vendor CSS -->
	<link href="<?=CDN?>/core/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=CDN?>/core/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?=CDN?>/core/css/simple-line-icons.css" rel="stylesheet">
	<link href="<?=CDN?>/core/css/magnific-popup.css" rel="stylesheet">
	<link href="<?=CDN?>/core/css/slick.css" rel="stylesheet">
	<!-- Template CSS-->
	<link href="<?=CDN?>/core/css/style.css" rel="stylesheet">
</head>
<body>
<div class="site-content">
  <div class="container">
	<!-- Error 404 -->
	<div class="error-404">
	  <div class="row">
		<div class="col-md-8 col-md-offset-2">
		  <figure class="error-404__figure">
			<img src="<?=CDN?>/core/images/icon-ghost.svg" alt="">
		  </figure>
		  <header class="error__header">
			<h2 class="error__title">Trang này không khả dụng</h2>
			<h3 class="error__subtitle">Liên kết bạn truy cập có thể bị hỏng hoặc trang có thể đã bị xóa.</h3>
		  </header>
		  <footer class="error__cta">
			<a href="/" class="btn btn-primary">Quay lại trang chủ</a>
			<a href="/" class="btn btn-primary-inverse">Tiếp tục</a>
		  </footer>
		</div>
	  </div>
	</div>
	<!-- Error 404 / End -->

  </div>
</div>
</body>
</html>