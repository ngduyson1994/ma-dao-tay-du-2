<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<title>404 Not Found</title>
        <meta name="title" content="Không tìm thấy trang" />
        <meta name="description" content="Không tìm thấy nội dung" />
        <link href="<?=CDN?>/css/style.css?v=2939366" rel="stylesheet">
    </head>
    <body id="backtotop">
        <div id="content-wrapper">
			<div class="container">
				<div style="display:block;text-align:center;height:400px;padding:50px">
					<p style="font-size:50pt;font-weight:bold;margin-bottom:0;"><?php echo $heading; ?></p>
					<p style="font-size:12pt"><p><?php echo $message; ?></p></p>
				</div>    
			</div>	  
        </div>
        <div class="clearfix"></div>
	</body>
</html>