<div id="cactus-body-container">
    <div class="cactus-sidebar-control" id="err_404">
        <div class="cactus-container">
            <div class="cactus-row">
                <div class="main-content-col">
                    <div class="main-content-col-body">
                        <div class="single-page-content">
                            <article class="cactus-single-content page-404">
                                <h1 class="title-404">Oops! 404</h1>
                                <div class="content-404 sub-lineheight">
                                    <p style="font-size:25px">Trang này không khả dụng</p>
                                    <p>Truy cập của bạn có thể bị lỗi hoặc không tìm thấy nội dung</p>
                                </div>
                                <div class="gotohome-404">
                                    <a href="<?=URL?>" class="btn btn-default"><i class="fa fa-home"></i> Quay trở về trang chủ</a>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>