<div class="con clearfix">
    <div class="con-l">
        <div class="game-txt">
            <p class="p-text"><img src="<?=CDN?>/core/images/taigame.jpg" alt=""></p>
            <div class="download-btn">
                <a href="javascript:;" target="_blank" class="n1"><img src="<?=CDN?>/core/images/icon1.png" alt=""></a>
                <a href="javascript:;" target="_blank" class="n2"><img src="<?=CDN?>/core/images/icon3.png" alt=""></a>
                <a href="javascript:;" target="_blank" class="n3 reg px_reg"><img src="<?=CDN?>/core/images/icon2.png" alt=""></a>
            </div>
        </div>
        <div class="data">
            <ul>
                <li><a href="/list/data.html" target="_blank"><img src="http://p6.yx-s.com/t01a7132591f975bf17.png" alt=""><em class="w1"></em></a></li>
                <li><a href="/list/data.html" target="_blank"><img src="http://p9.yx-s.com/t010f7040c3982b3738.png" alt=""><em class="w2"></em></a></li>
                <li><a href="/list/data.html" target="_blank"><img src="http://p5.yx-s.com/t011b2e9bf931fad032.png" alt=""><em class="w3"></em></a></li>
                <li><a href="/list/data.html" target="_blank"><img src="http://p9.yx-s.com/t017f26020cc615e64c.png" alt=""><em class="w4"></em></a></li>
            </ul>
        </div><!--左侧资料-->
    </div>
    <div class="con-r">
        <div class="loc"><b class="tit"> <a href="#"><?=$title?></a> </b><?=$icon?>
        </div>
        <div class="newsList tabs">
            <div class="new-tab clearfix">
                <?php
                $tamp = $this->uri->segment(1);
                $news = '';
                $event = '';
                $guide = '';
                $server = '';
                if ($tamp == 'tin-tuc'){
                    $news = 'cur';
                } else if ($tamp == 'su-kien'){
                    $event = 'cur';
                } else if ($tamp == 'huong-dan'){
                    $guide = 'cur';
                } else if ($tamp == 'lich-mo-sv'){
                    $server = 'cur';
                }
                ?>
                <ul>
                    <?php if(isset($categories) && is_array($categories)) { ?>
                        <li class="<?=$news?>"><a href="<?php echo getCatURL($categories[0]['slug']); ?>"><?=$categories[0]['name']?></a></li>
                        <li class="<?=$event?>"><a href="<?php echo getCatURL($categories[1]['slug']); ?>"><?=$categories[1]['name']?></a></li>
                        <li class="<?=$guide?>"><a href="<?php echo getCatURL($categories[2]['slug']); ?>"><?=$categories[2]['name']?></a></li>
                        <li class="<?=$server?>"><a href="<?php echo getCatURL($categories[3]['slug']); ?>"><?=$categories[3]['name']?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="news-tag active" id="news">
                <ul class="public">
                    <?php if(isset($posts) && is_array($posts)) { ?>
                        <?php foreach($posts as $post) { ?>
                            <li><b>[Tin tức]</b><a href="<?=getPostURL($post)?>" target="_blank" title="<?=$post['title']?>"><?php echo clearString($post['title'],20);?></a><span>10-25</span></li>
                        <?php } } ?>
                </ul>
            </div>
        </div>
        <div class="page">
            <div id="paginate">
                <center>
                    <?=$this->post_model->all_pages;?>
                </center>
            </div>

        </div>
    </div>
</div>