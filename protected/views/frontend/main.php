<div class="cons clearfix">
    <?php widget('core/sliders') ?>
    <!--Sliders-->
    <div class="game-txt">
        <p class="p-text"><img src="<?=CDN?>/core/images/taigame.jpg" alt=""></p>
        <div class="download-btn">
            <a href="javascript:;" target="_blank" class="n1"><img src="<?=CDN?>/core/images/icon1.png" alt=""></a>
            <a href="javascript:;" target="_blank" class="n2"><img src="<?=CDN?>/core/images/icon3.png" alt=""></a>
            <a href="javascript:;" target="_blank" class="n3 reg px_reg"><img src="<?=CDN?>/core/images/icon2.png" alt=""></a>
        </div>
    </div>
    <div class="news">
        <?php widget('core/tab_news') ?>
    </div>
    <!--Tin tuc-->
</div>

<div class="zy-pro">
    <div class="zy-tab">
        <a href="javascript:;" class="cur s1"><span class="z1"></span></a>
        <a href="javascript:;" class="s2"><span class="z2"></span></a>
        <a href="javascript:;" class="s3"><span class="z3"></span></a>
        <a href="javascript:;" class="s4"><span class="z4"></span></a>
    </div>
    <div class="zy-con">
        <div class="zy-tac">
            <div class="zy-txt zy-txt1"></div>
        </div>
        <div class="zy-tac" style="display:none;">
            <div class="zy-txt zy-txt2"></div>
        </div>
        <div class="zy-tac" style="display:none;">
            <div class="zy-txt zy-txt3"></div>
        </div>
        <div class="zy-tac" style="display:none;">
            <div class="zy-txt zy-txt4"></div>
        </div>
    </div>
</div>

<div class="sidebar">
    <a class="android_btn" target="_blank" href="https://www.google.com/">Android</a>
    <a class="ios_btn" target="_blank" href="https://facebook.com/">iOS</a>
    <a class="apk_btn" target="_blank" href="https://youtube.com/">APK</a>
    <div class="social_btn">
        <a class="facebook_btn" href="#" target="_blank"><i class="fab fa-facebook-f"></i></a>
        <a class="forum_btn" href="#" target="_blank"><i class="fas fa-user-friends"></i></a>
        <a class="youtube_btn" href="#" target="_blank"><i class="fas fa-caret-right"></i></a>
    </div>
    <a class="giftcode_btn" target="_blank" href="https://portal.1am.us/">Giftcode</a>
</div>
<div class="hide-sidebar">
    <a href="javascript:;"><i class="fas fa-chevron-down"></i></a>
</div>
<div class="dataC clearfix">
    <!--游戏资料-->
    <div class="data-con">
        <ul class="data-con-list">
            <li class="data-list-1">
                <div class="data-list">
<!--                    <p class="data-list-txt"> <a href="/491078795/491078735/210602916.html" target="_blank"> 快速升级</a>-->
<!--                        <a href="/491078795/491078735/210602914.html" target="_blank"> 提升战力</a>-->
<!--                        <a href="/491078795/491078735/210602911.html" target="_blank"> 神将提升</a>-->
<!--                        <a href="/491078795/491078735/210602909.html" target="_blank"> ​时间分配</a>-->
<!--                        <a href="/491078795/491078735/210602907.html" target="_blank"> 妖神录</a>-->
<!--                        <a href="/491078795/491078735/210602905.html" target="_blank"> 战力提升</a>-->
<!--                        <a href="/491078795/491078735/210602903.html" target="_blank"> 升级技巧</a>-->
<!--                        <a href="/491078795/491078735/491078737.html" target="_blank"> 奖励多多</a>-->
<!--                        </p>-->
<!--                    <a href="/list/data.html" target="_blank" class="data-more un-more">更多&gt;&gt;</a> -->
                </div>
            </li>
            <li class="data-list-2">
                <div class="data-list data-list-2">
<!--                    <p class="data-list-txt"> <a href="/491078795/491078739/491083090.html" target="_blank"> 主城禅悟</a>-->
<!--                        <a href="/491078795/491078739/491083088.html" target="_blank"> 远古凶兽</a>-->
<!--                        <a href="/491078795/491078739/491083086.html" target="_blank"> 御法除妖</a>-->
<!--                        <a href="/491078795/491078739/491083084.html" target="_blank"> 五倍杀怪</a>-->
<!--                        <a href="/491078795/491078739/491083082.html" target="_blank"> 天降宝箱</a>-->
<!--                        <a href="/491078795/491078739/491083080.html" target="_blank"> 双倍护送</a>-->
<!--                        <a href="/491078795/491078739/491083073.html" target="_blank"> 领地争夺战</a>-->
<!--                        <a href="/491078795/491078739/491083071.html" target="_blank"> 跨服竞技</a>-->
<!--                        <a href="/491078795/491078739/491083067.html" target="_blank"> 个人竞技场</a>-->
<!--                        <a href="/491078795/491078739/491083016.html" target="_blank"> 堕落之地</a>-->
<!--                        <a href="/491078795/491078739/491082975.html" target="_blank"> 答题活动</a>-->
<!--                        <a href="/491078795/491078739/491082971.html" target="_blank"> 帮会争霸</a>-->
<!--                        <a href="/491078795/491078739/491082965.html" target="_blank"> 帮派BOSS</a>-->
<!--                        <a href="/491078795/491078739/491082949.html" target="_blank"> 三界突破</a>-->
<!--                        <a href="/491078795/491078739/491082941.html" target="_blank"> 结婚系统</a>-->
<!--                        <a href="/491078795/491078739/491082935.html" target="_blank"> ​化神秘境</a>-->
<!--                        <a href="/491078795/491078739/210602921.html" target="_blank"> 化神系统</a>-->
<!--                        </p>-->
<!--                    <a href="/list/data.html" target="_blank" class="data-more un-more">更多&gt;&gt;-->
                    </a>
				</div>
            </li>
            <li class="data-list-3">
                <div class="data-list data-list-3">
<!--                    <p class="data-list-txt"> <a href="/491078795/491078743/491082948.html" target="_blank"> 三界突破</a>-->
<!--                        <a href="/491078795/491078743/491082940.html" target="_blank"> 结婚系统</a>-->
<!--                        <a href="/491078795/491078743/491082934.html" target="_blank"> ​化神秘境</a>-->
<!--                        <a href="/491078795/491078743/210602920.html" target="_blank"> 化神系统</a></p>-->
<!--                    <a href="/list/data.html" target="_blank" class="data-more un-more">更多&gt;&gt;</a>-->
				</div>
            </li>
            <li class="data-list-4">
                <div class="data-list data-list-4">
<!--                    <p class="data-list-txt"> <a href="/491078795/491078747/491083012.html" target="_blank"> 坐骑系统</a>-->
<!--                        <a href="/491078795/491078747/491083007.html" target="_blank"> 足迹系统</a>-->
<!--                        <a href="/491078795/491078747/491082984.html" target="_blank"> 天羽系统</a>-->
<!--                        <a href="/491078795/491078747/491082982.html" target="_blank"> 神识系统</a>-->
<!--                        <a href="/491078795/491078747/491082979.html" target="_blank"> 神兵系统</a>-->
<!--                        <a href="/491078795/491078747/491082962.html" target="_blank"> 境界系统</a>-->
<!--                        <a href="/491078795/491078747/491082959.html" target="_blank"> 法盾系统</a>-->
<!--                        <a href="/491078795/491078747/491082917.html" target="_blank"> 法宝系统</a>-->
                        <!--高手进阶-->
<!--                    </p>-->
<!--                    <a href="/list/data.html" target="_blank" class="data-more un-more">更多&gt;&gt;</a>-->
				</div>
            </li>
        </ul>
    </div>
    <div class="show">
        <div class="show-list video">
            <ul class="clearfix">
                <li><a href="javascript:;" target="_blank"><img src="http://p6.yx-s.com/t01a707dcee9492db0e.png" alt="" width="300" height="153"><i class="ani"></i></a></li>
            </ul>
        </div>
        <div class="show-list">
            <ul class="clearfix">
                <li><a href="javascript:;" target="_blank"><img src="http://p5.yx-s.com/t01d9f8cf495a896c03.jpg" alt="" width="300" height="153"></a></li>
                <li><a href="javascript:;" target="_blank"><img src="http://p8.yx-s.com/t018a858b4e071f42df.png" alt="" width="300" height="153"></a></li>
            </ul>
        </div>
        <div class="show-list">
            <ul class="clearfix">
                <li><a href="javascript:;" target="_blank"><img src="http://p7.yx-s.com/t018a3110d5b19ca915.jpg" alt="" width="300" height="153"></a></li>
                <li><a href="javascript:;" target="_blank"><img src="http://p5.yx-s.com/t019d87e6ed1070220c.jpg" alt="" width="300" height="153"></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="service">
    <div class="ser ser-1">096999999</div>
    <div class="ser ser-2">madao@gtv.com </div>
    <div class="ser ser-3">096988888 </div>
    <div class="ser ser-4">Ma Đạo
        <div id="search"> <span class="select">Ma Đạo Tây Du</span>
            <ul class="sub">
                <li><a href="javascript:;" target="_blank">Ma đạo tây du</a></li>
                <li><a href="javascript:;" target="_blank">GameTV</a></li>
            </ul>
        </div>
    </div>
    </ul>
</div>
</div>