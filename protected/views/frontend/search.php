<section class="main_wrapper">
    <div class="row fourth-row">
        <div class="col-md-8 row-col">
            <div class="row row-title">
                <h5>SEARCH<?=$title?></h5>
            </div>
            <?php
            if(isset($posts) && is_array($posts)) {
                foreach($posts as $post) {
                    ?>
                    <div class="row latest-news-col">
                        <div class="col-md-6 row-col late-news-img">
                            <a href="<?php echo getPostURL($post);?>" title="<?=isset($post['name'])?>">
                                <img src="<?php echo getThumb($post, 380, 240);?>" />
                            </a>
                        </div>
                        <div class="col-md-6 row-col late-news-text">
                            <h4><a href="<?php echo getPostURL($post);?>" title="<?=isset($post['name'])?>"><?php echo cutOf(clearString($post['title']),100);?></a></h4>
                            <p class="news-summary"><?php echo cutOf(clearString($post['title']),100);?></p>
                            <p class="news-info"><span><?=(isset($post['admin']['username']) && $post['admin']['username'])?$post['admin']['username']:'GTV News'?></span> - <? echo showDate($post['published']);?></p>
                        </div>
                    </div>
                <?php } } ?>
            <div class="pagination">
                <?=$this->post_model->all_pages;?>
            </div>
        </div>
        <div class="col-md-4 row-col side-row">
            <? widget('core/livestream_list');?>
            <div class="adv-widget">
                <?php widget('ads', 5); ?>
            </div>
            <? widget('core/top_game');?>
        </div>
    </div>
    <div class="row fifth-row">
        <div class="adv-nav">
            <?php widget('ads', 6); ?>
        </div>
    </div>
</section>