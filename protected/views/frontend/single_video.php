<?php
$post_url = getPostURL($post);
$content = strip_tags($post['content']);
$content = str_replace(array('\n', PHP_EOL), '|', $content);
$list = explode("|", $content);
$i = 0;
$temp = false;
$videos = array();
foreach ($list as $video) {
    if (strlen($video) > 10) {
        $videos[$i][] = getYoutubeID($video);
        $temp = true;
    } else if ($temp) {
        $i++;
        $temp = false;
    }
}
$set = intval($this->input->get('set'));
$match = intval($this->input->get('match'));
$set = ($set) ? $set - 1 : $set;
$match = ($match) ? $match - 1 : $match;
?>
<section class="main_wrapper">
    <div class="row">
        <div class="player-header" id="zplayer"></div>
    </div>
    <div class="row">
        <div class="col-md-8 row-col">
            <div class="core-section">
                <div class="tour-main">
                    <div class="tour-header">
                        <h4><?php echo clearString($post['title']); ?></h4>
                    </div>
                    <?php if ($post['home_id'] > 0) { ?>
                        <div class="tour-section">
                            <div class="tour-section-item tour-team">
                                <div class="tour-team-name">
                                    <h6><?= $post['home']['name'] ?></h6>
                                </div>
                                <div class="tour-team-img">
                                    <img src="<?= $post['home']['avatar'] ?>" width="90" height="90" />
                                </div>
                            </div>
                            <div class="tour-section-item tour-detail">
                                <h1 class="h1vs">VS</h1>
                            </div>
                            <div class="tour-section-item tour-team">
                                <div class="tour-team-img">
                                    <img src="<?= $post['away']['avatar'] ?>" width="90" height="90" />
                                </div>
                                <div class="tour-team-name">
                                    <h6><?= $post['away']['name'] ?></h6>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="single-video-info">
                        <script src="https://www.youtube.com/player_api"></script>
                        <?php if (strlen($content) <= 43) { ?>
                            <div class="style-post">
                                <script>
                                    function onPlayerStateChange(event) {
                                        if (event.data == YT.PlayerState.ENDED) {
                                            window.location.href = "<?php echo getPostURL($post_realted[1]);?>";
                                        }
                                    }

                                    var playerYTB = document.querySelector('iframe');

                                    function onYouTubeIframeAPIReady() {
                                        playerYTB = new YT.Player('zplayer', {
                                            height: '415px',
                                            width: '100%',
                                            videoId: '<?=$videos[0][0]?>',
                                            playerVars: {
                                                'autoplay': 1,
                                                'controls': 1
                                            },
                                            events: {
                                                'onStateChange': onPlayerStateChange
                                            }
                                        });
                                        playerYTB.current_video = 0;
                                    }

                                    jQuery('[data-video]').click(function() {
                                        playerYTB.current_video = jQuery(this).data('number');
                                        playVideoYTB();
                                    })

                                    function playVideoYTB() {
                                        jQuery('.part').removeClass('active');
                                        jQuery('#video_' + playerYTB.current_video).addClass('active');
                                        var video_id = jQuery('[data-video]').eq(playerYTB.current_video).attr('data-video');
                                        playerYTB.loadVideoById(video_id, 0, "large")
                                    }
                                </script>
                            </div>
                        <?php } else if (strlen($content) > 43) { ?>
                            <div class="style-post">
                                <div class="ct-series edisodes-style">
                                    <?php
                                    $i = 1;
                                    $k = 1;
                                    foreach ($videos as $list) {
                                        $j = 1;
                                        if (!$set && $i == 1) $class = ' active"';
                                        elseif ($i == $set + 1) $class = ' active"';
                                        else $class = '';
                                        ?>
                                        <br>
                                        <div class="series-content">
                                            <div class="series-content-row">
                                                <div class="series-content-item">
                                                    <div class="content-title"></div>
                                                    <div class="content-epls">
                                                        <h6 class="h6c" style="margin-right: 100px"><i class="fas fa-play"
                                                                                                       id="h6c"></i>C<?= $i ?>:&ensp;&ensp;
                                                            <?php
                                                            $active = false;
                                                            foreach ($list as $video) {
                                                                if ($i == $set + 1 && $j == $match + 1) {
                                                                    $class = ' active';
                                                                    $id = 'id="current_video"';
                                                                    $active = true;
                                                                } else $id = $class = '';
                                                                echo '<a class="part ' . $class . '" id="video_' . ($k - 1) . '" href="javascript:;" data-video="' . $videos[$i - 1][$j - 1] . '" data-number="' . ($k - 1) . '"> <span class="match">Trận </span>' . $j++ . '</a>';
                                                                $k++;
                                                            }
                                                            $i++;
                                                            ?></h6>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <script>
                                    function onPlayerStateChange(event) {
                                        if (event.data == YT.PlayerState.ENDED) {
                                            playerYTB.current_video++;
                                            playVideoYTB()
                                        }
                                    }

                                    var playerYTB = document.querySelector('iframe');

                                    function onYouTubeIframeAPIReady() {
                                        playerYTB = new YT.Player('zplayer', {
                                            height: '415px',
                                            width: '100%',
                                            videoId: '<?=$videos[0][0]?>',
                                            playerVars: {
                                                'autoplay': 1,
                                                'controls': 1
                                            },
                                            events: {
                                                'onStateChange': onPlayerStateChange
                                            }
                                        });
                                        playerYTB.current_video = 0;
                                    }

                                    jQuery('[data-video]').click(function() {
                                        playerYTB.current_video = jQuery(this).data('number');
                                        playVideoYTB();
                                    })

                                    function playVideoYTB() {
                                        jQuery('.part').removeClass('active');
                                        jQuery('#video_' + playerYTB.current_video).addClass('active');
                                        var video_id = jQuery('[data-video]').eq(playerYTB.current_video).attr('data-video');
                                        playerYTB.loadVideoById(video_id, 0, "large")
                                    }
                                </script>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="tour-section hashtag">
                        <?php if (count($post['tags'])) { ?>
                            <div class="tagnew">
                                <b>Xem thêm:</b>
                                <?php
                                foreach ($post['tags'] as $tag) {
                                    ?>
                                    <h3><a href="<?= URL . '/tag/' . trim($tag['slug']) ?>"
                                           title="dying light 2"><?= trim($tag['name']) ?></a></h3>
                                    <?php
                                }
                                ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <script>
                    $(document).ready(function() {
                        $(".video-related").slick({
                            dots: false,
                            infinite: true,
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            prevArrow: $('.prev'),
                            nextArrow: $('.next'),
                        });
                    });
                </script>
                <div class="related-news">
                    <div class="row row-title" id="video_rela">
                        <h5>Trận đấu khác</h5>
                        <div class="prev-next-slider elms-right" style="display:block">
                            <a href="javascript:void(0)" class="btn btn-default ct-gradient bt-action metadata-font font-size-1 icon-smart control-prev prev"><i class="fa fa-angle-left"></i></a>
                            <a href="javascript:void(0)" class="btn btn-default ct-gradient bt-action metadata-font font-size-1 icon-smart control-next next"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    <div class="row video-list video-related">
                        <?php
                        if (isset($post_realted) && is_array($post_realted)) {
                            foreach ($post_realted as $post_r) {
                                ?>
                                <div class="col-md-4 row-col">
                                    <div class="video-item">
                                        <a href="<?php echo getPostURL($post_r); ?>">
                                            <i class="icon-play"></i>
                                            <img src="<?php echo getThumb($post_r, 277, 156); ?>" />
                                            <p><?php echo cutOf(clearString($post_r['title']), 80); ?></p>
                                        </a>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="related-news">
                    <div class="row row-title" id="video_rela">
                        <h5>Bài viết liên quan</h5>
                    </div>
                    <div class="row video-list video-related">
                        <?php
                        if (isset($post_realted_news) && is_array($post_realted_news)) {
                            foreach ($post_realted_news as $post_r) {
                                ?>
                                <div class="col-md-4 row-col">
                                    <div class="video-item">
                                        <a href="<?php echo getPostURL($post_r); ?>">
                                            <img src="<?php echo getThumb($post_r, 277, 156); ?>" />

                                            <p><?php echo cutOf(clearString($post_r['title']), 80); ?></p>
                                        </a>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 row-col side-row">
            <?= widget('core/livestream_list'); ?>
            <?= widget('core/video_right'); ?>
        </div>
        <p class="top_p">
            Bình luận về bài viết
        </p>
        <div class="fb-comments" data-href="http://gametv.vn/video/<?=$post['slug']?>" data-width="100%" data-numposts="10"></div>

    </div>
    <div class="row adv-row-live">
        <div class="col-md-4 row-col">
            <div class="adv adv-section-2">
                <?php widget('ads', 9); ?>
            </div>
        </div>
        <div class="col-md-4 row-col">
            <div class="adv adv-section-2">
                <?php widget('ads', 10); ?>
            </div>
        </div>
        <div class="col-md-4 row-col">
            <div class="adv adv-section-2">
                <?php widget('ads', 11); ?>
            </div>
        </div>
    </div>
</section>