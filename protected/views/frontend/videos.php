	<section class="main_wrapper">
		<div class="row">
			<div class="col-md-8 row-col">
				<div class="core-section">
					<div class="video-type-name">
						<h4>Video</h4>
					</div>
					<!--div class="video-filter">
						<div class="filter-selection type-filter">
							<p class="filter-item">Type</p>
							<select id="type" name="type" class="filter-item">
								<option value="1">All</option>
								<option value="2">None</option>
								<option value="3">Half</option>
							</select>
						</div>
						<div class="filter-selection platform-filter">
							<p class="filter-item">Platform</p>
							<select id="platform" name="platform" class="filter-item">
								<option value="1">All</option>
								<option value="2">None</option>
								<option value="3">Half</option>
							</select>
						</div>
						<div class="filter-selection lang-filter">
							<p class="filter-item">Language</p>
							<select id="lang" name="lang" class="filter-item">
								<option value="1">All</option>
								<option value="2">None</option>
								<option value="3">Half</option>
							</select>
						</div>
						<input type="submit" name="submit" value="FILTER" id="video-filter" />
					</div-->
					<div class="row video-list">
						<?php
						if(isset($posts) && is_array($posts)) {
							foreach($posts as $post) {
							?>
							<div class="col-md-4 row-col">
								<div class="video-item">
									<a href="<?php echo getPostURL($post);?>">
                                        <i class="icon-play"></i>
										<img src="<?php echo getThumb($post, 760, 410);?>" />
										<p><?php echo cutOf(clearString($post['title']),180);?></p>
									</a>
								</div>
							</div>
					<?php } } ?>
					</div>
					<div id="paginate">
						<center>
							<?=$this->post_model->all_pages;?>
						</center>
					</div>
				</div>
			</div>
			<div class="col-md-4 row-col side-row">
				<? widget('core/livestream_list');?>
				<? widget('core/top_game');?>
			</div>
		</div>
	</section>