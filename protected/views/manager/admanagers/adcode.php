<?php
	if(permission('ads')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>AdCode </strong> manager</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <div class="pull-left">
										<a id="table-edit_new" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="renderadcode();">
                                            Add New <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
									<form id="form-search" method="GET">
									<div class="row">
										<div class="col-xs-9"></div>
										<div class="col-xs-3">
											<div class="input-group">
												<input type="text" class="form-control" name="search" value="<?=($this->input->get('search'))?$this->input->get('search'):''?>" placeholder="Search">
												<span class="input-group-addon bg-blue" style="cursor: pointer" onClick="$('#form-search').submit();">     
													<span class="arrow"></span><i class="fa fa-search"></i> 
												</span>
											</div>
										</div>
									</div>
									</form>
                                    <table class="table table-striped table-hover dataTable" id="table-editable">
                                        <thead>
                                            <tr role="row">
                                                <th class="align-center">ID</th>
                                                <th class="align-center">Name</th>
                                                <th class="align-center">Created</th>
                                                <th class="align-center">Width</th>
                                                <th class="align-center">Height</th>
                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
											$i=1;
											foreach($adcodes as $adcode) {
										?>
                                            <tr id="adcode-<?=$adcode['id']?>">
                                                <td class="align-center">
													<?=$i++;?>
												</td>
                                                <td class="align-center"><a style="color: #0090D9;" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="renderadcode(<?=$adcode['id']?>);"><? echo cutOf($adcode['name'],50);?></a></td>
                                                <td class="align-center"><?=date("d/m/Y",$adcode['created'])?></td>
                                                <td class="align-center"><?=$adcode['width'];?></td>
                                                <td class="align-center"><?=$adcode['height'];?></td>
                                                <td class="align-center">
													<a class="edit btn btn-sm btn-dark" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="renderadcode(<?=$adcode['id']?>);"><i class="fa fa-pencil-square-o"></i></a>
													<button class="delete btn btn-sm btn-danger" href="javascript:;" onClick="removeadcode(<?=$adcode['id']?>);"><i class="fa fa-times-circle"></i></button>
                                                </td>
                                            </tr>
											<?php 
												}
											?>
                                        </tbody>
                                    </table>
									<div class="col-xs-12" style="line-height: 24px;">
										<div class="col-sm-2"> Have <?=$total?> item</div>
										<?=$this->adcode_model->all_pages;?>
									</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- the overlay element -->
		</div>
		
		<div class="modal fade" id="item-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
			<form method="POST">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel"><strong>Edit</strong></h4>
					</div>
					<div class="modal-body">
						<div id="edit-content">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input type="hidden" name="id" value="" />
									<div class="form-group">
										<label for="name" class="control-label bold">Name</label>
										<div class="controls">
											<input type="text" name="name" value="" id="name" label="Name" required="" class="form-control" /> 
										</div>
									</div>
									<div class="form-group">
										<label for="width" class="control-label bold">Width</label>
										<div class="controls">
											<input type="text" name="width" value="" id="width" label="width"  required="" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label for="height" class="control-label bold">Height</label>
										<div class="controls">
											<input type="text" name="height" value="" id="height" label="height"  class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<div class="controls" style="width:48%; float:left">
											<label for="link" class="control-label bold">Link</label>
											<input type="text" name="link" value="" id="link" label="Link"  class="form-control" />
										</div>
										<div class="controls" style="width:50%;float:right">
											<label for="height" class="control-label bold">Img</label>
											<input type="text" name="image" value="" id="image" label="Image"  class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<button type="button" class="btn btn-success" value="" style="float: right; margin-top:2px" onClick="generate_code()">Generate Code</button>
									</div>
									<div class="form-group">
										<label for="code" class="control-label bold">Code</label>
										<div class="controls">
											<textarea id="code" rows="3"  style="width:100%; height: 72px" name="code"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-success" value="Save changes">
					</div>
				</div>
			</form>
			</div>
		</div>
		
		<!-- END MAIN CONTENT -->
		<script src="<?=CDN?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script>
			$('#created').datepicker();
			function removeadcode(id) {
				if (confirm("Are you want to delete this adcode permanent?")) {
					$.post("<?=URL?>/manager/adcodes/ajax/delete", {id:id},
					function(data,status){
						if(data=="Deleted") {
							$("#adcode-"+id).hide();
						}
					});
				}
			}
			
			function renderadcode(id) {
				if(id) {
					$.post("<?=URL?>/manager/adcodes/ajax/get", {id:id},
					function(data,status){
						var adcode = jQuery.parseJSON(data);
						$("input[name=id]").val(adcode.id);
						$("#name").val(adcode.name);
						$("#width").val(adcode.width);
						$("#height").val(adcode.height);
						$("#code").val(atob(adcode.code));
					});
				} else {
					$("#id").val(0);
					$("#name").val('');
					$("#width").val('');
					$("#height").val('');
					$("#code").val('');
				}
			}
			
			function generate_code() {
				var image = $('#image').val();
				var link = $('#link').val();
				if(image && link) {
					var img = new Image();
					img.src = image;
					img.onload = function(){
						$('#width').val(this.width);
						$('#height').val(this.height);
					};
					$("#code").val('<a href="'+link+'" target="_blank"><img src="'+image+'" /></a>');
				}
			}
		</script>
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>