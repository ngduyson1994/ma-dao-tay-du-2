<?php
	if(permission('ads')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Ads </strong> manager</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
									<div class="col-md-12 m-b-20" style="padding-left: 0">
										<div class="pull-left">
											<a id="table-edit_new" class="btn btn-sm btn-danger" href="javascript:;" onClick="clearForm();">
												Add New <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<div class="col-md-3 m-b-20">
										
									</div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red" style="padding-left: 0">
									<form method="POST">
										<div class="col-md-3 col-sm-3 col-xs-3" style="padding-left: 0">
											<input id="id" name="id" type="hidden">
											<div class="form-group">
												<label class="form-label"><strong>AdZone  <span id="z_id"></span></strong></label>
												<div class="controls">
													<select id="zid" name="zid" class="form-control" data-live-search="true" data-style="input-sm btn-default">
														<option value="0">Select AdZone</option>
														<?php foreach($adzones as $adzone) { ?>
															<option value="<?=$adzone['id']?>"><?=$adzone['name']?> [<?=$adzone['width']?>x<?=$adzone['height']?>]</option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="form-group" id="category-box">
												<label class="form-label"><strong>AdCode</strong></label>
												<div class="controls">
													<select id="cid" name="cid" class="form-control" data-live-search="true" data-style="input-sm btn-default">
														<option value="0">Select AdCode</option>
														<?php foreach($adcodes as $adcode) { ?>
															<option value="<?=$adcode['id']?>"><?=$adcode['name']?> [<?=$adcode['width']?>x<?=$adcode['height']?>]</option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="form-label"><strong>Expired</strong></label>
												<div class="controls">
												   <input name="expired" id="expired" class="input-s-sm input-s form-control" style="width:100%;float:left;margin-right:2px; margin-bottom: 10px;" data-date-format="dd-mm-yyyy" type="text" >
												</div>
											</div>
											<div class="form-group">
												<label class="form-label"><strong>Weight</strong></label>
												<div class="controls">
												   <input name="weight" id="weight" type="text" class="form-control input-sm">
												</div>
											</div>
											<div class="pull-right" >
												<input type="submit" id="submit" class="btn btn-success" value="Add New">
											</div>
										</div>
									</form>
									<div class="col-md-9 col-sm-9 col-xs-9">
										<table class="table table-striped table-hover dataTable" id="table-editable">
											<thead>
												<tr role="row">
												</tr>
											</thead>
											<tbody>
												<?php
													foreach($adzones as $adzone){
												?>
												<tr>
													<td colspan="2"><a style="color:#333; font-weight: bold; font-size:16px" href="javascript:;" onClick="renderAdZone(<?=$adzone['id']?>);"><?=$adzone['name']?> [<?=$adzone['width']?>x<?=$adzone['height']?>]</a></td>
													<td class="align-center"><b>Expired</b></td>
													<td class="align-center"><b>Weight</b></td>
													<td class="align-center"><b>Action</b></td>
												</tr>
												<?php
													if(isset($adzone['link']) && count($adzone['link'])) {
														foreach($adzone['link'] as $ads) {
														$style = ($ads['expired'] && $ads['expired']<time())?'style="color: #C60000"':'style="color: #333"';
												?>
												<tr id="ads-<?=$ads['id']?>">
													<td><i class="fa fa-caret-right"></i></td>
													<td>
														<a <?=$style?> href="javascript:;" onClick="render(<?=$ads['id']?>);">
															<?=$ads['adcodes']['name']?>
														</a>
													</td>
													<td class="align-center"><?=($ads['expired'])?date("d/m/Y",$ads['expired']):'Never'?></td>
													<td class="align-center"><?=$ads['weight']?></td>
													<td class="align-center">
														<a class="edit btn btn-sm btn-dark" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="renderadcode(<?=$ads['adcodes']['id']?>);"><i class="fa fa-pencil-square-o"></i></a>
														<button class="delete btn btn-sm btn-danger" href="javascript:;" onClick="removeads(<?=$ads['id']?>);"><i class="fa fa-times-circle"></i></button>
													</td>
												</tr>
														<? }
												} else echo '<tr><td colspan="5">None</td></tr>';
												?>
												<?php 
													}
												?>
											</tbody>
										</table>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           <!-- the overlay element -->
		</div>
		<div class="modal fade" id="item-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
			<form method="POST">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel"><strong>Edit</strong></h4>
					</div>
					<div class="modal-body">
						<div id="edit-content">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input type="hidden" name="id" value="" />
									<div class="form-group">
										<label for="name" class="control-label bold">Name</label>
										<div class="controls">
											<input type="text" name="name" value="" id="name" label="Name" required="" class="form-control" /> 
										</div>
									</div>
									<div class="form-group">
										<label for="width" class="control-label bold">Width</label>
										<div class="controls">
											<input type="text" name="width" value="" id="width" label="width"  required="" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label for="height" class="control-label bold">Height</label>
										<div class="controls">
											<input type="text" name="height" value="" id="height" label="height"  class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<div class="controls" style="width:48%; float:left">
											<label for="link" class="control-label bold">Link</label>
											<input type="text" name="link" value="" id="link" label="Link"  class="form-control" />
										</div>
										<div class="controls" style="width:50%;float:right">
											<label for="height" class="control-label bold">Img</label>
											<input type="text" name="image" value="" id="image" label="Image"  class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<button type="button" class="btn btn-success" value="" style="float: right; margin-top:2px" onClick="generate_code()">Generate Code</button>
									</div>
									<div class="form-group">
										<label for="code" class="control-label bold">Code</label>
										<div class="controls">
											<textarea id="code" rows="3"  style="width:100%; height: 72px" name="code"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-success" value="Save changes">
					</div>
				</div>
			</form>
			</div>
		</div>
		<script type="text/javascript" src="<?=CDN?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script type="text/javascript">
			$('#expired').datepicker();
		</script>
		<script>
			function removeads(id) {
				if (confirm("Are you want to delete this ads permanent?")) {
					$.post("<?=URL?>/manager/adlinks/ajax/delete", {id:id},
					function(data,status){
						if(data=="Deleted") {
							$("#ads-"+id).hide();
						}
					});
				}
			}
			function renderAdZone(id) {
				data = '';
				$.post("<?=URL?>/manager/adzones/ajax/get", {id:id},
				function(data, status) {
					var data = jQuery.parseJSON(data);
					$("#z_id").html(data.id);
					$("#zid").val(data.id);
					$("#zid").change();
					$("#cid").val(0);
					$("#cid").change();
					$("#expired").val('');
					$("#weight").val('');
					$("#submit").val('Add new');
				});
			}
			function render(id) {
				data = '';
				$.post("<?=URL?>/manager/adlinks/ajax/get", {id:id},
				function(data,status){
					var data = jQuery.parseJSON(data);
					$("#id").val(data.id);
					$("#z_id").html(data.zid);
					$("#zid").val(data.zid);
					$("#zid").change();
					$("#cid").val(data.cid);
					$("#cid").change();
					if(data.expired>0) {
						var date = new Date(data.expired*1000);
						var year = date.getFullYear();
						var month = date.getMonth()+1;
						var date = date.getDate();
						$("#expired").val(date+"-"+month+"-"+year);
						$("#expired").change();
					} else $("#expired").val('');
					$("#weight").val(data.weight);
					$("#submit").val('Update');
				});
			}
			function clearForm() {
					$("#id").val(0);
					$("#z_id").html('');
					$("#zid").val('');
					$("#zid").change();
					$("#cid").val('');
					$("#cid").change();						
					$("#expired").val('');
					$("#expired").change();
					$("#weight").val('');
					$("#submit").val('Add new');
			}
			
			function renderadcode(id) {
				if(id) {
					$.post("<?=URL?>/manager/adcodes/ajax/get", {id:id},
					function(data,status){
						var adcode = jQuery.parseJSON(data);
						$("input[name=id]").val(adcode.id);
						$("#name").val(adcode.name);
						$("#width").val(adcode.width);
						$("#height").val(adcode.height);
						$("#code").val(atob(adcode.code));
					});
				} else {
					$("#id").val(0);
					$("#name").val('');
					$("#width").val('');
					$("#height").val('');
					$("#code").val('');
				}
			}
			
			function generate_code() {
				var image = $('#image').val();
				var link = $('#link').val();
				if(image && link) {
					var img = new Image();
					img.src = image;
					img.onload = function(){
						$('#width').val(this.width);
						$('#height').val(this.height);
					};
					$("#code").val('<a href="'+link+'" target="_blank"><img src="'+image+'" /></a>');
				}
			}
		</script>
		<!-- END MAIN CONTENT -->
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>