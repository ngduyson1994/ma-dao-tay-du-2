<?php
	if(permission('ads')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>AdZone </strong> manager</h3>
                        </div>
                        <div class="panel-body">
							<div class="row">
								<div class="col-md-12 m-b-20">
									<div class="pull-left">
                                        <a id="table-edit_new" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#item-edit" href="javascript:;" 
										onClick="renderadzone();">
                                            Add New <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
								</div>
							</div>
							<div class="row">
								
								 <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
                                    <table class="table table-striped table-hover dataTable" id="table-editable">
                                        <thead>
                                            <tr role="row">
                                                <th class="align-center">ID</th>
                                                <th class="align-center">Name</th>
                                                <th class="align-center">Width</th>
                                                <th class="align-center">Height</th>
                                                <th class="align-center">Page</th>
                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
											$i=1;
											foreach($adzones as $adzone)
											{
										?>
                                            <tr id="adzone-<?=$adzone['id']?>">
                                                <td class="align-center">
													<?=$adzone['id']?>
												</td>
                                                <td class="align-center"><a style="color: #0090D9;" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="renderadzone(<?=$adzone['id']?>);"><?=$adzone['name']?></a></td>
                                                <td class="align-center"><?=$adzone['width']?></td>
                                                <td class="align-center"><?=$adzone['height']?></td>
                                                <td class="align-center"><?=$adzone['page']?></td>
                                                <td class="align-center">
													<a class="edit btn btn-sm btn-dark" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="renderadzone(<?=$adzone['id']?>);"><i class="fa fa-pencil-square-o"></i></a>
													<button class="delete btn btn-sm btn-danger" href="javascript:;" onClick="removeadzone(<?=$adzone['id'];?>);"><i class="fa fa-times-circle"></i></button>
                                                </td>
                                            </tr>
											<?php 
												}
											?>
                                        </tbody>
                                    </table>
									<div class="col-xs-12" style="line-height: 24px;">
										<div class="col-sm-2"> Tổng:  <?=$total?> phương thức</div>
										<?=$this->adzone_model->all_pages;?>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="modal fade" id="item-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
				<form method="POST">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel"><strong>Edit</strong></h4>
                        </div>
                        <div class="modal-body">
							<div id="edit-content">
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="hidden" id="id" name="id" value="" />
											<div class="form-group">
												<label for="name" class="control-label bold">Name</label>
												<div class="controls">
													<input type="text" name="name" value="" id="name" label="Name" required="" class="form-control" /> 
												</div>
											</div>
											<div class="form-group">
												<label for="width" class="control-label bold">Width</label>
												<div class="controls">
													<input type="text" name="width" value="" id="width" label="width"  required="" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="height" class="control-label bold">Height</label>
												<div class="controls">
													<input type="text" name="height" value="" id="height" label="height"  class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="page" class="control-label bold">Page</label>
												<div class="controls">
													<input type="text" name="page" value="" id="page" label="page"  required="" class="form-control" />
												</div>
											</div>
									</div>
								</div>
							</div>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Save changes">
                        </div>
                    </div>
				</form>
                </div>
            </div>
            <script src="<?=CDN?>/cpanel/plugins/nestable/jquery.nestable.js"></script>
			<script>
				function removeadzone(id) {
					if (confirm("Are you want to delete this adzone permanent?")) {
						$.post("<?=URL?>/manager/adzones/ajax/delete", {id:id},
						function(data,status){
							if(data=="Deleted") {
								$("#adzone-"+id).hide();
							}
						});
					}
				}
				function renderadzone(id) {
					if(id) {
						$.post("<?=URL?>/manager/adzones/ajax/get", {id:id},
						function(data,status){
							var adzone = jQuery.parseJSON(data);
							$("input[name=id]").val(adzone.id);
							$("#name").val(adzone.name);
							$("#width").val(adzone.width);
							$("#height").val(adzone.height);
							$("#description").val(adzone.description);
							$("#page").val(adzone.page);
						});
					} else {
						$("input[name=id]").val(0);
						$("#name").val('');
						$("#width").val('');
						$("#height").val('');
						$("#description").val('');
						$("#page").val('');
					}
				}
			</script>
            <!-- the overlay element -->
		</div>
		<!-- END MAIN CONTENT -->
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>