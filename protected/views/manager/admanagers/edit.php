<?php
	if(permission('ads')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong><?=(isset($adcode))?'Edit AdCode':'Add AdCode'?> </strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <div class="btn-group pull-left">
                                        <a id="table-edit_new" class="btn btn-danger btn-sm" href="javascript:window.history.back();">
                                            <i class="fa fa-arrow-left"></i> Back
                                        </a>
                                    </div>
                                    <div class="btn-group col-md-11">
									<? if(isset($return)) { ?>
										<div class="alert alert-null <?=($return['status']=='success')?'alert-info':'alert-danger';?> pull-right">
											<button type="button" class="close" data-dismiss="alert" style="padding-left:3px;">×</button>
											<?=$return['msg']?>
										</div>
									<? } ?>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
									<form method="POST" enctype="multipart/form-data">
										<div id="edit-content">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="col-md-4" style="padding-left: 0">
														<div class="form-group">
															<div class="controls">
																<div class="col-md-2" style="padding:0">
																	<label class="control-label"><strong>Name</strong></label>
																</div>
																<div class="col-md-10" style="padding:0">
																	<input type="text" value="<?=(isset($adcode))?$adcode['name']:'';?>" class="form-control" id="name" name="name"
																		placeholder="Enter name here">
																	<input id="id" name="id" value="<?=(isset($adcode))?$adcode['id']:'';?>" type="hidden">
																</div>
																<div class="clearfix"></div>
															</div>
														</div>
														<div class="form-group">
															<div class="controls">
																<div class="col-md-2" style="padding:0">
																	<label class="control-label"><strong>Width</strong></label>
																</div>
																<div class="col-md-10" style="padding:0">
																		<input type="text" value="<?=(isset($adcode))?$adcode['width']:'';?>" class="form-control" id="width" name="width">
																</div>
																<div class="clearfix"></div>
															</div>
														</div>
														<div class="form-group">
															<div class="controls">
																<div class="col-md-2" style="padding:0">
																	<label class="control-label"><strong>Height</strong></label>
																</div>
																<div class="col-md-10" style="padding:0">
																		<input type="text" value="<?=(isset($adcode))?$adcode['height']:'';?>" class="form-control" id="height" name="height">
																</div>
																<div class="clearfix"></div>
															</div>
														</div>
													</div>
													<div class="col-md-8" style="padding-left: 15px">
														<div class="form-group">
															<div class="controls">
																<div class="col-md-1">
																	<label class="control-label"style="margin-left:5px"><strong>Link:</strong></label>
																</div>
																<div class="col-md-4" style="padding:0">
																	<input type="text" value="" class="form-control" id="link" name="link"
																		placeholder="Enter link here">
																</div>
																<div class="col-md-1">
																	<label class="control-label"><strong>Img:</strong></label>
																</div>
																<div class="col-md-4" style="padding:0">
																	<input type="text" value="" class="form-control" id="image" name="image"
																		placeholder="Enter link here">
																</div>
																<button type="button" class="btn btn-success" value="" style="float: right;" onClick="generate_code()">Generate</button>
																<div class="clearfix"></div>
															</div>
														</div>
														 <div class="form-group">
															<div class="controls">
																<div class="col-md-1" style="padding:0">
																	<label class="control-label"><strong>Code</strong></label>
																</div>
																<div class="col-md-11" style="padding:0">
																	<textarea id="code" rows="3"  style="width:100%; height: 72px" name="code"><?=(isset($adcode))?base64_decode($adcode['code']):'';?></textarea>
																</div>
																<div class="clearfix"></div>
															</div>
														</div>
													</div>
												</div>
												<?php if(isset($adcode['code'])) { ?>
												<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="col-md-1" style="padding-left: 0">										
														<label class="control-label"><strong>Show</strong></label>
													</div>
													<div class="col-md-2" style="padding-left: 0">												
														<div id="show"><?php echo base64_decode($adcode['code']);?></div>
													</div>
												</div>
												<?php } ?>
											</div>
										</div>
										<div class="modal-footer">
											<input type="submit" class="btn btn-success" value="Save">
										</div>
									</form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		<!-- END MAIN CONTENT -->
		<script type="text/javascript">
			function generate_code() {
				var image = $('#image').val();
				var link = $('#link').val();
				if(image && link) {
					var img = new Image();
					img.src = image;
					img.onload = function(){
						$('#width').val(this.width);
						$('#height').val(this.height);
					};
					$("#code").val('<a href="'+link+'" target="_blank"><img src="'+image+'" /></a>');
				}
			}
		</script>
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>