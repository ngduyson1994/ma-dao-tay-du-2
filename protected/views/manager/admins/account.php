<?php
	if(permission('posts')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Đổi mật khẩu</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<?php
									if(isset($list['name'])) $name = $list['name'];
									else $name = '';
								?>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
									<form method="POST" enctype="multipart/form-data">
										<div id="edit-content">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0">
													</div>
													<div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0">
														<div class="form-group">
															<label class="form-label"><strong>Họ tên</strong></label>
															<div class="controls">
																<input id="name" name="name" value="<?=$name;?>" type="text" class="form-control input-sm">
																<input id="id" name="id" value="<?=(isset($list))?$list['id']:'';?>" type="hidden">
															</div>
														</div>
														<div class="form-group">
															<label class="form-label"><strong>Mật khẩu</strong></label>
															<div class="controls">
																<input name="password" id="password" type="password"  value="" class="form-control input-sm">
															</div>
														</div>
														<div class="form-group">
															<label class="form-label"><strong>Email</strong></label>
															<div class="controls">
																<input name="email" id="email" value="<?=(isset($list))?$list['email']:'';?>" type="text" class="form-control input-sm">
															</div>
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0">
													</div>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<input type="submit" name="submit" class="btn btn-success" value="Save">
										</div>
									</form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		<!-- END MAIN CONTENT -->
		<script src="<?=CDN?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script src="<?=CDN?>/cpanel/plugins/filestyle/bootstrap-filestyle.min.js"></script>
		<script type="text/javascript">
			$('#birthday').datepicker();
		</script>
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>