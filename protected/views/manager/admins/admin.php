<?php
	if(permission('users')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong><?=(isset($data))?'Edit list':'Add list'?> </strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <div class="btn-group col-md-6" style="padding-left: 0">
                                        <a id="table-edit_new" class="btn btn-danger btn-sm" href="<?=URL.'/manager/lists/'?>">
                                            <i class="fa fa-arrow-left"></i> Back
                                        </a>
                                    </div>
                                    <div class="btn-group col-md-6"></div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
									<?=$this->form_builder->open_form(array('action'=>'','method'=>'POST', 'enctype'=>'multipart/form-data'))?>
										<div id="edit-content">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 0">
														<?php
															echo $this->form_builder->build(
																array(
																	array(
																		'id' => 'id',
																		'type' => 'hidden',
																		'value' => (isset($data['id']))?$data['id']:''
																	),
																	array(
																		'id' => 'name',
																		'label' => 'Name',
																		'value' => (isset($data['name']))?$data['name']:'',
																		'help' => 'Display name'
																	),
																	array(
																		'id' => 'username',
																		'label' => 'Username',
																		'value' => (isset($data['username']))?$data['username']:'',
																		'required' => '',
																		'help' => '3 - 16 chars'
																	),
																	array(
																		'id' => 'password',
																		'label' => 'Password',
																		'type' => 'password',
																		'help' => 'Keep blank to not change'
																	),
																	array(
																		'id' => 'email',
																		'label' => 'Email',
																		'type' => 'email',
																		'required' => '',
																		'value' => (isset($data['email']))?$data['email']:'',
																		'help' => 'Valid email to confirm'
																	),
																)
															);
															echo '<div class="col-md-6 col-sm-6 col-xs-6">';
															echo $this->form_builder->build(
																array(
																	array(
																		'id' => 'uploadavatar',
																		'label' => 'Upload image',
																		'class' => 'filestyle',
																		'type' => 'file',
																		'data-size' => 'sm',
																		'data-buttonText' => '',
																	)
																)
															);
															echo '</div><div class="col-md-6 col-sm-6 col-xs-6">';
															echo $this->form_builder->build(
																array(
																	array(
																		'id' => 'avatar',
																		'label' => 'URL',
																		'value' => (isset($data['avatar']))?$data['avatar']:'',
																	)
																)
															);
															echo '</div>';
															if(isset($data['avatar'])) if($data['avatar']) {
															if(strpos($data['avatar'], 'blogspot.com'))
																$data['avatar'] = str_replace('/s0/', '/w50-h50-c/', $data['avatar']);
														?>
															<img src="<?=$data['avatar']?>" style="max-width: 218px" />
														<? } ?>
													</div>
													<div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 0">
														<?php
															$group_data = array(''=>'Select Group');
															foreach($groups as $group) $group_data[$group['id']] = $group['name'];
															echo $this->form_builder->build(
																array(
																	array(
																		'id' => 'gid',
																		'label' => 'Group',
																		'type' => 'dropdown',
																		'options' => $group_data,
																		'value' => (isset($data['gid']))?$data['gid']:'',
																		'required' => '',
																	),
																	array(
																		'id' => 'gender',
																		'label' => 'Gender',
																		'type' => 'dropdown',
																		'options' => array('other' => 'Other', 'male' => 'Male', 'female' => 'Female'),
																		'value' => (isset($data['gender']))?$data['gender']:'',
																		'required' => '',
																	),
																	array(
																		'id' => 'status',
																		'label' => 'Status',
																		'type' => 'dropdown',
																		'options' => array('valid' => 'Valid', 'pending' => 'Pending', 'banned' => 'Banned'),
																		'value' => (isset($data['status']))?$data['status']:'',
																		'required' => '',
																	),
																	array(
																		'id' => 'birthday',
																		'label' => 'Birthday',
																		'value' => (isset($data['birthday']))?date("m/d/Y", strtotime($data['birthday'])):'',
																	),
																	array(
																		'id' => 'ipaddress',
																		'label' => 'IPaddress',
																		'value' => (isset($data['ipaddress']))?$data['ipaddress']:'',
																	),
																)
															);
														?>
													</div>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<input type="submit" class="btn btn-success btn btn-primary btn-sm" value="Save" />
										</div>
									<?=$this->form_builder->close_form();?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		<!-- END MAIN CONTENT -->
		<script src="<?=CDN?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script src="<?=CDN?>/cpanel/plugins/filestyle/bootstrap-filestyle.min.js"></script>
		<script type="text/javascript">
			$('#birthday').datepicker();
		</script>
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>