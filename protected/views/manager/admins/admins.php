<?php
	if(permission('admins')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Admins </strong> manager</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <div class="pull-left">
                                        <a id="table-edit_new" class="btn btn-sm btn-danger" href="<?=URL?>/manager/admins/item">
                                            Add New <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
									<form id="form-search" action="<?=URL.'/manager/admins';?>" method="GET">
										<div class="row">
											<div class="col-xs-9">
												<div class="col-xs-3" style="padding: 0 5px 0 0">
													<select id="group" name="group" class="form-control" data-style="input-sm btn-default">
														<? $s = $this->input->get('group') ?>
														<option value="">Groups</option>
														<? foreach($groups as $group) { ?>
														<option value="<?=$group['id']?>" <?=($s==$group['id'])?'selected':''?> data-content="<span style='<?=$group['style']?>'><?=$group['name']?></span>"><?=$group['name']?></option>
														<? } ?>
													</select>
												</div>
												<div class="col-xs-2" style="padding: 0 5px 0 0">
													<select id="status" name="status" class="form-control" data-style="input-sm btn-default">
														<? $s = $this->input->get('status') ?>
														<option value="">Status</option>
														<option value="valid" <?=($s=='valid')?'selected':''?> data-icon="fa-check-circle">Valid</option>
														<option value="pending"<?=($s=='pending')?'selected':''?> data-icon="fa-clock-o">Pending</option>
														<option value="banned" <?=($s=='banned')?'selected':''?> data-icon="fa-minus-circle">Banned</option>
													</select>
												</div>
												<div class="col-xs-3" style="padding: 0 5px 0 0">
													<div class="col-xs-5" style="padding: 0 5px 0 0">
														<button type="button" class="btn btn-sm btn-danger" onClick="$('#form-search').submit();">Filter <i class="fa fa-info-circle"></i></button>
													</div>
													<div class="col-xs-5">
														<a class="btn btn-sm btn-danger" href="<?=URL?>/manager/admins">Clear <i class="fa fa-times-circle"></i></a>
													</div>
												</div>
												<div class="col-xs-2"></div>
											</div>
											<div class="col-xs-3">
												<div class="input-group">
													<input type="text" class="form-control" name="search" value="<?=($this->input->get('search'))?$this->input->get('search'):''?>" placeholder="Search">
													<span class="input-group-addon bg-blue" style="cursor: pointer" onClick="$('#form-search').submit();">     
														<span class="arrow"></span><i class="fa fa-search"></i> 
													</span>
												</div>
											</div>
										</div>
									</form>
                                    <table class="table table-striped table-hover dataTable" id="table-editable">
                                        <thead>
                                            <tr role="row">
                                                <th class="align-center">ID</th>
                                                <th class="align-center">UserName</th>
                                                <th class="align-center">Name</th>
                                                <th class="align-center">Email</th>
                                                <th class="align-center">Status</th>
                                                <th class="align-center">Created</th>
                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
										if(isset($admins) && is_array($admins)) {
											foreach($admins as $admin) {
										?>
                                            <tr id="admin-<?=$admin['id']?>">
                                                <td class="align-center"><?=$admin['id']?></td>
                                                <td><a class="admin_style" style="<?=$admin['group']['style']?>" href="<?=URL?>/manager/admins/item/<?=$admin['id']?>" title="<?=$admin['email']?>"><?=$admin['username']?></a></td>
                                                <td class="align-center"><span class="admin_style"><?=$admin['name']?></span></td>
                                                <td class="align-center"><?=$admin['email']?></td>
                                                <td class="align-center"><?
												if($admin['status'] == 'valid') echo '<i class="fa fa-check-circle"></i> Valid';
												elseif($admin['status'] == 'pending') echo '<i class="fa fa-clock-o"></i> Pending';
												else echo '<i class="fa fa-minus-circle"></i> Banned';
												?></td>
                                                <td class="align-center"><?=date("d/m/Y", $admin['created'])?></td>
                                                <td class="align-center">
													<a class="edit btn btn-sm btn-dark" href="<?=URL?>/manager/admins/item/<?=$admin['id']?>"><i class="fa fa-pencil-square-o"></i></a>
													<button class="delete btn btn-sm btn-danger" href="javascript:;" onclick="removeadmin(<?=$admin['id']?>);"><i class="fa fa-times-circle"></i></button>
                                                </td>
                                            </tr>
										<?php 
											}
										} else echo '<tr><td colspan="10" style="text-align: center">No results</td></tr>';
										?>
                                        </tbody>
                                    </table>
									<div class="col-xs-12" style="line-height: 24px;">
										<div class="col-sm-2"> Have <?=$total?> admins</div>
										<?=$this->admin_model->all_pages;?>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<script>
				function removeadmin(id) {
					if (confirm("Are you want to delete this tag permanent?")) {
						$.post("<?=URL?>/manager/admins/ajax/del", {id:id},
						function(data,status){
							$("#admin-"+id).hide();
						});
					}
				}
			</script>
            <!-- the overlay element -->
		</div>
		<!-- END MAIN CONTENT -->
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>