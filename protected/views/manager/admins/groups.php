<?php
	if(permission('users')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Groups </strong> manager</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <div class="pull-left">
                                        <a id="table-edit_new" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#user-edit" href="javascript:;" onClick="render();">
                                            Add New <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
                                    <table class="table table-striped table-hover dataTable" id="table-editable">
                                        <thead>
                                            <tr role="row">
                                                <th class="align-center">ID</th>
                                                <th class="align-center">Name</th>
                                                <th class="align-center">Style</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
											foreach($groups as $group) {
										?>
                                            <tr id="group_<?=$group['id']?>">
                                                <td class="align-center"><?=$group['id']?></td>
                                                <td class="align-center"><a style="<?=$group['style']?>"><?=$group['name']?></a></td>
                                                <td class="align-center"><a style="<?=$group['style']?>"><?=$group['style']?></a></td>
                                                <td class="align-center">
													<a class="edit btn btn-sm btn-dark" data-toggle="modal" data-target="#user-edit" href="javascript:;" onClick="render(<?=$group['id']?>);"><i class="fa fa-pencil-square-o"></i></a>
													<button class="delete btn btn-sm btn-danger" href="javascript:;" onclick="del(<?=$group['id']?>);"><i class="fa fa-times-circle"></i></button>
                                                </td>
                                            </tr>
											<?php 
												}
											?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="modal fade" id="user-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
				<form method="POST">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel"><strong>Edit</strong> <span id="edit-user"></span></h4>
                        </div>
                        <div class="modal-body">
							<div id="edit-content">
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<?php
											echo $this->form_builder->build(
												array(
													array(
														'id' => 'id',
														'label' => 'ID',
														'type'=>'hidden',
													),
													array(
														'id' => 'name',
														'label' => 'Group name',
														'required' => '',
													),
													array(
														'id' => 'style',
														'label' => 'Group Style',
														'required' => '',
													),
												)
											);
										?>
									</div>
								</div>
							</div>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Save changes">
                        </div>
                    </div>
				</form>
                </div>
            </div>
			<script>
				function del(id) {
					if (confirm("Are you want to delete this tag permanent?")) {
						$.post("<?=URL?>/manager/users/ajax/delGroup", {id:id},
						function(data,status){
							$("#group_"+id).remove();
						});
					}
				}
				function render(id) {
					if(id) {
						$.post("<?=URL?>/manager/users/ajax/getGroup", {id:id},
						function(data,status){
							var data = jQuery.parseJSON(data);
							$("input[name=id]").val(data.id);
							$("#name").val(data.name);
							$("#style").val(data.style);
						});
					} else {
							$("input[name=id]").val('');
							$("#name").val('');
							$("#style").val('');
					}
				}
			</script>
            <!-- the overlay element -->
		</div>
		<!-- END MAIN CONTENT -->
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>