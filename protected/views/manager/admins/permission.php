<?php
	if(permission('permission')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Manager </strong> permission</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<div class="col-md-12 m-b-20">
									<div class="pull-left">
										<a id="table-edit_new" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="render();">
											Add New <i class="fa fa-plus"></i>
										</a>
									</div>
								</div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
                                    <table class="table table-striped table-hover" id="table-editable">
                                        <thead>
                                            <tr role="row">
                                                <th class="align-center"></th>
												<?php
													foreach ($groups as $group) echo '<th class="align-center"><span style="'.$group['style'].'">'.$group['name'].'</span></th>';
												?>
                                                <th class="align-center"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
											$g = 0;
											foreach ($permissions as $permission) {
												if($g!=$permission['group']) {
													$g= $permission['group'];
													echo '<tr><td colspan="'.(count($groups)+2).'">&nbsp;</td></tr>';
												}
										?>
                                            <tr role="row" id="row_<?=$permission['id']?>">
                                                <td><a href="javascript:;" data-toggle="modal" data-target="#item-edit" onclick="render(<?=$permission['id']?>)"><?=$permission['name']?><a/></td>
												<?php
													foreach ($groups as $group) {
														$per = 0;
														if(isset($permission['data']))
														foreach($permission['data'] as $data) {
															if($data['gid']==$group['id']) {
																$per = $data;
																break;
															}
														}
												?>
												<td class="align-center">
													<input type="checkbox" name="permission" value="1" data-pid="<?=(isset($permission['id'])?$permission['id']:0)?>" data-group="<?=(isset($per['gid'])?$per['gid']:$group['id'])?>" data-size="small" data-on-text="On" data-off-text="Off" <?php if(isset($per['allow'])) echo ($per['allow'])?'checked':''?>>
												</td>
												<?php
													}
												?>
												<td class="align-center"><a href="javascript:;" class="pull-right" onclick="del(<?=$permission['id']?>)"><i class="fa fa-close"></i><a/></td>
                                            </tr>
                                       	<?php } ?>
                                        </tbody>
                                    </table>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="modal fade" id="item-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
				<form method="POST">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel"><strong>Edit</strong></h4>
                        </div>
                        <div class="modal-body">
							<div id="edit-content">
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<?php
											echo $this->form_builder->build(
												array(
													array(
														'id' => 'id',
														'label' => 'ID',
														'type'=>'hidden',
													),
													array(
														'id' => 'name',
														'label' => 'Group name',
														'required' => '',
													),
													array(
														'id' => 'function',
														'label' => 'Function',
														'required' => '',
													),
													array(
														'id' => 'group',
														'label' => 'Group',
														'required' => '',
													),
													array(
														'id' => 'order',
														'label' => 'Order',
														'required' => '',
													),
												)
											);
										?>
									</div>
								</div>
							</div>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Save changes">
                        </div>
                    </div>
				</form>
                </div>
            </div>
			<script>
			$("[name='permission']").bootstrapSwitch();
			$("[name='permission']").on('switchChange.bootstrapSwitch', function(event, state) {
				var pid = $(this).data('pid');
				var gid = $(this).data('group');
				if(state) allow=1;
				else allow=0;
				$.post("<?=URL?>/manager/users/ajax/permission", {pid:pid, gid:gid, allow:allow},
				function(data,status){});
			});
			function del(id) {
				if(id) {
					$.post("<?=URL?>/manager/users/ajax/delPermission", {id:id},
					function(data,status){
						$("#row_"+id).remove();
					});
				}
			}
			function render(id) {
				if(id) {
					$.post("<?=URL?>/manager/users/ajax/getPermission", {id:id},
					function(data,status){
						var data = jQuery.parseJSON(data);
						$("input[name=id]").val(data.id);
						$("#name").val(data.name);
						$("#function").val(data.function);
						$("#group").val(data.group);
						$("#order").val(data.order);
					});
				} else {
					$("input[name=id]").val('');
					$("#name").val('');
					$("#function").val('');
					$("#group").val('');
					$("#order").val('');
				}
			}
			</script>
            <!-- the overlay element -->
		</div>
		<!-- END MAIN CONTENT -->
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>