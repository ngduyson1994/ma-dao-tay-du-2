    </div>
    <!-- END WRAPPER -->
    <!-- BEGIN MANDATORY SCRIPTS -->
    <script src="<?=CDN?>/cpanel/plugins/jquery-migrate-1.2.1.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/jquery-ui/jquery-ui-1.10.4.min.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/bootstrap-select/bootstrap-select.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/mmenu/js/jquery.mmenu.min.all.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/nprogress/nprogress.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/charts-sparkline/sparkline.min.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/breakpoints/breakpoints.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/numerator/jquery-numerator.js"></script>
	<script src="<?=CDN?>/cpanel/plugins/select2/select2.min.js"></script>
	<script src="<?=CDN?>/cpanel/plugins/jnotify/jNotify.jquery.min.js"></script>
    <!-- END  PAGE LEVEL SCRIPTS -->
    <script src="<?=CDN?>/cpanel/js/application.js"></script>
	<? if(isset($return) && isset($return['message']) && isset($return['status'])) { ?>
		<script>
			notice('<?=$return['message']?>', '<?=$return['status']?>');
		</script>
	<? } ?>
</body>
</html>