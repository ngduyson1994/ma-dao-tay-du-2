<?php
	if(permission('game')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>GAME </strong> manager</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <div class="pull-left">
										<a id="table-edit_new" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="rendertag();">
                                            Add New <i class="fa fa-plus"></i>
                                        </a>
                                    </div>                                
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
									<form id="form-search" method="GET">
									<div class="row">
										<div class="col-xs-9"></div>
										<div class="col-xs-3">
											<div class="input-group">
												<input type="text" class="form-control" name="search" value="<?=($this->input->get('search'))?$this->input->get('search'):''?>" placeholder="Search">
												<span class="input-group-addon bg-blue" style="cursor: pointer" onClick="$('#form-search').submit();">     
													<span class="arrow"></span><i class="fa fa-search"></i> 
												</span>
											</div>
										</div>
									</div>
									</form>
                                    <table class="table table-striped table-hover dataTable" id="table-editable">
                                        <thead>
                                            <tr role="row">
                                                <th class="align-center">ID</th>
                                                <th class="align-center">Rank</th>
                                                <th class="align-center">Name</th>
                                                <th class="align-center">Thumb</th>

                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
											$i=1;
											if(isset($game) && is_array($game)) {
											foreach($game as $item)
											{
										?>
                                            <tr id="kol-<?=$item['id']?>">
                                                <td class="align-center">
													<?=$item['id']?>
												</td> <td class="align-center">
													<?=$item['rank']?>
												</td>
                                                <td class="align-center">
                                                    <a style="color: #0090D9;" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="rendertag(<?=$item['id']?>);"><? echo cutOf($item['name'],50);?></a>
												</td>
                                                <td class="align-center"><img src="<?=$item['thumb']?>" style="width:30px;"> </td>


                                                <td class="align-center">
                                                    <input type="checkbox" name="feature" value="<?=$item['id']?>" data-size="small" data-on-text="On" data-off-text="Off" <?=($item['feature'])?'checked':''?>>
													<a class="edit btn btn-sm btn-dark" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="rendertag(<?=$item['id']?>);"><i class="fa fa-pencil-square-o"></i></a>
													<button class="delete btn btn-sm btn-danger" href="javascript:;" onClick="removetag(<?=$item['id'];?>);"><i class="fa fa-times-circle"></i></button>
                                                </td>
                                            </tr>
											<?php
												}
												}
											?>
                                        </tbody>
                                    </table>
									<div class="col-xs-12" style="line-height: 24px;">
										<div class="col-sm-2"> Có <?=$total?> GAME</div>
										<?=$this->game_model->all_pages;?>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- the overlay element -->
			<div class="modal fade" id="item-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
				<form method="POST">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel"><strong>Edit</strong></h4>
						</div>
						<div class="modal-body">
							<div id="edit-content">
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<input type="hidden" name="id" value="" />
										
										<div class="form-group">
											<label for="name" class="control-label bold">Thứ hạng</label>
											<div class="controls">
												<input type="number" name="rank" value="" id="rank" label="rank" required="" class="form-control"/>
											</div>
										</div>
										<div class="form-group">
											<label for="name" class="control-label bold">Name</label>
											<div class="controls">
												<input type="text" name="name" value="" id="name" label="Name" required="" class="form-control" /> 
											</div>
										</div>
										<div class="form-group">
											<label for="slug" class="control-label bold">Thumb</label>
											<div class="controls">
												<input type="text" name="thumb" value="" id="thumb" label="thumb"  class="form-control" />
											</div>
										</div>
										<div class="form-group">
											<label for="slug" class="control-label bold">Type</label>
											<div class="controls">
												<input type="text" name="type" value="" id="type" label="type"  class="form-control" />
											</div>
										</div>
										<div class="form-group">
											<label for="slug" class="control-label bold">Link</label>
											<div class="controls">
												<input type="text" name="link" value="" id="link" label="link" required=""  class="form-control" />
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="submit" class="btn btn-success" value="Save changes">
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
		<!-- END MAIN CONTENT -->
		<script type="text/javascript" src="<?=CDN?>/cpanel/tinymce/tinymce.min.js"></script>
		<script src="<?=CDN?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script>
            $("[name='feature']").bootstrapSwitch();
            $("[name='feature']").on('switchChange.bootstrapSwitch', function(event, state) {
                var id = $(this).val();
                var value;
                if(state) value=1;
                else value=0;
                $.post("<?=URL?>/manager/game/ajax/feature", {id:id, value: value},
                    function(data,status){});
            });
			function submitFilter(user) {
				$("#user").val(user);
				$('#form-search').submit();
			}

			function removetag(id) {
				if (confirm("Are you want to delete this tag permanent?")) {
					$.post("<?=URL?>/manager/game/ajax/delete", {id:id},
					function(data,status){
						if(data=="Deleted") {
							$("#game-"+id).hide();
						}
					});
				}
			}
			
			function rendertag(id) {
				if(id) {
					$.post("<?=URL?>/manager/game/ajax/get", {id:id},
					function(data,status){
						var tag = jQuery.parseJSON(data);
						if(tag.description) {
							tinymce.get('description').setContent(tag.description);
						}
						$("input[name=id]").val(tag.id);
						$("#name").val(tag.name);
						$("#rank").val(tag.rank);
						$("#thumb").val(tag.thumb);
						$("#link").val(tag.link);
						$("#type").val(tag.type).change();
					});
				} else {
					$("#id").val(0);
					$("#rank").val('');
					$("#name").val('');
					$("#thumb").val('');
					$("#link").val('');
					$("#type").val('').change();
				}
			}
		</script>
		<script>
			$(document).ready(function() {
				tinymce.init({
					selector: "textarea",
					theme: "modern",
					relative_urls : false,
					remove_script_host: false,
					toolbar: 'codesample | bold italic sizeselect fontselect fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | insertfile undo redo | forecolor backcolor | code youtube',
					fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
					plugins: [
						 "link image preview",
						 "wordcount code fullscreen",
						 "table textcolor youtube"
				   ],
					external_filemanager_path:"<?=URL?>/filemanager/",
					filemanager_title:"Filemanager" ,
					external_plugins: { "filemanager" : "<?=URL?>/filemanager/plugin.min.js"}
				 }); 
			});
		</script>
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>