<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js sidebar-large"> <!--<![endif]-->
<head>
    <!-- BEGIN META SECTION -->
    <title>Manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8">
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    <link href="<?=CDN?>/icons/icons.css" rel="stylesheet">
    <link href="<?=CDN?>/cpanel/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=CDN?>/cpanel/css/plugins.min.css" rel="stylesheet">
    <link href="<?=CDN?>/cpanel/css/style.min.css" rel="stylesheet">
    <link href="<?=CDN?>/cpanel/plugins/bootstrap-switch/bootstrap-switch.min.css" rel="stylesheet">
    <!-- END  MANDATORY STYLE -->
    <!-- BEGIN PAGE LEVEL STYLE -->
	<link href="<?=CDN?>/cpanel/plugins/modal/css/component.css" rel="stylesheet">
	<link rel="stylesheet" href="<?=CDN?>/cpanel/plugins/select2/select2.css" type="text/css" />
	<link rel="stylesheet" href="<?=CDN?>/cpanel/plugins/select2/theme.css" type="text/css" />
	<link rel="stylesheet" href="<?=CDN?>/cpanel/plugins/jnotify/jNotify.jquery.css">
    <!-- END PAGE LEVEL STYLE -->
    <script src="<?=CDN?>/cpanel/plugins/jquery-1.11.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
</head>
<body>
    <!-- BEGIN TOP MENU -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <!--button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#sidebar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a id="menu-medium" class="sidebar-toggle tooltips">
					<i class="fa fa-outdent"></i>
				</a-->
                <a class="navbar-brand" href="<?=URL?>/manager"><img src="<?=CDN?>/core/images/logo.png"  style="height: 28px" /></a>
            </div>
            <div class="navbar-center">Dashboard</div>
            <div class="navbar-collapse collapse">
                <!-- BEGIN TOP NAVIGATION MENU -->
                <ul class="nav navbar-nav pull-right header-menu">
                    <li id="user-header">
                        <a href="javascript:;" onClick="clear_cache();" class="c-white">
                            <span class="clear_cache">Clear Cache</span>
                        </a>
                        <script type="text/javascript">
                            function clear_cache() {
                                $.get("<?=URL?>/manager/ajax/clear_cache", function( data ) {
                                    $(".clear_cache" ).html(data);
                                    setTimeout(function(){$(".clear_cache" ).html('Clear Cache');}, 3000);
                                });
                            }
                        </script>
                    </li>
					<?php
						if(isset($admin['name'])) $name = $admin['name'];
						else $name = $admin['username'];
						$avatar = (isset($admin['avatar']))?$admin['avatar']:null;
						if(!$avatar) $avatar = CDN.'/cpanel/img/avatars/avatar1.png';
					?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle c-white" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img src="<?=$avatar;?>" alt="user avatar" style="border-radius: 50%;height: 30px; width: 30px;">
                            <span class="username"><?=$name;?></span>
                            <i class="fa fa-angle-down p-r-10"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?=URL?>/manager/users/account">
                                    <i class="fa fa-cogs"></i> Account Settings
                                </a>
                            </li>
                            <li>
                                <a href="<?=URL?>/manager/main/logout">
                                    <i class="fa fa-power-off"></i> Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER DROPDOWN -->
                </ul>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </div>
    </nav>
    <!-- END TOP MENU -->

    <!-- BEGIN WRAPPER -->
    <div id="wrapper">
        <!-- BEGIN MAIN SIDEBAR -->
        <nav id="sidebar">
            <div id="main-menu">
                <ul class="sidebar-nav">
                    <li <? if(isset($page)) if($page=='main') echo 'class="current"';?>>
                        <a href="<?=URL?>/manager/"><i class="fa fa-dashboard"></i><span class="sidebar-text">Dashboard</span></a>
                    </li>
					<?php if(permission('posts')) { ?>
                    <li>
                        <a href="#"><i class="fa fa-pencil-square-o"></i><span class="sidebar-text">Bài viết</span><span class="fa arrow"></span></a>
                         <ul class="submenu  collapse <? if(isset($page)) if(strstr($page, 'post')) echo 'in';?>">
                            <li <? if(isset($page)) if($page=='post_add') echo 'class="current"';?>><a href="<?=URL?>/manager/posts/item/"><i class="fa fa-plus-square"></i><span class="sidebar-text">Thêm mới</span></a></li>
                            <li <? if(isset($page)) if(($page=='posts'  && $this->input->get('status')!='pending' && $this->input->get('type')!='page' && $this->input->get('trash')!='true') || $page=='post_edit') echo 'class="current"';?>><a href="<?=URL?>/manager/posts/"><i class="fa fa-table"></i><span class="sidebar-text">Đã đăng</span></a></li>
                            <li <? if(isset($page)) if($page=='posts' && $this->input->get('status')=='pending') echo 'class="current"';?>><a href="<?=URL?>/manager/posts/?status=pending"><i class="fa fa-clock-o"></i><span class="sidebar-text">Chờ duyệt</span></a></li>
                            <li <? if(isset($page)) if($page=='posts' && $this->input->get('trash')=='true') echo 'class="current"';?>><a href="<?=URL?>/manager/posts/?trash=true"><i class="fa fa-trash"></i><span class="sidebar-text">Thùng rác</span></a></li>
                        </ul>
                    </li>
					<?php } ?>
					<?php if(permission('posts')) { ?>
                    <li>
                        <a href="#"><i class="fa fa-newspaper-o"></i><span class="sidebar-text">Nội dung</span><span class="fa arrow"></span></a>
                         <ul class="submenu  collapse <? if(isset($page)) if(strstr($page, 'menus') || strstr($page, 'categories') || strstr($page, 'tags') || strstr($page, 'pages')) echo 'in';?>">
							<?php if(permission('menu')) {?>
							<li <? if(isset($page)) if($page=='menus') echo 'class="current"';?>>
								<a href="<?=URL?>/manager/menus/"><i class="fa fa-sitemap"></i><span class="sidebar-text">Menus</span></a>
							</li>
							<?php } ?>
							<?php if(permission('categories')) {?>
							<li <? if(isset($page)) if($page=='categories') echo 'class="current"';?>>
								<a href="<?=URL?>/manager/categories/"><i class="fa fa-list-alt"></i><span class="sidebar-text">Thể loại</span></a>
							</li>
							<?php } ?>
							<?php if(permission('pages')) {?>
							<li <? if(isset($page)) if($page=='pages') echo 'class="current"';?>>
								<a href="<?=URL?>/manager/pages/"><i class="fa fa-list"></i><span class="sidebar-text">Pages</span></a>
							</li>
							<?php } ?>
                        </ul>
                    </li>
					<?php } ?>
					<?php if(permission('sliders')) { ?>
					 <li <? if(isset($page)) if($page=='sliders') echo 'class="current"';?>>
                        <a href="<?=URL?>/manager/sliders/"><i class="fa fa-list"></i><span class="sidebar-text">Sliders</span></a>
                    </li>
					<?php } ?>
					<?php if(permission('admins') || permission('permission')) { ?>
					 <li>
                        <a href="#"><i class="fa fa-user-secret"></i><span class="sidebar-text">Admins</span><span class="fa arrow"></span></a>
                        <ul class="submenu  collapse <? if(isset($page)) if(strstr($page, 'admin')) echo 'in';?>">
                            <?php if(permission('admins')) { ?><li <? if(isset($page)) if($page=='admins' || $page=='admin_edit') echo 'class="current"';?>><a href="<?=URL?>/manager/admins/"><span class="sidebar-text"><i class="fa fa-table"></i>Manager admins</span></a></li><?php } ?>
                            <?php if(permission('admins')) { ?><li <? if(isset($page)) if($page=='admin_group') echo 'class="current"';?>><a href="<?=URL?>/manager/admins/group/"><i class="fa fa-group"></i><span class="sidebar-text">Admin groups</span></a></li><?php } ?>
                            <?php if(permission('permission')) { ?><li <? if(isset($page)) if($page=='admin_permission') echo 'class="current"';?>><a href="<?=URL?>/manager/admins/permission/"><i class="fa fa-lock"></i><span class="sidebar-text">Permission</span></a></li><?php } ?>
                        </ul>
                    </li>
					<?php } ?>
					 <?php if(permission('logsxxxx')) { ?>
					 <li <? if(isset($page)) if($page=='logs') echo 'class="current"';?>>
                        <a href="<?=URL?>/manager/logs/"><i class="fa fa-exclamation-circle"></i><span class="sidebar-text">Logs</span></a>
                     </li>
					 <?php } ?>
					 <?php if(permission('ads')) { ?>
					 <li>
                        <a href="#"><i class="fa fa-buysellads"></i><span class="sidebar-text">Quảng cáo</span><span class="fa arrow"></span></a>
                         <ul class="submenu  collapse <? if(isset($page)) if(strstr($page, 'ad_') && !strpos($page, 'ad_')) echo 'in';?>">
                             <?php if(permission('ads')) {?>
                                <li <? if(isset($page)) if($page=='ad_links') echo 'class="current"';?>><a href="<?=URL?>/manager/adlinks/"><i class="fa fa-buysellads"></i><span class="sidebar-text"> Quản lý QC</span></a></li>
                            <?php } ?>
                            <?php if(permission('ads')) {?>
								<li <? if(isset($page)) if($page=='ad_zones') echo 'class="current"';?>><a href="<?=URL?>/manager/adzones/"><i class="fa fa-object-group"></i><span class="sidebar-text"> Zone</span></a></li>
							<?php } ?>
                            <?php if(permission('ads')) {?>
								<li <? if(isset($page)) if($page=='ad_codes') echo 'class="current"';?>><a href="<?=URL?>/manager/adcodes/"><i class="fa fa-code"></i><span class="sidebar-text"> Banner</span></a></li>
							<?php } ?>
                        </ul>
                    </li>
					<?php } ?>
					<?php if(permission('settings')) { ?>
					 <li <? if(isset($page)) if($page=='settings') echo 'class="current"';?>>
                        <a href="<?=URL?>/manager/settings"><i class="fa fa-cogs"></i><span class="sidebar-text">Cài đặt</span></a>
                     </li>
					 <?php } ?>
                </ul>
            </div>
			<div class="footer-widget">
            </div>
        </nav>
        <!-- END MAIN SIDEBAR -->