<?php
	if(permission('manager')) {
?>
  <!-- BEGIN MAIN CONTENT -->
  <div id="main-content" class="dashboard">
	<div class="clearfix" style="margin-bottom: 10px"></div>
		  <div class="row">
        <div class="col-md-12">
        <div class="col-md-12">
					<div class="panel-body bg-red panel-stat">
						<div id="chart_div" style="height: 300px"></div>
					</div>
				</div>
				</div>
			</div>
		</div>
		<script src="<?=CDN?>/cpanel/js/google-chart.js"></script>
		<script>
			google.charts.load('current', {packages: ['corechart']});
			google.charts.setOnLoadCallback(drawTrendlines);
			function drawTrendlines() {
	        var data = google.visualization.arrayToDataTable([
	          ['Ngày', 'Đăng ký'],
						<?php foreach($charts as $chart)
							echo "['".$chart['date']."', ".$chart['user']."],";
						?>
	        ]);
		      var options = {
        		height: 300,
          	curveType: 'function',
          	legend: {position: 'bottom'},
		        hAxis: {textStyle: {color: '#fff', bold: true}},
		        vAxis: {textStyle: {color: '#fff', bold: true}},
						backgroundColor: {
		          fill: '#C75757',
		        },
		        colors: ['#D9E557', '#424C6D'],
						lineWidth: 3,
		      };
		      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
		      chart.draw(data, options);
		    }
			</script>
    <!-- END MAIN CONTENT -->
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>
