<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js sidebar-large"> <!--<![endif]-->

<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title>Manager Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    <link href="<?=CDN?>/cpanel/css/icons/icons.min.css" rel="stylesheet">
    <link href="<?=CDN?>/cpanel/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=CDN?>/cpanel/css/plugins.min.css" rel="stylesheet">
    <link href="<?=CDN?>/cpanel/css/style.min.css" rel="stylesheet">
    <!-- END  MANDATORY STYLE -->
    <!-- BEGIN PAGE LEVEL STYLE -->
    <link href="<?=CDN?>/cpanel/css/animate-custom.css" rel="stylesheet">
    <!-- END PAGE LEVEL STYLE -->
    <script src="<?=CDN?>/cpanel/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body class="login fade-in" data-page="login">
    <!-- BEGIN LOGIN BOX -->
    <div class="container" id="login-block">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
                <div class="login-box clearfix animated flipInY">
                    <div class="page-icon animated bounceInDown">
                        <img width="30" src="<?=CDN?>/core/images/logo.png">
                    </div>
					<br />
                    <div class="login-form">
						<? if(isset($login['message'])) { ?>
                        <!-- BEGIN ERROR BOX -->
                        <div class="alert alert-danger" style="width: 250px;">
                            <button type="button" class="close" data-dismiss="alert">×</button>
							<?=$login['message']?>
                        </div>
                        <!-- END ERROR BOX -->
						<? } ?>
                        <form action="<?=URL?>/manager/login" method="post">
                            <input type="text" placeholder="Username" name="username" value="<? if($this->input->post('username')) echo $this->input->post('username'); ?>" class="input-field form-control user" />
                            <input type="password" placeholder="Password" name="password" class="input-field form-control password" />
                            <?php if($this->config->item('passcode')) { ?>
                            <input type="password" placeholder="Passcode" name="passcode" class="input-field form-control password" />
                            <?php } ?>
                            <button type="submit" class="btn btn-login">Login</button>
                        </form>
                        <div class="login-links">
                            <a href="password_forgot.html">Forgot password?</a>
                            <br>
                            <a href="signup.html">Don't have an account? <strong>Sign Up</strong></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END LOCKSCREEN BOX -->
    <!-- BEGIN MANDATORY SCRIPTS -->
    <script src="<?=CDN?>/cpanel/plugins/jquery-1.11.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/jquery-migrate-1.2.1.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/jquery-ui/jquery-ui-1.10.4.min.js"></script>
    <script src="<?=CDN?>/cpanel/plugins/bootstrap/bootstrap.min.js"></script>
    <!-- END MANDATORY SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?=CDN?>/cpanel/plugins/backstretch/backstretch.min.js"></script>
    <script src="<?=CDN?>/cpanel/js/account.js"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
</body>
</html>