<?php
	if(permission('logs')) {
		$this->load->library('user_agent');
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Logs </strong> manager</h3>
                        </div>
                        <div class="panel-body">
							<div class="row">
								 <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
									<form id="form-search" action="<?=URL.'/manager/logs';?>" method="GET">
										<div class="row">
											<div class="col-xs-9">
												<div class="col-xs-3" style="padding: 0 5px 0 0">
													<select id="model" name="model" class="form-control" data-style="input-sm btn-default">
														<? $s = $this->input->get('model') ?>
														<option value="">Model</option>
														<? foreach($models as $model) { ?>
														<option value="<?=strtolower($model['model'])?>" <?=($s==$model['model'])?'selected':''?>><?=$model['model']?></option>
														<? } ?>
													</select>
												</div>
												<div class="col-xs-3" style="padding: 0 5px 0 0">
													<div class="col-xs-5" style="padding: 0 5px 0 0">
														<button type="button" class="btn btn-sm btn-danger" onClick="$('#form-search').submit();">Filter <i class="fa fa-info-circle"></i></button>
													</div>
													<div class="col-xs-5">
														<a class="btn btn-sm btn-danger" href="<?=URL?>/manager/logs">Clear <i class="fa fa-times-circle"></i></a>
													</div>
												</div>
												<div class="col-xs-6"></div>
											</div>
											<div class="col-xs-3">
												<div class="input-group">
													<input type="text" class="form-control" name="search" value="<?=($this->input->get('search'))?$this->input->get('search'):''?>" placeholder="Search">
													<span class="input-group-addon bg-blue" style="cursor: pointer" onClick="$('#form-search').submit();">     
														<span class="arrow"></span><i class="fa fa-search"></i> 
													</span>
												</div>
											</div>
										</div>
									</form>
                                    <table class="table table-striped table-hover dataTable" id="table-editable">
                                        <thead>
                                            <tr role="row">
                                                <th class="align-center" width="20px" > ID</th>
                                                <th class="align-center">User</th>
                                                <th class="align-center">Model</th>
                                                <th class="align-center">Action</th>
                                                <th class="align-center">Item</th>
                                                <th class="align-center">Browser</th>
                                                <th class="align-center">OS</th>
                                                <th class="align-center">Ipaddress</th>
                                                <th class="align-center">Datetime</th>
                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
											$i=1;
											if(isset($logs) && is_array($logs)) {
												foreach($logs as $log) {
													$this->agent->parse($log['user_agent']);
										?>
												<tr id="log-<?=$log['id']?>">
													<td class="align-center"  width="20px" >	<?=$log['id']?></td>
													<td class="align-center"><a href="<?=URL?>/manager/users/edit/<?=$log['admin']['id']?>" style="<?=(isset($groups[$log['admin']['gid']]))?$groups[$log['admin']['gid']]['style']:''?>"><?=$log['admin']['username']?></a></td>
													<td class="align-center"><?=$log['model']?></td>
													<td class="align-center"><?=$log['function']?></td>
													<td class="align-center"><?=$log['item']?></td>
													<td class="align-center"><?=$this->agent->browser()?></td>
													<td class="align-center"><?=$this->agent->platform()?></td>
													<td class="align-center"><?=$log['ipaddress']?></td>
													<td class="align-center"><?=date(" h:i:s A - d/m/Y",$log['created'])?></td>
													<td class="align-center">
														<button class="delete btn btn-sm btn-danger" href="javascript:;" onclick="del(<?=$log['id']?>);"><i class="fa fa-times-circle"></i></button>
													</td>
												</tr>
										<?php
												}
											} else echo '<tr><td colspan="6" style="text-align: center">No results</td></tr>';
										?>
                                        </tbody>
                                    </table>
									<?=$this->log_model->all_pages;?>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<script>
				function del(id) {
					if (confirm("Are you want to delete this log permanent?")) {
						$.post("<?=URL?>/manager/logs/ajax/delete", {id:id},
						function(data,status){
							if(data=="Deleted") {
								$("#log-"+id).hide();
							}
						});
					}
				}
			</script>
            <!-- the overlay element -->
		</div>
		<!-- END MAIN CONTENT -->
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>