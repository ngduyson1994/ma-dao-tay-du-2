<?php
	if(permission('menu')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Menu </strong> manager</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
									<div class="col-md-9 m-b-20" style="padding-left: 0">
										<div class="pull-left">
											<a id="table-edit_new" class="btn btn-sm btn-danger" href="javascript:;" onClick="clearMenu();">
												Add New <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<div class="col-md-3 m-b-20">
										<form id="form-menu" method="GET">
											<div class="col-md-9" style="padding: 0 5px 0 0">
												<select id="select_mid" name="mid" class="form-control" data-style="input-sm btn-default">
													<? $mid = $this->input->get('mid') ?>
													<? foreach($menus as $menu) { ?>
														<option value="<?=$menu['id']?>" <?=($mid==$menu['id'])?'selected':''?> data-content=<?=$menu['name']?>><?=$menu['name']?></option>
													<? } ?>
												</select>
											</div>
											<div class="col-md-3" style="padding: 0 5px 0 0">
												<div class="col-xs-4" style="padding: 0 5px 0 0">
													<a type="button" class="btn btn-sm btn-danger" onClick="$('#form-menu').submit();" > Select </a>
												</div>
											</div>
										</form>
									</div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red" style="padding-left: 0">
									<form method="POST">
										<div class="col-md-3 col-sm-3 col-xs-3" style="padding-left: 0">
											<div class="form-group">
												<label class="form-label"><strong>Type</strong></label>
												<div class="controls">
													<select id="type" name="type" class="form-control" data-style="input-sm btn-default"  onChange="updateEvent()">
														<? $type = $this->input->get('type') ?>
														<option value="category"<?=($type=='category')?'selected':''?> data-icon="fa fa-paperclip" selected>Category</option>
														<option value="link" <?=($type=='link')?'selected':''?> data-icon="fa fa-link">Link</option>
													</select>
												</div>
											</div>
											<div class="form-group" id="category-box">
												<label class="form-label"><strong>Category</strong></label>
												<div class="controls">
													<select id="category" name="category" class="form-control" data-live-search="true" data-style="input-sm btn-default">
														<option value="0">None Category</option>
														<?=catSelectBox($categories);?>
													</select>
												</div>
											</div>
											<div class="form-group" id="link-box">
												<label>Link</label><input id="link" name="link" type="text" class="form-control input-sm">
											</div>
											<div class="form-group">
												<label class="form-label"><strong>Title</strong></label>
												<div class="controls">
													<input id="title" name="title" type="text" class="form-control input-sm">
													<input id="id" name="id" type="hidden">
													<input id="mid" name="mid" value="<?=($mid)?$mid:1?>" type="hidden">
												</div>
											</div>
											<div class="form-group">
												<label class="form-label"><strong>Target Blank</strong></label>
												<div class="controls">
													<select id="target_blank" name="target_blank" class="form-control" data-style="input-sm btn-default">
														<option value="0">No</option>
														<option value="1">Yes</option>
													</select>
												</div>
											</div>
										</div>									
										<div class="col-md-3 col-sm-3 col-xs-3">
											<div class="form-group">
												<label class="form-label"><strong>Parent</strong></label>
												<div class="controls">
													<select id="parent" name="parent" class="form-control" data-live-search="true" data-style="input-sm btn-default">
														<option value="0">None Parent</option>
														<?=menuSelectBox($menu_items);?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="form-label"><strong>Order</strong></label>
												<div class="controls">
												   <input name="order" id="order" type="text" class="form-control input-sm">
												</div>
											</div>
											<div class="form-group">
												<label class="form-label"><strong>Icon</strong></label>
												<div class="controls">
												   <input name="icon" id="icon" type="text" class="form-control input-sm">
												</div>
											</div>
											<div class="pull-right" >
												<input type="submit" id="submit" class="btn btn-success" value="Add New">
											</div>
										</div>
									</form>
									<div class="col-md-6 col-sm-6 col-xs-6">
	                                    <div class="dd nestable">
										<?php
											function menuListItem($menu_items) {
												$code = '<ol class="dd-list">';
												if(is_array($menu_items) && count($menu_items))
												foreach ($menu_items as $menu) {
		                                            $code .= '<li id="menu-'.$menu['id'].'" class="dd-item" data-id="'.$menu['id'].'">';
		                                                $code .= '<div class="dd-handle">'.clearString($menu['icon'], false).clearString($menu['title'], false).'</div>';
		                                                $code .= '<div style="display: inline-block;position: absolute;right: 2px;top: 2px;">';
		                                                $code .= '<span style="margin-right: 5px;font-size: 11px;">'.ucfirst($menu['type']).'</span>';
														$code .= '<a class="edit btn btn-sm btn-dark" data-toggle="modal" data-target="#menu-edit" href="javascript:;" onClick="render('.$menu['id'].');" ><i class="fa fa-pencil-square-o"></i></a>';
														$code .= '&nbsp;<button class="delete btn btn-sm btn-danger" href="javascript:;" onClick="removemenu('.$menu['id'].');"><i class="fa fa-times-circle"></i></button></div>';
		                                            	if(is_array($menu['child']) && count($menu['child'])) $code .= menuListItem($menu['child']);
		                                            $code .= '</li>';
												}
												$code .= '</ol>';
												return $code;
											}
											echo menuListItem($menu_items);
										?>
	                                    </div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="<?=CDN?>/cpanel/plugins/nestable/jquery.nestable.js"></script>
			<script>
				function removemenu(id) {
					if (confirm("Are you want to delete this menu permanent?")) {
						$.post("<?=URL?>/manager/menus/ajax/delete", {id:id},
						function(data,status){
							if(data=="Deleted") {
								$("#menu-"+id).remove();
							}
						});
					}
				}
				function render(id) {
					$.post("<?=URL?>/manager/menus/ajax/get", {id:id},
					function(data,status){
						var menu = jQuery.parseJSON(data);
						$("#id").val(menu.id);
						$("#type").val(menu.type);
						$("#type").change();
						$("#link").val(menu.value);
						$("#category").val(menu.value);
						$("#category").change();
						$("#target_blank").val(menu.target_blank);
						$("#target_blank").change();
						$("#parent").val(menu.parent);
						$("#parent").change();
						$("#order").val(menu.order);
						$("#icon").val(menu.icon);
						$("#title").val(menu.title);
						$("#submit").val('Update');
					});
				}
				function clearMenu() {
					$("#id").val(0);
					$("#title").val('');
					$("#type").val('');
					$("#type").change();
					$("#category").val('');
					$("#target_blank").val('');
					$("#link").val('');
					$("#desc").val('');
					$("#icon").val('');
					$("#parent").val('');
					$("#submit").val('Add new');
				}
				function updateEvent() {
					$("#title").val('');
					if ($("#type").val() =="link")
					{
						$("#link-box").show();
						$("#category-box").hide();
					}
					else
					{
						$("#category-box").show();
						$("#link-box").hide();
					}
				}
				$(document).ready(function(e) {
					$('#category').on('change', function() {
						var category = $('#category option:selected').text();
						$("#title").val(category);
					});
					if ($('.nestable').length && $.fn.nestable) {
					    $('.nestable').nestable();
					}
					$('.dd').on('change', function() {
					    var data = $('.nestable').nestable('serialize');
					    data = window.JSON.stringify(data);
						$.post("<?=URL?>/manager/menus/ajax/update", {data:data}, function(data,status){});
					});
					updateEvent();
				});
				</script>
           <!-- the overlay element -->
			</div>
		</div>
		<!-- END MAIN CONTENT -->
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>