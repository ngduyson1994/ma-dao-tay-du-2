<?php
	if(permission('tags')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Players </strong> manager</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <div class="pull-left">
										<a id="table-edit_new" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="rendertag();">
                                            Add New <i class="fa fa-plus"></i>
                                        </a>
                                    </div>                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
									<form id="form-search" method="GET">
									<div class="row">
										<div class="col-xs-9"></div>
										<div class="col-xs-3">
											<div class="input-group">
												<input type="text" class="form-control" name="search" value="<?=($this->input->get('search'))?$this->input->get('search'):''?>" placeholder="Search">
												<span class="input-group-addon bg-blue" style="cursor: pointer" onClick="$('#form-search').submit();">     
													<span class="arrow"></span><i class="fa fa-search"></i> 
												</span>
											</div>
										</div>
									</div>
									</form>
                                    <table class="table table-striped table-hover dataTable" id="table-editable">
                                        <thead>
                                            <tr role="row">
                                                <th class="align-center"></th>
                                                <th class="align-center">Order</th>
                                                <th class="align-center">Name</th>
                                                <th class="align-center">Slug</th>
                                                <th class="align-center">Count</th>
                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
											$i=1;
											foreach($tags as $tag)
											{
										?>
                                            <tr id="tag-<?=$tag['id']?>">
                                                <td class="align-center">
													<?=$tag['id']?>
												</td>
                                                <td class="align-center">
													<?=$tag['position']?>
												</td>
                                                <td class="align-center"><img src="<?=$tag['thumb']?>" style="width:30px;"> <a style="color: #0090D9;" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="rendertag(<?=$tag['id']?>);"><? echo cutOf($tag['name'],50);?></a></td>
                                                <td class="align-center"><span id="slug_<?=$tag['id']?>"><?=$tag['slug']?></span></td>
                                                <td class="align-center"><?=$tag['count']?></td>
                                                <td class="align-center">
													<a class="edit btn btn-sm btn-dark" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="rendertag(<?=$tag['id']?>);"><i class="fa fa-pencil-square-o"></i></a>
													<button class="delete btn btn-sm btn-danger" href="javascript:;" onClick="removetag(<?=$tag['id'];?>);"><i class="fa fa-times-circle"></i></button>
                                                </td>
                                            </tr>
											<?php
												}
											?>
                                        </tbody>
                                    </table>
									<div class="col-xs-12" style="line-height: 24px;">
										<div class="col-sm-2"> Have <?=$total?> tags</div>
										<?=$this->tag_model->all_pages;?>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- the overlay element -->
			<div class="modal fade" id="item-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
				<form method="POST">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel"><strong>Edit</strong></h4>
						</div>
						<div class="modal-body">
							<div id="edit-content">
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<input type="hidden" name="id" value="" />
										<div class="form-group">
											<label for="name" class="control-label bold">Thứ tự sắp xếp</label>
											<div class="controls">
												<input type="text" name="position" value="" id="position" label="position" required="" class="form-control"/> 
											</div>
										</div>
										<div class="form-group">
											<label for="name" class="control-label bold">Name</label>
											<div class="controls">
												<input type="text" name="name" value="" id="name" label="Name" required="" class="form-control" onkeyup="autoSlug();" /> 
											</div>
										</div>
										<div class="form-group">
											<label for="slug" class="control-label bold">Slug</label>
											<div class="controls">
												<input type="text" name="slug" value="" id="slug" label="slug"  required="" class="form-control" />
											</div>
										</div>
										<div class="form-group">
											<label for="slug" class="control-label bold">Thumb</label>
											<div class="controls">
												<input type="text" name="thumb" value="" id="thumb" label="slug"  required="" class="form-control" />
											</div>
										</div>
										<div class="form-group">
											<label for="slug" class="control-label bold">Cover</label>
											<div class="controls">
												<input type="text" name="cover" value="" id="cover" label="slug"  required="" class="form-control" />
											</div>
										</div>
										<div class="form-group">
											<label for="slug" class="control-label bold">Facebook</label>
											<div class="controls">
												<input type="text" name="facebook" value="" id="facebook" label="slug" class="form-control" />
											</div>
										</div>
										<div class="form-group">
											<label for="type" class="control-label bold">Type</label>
											<div class="controls">
												<select name="type" id="type" data-live-search="true" label="Tour" class="valid form-control" required>
													<option value="tag">Tag</option>
													<option value="player">Player</option>
													<option value="team">Team</option>
													<option value="com">Bình luận viên</option>
												</select> 
											</div>
										</div>
										<div class="form-group">
											<label for="description" class="control-label bold">Description</label>
											<div class="controls">
												<textarea id="description" name="description" class="ckeditor" rows="6" ></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="submit" class="btn btn-success" value="Save changes">
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
		<!-- END MAIN CONTENT -->
		<script type="text/javascript" src="<?=CDN?>/cpanel/tinymce/tinymce.min.js"></script>
		<script src="<?=CDN?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script>
			function submitFilter(user) {
				$("#user").val(user);
				$('#form-search').submit();
			}
			function autoSlug() {
				var slug = $("#name").val();
				$.post("<?=URL?>/manager/tags/ajax/slug", {slug:slug},
				function(data,status){
					$("#slug").val(data);
				});
			}
			function removetag(id) {
				if (confirm("Are you want to delete this tag permanent?")) {
					$.post("<?=URL?>/manager/tags/ajax/delete", {id:id},
					function(data,status){
						if(data=="Deleted") {
							$("#tag-"+id).hide();
						}
					});
				}
			}
			
			function rendertag(id) {
				if(id) {
					$.post("<?=URL?>/manager/tags/ajax/get", {id:id},
					function(data,status){
						var tag = jQuery.parseJSON(data);
						if(tag.description) {
							tinymce.get('description').setContent(tag.description);
						}
						$("input[name=id]").val(tag.id);
						$("#name").val(tag.name);
						$("#position").val(tag.position);
						$("#slug").val(tag.slug);
						$("#thumb").val(tag.thumb);
						$("#cover").val(tag.cover);
						$("#facebook").val(tag.facebook);
						$("#type").val(tag.type).change();
					});
				} else {
					$("#id").val(0);
					$("#position").val('');
					$("#name").val('');
					$("#slug").val('');
					$("#description").val('');
					$("#thumb").val('');
					$("#cover").val('');
					$("#facebook").val('');
					$("#type").val('');
				}
			}
		</script>
		<script>
			$(document).ready(function() {
				tinymce.init({
					selector: "textarea",
					theme: "modern",
					relative_urls : false,
					remove_script_host: false,
					toolbar: 'codesample | bold italic sizeselect fontselect fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | insertfile undo redo | forecolor backcolor | code youtube',
					fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
					plugins: [
						 "link image preview",
						 "wordcount code fullscreen",
						 "table textcolor youtube"
				   ],
					external_filemanager_path:"<?=URL?>/filemanager/",
					filemanager_title:"Filemanager" ,
					external_plugins: { "filemanager" : "<?=URL?>/filemanager/plugin.min.js"}
				 }); 
			});
		</script>
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>