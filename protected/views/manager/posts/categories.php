<?php
	if(permission('categories')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Category </strong> manager</h3>
                        </div>
                        <div class="panel-body">
							<div class="row">
								<div class="col-md-12 m-b-20">
									<div class="pull-left">
                                        <a id="table-edit_new" class="btn btn-sm btn-danger" href="javascript:;" onClick="addnew();">
                                            Add New <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
								</div>
							</div>
							<div class="row">
								<form method="POST">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<div class="form-group">
											<label class="form-label"><strong>Name</strong></label>
											<div class="controls">
												<input id="name" name="name" onkeyup="autoSlug();" type="text" class="form-control input-sm">
												<input id="id" name="id" type="hidden">
											</div>
										</div>
										<div class="form-group">
											<label class="form-label"><strong>Slug</strong></label>
											<div class="controls">
												<input id="slug" name="slug" type="text" class="form-control input-sm">
											</div>
										</div>
										<div class="form-group">
											<label class="form-label"><strong>Icon</strong></label>
											<div class="controls">
												<input id="icon" name="icon" type="text" class="form-control input-sm">
											</div>
										</div>
										<div class="form-group">
											<label class="form-label"><strong>Parent</strong></label>
											<div class="controls">
												<select id="parent" name="parent" ><option value="0">None Parent</option>
												<?=catSelectBox($categories, true);?>
												</select>
											</div>
										</div>
										<div class="pull-right form-group">
											<div class="controls">
												<input type="submit" id="submit" class="btn btn-success" value="Add new">
											</div>
										</div>
									</div>
								</form>
								<div class="col-md-6 col-sm-6 col-xs-6 table-responsive table-red">
                                    <div class="dd nestable">
									<?php
										function catListItem($categories) {
											$code = '<ol class="dd-list">';
											if(is_array($categories) && count($categories))
											foreach ($categories as $category) {
	                                            $code .= '<li id="cat-'.$category['id'].'" class="dd-item" data-id="'.$category['id'].'">';
                                                $code .= '<div class="dd-handle">'.$category['id'].' - '.clearString($category['name'], false).'</div>';
                                                $code .= '<div style="display: inline-block;position: absolute;right: 2px;top: 2px;"><a class="edit btn btn-sm btn-dark" data-toggle="modal" data-target="#menu-edit" href="javascript:;" onClick="render('.$category['id'].');" ><i class="fa fa-pencil-square-o"></i></a>';
												$code .= '&nbsp;<button class="delete btn btn-sm btn-danger" href="javascript:;" onClick="removecat('.$category['id'].');"><i class="fa fa-times-circle"></i></button></div>';
                                            	if($category['child'] && count($category['child'])) $code .= catListItem($category['child']);
	                                            $code .= '</li>';
											}
											$code .= '</ol>';
											return $code;
										}
										echo catListItem($categories);
									?>
                                    </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="<?=CDN?>/cpanel/plugins/nestable/jquery.nestable.js"></script>
			<script>
				function removecat(id) {
					if (confirm("Are you want to delete this category permanent?")) {
						$.post("<?=URL?>/manager/categories/ajax/delete", {id:id},
						function(data,status){
							if(data=="Deleted") {
								$("#cat-"+id).hide();
							}
						});
					}
				}
				function render(id) {
					$.post("<?=URL?>/manager/categories/ajax/get", {id:id},
					function(data,status){
						var cat = jQuery.parseJSON(data);
						$("#id").val(cat.id);
						$("#name").val(cat.name);
						$("#slug").val(cat.slug);
						$("#parent").val(cat.parent);
						$("#parent").change();
						$("#order").val(cat.order);
						$("#icon").val(cat.icon);
						$("#submit").val('Update');
					});
				}
				function addnew() {
						$("#id").val(0);
						$("#name").val('');
						$("#slug").val('');
						$("#parent").val('');
						$("#parent").change();
						$("#order").val('');
						$("#icon").val('');
						$("#submit").val('Add new');
				}
				function autoSlug() {
					var slug = $("#name").val();
					$.post("<?=URL?>/manager/categories/ajax/slug", {slug:slug},
					function(data,status){
						$("#slug").val(data);
					});
				}
				$(document).ready(function(e) {
					if ($('.nestable').length && $.fn.nestable) {
					    $('.nestable').nestable();
					}
					$('.dd').on('change', function() {
					    var data = $('.nestable').nestable('serialize');
					    data = window.JSON.stringify(data);
						$.post("<?=URL?>/manager/categories/ajax/update", {data:data}, function(data,status){});
					});
				});
			</script>
			<script type="text/javascript" src="<?=CDN?>/cpanel/ckeditor/ckeditor.js"></script>
            <!-- the overlay element -->
		</div>
		<!-- END MAIN CONTENT -->
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>