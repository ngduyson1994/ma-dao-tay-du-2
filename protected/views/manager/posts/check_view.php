<?php
if(permission('posts')) {
    ?>
    <!-- BEGIN MAIN CONTENT -->
    <div id="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading bg-red">
                        <h3 class="panel-title"><strong>Checks </strong>Views Posts</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 m-b-20">
                                <!--
                                <div class="pull-left">
                                    <a id="table-edit_new" class="btn btn-sm btn-danger" href="<?=URL?>/manager/posts/item">
                                      Add New <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                                 -->
                                <? if(isset($return)) { ?>
                                    <div class="alert alert-null <?=($return['status']=='success')?'alert-info':'alert-danger';?> pull-right">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <?=$return['msg']?>&nbsp;
                                    </div>
                                <? } ?>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
                                <form id="form-search" method="GET">
                                    <div class="row">
                                        <div class="col-xs-9">

                                            <div class="col-xs-6" style="padding: 0 5px 0 0">
                                                <input name="sort-start" id="sort-start" class="input-s-sm input-s datepicker-input form-control" value="<?=$this->input->get('sort-start')?>" style="width:90px;" type="text" value="" data-date-format="mm-dd-yyyy">
                                                <input name="sort-end" id="sort-end" class="input-s-sm input-s datepicker-input form-control" value="<?=$this->input->get('sort-end')?>" style="width:90px;" type="text" value="" data-date-format="mm-dd-yyyy">
                                                <button type="button" class="btn btn-sm btn-danger" onClick="$('#form-search').submit();">Filter <i class="fa fa-info-circle"></i></button>
                                                <a class="btn btn-sm btn-danger" href="<?=URL?>/manager/posts/checkviews">Clear <i class="fa fa-times-circle"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped table-hover dataTable" id="table-editable">
                                    <thead>
                                    <tr role="row">
                                        <th class="align-center">Số lượng views </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <td class="align-center"><?= number_format($sum_views) ?> <V></V>iews</td>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- the overlay element -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
    <script src="<?=CDN?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script>
        $('#sort-start').datepicker();
        $('#sort-end').datepicker();
        $("[name='feature']").bootstrapSwitch();
        $("[name='feature']").on('switchChange.bootstrapSwitch', function(event, state) {
            var id = $(this).val();
            var value;
            if(state) value=1;
            else value=0;
            $.post("<?=URL?>/manager/posts/ajax/feature", {id:id, value: value},
                function(data,status){});
        });
        function submitFilter(user) {
            $("#user").val(user);
            $('#form-search').submit();
        }
        function thumb(element, id) {
            $(element).html('<img src="<?=CDN?>/cpanel/img/loading.gif" height="14px" />');
            $.post("<?=URL?>/manager/posts/ajax/thumb", {id:id}, function(data, status) {
                if(data=='success') $(element).html('<i class="fa fa-check" style="color: green"></i>');
                else {
                    $(element).html('<i class="fa fa-warning" style="color: red"></i>');
                    alert(data);
                }
            });
        }
        function removepost(id) {
            if (confirm("Are you want to delete this post permanent?")) {
                $.post("<?=URL?>/manager/posts/ajax/delete", {id:id},
                    function(data,status){
                        if(data=="Success") {
                            $("#post-"+id).hide();
                        }
                    });
            }
        }
        function force_delete(id) {
            if (confirm("Are you want to delete this post permanent?")) {
                $.post("<?=URL?>/manager/posts/ajax/force_delete", {id:id},
                    function(data,status){
                        if(data=="Success") {
                            $("#post-"+id).hide();
                        }
                    });
            }
        }
        function restore(id) {
            $.post("<?=URL?>/manager/posts/ajax/restore", {id:id},
                function(data,status){
                    if(data=="Success") {
                        $("#post-"+id).hide();
                    }
                });
        }
    </script>
    <?
} else {
    ?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>