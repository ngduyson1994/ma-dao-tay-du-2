<?php
	if(permission('pages')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Posts </strong> manager</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <div class="pull-left">
                                        <a id="table-edit_new" class="btn btn-sm btn-danger" href="<?=URL?>/manager/pages/item">
                                            Add New <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
									<form id="form-search" method="GET">
									<div class="row">
										<div class="col-xs-9">
											<div class="col-xs-2" style="padding: 0 5px 0 0">
												<select id="status" name="status" class="form-control" data-style="input-sm btn-default" onChange="$('#form-search').submit();">
													<? $s = $this->input->get('status') ?>
													<option value="">Status</option>
													<option value="publish" <?=($s=='publish')?'selected':''?> data-icon="fa-globe">Publish</option>
													<option value="pending"<?=($s=='pending')?'selected':''?> data-icon="fa-clock-o">Pending</option>
													<option value="draft" <?=($s=='draft')?'selected':''?> data-icon="fa-save">Draft</option>
													<option value="trash" <?=($s=='trash')?'selected':''?> data-icon="fa-trash-o">Trash</option>
												</select>
											</div>
											<div class="col-xs-6" style="padding: 0 5px 0 0">
												<input name="sort-start" id="sort-start" class="input-s-sm input-s datepicker-input form-control" value="<?=$this->input->get('sort-start')?>" style="width:90px;" type="text" value="" data-date-format="mm-dd-yyyy">
												<input name="sort-end" id="sort-end" class="input-s-sm input-s datepicker-input form-control" value="<?=$this->input->get('sort-end')?>" style="width:90px;" type="text" value="" data-date-format="mm-dd-yyyy">
												<input type="hidden" value="<?=$this->input->get('user')?>" name="user" id="user" />
												<button type="button" class="btn btn-sm btn-danger" onClick="$('#form-search').submit();">Filter <i class="fa fa-info-circle"></i></button>
												<a class="btn btn-sm btn-danger" href="<?=URL?>/manager/pages">Clear <i class="fa fa-times-circle"></i></a>
											</div>
										</div>
										<div class="col-xs-3">
											<div class="input-group">
												<input type="text" class="form-control" name="search" value="<?=($this->input->get('search'))?$this->input->get('search'):''?>" placeholder="Search">
												<span class="input-group-addon bg-blue" style="cursor: pointer" onClick="$('#form-search').submit();">     
													<span class="arrow"></span><i class="fa fa-search"></i> 
												</span>
											</div>
										</div>
									</div>
									</form>
                                    <table class="table table-striped table-hover dataTable" id="table-editable">
                                        <thead>
                                            <tr role="row">
                                                <th class="align-center">ID</th>
                                                <th class="align-center">Title</th>
                                                <th class="align-center">Author</th>
                                                <th class="align-center">Status</th>
                                                <th class="align-center">Social</th>
                                                <th class="align-center">Created</th>
                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
											$i=1;
											if(is_array($posts)) foreach($posts as $post) {
										?>
                                            <tr id="page-<?=$post['id']?>">
                                                <td class="align-center"><?=$i++?></td>
                                                <td><a style="color: #0090D9;" href="<?=URL?>/manager/pages/item/<?=$post['id']?>"><? echo cutOf($post['title'],70);?></a></td>
												<td class="align-center">
													<?php if(isset($post['list'])) { ?>
													<a style="<?=(isset($groups[$post['list']['gid']]))?$groups[$post['list']['gid']]['style']:''?>" href="javascript:;" onClick="submitFilter('<?=strtolower($post['admin']['username'])?>');"><i class="fa fa-user"></i> <?=$post['admin']['username']?></a>
												<?php } ?>
												</td>
                                                <td class="align-center">
													 <?php
														if($post['status'] == 'publish') echo '<i class="fa fa-globe"></i> Publish';
														elseif($post['status'] == 'pending') echo '<i class="fa fa-clock-o"></i> Pending';
														elseif($post['status'] == 'trash') echo '<i class="fa fa-trash-o"></i> Trash';
														else echo '<i class="fa fa-save"></i> Draft';
													 ?>
												</td>
                                                <td class="align-center" style="font-size: 12px"><i class="fa fa-eye"></i> <?=$post['views']?> <i class="fa fa-thumbs-o-up"></i> <?=$post['likes']?> <i class="fa fa-comments"></i> <?=$post['comments']?></td>
                                                <td class="align-center"><?=date("d/m/Y",$post['published'])?></td>
                                                <td class="align-center">
													<input type="checkbox" name="feature" value="<?=$post['id']?>" data-size="small" data-on-text="On" data-off-text="Off" <?=($post['feature'])?'checked':''?>>
													<a class="edit btn btn-sm btn-dark"  href="<?=URL?>/manager/pages/item/<?=$post['id']?>"><i class="fa fa-pencil-square-o"></i></a>
													<?php  if(permission('page_delete')) { ?>
													<button class="delete btn btn-sm btn-danger" href="javascript:;" onClick="removepages(<?=$post['id']?>);"><i class="fa fa-times-circle"></i></button>
													<?php } ?>
                                                </td>
                                            </tr>
											<?php 
												}
											?>
                                        </tbody>
                                    </table>
									<div class="col-xs-12" style="line-height: 24px;">
										<div class="col-sm-2"> Have <?=$total?> posts</div>
										<?=$this->post_model->all_pages;?>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- the overlay element -->
		</div>
		<!-- END MAIN CONTENT -->
		<script src="<?=CDN?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script>
			$('#sort-start').datepicker();
			$('#sort-end').datepicker();
			$("[name='feature']").bootstrapSwitch();
			$("[name='feature']").on('switchChange.bootstrapSwitch', function(event, state) {
				var id = $(this).val();
				var value;
				if(state) value=1;
				else value=0;
				$.post("<?=URL?>/manager/pages/ajax/feature", {id:id, value: value},
				function(data,status){});
			});
			function submitFilter(user) {
				$("#user").val(user);
				$('#form-search').submit();
			}
			function removepages(id) {
				if (confirm("Are you want to delete this post permanent?")) {
					$.post("<?=URL?>/manager/pages/ajax/delete", {id:id},
					function(data,status){
						if(data=="Deleted") {
							$("#page-"+id).hide();
						}
					});
				}
			}
		</script>
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>