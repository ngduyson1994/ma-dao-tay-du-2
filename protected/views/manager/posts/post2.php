<?php
	if(permission('posts')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong><?=(isset($data))?'Edit post':'Add post'?> </strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <div class="btn-group pull-left">
                                        <a id="table-edit_new" class="btn btn-danger btn-sm" href="<?=URL?>/manager/posts">
                                            <i class="fa fa-arrow-left"></i> Back
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
									<form method="POST" enctype="multipart/form-data">
										<div id="edit-content">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="col-md-9" style="padding-left: 0">
														<div class="form-group">
															<div class="controls">
																<div class="col-md-1" style="padding:0">
																	<label class="control-label"><strong>Title</strong></label>
																</div>
																<div class="col-md-11" style="padding:0">
																	<input type="text" value="<?=(isset($data))?clearString($data['title']):'';?>" required="" class="form-control" id="title" name="title" value="" autocomplete="off" onkeyup="autoSlug();" placeholder="Enter title here">
																	<input id="id" name="id" value="<?=(isset($data))?$data['id']:'';?>" type="hidden">
																	<input id="slug" name="slug" value="<?=(isset($data))?$data['slug']:'';?>" type="hidden">
																</div>
																<div class="clearfix"></div>
															</div>
														</div>
														<div class="form-group">
															<div class="controls">
																<label class="control-label"><strong>Permalink:</strong></label>
																<?=URL?>/<span id="showslug"><?=(isset($data))?$data['slug']:'';?></span>
																<div >
																	<textarea id="content" name="content" rows="15" ><?=(isset($data))?clearString($data['content']):'';?></textarea>
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="controls">
																<label class="control-label"><strong>Tóm tắt</strong></label>
																<div >
																	<textarea id="summary" name="summary" rows="15" ><?=(isset($data))?clearString($data['summary']):'';?></textarea>
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="panel panel-default">
																<div class="panel-heading">
																	<h4 class="panel-title">Tags</h4>
																</div>
																<div id="tags-box" class="panel-collapse collapse in">
																	<div class="panel-body">
																		<?php
																			$list = array();
																			if(isset($data['tags'])) foreach($data['tags'] as $key=>$tag) {
																				$list[] = $key;
																			}
																			$list = implode(',', $list);
																		?>
																		<input type="hidden" id="tags" name="tags" style="width:100%;" value="<?=$list?>"/>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-6 col-sm-6 col-xs-6">
															<div class="form-group">
																<label class="form-label"><strong>Description</strong></label><br />
																<div class="controls">
																	<input name="description" type="text" value="<?if((isset($data))) echo $data['description'];?>" class="form-control" >
																</div>
															</div>
														</div>
														<div class="col-md-6 col-sm-6 col-xs-6">
															<div class="form-group">
																<label class="form-label"><strong>Keyword</strong></label><br />
																<div class="controls">
																	<input name="keyword" type="text" value="<?if((isset($data))) echo $data['keyword'];?>" class="form-control" >
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-3" style="padding: 0">
														<div class="panel-group panel-accordion">
															<div class="panel panel-default">
																<div class="panel-heading">
																	<h4 class="panel-title">
																		<a data-toggle="collapse" data-parent="#accordion2" href="#pub">Publish</a>
																	</h4>
																</div>
																<div id="pub" class="panel-collapse collapse in">
																	<div class="panel-body">
																		<div class="form-group">
																			<div class="controls" style="margin-bottom: 5px">
																				<input id="username" name="username" class="form-control" value="<?=((isset($data['user'])))?$data['user']['username']:''?>" />
																				<input id="user_id" name="user_id" type="hidden" value="<?=((isset($data['user'])))?$data['user']['id']:''?>" />
																			</div>
																			<div class="controls" style="margin-bottom: 5px">
																				<div class="col-md-6" style="padding-left: 0">
                                                                                    <select id="type" name="type" class="form-control" data-style="input-sm btn-default" onChange="ChangeType()" >
																						<option value="news" <?if((isset($data))) echo ($data['type']=='news')?'selected':''?> data-icon="fa-file-text">News</option>
																						<option value="video" <?if((isset($data))) echo ($data['type']=='video')?'selected':''?> data-icon="fa-video-camera">Video</option>
																						<option value="page" <?if((isset($data))) echo ($data['type']=='page')?'selected':''?> data-icon="fa-pencil-square">Page</option>
																					</select>
																				</div>
																				<div class="col-md-6" style="padding: 0">
																					<select id="status" name="status" class="form-control" data-style="input-sm btn-default">
																						<?php if(permission('post_publish')) {?><option value="publish" <?if((isset($data))) echo ($data['status']=='publish')?'selected':''?> data-icon="fa-globe">Publish</option><?php } ?>
																						<option value="pending" <?if((isset($data))) echo ($data['status']=='pending')?'selected':''?> data-icon="fa-clock-o">Pending</option>
																					</select>
																				</div>
																				<div class="clearfix"></div>
																			</div>
																			<div class="controls pull-right" style="position: relative;">
																				<input type="checkbox" name="feature" value="1" data-size="small" data-on-text="Pin" data-off-text="Off" <? if(isset($data)) echo ($data['feature'])?'checked':''?>>
																			</div>
																			<label class="form-label pull-left"> <i class="fa fa-calendar"></i> <strong>&nbsp;Time&nbsp;</strong></label>
																			<div class="clearfix"></div>
																			<input name="date" value="<?if((isset($data))) echo date("m-d-Y", $data['published']);?>" id="date" class="input-s-sm input-s form-control" style="width:60%;float:left;margin-right:2px" data-date-format="mm-dd-yyyy" type="text" >
																			<input name="hour" type="text" value="<?if((isset($data))) echo date("G", $data['published']);?>" style="width:15%;" class="form-control" >
																			<span style="margin-left: 2%;">:</span>
																			<input name="min" type="text" value="<?if((isset($data))) echo date("i", $data['published']);?>" style="width:15%;" class="form-control"  >
																		</div>
																		<hr style="margin: 8px;" >
																		<div class="form-group" >
																			<?if(isset($data)) { ?>
																				<a href="<?=getPostURL($data);?>" class="btn btn-sm btn-default pull-left" style="width: 80px" target="_blank">
																					View
																				</a> 
																			<? } ?>
																			<input type="submit" class="btn btn-primary pull-right" style="padding: 3px 18px; margin-bottom: 2px;" value="<?php if(isset($data)) echo "Update"; else echo (permission('post_publish'))?'Publish':'Pending';?>">
																		</div>
																	</div>
																</div>
															</div>
															<div class="panel panel-default">
																<div class="panel-heading">
																	<h4 class="panel-title">
																		<a data-toggle="collapse" data-parent="#accordion2" href="#categories-box">Categories</a>
																	</h4>
																</div>
																<div id="categories-box" class="panel-collapse collapse in">
																	<div class="panel-body">
																		<?php
																			$this->load->helper('views');
																			if(!isset($data['categories'])) $data['categories'] = null;
																			echo catPostBox($categories, $data['categories']);
																		?>
																	</div>
																</div>
															</div>
															<div class="panel panel-default">
																<div class="panel-heading">
																	<h4 class="panel-title">
																		<a data-toggle="collapse" data-parent="#accordion2" href="#media-box">Orther</a>
																	</h4>
																</div>
																<div id="media-box" class="panel-collapse collapse in">
																	<div class="panel-body">
                                                                        <div class="form-group">
                                                                            <label for="tournament_id" class="control-label bold">Tournament</label>
                                                                            <div class="controls">
                                                                                <select id="tournament_id" name="tournament_id" class="form-control" data-live-search="true" data-style="input-sm btn-default">
                                                                                    <option value="">None Tournament </option>
                                                                                    <? if (isset($touranament) && count($tournamen)) foreach($tournament as $a) { ?>
                                                                                        <option value="<?=$a['id']?>" <?if((isset($data))) echo ($data['tournament_id']==$a['id'])?'selected':''?>  data-content="<?=$a['name']?>"><?=$a['name']?></option>
                                                                                    <? } ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">

                                                                            <label for="home_id" class="control-label bold">Đội nhà</label>
                                                                            <div class="controls">
                                                                                <select id="home_id" name="home_id"  data-live-search="true" class="form-control" data-style="input-sm btn-default">
                                                                                    <option value="">Chọn đội nhà</option>
                                                                                    <? foreach($home as $h) { ?>
                                                                                        <option value="<?=$h['id']?>" <?if((isset($data))) echo ($data['home_id']==$h['id'])?'selected':''?> data-content="<?=$h['name']?>"><?=$h['name']?></option>
                                                                                    <? } ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="away_id" class="control-label bold">Đội khách</label>
                                                                            <div class="controls">
                                                                                <select id="away_id" name="away_id" data-live-search="true" class="form-control" data-style="input-sm btn-default">
                                                                                    <option value="">Chọn đội khách</option>
                                                                                    <? foreach($home as $a) { ?>
                                                                                        <option value="<?=$a['id']?>" <?if((isset($data))) echo ($data['away_id']==$a['id'])?'selected':''?> data-content="<?=$a['name']?>"><?=$a['name']?></option>
                                                                                    <? } ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="form-label pull-left "> <i class="fa fa-image"></i><strong>&nbsp;Thumb&nbsp;</strong></label>
                                                                            <div class="input-group">
                                                                                <input type="file" name="uploadthumb" class="filestyle" data-buttonText="" data-size="sm">
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
																		<div class="form-group">
																			<label class="form-label pull-left "><i class="fa fa-chain"></i><strong>URL&nbsp;</strong></label>
																			<?php
																			if(isset($data)) $thumb = getThumb($data);
																			else $thumb = '';
																			?>
																			<div class="input-group">
																				<input type="text" class="input-s-sm input-s form-control" name="thumb"
																				value="<?=(isset($data['thumb']) && $data['thumb'])?$data['thumb']:$thumb;?>">
																			</div>
																			<div class="clearfix"></div>
																			<?php
																			if(isset($thumb) && $thumb) {
																			?>
																			<img src="<?=$thumb?>" style="max-width: 200px; max-height: 185px;" />
																			<?php
																			} ?>
																		</div>
																	</div>
																</div>
															</div>
															
															<div class="panel panel-default">
																<div class="panel-heading">
																	<h4 class="panel-title">
																		<a data-toggle="collapse" data-parent="#accordion2" href="#media-box">Upload image</a>
																	</h4>
																</div>
																<div id="media-box" class="panel-collapse collapse in">
																	<div class="panel-body">
																		<div class="form-group">	                           
																			<span class="btn btn-lg btn-default fileinput-button">
																				<span class="fa fa-upload"></span> Select Images
																				<input id="fileupload" type="file" name="files" data-url="<?=URL?>/manager/posts/ajax/upload" />
																			</span>
																			<br />
																			<div class="progress progress-sm m-t-sm">
																				<div class="progress-bar progress-bar-info" style="width: 0%"></div>
																			</div>
																			<div id="image_upload" style="max-height: 270px;overflow: auto;"></div>
																		</div>
																	</div>
																</div>
															</div>
															
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		<!-- END MAIN CONTENT -->
		<script type="text/javascript" src="<?=CDN?>/cpanel/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="<?=CDN?>/cpanel/ckfinder/ckfinder.js"></script>
		<script type="text/javascript" src="<?=CDN?>/cpanel/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="<?=CDN?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="<?=CDN?>/cpanel/plugins/filestyle/bootstrap-filestyle.min.js"></script>
		<script type="text/javascript" src="<?=CDN?>/cpanel/plugins/fileupload/vendor/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="<?=CDN?>/cpanel/plugins/fileupload/jquery.iframe-transport.js"></script>
		<script type="text/javascript" src="<?=CDN?>/cpanel/plugins/fileupload/jquery.fileupload.js"></script>
		<script type="text/javascript">
			$(function() {
				var editor = CKEDITOR.replace('content',
				{
				filebrowserBrowseUrl : '<?=CDN."/cpanel/ckfinder/ckfinder.html"; ?>',
				filebrowserImageBrowseUrl : '<?=CDN."/cpanel/ckfinder/ckfinder.html?Type=Images";?>',
				filebrowserFlashBrowseUrl : '<?=CDN."/cpanel/ckfinder/ckfinder.html?Type=Flash" ?>',
				filebrowserUploadUrl : '<?=CDN."/cpanel/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
				filebrowserImageUploadUrl : '<?=CDN."/cpanel/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images";?>',
				filebrowserFlashUploadUrl : '<?=CDN."/cpanel/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash";?>',
				filebrowserWindowWidth : '800',
				filebrowserWindowHeight : '480'
				});
				CKFinder.setupCKEditor( editor, "<?=CDN.'/cpanel/ckfinder/'?>" );
			})
		</script>
		<script type="text/javascript">
			$(function() {
				var editor = CKEDITOR.replace('summary',
				{
				filebrowserBrowseUrl : '<?=CDN."/cpanel/ckfinder/ckfinder.html"; ?>',
				filebrowserImageBrowseUrl : '<?=CDN."/cpanel/ckfinder/ckfinder.html?Type=Images";?>',
				filebrowserFlashBrowseUrl : '<?=CDN."/cpanel/ckfinder/ckfinder.html?Type=Flash" ?>',
				filebrowserUploadUrl : '<?=CDN."/cpanel/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
				filebrowserImageUploadUrl : '<?=CDN."/cpanel/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images";?>',
				filebrowserFlashUploadUrl : '<?=CDN."/cpanel/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash";?>',
				filebrowserWindowWidth : '800',
				filebrowserWindowHeight : '480'
				});
				CKFinder.setupCKEditor( editor, "<?=CDN.'/cpanel/ckfinder/'?>" );
			})
		</script>
		<script type="text/javascript">
			$('#date').datepicker();
			$("[name='feature']").bootstrapSwitch();
			$("[name='remote_image']").bootstrapSwitch();
			function autoSlug() {
				var slug = $("#title").val();
				var id = $("#id").val();
				$.post("<?=URL?>/manager/posts/ajax/slug", {slug:slug, id:id},
				function(data,status){
					$("#showslug").html(data);
					$("#slug").val(data);
				});
			}
			function ChangeType()
			{
				if ($("#type").val()=="news")
				{
					$("#news").show();
					$("#video").hide();
					$("#content").show();
					$("#text").hide();
				}
				else
				{
					$("#video").show();
					$("#news").hide();
					$("#content").hide();
					$("#text").show();
				}
			}
			var countimg = 0;
			$(document).ready(function(e) {
				ChangeType();
				$('.progress').hide();
				$('#fileupload').fileupload({
					dataType: 'json',
					done: function (e, data) {
						if(data.result.status=='success') {
							countimg++;
							$('#image_upload').prepend('<div class="input-group" style="margin-top: 5px;"><span class="input-group-addon">'+countimg+'<\/span><input class="form-control" onClick="select();" value="'+data.result.img+'" \/><\/div>');
							$('.progress .progress-bar').css('width','0%');
							$('.progress').hide();
						} else {
							$('#image_upload').prepend('<div style="margin-top: 5px;">'+data.result.msg+'<\/div>');
							$('.progress .progress-bar').css('width','0%');
							$('.progress').hide();
						}
					},
					progressall: function (e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
						$('.progress').show();
						$('.progress .progress-bar').css('width', progress + '%');
					},
					progress: function (e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
						$('.progress').show();
						$('.progress .progress-bar').css('width',progress + '%');
					},
				});
				<?php
					$list = '';
					if(is_array($tags))
					foreach($tags as $tag) {
						$list .= '{id:'.$tag['id'].', text:"'.trim($tag['name']).'"},';
					}
				?>
				if ($.fn.select2) {
					$("#tags").select2({
						tags:[<?=$list?>]
					});
				}
			});
		</script>
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>