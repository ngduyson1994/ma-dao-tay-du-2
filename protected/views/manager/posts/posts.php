<?php
	if(permission('posts')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Posts </strong> manager</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <div class="pull-left">
                                        <a id="table-edit_new" class="btn btn-sm btn-danger" href="<?=URL?>/manager/posts/item">
                                            Add New <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
									<? if(isset($return)) { ?>
									<div class="alert alert-null <?=($return['status']=='success')?'alert-info':'alert-danger';?> pull-right">
										<button type="button" class="close" data-dismiss="alert">×</button>
										<?=$return['msg']?>&nbsp;
									</div>
									<? } ?>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
									<form id="form-search" method="GET">
									<div class="row">
										<div class="col-xs-9">
											<div class="col-xs-2" style="padding: 0 5px 0 0">
												<select id="category" name="category" class="form-control" data-style="input-sm btn-default" onChange="$('#form-search').submit();">
													<option value="">Category</option>
													<?=catSelectBox($categories, true);?>
												</select>
											</div>
											<div class="col-xs-2" style="padding: 0 5px 0 0">
												<select id="type" name="type" class="form-control" data-style="input-sm btn-default" onChange="$('#form-search').submit();">
													<? $s = $this->input->get('type') ?>
													<option value="video"<?=($s=='video')?'selected':''?> data-icon="fa-video-camera">Video</option>
													<option value="news" <?=($s=='news')?'selected':''?> data-icon="fa-file-text">News</option>
													<option value="page"<?=($s=='page')?'selected':''?> data-icon="fa-pencil-square">Page</option>
												</select>
											</div>
											<div class="col-xs-2" style="padding: 0 5px 0 0">
												<select id="status" name="status" class="form-control" data-style="input-sm btn-default" onChange="$('#form-search').submit();">
													<? $s = $this->input->get('status') ?>
													<option value="">Status</option>
													<option value="" data-icon="fa-globe">Publish</option>
													<option value="pending"<?=($s=='pending')?'selected':''?> data-icon="fa-clock-o">Pending</option>
												</select>
											</div>
											<div class="col-xs-2" style="padding: 0 5px 0 0">
											</div>
											<div class="col-xs-6" style="padding: 0 5px 0 0">
												<input name="sort-start" id="sort-start" class="input-s-sm input-s datepicker-input form-control" value="<?=$this->input->get('sort-start')?>" style="width:90px;" type="text" value="" data-date-format="mm-dd-yyyy">
												<input name="sort-end" id="sort-end" class="input-s-sm input-s datepicker-input form-control" value="<?=$this->input->get('sort-end')?>" style="width:90px;" type="text" value="" data-date-format="mm-dd-yyyy">
												<input type="hidden" value="<?=$this->input->get('user')?>" name="user" id="user" />
												<button type="button" class="btn btn-sm btn-danger" onClick="$('#form-search').submit();">Filter <i class="fa fa-info-circle"></i></button>
												<a class="btn btn-sm btn-danger" href="<?=URL?>/manager/posts">Clear <i class="fa fa-times-circle"></i></a>
											</div>
										</div>
										<div class="col-xs-3">
											<div class="input-group">
												<input type="text" class="form-control" name="search" value="<?=($this->input->get('search'))?$this->input->get('search'):''?>" placeholder="Search">
												<span class="input-group-addon bg-blue" style="cursor: pointer" onClick="$('#form-search').submit();">     
													<span class="arrow"></span><i class="fa fa-search"></i> 
												</span>
											</div>
										</div>
									</div>
									</form>
                                    <table class="table table-striped table-hover dataTable" id="table-editable">
                                        <thead>
                                            <tr role="row">
                                                <th class="align-center">ID</th>
                                                <th class="align-center">Title</th>
                                                <th class="align-center">Author</th>
                                                <th class="align-center">Status</th>
                                                <th class="align-center">Views</th>
                                                <th class="align-center">Date</th>
                                                <th class="align-center">Is slider</th>
                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
											$i=1;
										if(isset($posts) && is_array($posts)) {
											foreach($posts as $post) {
												switch($post['type']) {
													case 'news': $icon = '<i class="fa fa-file-text"></i>'; break;
													case 'video': $icon = '<i class="fa fa-video-camera"></i>'; break;
													case 'page': $icon = '<i class="fa fa-pencil-square"></i>'; break;
													default: $icon = '';
												}
										?>
                                            <tr id="post-<?=$post['id']?>">
                                                <td class="align-center">
													<?=$i++;?>
												</td>
                                                <td>
                                                	&nbsp;<a href="<?=URL?>/manager/posts/item/<?=$post['id']?>"><?=$icon?> <? echo cutOf(clearString($post['title']), 50);?></a>
                                                </td>
												<td class="align-center">
												<?php if(isset($post['admin'])) { ?>
													<a href="javascript:;" onClick="submitFilter('<?=$post['admin']['id']?>');"><i class="fa fa-user"></i> <?=$post['admin']['username']?></a>
												<?php } ?>
												</td>
                                                <td class="align-center">
													 <?php
														if($post['status'] == 'publish' && $post['published'] >= time()) echo '<i class="fa fa-calendar"></i> Scheduled';
														elseif($post['status'] == 'publish') echo '<i class="fa fa-globe"></i> Publish';
														elseif($post['status'] == 'pending') echo '<i class="fa fa-clock-o"></i> Pending';
														else echo '<i class="fa fa-save"></i> Draft';
													 ?>
												</td>
                                                <td class="align-center" style="font-size: 12px"><i class="fa fa-eye"></i> <?=number_format($post['views'])?></td>
                                                <td class="align-center"><?=date("G:i d/m",$post['published'])?></td>
                                                <td class="align-center">
													<input type="checkbox" name="is_slider" value="<?=$post['id']?>" data-size="small" data-on-text="On" data-off-text="Off" <?=($post['is_slider'])?'checked':''?>>
                                                </td>
                                                <td class="align-center">
                                                    <?php
                                                    if($this->input->get('trash')==true) {
                                                        $delete = 'force_delete';
                                                        ?>
                                                        <a class="edit btn btn-sm btn-dark"  href="javascript:;" onClick="restore(<?=$post['id']?>)"><i class="fa fa-asterisk"></i> Restore</a>
                                                    <?php } else {
                                                        $delete = 'removepost';
                                                        ?>
                                                        <input type="checkbox" name="feature" value="<?=$post['id']?>" data-size="small" data-on-text="On" data-off-text="Off" <?=($post['feature'])?'checked':''?>>
                                                    <?php } ?>
                                                    <a class="edit btn btn-sm btn-dark"  href="<?=URL?>/manager/posts/item/<?=$post['id']?>"><i class="fa fa-pencil-square-o"></i></a>
                                                    <button class="delete btn btn-sm btn-danger" href="javascript:;" onClick="<?=$delete?>(<?=$post['id']?>);"><i class="fa fa-times-circle"></i></button>
                                                </td>
                                            </tr>
											<?php 
												}
											} else echo '<tr><td colspan="10" style="text-align: center">No results</td></tr>';
											?>
                                        </tbody>
                                    </table>
									<div class="col-xs-12" style="line-height: 24px;">
										<div class="col-sm-2"> Have <?=$total?> posts</div>
										<?=$this->post_model->all_pages;?>
									</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- the overlay element -->
			</div>
		</div>
		<style>
			#main-content .table a {
				color: #666 !important;
			}
		</style>
		<!-- END MAIN CONTENT -->
		<script src="<?=CDN?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script>
			$('#sort-start').datepicker();
			$('#sort-end').datepicker();
			$("[name='feature']").bootstrapSwitch();
            $("[name='is_slider']").bootstrapSwitch();
			$("[name='feature']").on('switchChange.bootstrapSwitch', function(event, state) {
				var id = $(this).val();
				var value;
				if(state) value=1;
				else value=0;
				$.post("<?=URL?>/manager/posts/ajax/feature", {id:id, value: value},
				function(data,status){});
			});$("[name='is_slider']").on('switchChange.bootstrapSwitch', function(event, state) {
				var id = $(this).val();
				var value;
				if(state) value=1;
				else value=0;
				$.post("<?=URL?>/manager/posts/ajax/is_slider", {id:id, value: value},
				function(data,status){});
			});
			function submitFilter(user) {
				$("#user").val(user);
				$('#form-search').submit();
			}
			function thumb(element, id) {
				$(element).html('<img src="<?=CDN?>/cpanel/img/loading.gif" height="14px" />');
				$.post("<?=URL?>/manager/posts/ajax/thumb", {id:id}, function(data, status) {
					if(data=='success') $(element).html('<i class="fa fa-check" style="color: green"></i>');
					else {
						$(element).html('<i class="fa fa-warning" style="color: red"></i>');
						alert(data);
					}
				});
			}
			function removepost(id) {
				if (confirm("Are you want to delete this post permanent?")) {
					$.post("<?=URL?>/manager/posts/ajax/delete", {id:id},
					function(data,status){
						if(data=="Success") {
							$("#post-"+id).hide();
						}
					});
				}
			}
			function force_delete(id) {
				if (confirm("Are you want to delete this post permanent?")) {
					$.post("<?=URL?>/manager/posts/ajax/force_delete", {id:id},
					function(data,status){
						if(data=="Success") {
							$("#post-"+id).hide();
						}
					});
				}
			}
			function restore(id) {
				$.post("<?=URL?>/manager/posts/ajax/restore", {id:id},
				function(data,status){
					if(data=="Success") {
						$("#post-"+id).hide();
					}
				});
			}
            function rendertag(id) {
                if(id) {
                    $.post("<?=URL?>/manager/posts/ajax/get", {id:id},
                        function(data,status){
                            var tag = jQuery.parseJSON(data);
                            if(tag.description) {
                                tinymce.get('description').setContent(tag.description);
                            }
                            $("input[name=id]").val(tag.id);
                            $("#name").val(tag.name).change();
                        });
                } else {
                    $("#id").val(0);
                    $("#name").val('');
                }
            }
		</script>

<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>