<?php
	if(permission('sliders')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Slider </strong> manager</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <div class="pull-left">
										<a id="table-edit_new" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="renderslider();">
                                            Add New <i class="fa fa-plus"></i>
                                        </a>
                                    </div>                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
									<form id="form-search" method="GET">
									<div class="row">
										<div class="col-xs-9"></div>
										<div class="col-xs-3">
											<div class="input-group">
												<input type="text" class="form-control" name="search" value="<?=($this->input->get('search'))?$this->input->get('search'):''?>" placeholder="Search">
												<span class="input-group-addon bg-blue" style="cursor: pointer" onClick="$('#form-search').submit();">
													<span class="arrow"></span><i class="fa fa-search"></i>
												</span>
											</div>
										</div>
									</div>
									</form>
                                    <table class="table table-striped table-hover dataTable" id="table-editable">
                                        <thead>
                                            <tr role="row">
                                                <th class="align-center">ID</th>
                                                <th class="align-center">Name</th>
                                                <th class="align-center">Link</th>
                                                <th class="align-center">Thumb</th>
                                                <th class="align-center">Order</th>
                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
											$i=1;
											if(isset($sliders) && is_array($sliders)){
											foreach($sliders as $slider)
											{
										?>
                                            <tr id="slider-<?=$slider['id']?>">
                                                <td class="align-center">
													<?=$i++;?>
												</td>
                                                <td class="align-center"><a style="color: #0090D9;" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="renderslider(<?=$slider['id']?>);"><?=$slider['name']?></a></td>
                                                <td class="align-center"><?=$slider['link']?></td>
                                                <td class="align-center">
                                                    <img src="<?= $slider['thumb'] ?>" style="width: 150px;height: 90px;"/>
                                                </td>
                                                <td class="align-center"><?=$slider['order']?></td>
                                                <td class="align-center">
													<a class="edit btn btn-sm btn-dark" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="renderslider(<?=$slider['id']?>);"><i class="fa fa-pencil-square-o"></i></a>
													<button class="delete btn btn-sm btn-danger" href="javascript:;" onClick="removeslider(<?=$slider['id'];?>);"><i class="fa fa-times-circle"></i></button>
                                                </td>
                                            </tr>
											<?php
												}
											}
											?>
                                        </tbody>
                                    </table>
									<div class="col-xs-12" style="line-height: 24px;">
										<div class="col-sm-2"> Have <?=$total?> sliders</div>
										<?=$this->slider_model->all_pages;?>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- the overlay element -->
			<div class="modal fade" id="item-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
				<form method="POST">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel"><strong>Edit</strong></h4>
						</div>
						<div class="modal-body">
							<div id="edit-content">
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<input type="hidden" name="id" value="" />
										<div class="form-group">
											<label for="name" class="control-label bold">Name</label>
											<div class="controls">
												<input type="text" name="name" value="" id="name" label="Name" required="" class="form-control" />
											</div>
										</div>
                                        <div class="form-group">
                                            <label for="note" class="control-label bold">Trạng Thái</label>
                                            <div class="controls">
                                                <select id="status" name="status" class="form-control">
                                                    <? $s = $this->input->get('status') ?>
                                                    <option value="1">Active</option>
                                                    <option value="0">Inactive</option>
                                                </select>
                                            </div>
                                        </div>
										<div class="form-group">
											<label for="link" class="control-label bold">Link</label>
											<div class="controls">
												<input type="text" name="link" value="" id="link" label="link"  required="" class="form-control" />
											</div>
										</div>
                                        <div class="form-group">
                                            <label for="link" class="control-label bold">Link image</label>
                                            <div class="controls">
                                                <input type="text" name="thumb" value="" id="thumb" label="link" class="form-control" />
                                            </div>
                                            </br>
                                            <span class="btn btn-lg btn-default fileinput-button">
												<span class="fa fa-upload"></span> Select Images
												<input id="fileupload" type="file" name="files" data-url="<?=URL?>/manager/sliders/ajax/upload" />
											</span>
                                            <br />
                                            <div class="progress progress-sm m-t-sm">
                                                <div class="progress-bar progress-bar-info" style="width: 0%"></div>
                                            </div>
                                            <div id="image_upload" style="max-height: 270px;overflow: auto;"></div>
                                        </div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="submit" class="btn btn-success" value="Save changes">
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
		<!-- END MAIN CONTENT -->
        <script src="<?= CDN ?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?=CDN?>/cpanel/plugins/filestyle/bootstrap-filestyle.min.js"></script>
        <script type="text/javascript" src="<?=CDN?>/cpanel/plugins/fileupload/vendor/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="<?=CDN?>/cpanel/plugins/fileupload/jquery.iframe-transport.js"></script>
        <script type="text/javascript" src="<?=CDN?>/cpanel/plugins/fileupload/jquery.fileupload.js"></script>
        <script>
			function removeslider(id) {
				if (confirm("Are you want to delete this tag permanent?")) {
					$.post("<?=URL?>/manager/sliders/ajax/delete", {id:id},
					function(data,status){
						if(data=="Deleted") {
							$("#slider-"+id).hide();
						}
					});
				}
			}
            function renderslider(id) {
                if(id) {
                    $.post("<?=URL?>/manager/sliders/ajax/get", {id:id},
                        function(data,status){
                            var slider = jQuery.parseJSON(data);
                            $("input[name=id]").val(slider.id);
                            $("#name").val(slider.name);
                            $("#link").val(slider.link);
                            $("#thumb").val(slider.thumb);
                            $('#image_upload').html('<img style="width:100%" src="'+slider.thumb+'" alt="">');
                        });
                } else {
                    $("#id").val(0);
                    $("#name").val('');
                    $("#link").val('');
                }
            }
            $(document).ready(function(e) {
                $('.progress').hide();
                $('#fileupload').fileupload({
                    dataType: 'json',
                    done: function (e, data) {
                        if(data.result.status=='success') {
                            $('#thumb').val(data.result.img);
                            $('#image_upload').html('<img style="width:100%" src="'+data.result.img+'" alt="">');
                            $('.progress .progress-bar').css('width','0%');
                            $('.progress').hide();
                        } else {
                            $('.progress .progress-bar').css('width','0%');
                            $('.progress').hide();
                        }
                    },
                    progressall: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $('.progress').show();
                        $('.progress .progress-bar').css('width', progress + '%');
                    },
                    progress: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $('.progress').show();
                        $('.progress .progress-bar').css('width',progress + '%');
                    },
                });
            });
		</script>

<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>