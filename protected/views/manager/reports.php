<?php
	if(permission('reports')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Thống kê </strong></h3>
                        </div>
						<div class="panel-body">
							<div class="col-xs-12">
								<div class="col-lg-3 col-md-6">
									<div class="panel-body bg-red panel-stat">
										<div class="row m-b-6">
											<div class="icon">
												<i class="fa fa-user"></i>
											</div>
											<div class="col-xs-12">
												<strong class="stat-title">Tổng tiền tồn trong hệ thống</strong>
												<h2 class="stat-value"><?=number_format($money_total,0,',','.')?></h2>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-6">
									<div class="panel-body bg-red panel-stat">
										<div class="row m-b-6">
											<div class="icon">
												<i class="fa fa-user"></i>
											</div>
											<div class="col-xs-12">
												<strong class="stat-title">Tổng tiền nạp thẻ</strong>
												<h2 class="stat-value"><?=number_format($money_mobile,0,',','.')?></h2>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-6">
									<div class="panel-body bg-red panel-stat">
										<div class="row m-b-6">
											<div class="icon">
												<i class="fa fa-user"></i>
											</div>
											<div class="col-xs-12">
												<strong class="stat-title">Tổng tiền Admin đã SET</strong>
												<h2 class="stat-value"><?=number_format($money_add,0,',','.')?></h2>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-6">
									<div class="panel-body bg-red panel-stat">
										<div class="row m-b-6">
											<div class="icon">
												<i class="fa fa-user"></i>
											</div>
											<div class="col-xs-12">
												<strong class="stat-title">Tổng tiền list đã trừ</strong>
												<h2 class="stat-value"><?=number_format($money_sub,0,',','.')?></h2>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12" style="margin-top: 5px;">
								<div class="col-lg-3 col-md-6">
									<div class="panel-body bg-red panel-stat">
										<div class="row m-b-6">
											<div class="icon">
												<i class="fa fa-user"></i>
											</div>
											<div class="col-xs-12">
												<strong class="stat-title">Tổng tiền đã chuyển</strong>
												<h2 class="stat-value"><?=number_format($money_send,0,',','.')?></h2>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-6">
									<div class="panel-body bg-red panel-stat">
										<div class="row m-b-6">
											<div class="icon">
												<i class="fa fa-user"></i>
											</div>
											<div class="col-xs-12">
												<strong class="stat-title">Phế chuyển tiền</strong>
												<h2 class="stat-value">
													<?=number_format($money_send_rate,0,',','.')?>
												</h2>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12" style="margin-top: 15px;">
								<form id="form-search1" method="GET">
									<div class="row">
										<div class="col-xs-9">
											<div class="col-xs-4" style="padding: 0 5px 0 0">
												<input name="date-start" id="date-start" class="input-s-sm input-s datepicker-input form-control" value="<?=$this->input->get('date-start')?>" style="width:90px;" type="text" value="" data-date-format="dd-mm-yyyy">
												<input name="date-end" id="date-end" class="input-s-sm input-s datepicker-input form-control" value="<?=$this->input->get('date-end')?>" style="width:90px;" type="text" value="" data-date-format="dd-mm-yyyy">
											</div>
											<div class="col-xs-3" style="padding: 0 5px 0 0">
												<div class="col-xs-5" style="padding: 0 5px 0 0">
													<button type="submit" class="btn btn-sm btn-danger" >Filter <i class="fa fa-info-circle"></i></button>
												</div>
												<div class="col-xs-5">
													<a class="btn btn-sm btn-danger" onClick="Clear1();">Clear <i class="fa fa-times-circle"></i></a>
												</div>
											</div>
										</div>
									</div>
								</form>
								<div class="col-xs-6">
									<table id="table-editable" class="table table-striped table-hover dataTable">
										<thead>
											<tr role="row">
												<th class="align-center">Dòng tiền</th>
												<th class="align-center">Tổng tiền</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="align-center">Nạp thẻ điện thoại</td>
												<td class="align-right"><?=number_format($money_mobile_filter,0,',','.')?></td>
											</tr>
											<tr>
												<td class="align-center">list Cộng</td>
												<td class="align-right"><?=number_format($money_add_filter,0,',','.')?></td>
											</tr>
											<tr>
												<td class="align-center">list Trừ</td>
												<td class="align-right"><?=number_format($money_sub_filter,0,',','.')?></td>
											</tr>
											<tr>
												<td class="align-center">Số tiền được chuyển</td>
												<td class="align-right"><?=number_format($money_send_filter,0,',','.')?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
			<script src="<?=CDN?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
			<script>
				$('#date-start').datepicker();
				$('#date-end').datepicker();
			</script>
		</div>
		<!-- END MAIN CONTENT -->
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>