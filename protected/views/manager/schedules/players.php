<?php
if (permission('players')) {
    ?>
    <!-- BEGIN MAIN CONTENT -->
    <div id="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading bg-red">
                        <h3 class="panel-title"><strong>Quản lý player</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 m-b-20">
                                <div class="pull-left">
                                    <a id="table-edit_new" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="renderplayer();"> Add New 
                                        <i class="fa fa-plus"></i> 
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
                                <form id="form-search" action="<?= URL . '/manager/players'; ?>" method="GET">
                                    <div class="row">
                                        <div class="col-xs-9"></div>
                                        <div class="col-xs-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="search" 
                                                       value="<?= ($this->input->get('search')) ? $this->input->get('search') : '' ?>" 
                                                       placeholder="Search">
                                                <span class="input-group-addon bg-blue" style="cursor: pointer" onClick="$('#form-search').submit();">     													
                                                    <span class="arrow"></span>
                                                    <i class="fa fa-search"></i> 
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped table-hover dataTable" id="table-editable">
                                    <thead>
                                    <tr role="row">
                                        <th class="align-center">ID</th>
                                        <th class="align-center">Tên</th>
                                        <th class="align-center">Thumb</th>
                                        <th class="align-center"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (isset($players) && is_array($players)) {
                                        foreach ($players as $player) { ?>
                                            <tr id="player-<?= $player['id'] ?>">
                                                <td class="align-center">
                                                    <?= $player['id'] ?>
                                                </td>
                                                <td class="align-center"><img src="<?= $player['avatar'] ?>"
                                                                              style="height: 30px"/> <b><a
                                                                href="javascript:;" data-toggle="modal"
                                                                data-target="#item-edit"
                                                                onclick="renderplayer(<?= $player['id'] ?>)"><?= $player['name'] ?>
                                                            <a/></b></td>
                                                <td class="align-center">
                                                    <?php if (strpos($player['avatar'], 'cdn8.net')) {
                                                        echo '<a href="javascript:;"><i class="fa fa-check" style="color: green"></i></a>';
                                                    } else {
                                                        echo '<a href="javascript:;" onclick="thumb(this, ' . $player['id'] . ')"><i class="fa fa-warning" style="color: red"></i></a>';
                                                    } ?> </td>
                                                <td class="align-center"><a class="edit btn btn-sm btn-dark"
                                                                            data-toggle="modal" data-target="#item-edit"
                                                                            href="javascript:;"
                                                                            onClick="renderplayer(<?= $player['id'] ?>);"><i
                                                                class="fa fa-pencil-square-o"></i></a>
                                                    <button class="delete btn btn-sm btn-danger" href="javascript:;"
                                                            onclick="removeplayer(<?= $player['id'] ?>);"><i
                                                                class="fa fa-times-circle"></i></button>
                                                </td>
                                            </tr>
                                        <?php }
                                    } else echo '<tr><td colspan="10" style="text-align: center">No results</td></tr>'; ?>
                                    </tbody>
                                </table>
                                <div class="col-xs-12" style="line-height: 24px;">
                                    <div class="col-sm-2"> Tổng:
                                        <?= $total ?> players
                                    </div>
                                    <?= $this->player_model->all_pages; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="item-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <form method="POST">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel"><strong>Edit</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div id="edit-content">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12"><input type="hidden" name="id" value=""/>
                                        <div class="form-group">
                                            <label for="name" class="control-label bold">Tên</label>
                                            <div class="controls">
                                                <input type="text" name="name" value="" id="name" label="Name" required="" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="form-group"><label for="game" class="control-label bold">Loại
                                                Game</label>
                                            <div class="controls"><select name="game" id="game" data-live-search="true" label="Game Type" class="valid form-control">
                                                    <option value="">Chọn Game</option>
                                                    <option value="fo3">FO3</option>
                                                    <option value="aoe">AOE</option>
                                                    <option value="cs">CS</option>
                                                    <option value="lol">Liên Minh</option>
                                                    <option value="dota">Dota</option>
                                                    <option value="pes">Pes</option>
                                                    <option value="lienquan">Liên Quân</option>
                                                    <option value="other">Khác</option>
                                                </select></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"><label class="form-label"><strong>URL Avatar
                                                        &nbsp;</strong></label>
                                                <div class="controls"><input type="text" class="form-control input-sm"
                                                                             id="avatar" name="avatar" value=""></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer"><input type="submit" class="btn btn-success" value="Save changes">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.isLogged').click(function () {
                    var id = $(this).data('id');
                    var value = $(this).data('value');
                    var element = $(this);
                    $.post("<?=URL?>/manager/channels/ajax/is_logged", {
                        id: id,
                        value: value
                    }, function (data, status) {
                        if (value == 0) {
                            element.html('<i class="fa fa-lock" style="color: #F7BD00"></i> Yes');
                            element.data('value', 1);
                        } else {
                            element.html('<i class="fa fa-unlock" style="color: #843E3E"></i> No');
                            element.data('value', 0);
                        }
                    });
                });
            });

            function thumb(element, id) {
                $(element).html('<img src="<?=CDN?>/images/loading.gif" height="14px" />');
                $.post("<?=URL?>/manager/players/ajax/thumb", {
                    id: id
                }, function (data, status) {
                    if (data == 'success') $(element).html('<i class="fa fa-check" style="color: green"></i>');
                    else {
                        $(element).html('<i class="fa fa-warning" style="color: red"></i>');
                        alert(data);
                    }
                });
            }

            function renderplayer(id) {
                if (id) {
                    $.post("<?=URL?>/manager/players/ajax/get", {
                        id: id
                    }, function (data, status) {
                        var data = jQuery.parseJSON(data);
                        $("input[name=id]").val(data.id);
                        $("#name").val(data.name);
                        $("#game").val(data.game).change();
                        $("#avatar").val(data.avatar);
                    });
                } else {
                    $("input[name=id]").val('');
                    $("#name").val('');
                    $("#game").val('');
                }
            }
        </script>
        <!-- the overlay element -->
    </div>
    <!-- END MAIN CONTENT -->
    <?
} else { ?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our <a href="<?= URL ?>">site</a> or contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>