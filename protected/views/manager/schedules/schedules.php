<?php
if (permission('channel')) {
    ?>
    <!-- BEGIN MAIN CONTENT -->
    <div id="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading bg-red">
                        <h3 class="panel-title"><strong>Quản lý kênh quay phát</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 m-b-20">
                                <div class="pull-left">
                                    <a id="table-edit_new" class="btn btn-sm btn-danger" data-toggle="modal"
                                       data-target="#item-edit" href="javascript:;" onClick="render();">
                                        Add New <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
                                <form id="form-search" action="<?= URL . '/manager/schedules'; ?>" method="GET">
                                    <div class="row">
                                        <div class="col-xs-9"></div>
                                        <div class="col-xs-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="search"
                                                       value="<?= ($this->input->get('search')) ? $this->input->get('search') : '' ?>"
                                                       placeholder="Search">
                                                <span class="input-group-addon bg-blue" style="cursor: pointer" onClick="$('#form-search').submit();">     
														<span class="arrow"></span>
                                                    <i class="fa fa-search"></i> 
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <table class="table table-striped table-hover dataTable" id="table-editable">
                                    <thead>
                                    <tr role="row">
                                        <th class="align-center">ID</th>
                                        <!--th class="align-center">Tên</th-->
                                        <th class="align-center">Đội nhà</th>
                                        <th class="align-center">Đội khách</th>
                                        <th class="align-center">Thời gian</th>
                                        <th class="align-center">Tỷ số</th>
                                        <th class="align-center">Giải đấu</th>
                                        <th class="align-center">Trạng thái</th>
                                        <th class="align-center">Featured</th>
                                        <th class="align-center"></th>
                                        <th class="align-center"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (isset($channels) && is_array($channels)) {
                                        foreach ($channels as $channel) {
                                            ?>
                                            <tr id="channel-<?= $channel['id'] ?>">
                                                <td class="align-center"><?= $channel['id'] ?></td>
                                                <td class="align-center"><?= (isset($channel['home'])) ? $channel['home']['name'] : '' ?></td>
                                                <td class="align-center"><?= (isset($channel['away'])) ? $channel['away']['name'] : '' ?></td>
                                                <td class="align-center"><?= date("d/m/Y", $channel['start_time']) ?></td>
                                                <td class="align-center"><?= $channel['score_home'] ?>
                                                    - <?= $channel['score_away'] ?></td>
                                                <td class="align-center"><?= (isset($channel['tournament'])) ? $channel['tournament']['name'] : '' ?></td>
                                                <td class="align-center">
                                                    <a href="javascript:;" class="changeStatus"
                                                       data-id="<?= $channel['id'] ?>"
                                                       data-value="<?= $channel['status'] ?>">
                                                        <?php
                                                        if ($channel['status'] == 'live') echo '<span style="color: #C75757; font-weight: bold"><i class="fa fa-check-circle"></i> Live</span>';
                                                        elseif ($channel['status'] == 'on') echo '<span style="color: green; font-weight: bold"><i class="fa fa-clock-o"></i> On</span>';
                                                        else echo '<span style="font-weight: bold"><i class="fa fa-minus-circle"></i> Off</span>';
                                                        ?>
                                                    </a>
                                                </td>
                                                <td class="align-center">
                                                    <input type="checkbox" name="featured_action"
                                                           value="<?= $channel['id'] ?>" <?= ($channel['featured']) ? 'checked' : '' ?>
                                                           data-size="small" data-on-text="On" data-off-text="Off">
                                                </td>
                                                <td class="align-center">
                                                    <a class="edit btn btn-sm btn-dark" data-toggle="modal"
                                                       data-target="#item-edit" href="javascript:;"
                                                       onClick="render(<?= $channel['id'] ?>);"><i
                                                                class="fa fa-pencil-square-o"></i></a>
                                                    <button class="delete btn btn-sm btn-danger" href="javascript:;"
                                                            onclick="removechannel(<?= $channel['id'] ?>);"><i
                                                                class="fa fa-times-circle"></i></button>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else echo '<tr><td colspan="10" style="text-align: center">No results</td></tr>';
                                    ?>
                                    </tbody>
                                </table>
                                <div class="col-xs-12" style="line-height: 24px;">
                                    <div class="col-sm-2"> Tổng: <?= $total ?> kênh</div>
                                    <?= $this->schedule_model->all_pages; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="item-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <form method="POST">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel"><strong>Edit</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div id="edit-content">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 0">
                                            <input type="hidden" name="id" value=""/>
                                            <div class="form-group">
                                                <label for="name" class="control-label bold">Tên kênh</label>
                                                <div class="controls">
                                                    <input type="text" name="name" value="" id="name" label="Name"
                                                           required="" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="home_id" class="control-label bold">Đội nhà</label>
                                                <div class="controls">
                                                    <select id="home_id" name="home_id" data-live-search="true"
                                                            class="form-control" data-style="input-sm btn-default">
                                                        <option value="">Chọn đội nhà</option>
                                                        <? foreach ($home as $h) { ?>
                                                            <option value="<?= $h['id'] ?>"
                                                                    data-content="<?= $h['name'] ?>"><?= $h['name'] ?></option>
                                                        <? } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="away_id" class="control-label bold">Đội khách</label>
                                                <div class="controls">
                                                    <select id="away_id" name="away_id" data-live-search="true"
                                                            class="form-control" data-style="input-sm btn-default">
                                                        <option value="">Chọn đội khách</option>
                                                        <? foreach ($home as $a) { ?>
                                                            <option value="<?= $a['id'] ?>"
                                                                    data-content="<?= $a['name'] ?>"><?= $a['name'] ?></option>
                                                        <? } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="score" class="control-label bold">Tỉ số</label>
                                                <div class="controls">
                                                    <input type="text" name="score_home" value="" id="score_home"
                                                           style="width: 49%; float: left;margin-right: 2%"
                                                           class="form-control"/>
                                                    <input type="text" name="score_away" value="" id="score_away"
                                                           style="width: 49%" label="Score" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12" style="padding-left: 0">
                                                <div class="col-md-12">
                                                    <div class="form-group"><label class="form-label"><strong>URL Avatar
                                                                &nbsp;</strong></label>
                                                        <div class="controls"><input type="text"
                                                                                     class="form-control input-sm"
                                                                                     id="avatar" name="avatar" value="">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 0">
                                            <div class="col-md-12 col-sm-12col-xs-12">
                                                <div class="form-group">
                                                    <label for="tournament_id"
                                                           class="control-label bold">Tournament</label>
                                                    <div class="controls">
                                                        <select id="tournament_id" name="tournament_id"
                                                                class="form-control" data-live-search="true"
                                                                data-style="input-sm btn-default">
                                                            <option value="0">None Tournament</option>
                                                            <? foreach ($tournament as $a) { ?>
                                                                <option value="<?= $a['id'] ?>"
                                                                        data-content="<?= $a['name'] ?>"><?= $a['name'] ?></option>
                                                            <? } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label for="time" class="control-label bold">Bắt đầu</label>
                                                <div class="controls">
                                                    <input type="number" name="start_hour" value="" id="start_hour"
                                                           label="Start Time" min="0" max="23"
                                                           style="width: 50px; float: left;" class="form-control"/>
                                                    <input type="number" name="start_min" value="" id="start_min"
                                                           label="Start Time" min="00" max="59"
                                                           style="width: 50px; margin-left: 5px; float: left"
                                                           class="form-control"/>
                                                    <input type="text" name="start_time" value="" id="start_time"
                                                           label="Start Time" class="form-control start_time"
                                                           style="width: 150px; margin-left: 5px;"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="status" class="control-label bold">Trạng thái</label>
                                                    <div class="controls">
                                                        <select required name="status" id="status" label="status"
                                                                style="width: 50px; margin-left: 5px; float: left"
                                                                class="valid form-control">
                                                            <option selected></option>
                                                            <option value="off">Off</option>
                                                            <option value="on">On</option>
                                                            <option value="live">Live</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="link" class="control-label bold">Video sau trận đấu</label>
                                                <div class="controls">
                                                    <input type="text" name="link" value="" id="link" label="Link"
                                                           class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="game" class="control-label bold">Loại Game</label>
                                                <div class="controls">
                                                    <select name="game" id="game" data-live-search="true"
                                                            label="Game Type" class="valid form-control">
                                                        <option value="">Chọn Game</option>
                                                        <option value="aoe">AOE</option>
                                                        <option value="cs">CS</option>
                                                        <option value="lol">Liên Minh</option>
                                                        <option value="dota">Dota</option>
                                                        <option value="pes">Pes</option>
                                                        <option value="lienquan">Liên Quân</option>
                                                        <option value="other">Khác</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="padding-left: 0">
                                            <div class="form-group">
                                                <label for="stream_code" class="control-label bold">Stream code <a
                                                            data-toggle="modal" data-target="#stream__"
                                                            href="javascript:;"> ??? </a></label>
                                                <div class="controls">
                                                    <textarea id="stream_code" name="stream_code" rows="2"
                                                              style="width:100%"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="stream__" tabindex="-1" role="dialog"
                                             aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" onclick="closed_md();">
                                                            &times;
                                                        </button>
                                                        <h4 class="modal-title" id="myModalLabel"><strong>Edit</strong>
                                                        </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div id="edit-content">
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    Lấy link trong src của iframe <br>
                                                                    Ví dụ: <img src="https://i.imgur.com/tmSHi9X.png"
                                                                                width='360'><br>
                                                                    thì lấy link stream code là : <b>https://www.youtube.com/embed/XmSP-CjYpB0</b><br><br>
                                                                    Link FB dạng như: <br><b>https://www.facebook.com/plugins/video.php?href=https://www.facebook.com/chimsedinangFanpage/videos/2341661562731025/</b>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="submit" class="btn btn-success"
                                                               value="Save changes">
                                                    </div>
                                                </div>
                                            </div>
                                            <script>
                                                function closed_md() {
                                                    $('#stream__').modal('hide');
                                                }
                                            </script>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="description" class="control-label bold">Description</label>
                                                <div class="controls">
                                                    <textarea id="description" name="description" class="ckeditor"
                                                              rows="6"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Save changes">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script src="<?= CDN ?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?= CDN ?>/cpanel/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
            $('#start_time').datepicker();
            $("[name='featured']").bootstrapSwitch();
            $("[name='featured_action']").bootstrapSwitch();
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.changeStatus').click(function () {
                    var id = $(this).data('id');
                    var value = $(this).data('value');
                    var element = $(this);
                    $.post("<?=URL?>/manager/schedules/ajax/change_status", {id: id, value: value},
                        function (data, status) {
                            if (value == 'live') {
                                element.html('<span style="color: green; font-weight: bold"><i class="fa fa-clock-o"></i> On</span>');
                                element.data('value', 'on');
                            } else if (value == 'on') {
                                element.html('<span style="font-weight: bold"><i class="fa fa-minus-circle"></i> Off</span>');
                                element.data('value', 'off');
                            } else {
                                element.html('<span style="color: #C75757; font-weight: bold"><i class="fa fa-check-circle"></i> Live</span>');
                                element.data('value', 'live');
                            }
                        });
                });
                $('.changeSetSlider').click(function () {
                    var id = $(this).data('id');
                    var value = $(this).data('value');
                    var element = $(this);
                    $.post("<?=URL?>/manager/schedules/ajax/change_set_slider", {id: id, value: value},
                        function (data, set_slider) {
                            if (value == 'On slider') {
                                element.html('<span style="color: blue; font-weight: bold"><i class="fa fa-minus-circle"></i> Off slider</span>');
                                element.data('value', 'Off slider');
                            } else {
                                element.html('<span style="color: #C75757; font-weight: bold"><i class="fa fa-check-circle"></i> On slider</span>');
                                element.data('value', 'On slider');
                            }
                        });
                });
                $("[name='featured_action']").on('switchChange.bootstrapSwitch', function (event, state) {
                    var id = $(this).val();
                    var value;
                    if (state) value = 1;
                    else value = 0;
                    $.post("<?=URL?>/manager/schedules/ajax/featured", {id: id, value: value},
                        function (data, status) {
                        });
                });
            });

            function removechannel(id) {
                if (confirm("Are you want to delete this tag permanent?")) {
                    $.post("<?=URL?>/manager/schedules/ajax/delete", {id: id},
                        function (data, status) {
                            $("#channel-" + id).hide();
                        });
                }
            }

            function render(id) {
                if (id) {
                    $.post("<?=URL?>/manager/schedules/ajax/get", {id: id},
                        function (data, status) {
                            var data = jQuery.parseJSON(data);
                            console.log(data);
                            if (data.description) {
                                tinymce.get('description').setContent(data.description);
                            }
                            var date = new Date(data.start_time * 1000);
                            var year = date.getFullYear();
                            var month = date.getMonth() + 1;
                            var day = date.getDate();
                            var hour = date.getHours();
                            var min = date.getMinutes();
                            if (data.featured == 1) $("[name='featured']").bootstrapSwitch('state', true);
                            else $("[name='featured']").bootstrapSwitch('state', false);
                            $("input[name=id]").val(data.id);
                            $("#name").val(data.name);
                            $("#game").val(data.game).change();
                            $("#home_id").val(data.home_id).change();
                            $("#away_id").val(data.away_id).change();
                            $("#tournament_id").val(data.tournament_id).change();
                            $("#status").val(data.status).change();
                            $("#set_slider").val(data.set_slider).change();
                            $("#game_info").val(data.game_info);
                            $("#game_type").val(data.game_type).change();
                            $("#avatar").val(data.avatar).change();
                            $("#description").val(data.description).change();
                            $("#game_image").val(data.game_image).change();
                            $("#tournament_id").val(data.tournament_id);
                            $("#link").val(data.link);
                            $("#white_list").val(data.white_list);
                            $("#score_home").val(data.score_home);
                            $("#score_away").val(data.score_away);
                            $("#stream_code").val(data.stream_code);
                            $("#start_time").val(month + '/' + day + '/' + year);
                            $("#start_hour").val(hour);
                            $("#start_min").val(min);
                        });
                } else {
                    $("input[name=id]").val('');
                    $("#name").val('');
                    $("#game").val('');
                    $("#home_id").val('').change();
                    $("#away_id").val('').change();
                    $("#game_type").val('').change();
                    $("#game_image").val('').change();
                    $("#game_info").val('');
                    $("#status").val('');
                    $("#set_slider").val('');
                    $("#description").val('');
                    $("#avatar").val('');
                    $("#tournament_id").val('');
                    $("#link").val('');
                    $("#white_list").val('');
                    $("#start_time").val('');
                    $("#start_hour").val('');
                    $("#start_min").val('');
                    $("#score").val('');
                    $("#stream_code").val('');
                }
            }
        </script>
        <!-- the overlay element -->

    </div>
    <!-- END MAIN CONTENT -->
    <script>
        $(document).ready(function () {
            tinymce.init({
                selector: "#description",
                theme: "modern",
                relative_urls: false,
                remove_script_host: false,
                toolbar: 'codesample | bold italic sizeselect fontselect fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | insertfile undo redo | forecolor backcolor | code youtube',
                fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
                plugins: [
                    "link image preview",
                    "wordcount code fullscreen",
                    "table textcolor youtube"
                ],
            });
        });
    </script>
    <?
} else {
    ?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our <a href="<?= URL ?>">site</a> or contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>