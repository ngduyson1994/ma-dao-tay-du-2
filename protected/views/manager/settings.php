<?php
	if(permission('settings')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Setting </strong> manager</h3>
                        </div>
                        <div class="panel-body">
							<div class="row">
								<form method="POST">
									<?php
										foreach($settings as $setting) {
									?>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="col-md-2 col-sm-2 col-xs-2">
											<div class="form-group">
												<div class="controls">
													<?
													$name = ucfirst(str_replace('_', ' ', $setting['name']));
													?>
													<label class="form-label"><strong><?=$name?></strong></label>
												</div>
											</div>
										</div>
										<div class="col-md-5 col-sm-5 col-xs-5">
												<div class="form-group">
													<div class="controls">
													<?php if($setting['type']=='password') {?>
														<input name="value[<?=$setting['id']?>]" type="password" class="form-control input-sm" value="<?=$setting['value']?>">
													<?php } else  if($setting['type']=='bool') {?>
														<input name="value[<?=$setting['id']?>]" value="0" type="hidden">
														<input name="value[<?=$setting['id']?>]" value="1" id="switch" type="checkbox" data-size="small" data-on-text="On" data-off-text="Off" <?=($setting['value'])?'checked':''?>>
													<?php } else  if($setting['type']=='text') {?>
														<textarea class="form-control input-sm" name="value[<?=$setting['id']?>]" style="height: 30px;resize: vertical;"><?=clearString($setting['value'])?></textarea>
													<?php } else { ?>
														<input name="value[<?=$setting['id']?>]" type="text" class="form-control input-sm" value="<?=$setting['value']?>">
													<?php } ?>
													</div>
												</div>
										</div>
										<div class="col-md-5 col-sm-5 col-xs-5">
												<div class="form-group">
													<div class="controls">
														<label class="form-label"><?=$setting['description']?></label>
													</div>
												</div>
										</div>
									</div>
									<?php
									}
									?>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div>
											<input type="submit" id="submit" class="btn btn-success pull-right" value="Update">
										</div>
									</div>

								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- the overlay element -->
		</div>
		<script>
			$("#switch").bootstrapSwitch();	
		</script>
		<!-- END MAIN CONTENT -->
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>