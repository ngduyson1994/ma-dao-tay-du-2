<?php
	if(permission('log_betting')) {
?>
<div class="panel-body">
	<div class="row">
		 <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
			<form id="form-search1" method="GET">
				<div class="row">
					<div class="col-xs-9">
						<div class="col-xs-2" style="padding: 0 5px 0 0">
							<select id="method" name="method" class="form-control selectpicker" onChange="$('#form-search1').submit();" >
								<? $s = $this->input->get('method') ?>
								<option value="">Tất cả</option>
								<? foreach($payment_methods as $payment_method) {
									if(strstr($payment_method['code'], 'betting')){ ?>
										<option value="<?=$payment_method['code']?>" <?=($s==$payment_method['code'])?'selected':''?> data-content=""><?=$payment_method['name']?></option>
								<? } } ?>
							</select>
							<script>
								$('.selectpicker').selectpicker({
									style: 'input-sm btn-default'
								});
							</script>
						</div>
						<div class="col-xs-4" style="padding: 0 5px 0 0">
								<input name="date1-start" id="date1-start" class="input-s-sm input-s datepicker-input form-control" value="<?=$this->input->get('date1-start')?>" style="width:90px;" type="text" value="" data-date-format="dd-mm-yyyy">
								<input name="date1-end" id="date1-end" class="input-s-sm input-s datepicker-input form-control" value="<?=$this->input->get('date1-end')?>" style="width:90px;" type="text" value="" data-date-format="dd-mm-yyyy">
							</div>
							<div class="col-xs-3" style="padding: 0 5px 0 0">
								<div class="col-xs-5" style="padding: 0 5px 0 0">
									<button type="submit" class="btn btn-sm btn-danger" >Filter <i class="fa fa-info-circle"></i></button>
								</div>
								<div class="col-xs-5">
									<a class="btn btn-sm btn-danger" onClick="Clear1();">Clear <i class="fa fa-times-circle"></i></a>
								</div>
							</div>
					</div>
					<div class="col-xs-3">
						<div class="input-group">
							<input type="text" class="form-control" name="search" value="<?=($this->input->get('search'))?$this->input->get('search'):''?>" placeholder="Search" >
							<span class="input-group-addon bg-blue" style="cursor: pointer" onClick="$('#form-search').submit();">     
								<span class="arrow"></span><i class="fa fa-search"></i> 
							</span>
						</div>
					</div>
				</div>
			</form>
			<table class="table table-striped table-hover dataTable" id="table-editable">
				<thead>
					<tr role="row">
						<th class="align-center" width="20px" >Mã GD</th>
						<th class="align-center">Ngày giờ</th>
						<th class="align-center">Trận đấu</th>
						<th class="align-center">Kết quả</th>
						<th class="align-center">Đội chọn</th>
						<th class="align-center">Tiền đặt</th>
						<th class="align-center">Tỉ lệ</th>
						<th class="align-center">Tiền thắng</th>
						<th class="align-center">Kiểu</th>
					</tr>
				</thead>
				<tbody>
				<?php
					$i=1;
					if(isset($logs) && is_array($logs)) {
						foreach($logs as $log) {
						
						if($log['betting']['match']['match_type']==1) {
							if($log['betting']['match']['bet_type']==1) $bet_type = 'Thắng thua';
							else if($log['betting']['match']['bet_type']==2) $bet_type = 'Cung chém';
							else if($log['betting']['match']['bet_type']==3) $bet_type = 'Chẵn lẻ';
						} else {
							if($log['betting']['match']['bet_type']==1) $bet_type = 'Thắng thua';
							else if($log['betting']['match']['bet_type']==2) $bet_type = 'Tài xỉu';
							else if($log['betting']['match']['bet_type']==3) $bet_type = 'Phạt góc';
						}
						
					?>
						<tr id="log-<?=$log['id']?>" onclick="$('#detail_bet_<?=$log['id']?>').toggle()" class="betting_log">
							<td class="align-center" width="20px"><?=$log['id']?></td>
							<td class="align-center"><?=date("G:i:s - d/m/Y",$log['betting']['created'])?></td>
							<td class="align-center"><?=$log['betting']['channel']['name']?> <span style="color:#00496B"><?=$log['betting']['match']['name']?></span> <span style="color:red">(<?=$bet_type?>)</span></td>
							<td class="align-center">
							<?php 
								$winner = '';
								if($log['betting']['match']['result']=='1'){
									if($log['betting']['match']['match_type']==1) {
										if($log['betting']['match']['bet_type']==1) {
											$winner = $log['betting']['channel']['home']['name'];
										} elseif($log['betting']['match']['bet_type']==2) {
											$winner = 'Cung';
										} elseif($log['betting']['match']['bet_type']==3) {
											$winner = 'Chẵn';
										}
									}
								} elseif($log['betting']['match']['result']=='2'){
									if($log['betting']['match']['match_type']==1) {
										if($log['betting']['match']['bet_type']==1) {
											$winner = $log['betting']['channel']['away']['name'];
										} elseif($log['betting']['match']['bet_type']==2) {
											$winner = 'Chém';
										} elseif($log['betting']['match']['bet_type']==3) {
											$winner = 'Lẻ';
										}
									}
								} elseif($log['betting']['match']['result']=='0') { $winner = 'Hoà'; }
								elseif($log['betting']['match']['result']=='-1') { $winner = 'Huỷ trận';}
								elseif($log['betting']['match']['result']!=NULL) { $winner = $log['betting']['match']['result'];}
								if(!$winner) $winner = 'Chưa tổng kết';
								echo $winner;
							?>
							</td>
							<td class="align-center">
								<?php
									$selected = '';
									$bet_selected = '';
									if($log['betting']['match']['match_type']==1) {
										if($log['betting']['match']['bet_type']==1) {
											if($log['betting']['selected']==1){
												if($id == $log['betting']['user']['id']) {
													$selected = $log['betting']['channel']['home']['name'];
													$bet_selected = $log['betting']['channel']['away']['name'];
												} else { 
													$selected = $log['betting']['channel']['away']['name'];
													$bet_selected = $log['betting']['channel']['home']['name'];
												}
											} else {
												if($id == $log['betting']['user']['id']) {
													$selected = $log['betting']['channel']['away']['name'];
													$bet_selected = $log['betting']['channel']['home']['name'];
												} else {
													$selected = $log['betting']['channel']['home']['name'];
													$bet_selected = $log['betting']['channel']['away']['name'];
												}
											}
										} elseif($log['betting']['match']['bet_type']==2){
											if($log['betting']['selected']==1){
												if($id == $log['betting']['user']['id']) {
													$selected = 'Cung'; $bet_selected ='Chém';
												} else { $selected = 'Chém'; $bet_selected ='Cung'; }
											} else {
												if($id == $log['betting']['user']['id']) {
													$selected = 'Chém'; $bet_selected ='Cung';
												} else { $selected = 'Cung'; $bet_selected ='Chém'; }
											}
										} elseif($log['betting']['match']['bet_type']==3){
											if($log['betting']['selected']==1){
												if($id == $log['betting']['user']['id']) {
													$selected = 'Chẵn'; $bet_selected ='Lẻ';
												} else { $selected = 'Lẻ'; $bet_selected ='Chẵn'; }
											} else {
												if($id == $log['betting']['user']['id']) {
													$selected = 'Lẻ'; $bet_selected ='Chẵn';
												} else $selected = 'Chẵn'; $bet_selected ='Lẻ';
											}
										}
									} elseif($log['betting']['match']['match_type']==2){
										if($log['betting']['match']['bet_type']==1) {
											if($log['betting']['selected']==1){
												if($id == $log['betting']['user']['id']) {
													$selected = $log['betting']['channel']['home']['name'];
													$bet_selected = $log['betting']['channel']['away']['name'];
												} else { 
													$selected = $log['betting']['channel']['away']['name'];
													$bet_selected = $log['betting']['channel']['home']['name'];
												}
											} else {
												if($id == $log['betting']['user']['id']) {
													$selected = $log['betting']['channel']['away']['name'];
													$bet_selected = $log['betting']['channel']['home']['name'];
												} else { 
													$selected = $log['betting']['channel']['home']['name'];
													$bet_selected = $log['betting']['channel']['away']['name'];
												}	
											}
										} elseif($log['betting']['match']['bet_type']==2){
											if($log['betting']['selected']==1){
												if($id == $log['betting']['user']['id']) {
													$selected = 'Tài'; $bet_selected ='Xỉu';
												} else { $selected = 'Xỉu'; $bet_selected ='Tài'; }
											} else {
												if($id == $log['betting']['user']['id']) {
													$selected = 'Xỉu'; $bet_selected ='Tài';
												} else { $selected = 'Tài'; $bet_selected ='Xỉu'; }
											}
										} elseif($log['betting']['match']['bet_type']==3){
											if($log['betting']['selected']==1){
												if($id == $log['betting']['user']['id']) {
													$selected = 'Tài'; $bet_selected ='Xỉu';
												} else { $selected = 'Xỉu'; $bet_selected ='Tài'; }
											} else {
												if($id == $log['betting']['user']['id']) {
													$selected = 'Xỉu'; $bet_selected ='Tài';
												} else { $selected = 'Tài'; $bet_selected ='Xỉu'; }
											}
										}
									}
									echo $selected;
								?>
							</td>
							<td class="align-center"><?=number_format($log['money'],0,',','.')?></td>
							<td class="align-center">
								<?php
									$rate = '';
									$football_rate ='';
									if($log['betting']['match']['match_type']==1) { $rate = $log['betting']['match'];}
									else {
										$rate = $log['betting']['football_rate'] - floor($log['betting']['football_rate']);
										if($rate=='0.25') $rate = '1/4';
										else if($rate=='0.5') $rate = '1/2';
										else if($rate=='0.75') $rate = '3/4';
										else $rate = '0';
										$football_rate = floor($log['betting']['football_rate'])."&nbsp;".$rate;
										$higher = '';
										if($log['betting']['match']['bet_type']==1){
											if($log['betting']['football_higher'] ==1) $higher = $log['betting']['channel']['home']['name'];
											else $higher = $log['betting']['channel']['away']['name'];
										}
									}
									echo $higher."&nbsp;&nbsp;&nbsp;".$football_rate;
								?>
							</td>
							<?php
								$win_money = '';
								if($log['betting']['data']) {
									foreach($log['betting']['data'] as $data){
										if($log['id'] == $data['payment_id']) {
											$win_money = $data['win_amount'];
											if($win_money <0 ) $win_money = '<span style="color:red">'.number_format($win_money,0,',','.').'</span>';
											else $win_money = '<span>'.number_format($win_money,0,',','.').'</span>';
											if($log['betting']['match']['result']==null) $win_money = "Chưa tổng kết";
										}
									}
								}
								if($id == $log['betting']['user']['id']) {
									if($log['betting']['win_amount']!=0) {
										$win_money = $log['betting']['win_amount'];
										if($win_money<0) $win_money = '<span style="color:red">'.number_format($log['money'],0,',','.').'</span>';
										else $win_money = '<span>'.number_format($win_money,0,',','.').'</span>';
									}
								}
							?>
							<td class="align-center"><?=$win_money?></td>
							<td class="align-center"><?=$log['method']['name']?></td>
						</tr>
						<tr style="display: none;" id="detail_bet_<?=$log['id']?>" class="detail_bet">
							<td colspan="8" style="padding-left: 20px">
								<ul>
									<li>Kèo đấu: [<?=$log['betting']['channel']['name']?>] - Trận: [ <?=$log['betting']['match']['name']?>] - Thể loại: (<?=$bet_type?>)</li>
									<li>
									<?php
										$time_summarize = 'Chưa tổng kết';
										if($log['betting']['match']['result'] != NULL) {
											$time_summarize = date("G:i:s - d/m/Y",$log['betting']['match']['summarize_time']);
										}
									?>
										Thời gian tổng kết: [<?=$time_summarize?>]
									</li>
									<li>Kết quả: <?=$winner?></li>
									<ul style="padding-left: 25px;">
										<?php if($log['id'] == $log['betting']['payment_id']) { ?>
											<li class="folder expanded" style="color:green">Tên bàn: <?=$log['betting']['name'] ?> - <?=$log['betting']['user']['username']?> là chủ bàn - Đặt cho: [<?=$selected?>] - Thời gian tạo: [<?=date("G:i:s - d/m/Y",$log['betting']['created'])?>]</li>
										<?php } else { ?>
											<li class="folder expanded">Tên bàn:<?=$log['betting']['name'] ?> - Người tạo: <?=$log['betting']['user']['username']?> - Đặt cho: [<?=$bet_selected?>] - Thời gian tạo: [<?=date("G:i:s - d/m/Y",$log['betting']['created'])?>]</li>
										<?php } ?>
										<li>Gcash đã đặt : (<?=number_format($log['betting']['origin_amount'],0,',','.')?>) - Gcash chưa ai khớp : (<?=number_format($log['betting']['amount'],0,',','.')?>)</li>
										<li>
										<span class="win-vcoin">
										<?php
											$win_amount = '';
											if($log['betting']['match']['result']==null) $win_amount = 'Chưa tổng kết';
											elseif($log['betting']['win_amount'] != 0) $win_amount = number_format($log['betting']['win_amount'],0,',','.');
											else $win_amount = 0;
											if($log['betting']['match']['result'] != null && $log['betting']['created'] > $log['betting']['match']['summarize_time']) 
												echo 'Vào bàn muộn';
											else 
												echo 'Gcash thắng: ('.$win_amount.')';
										?>
										</span></li>
										<ul style="padding-left: 25px;">
											<?php
											$style='';
											if($log['betting']['data']) {
												foreach($log['betting']['data'] as $data){
													$show_name = $data['user']['username'];
													if($log['id'] == $data['payment_id']) {
														$style = 'color: green';
													}
											?>
											<li style="<?=$style?>" class="folder expanded">
												<div><b><?=$show_name?></b> theo: (<?=$data['amount']?>) Gcash - Đặt cho: [<?=$bet_selected?>] - Thời gian: [<?=date("G:i:s - d/m/Y",$data['created'])?>]</div>
											</li>
											<li style="<?=$style?>">
											<?php
												$win ='';
												if($data['win_amount']!=null) $win = number_format($data['win_amount'],0,',','.');
												else $win = 'Chưa tổng kết';
												if($log['betting']['match']['result'] != null && $data['created'] > $log['betting']['match']['summarize_time']){
													echo 'Đặt cược muộn';
												}
												else echo 'Gcash thắng: ('.$win.')';
											?>
											</li>
										<?php  } } ?>
										</ul>
									</ul>
								</ul>
							</td>
						</tr>
				<?php
						}
					} else echo '<tr><td colspan="6" style="text-align: center">No results</td></tr>';
				?>
				</tbody>
			</table>
			<div class="col-sm-2"> Have <?=$total?> betting</div>
			<?=$this->payment_transactions_model->all_pages;?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$(".pagination a").click(function() {
			var href = $(this).attr("href");
			$.get(href, function(data, status){
				$("#log_betting").html(data);
			});
			return false;
		});
		
		$("#form-search1").submit(function() {
			var str = $("#form-search1").serialize();
			var url = '<?=URL?>/manager/users/log_betting/<?=$id?>';
			var href = url+'?'+str;
			$.get(href, function(data, status){
				$("#log_betting").html(data);
			});
			return false;
		});
		
		function Clear1(){
			var url = '<?=URL?>/manager/users/log_betting/<?=$id?>';
			$.get(url, function(data, status){
				$("#log_betting").html(data);
			});
		}
	});
</script>
<div class="clearfix"></div>
<script src="<?=CDN?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
	$('#date1-start').datepicker();
	$('#date1-end').datepicker();
</script>
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>
<style>
.detail_bet ul{
	list-style-type:inherit;
}
</style>