<?php
	if(permission('users')) {
?>
<table class="table table-striped">
	<thead>
		<tr>
			<th class="align-center">#</th>
			<th class="align-center">IPadress</th>
			<th class="align-center">Trình duyệt</th>
			<th class="align-center">Hệ điều hành</th>
			<th class="align-center">Kết quả</th>
			<th class="align-center">Thời gian</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if(isset($logs) && is_array($logs))
			foreach($logs as $key=>$log) {
				$this->agent->parse($log['user_agent']);
		?>
		<tr>
			<td class="align-center"><?=$key+1?></td>
			<td class="align-center"><a target="_blank" href="<?=URL?>/manager/ip_info?ip=<?=$log['ipaddress']?>"><?=$log['ipaddress']?></a></td>
			<td class="align-center"><?=$this->agent->browser()?></td>
			<td class="align-center"><?=$this->agent->platform()?></td>
			<td class="align-center"><?=($log['status']=='success')?'<span style="color:green">Thành công</span>':$log['note']?></td>
			<td class="align-center"><?=date(" h:i:s A - d/m/Y",$log['created'])?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<div class="col-xs-12" id="jquery_link" style="line-height: 24px;">
	<div class="col-sm-2">Tổng <?=$total?></div>
	<?=$this->log_login_model->all_pages;?>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$(".pagination a").click(function() {
			var href = $(this).attr("href");
			$.get(href, function(data, status){
				$("#log_login").html(data);
			});
			return false;
		});
	});
</script>
<div class="clearfix"></div>
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>