<?php
	if(permission('log_payment')) {
?>
		<div class="panel-body">
			<div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
				<form id="form-search" method="GET">
					<div class="row">
						<div class="col-xs-9">
							<div class="col-xs-2" style="padding: 0 5px 0 0">
								<select id="method" name="method" class="form-control selectpicker" onChange="$('#form-search').submit();">
									<? $s = $this->input->get('method') ?>
										<option value="">Phương thức</option>
										<? foreach($payment_methods as $payment_method) 
										if(!strstr($payment_method['code'], 'betting')){ { ?>
										<option value="<?=$payment_method['code']?>" <?=($s==$payment_method['code'])?'selected':''?> data-content=""><?=$payment_method['name']?></option>
										<? } } ?>
								</select>
								<script>
									$('.selectpicker').selectpicker({
										style: 'input-sm btn-default'
									});
								</script>
							</div>
							<div class="col-xs-4" style="padding: 0 5px 0 0">
								<input name="date-start" id="date-start" class="input-s-sm input-s datepicker-input form-control" value="<?=$this->input->get('date-start')?>" style="width:90px;" type="text" value="" data-date-format="dd-mm-yyyy">
								<input name="date-end" id="date-end" class="input-s-sm input-s datepicker-input form-control" value="<?=$this->input->get('date-end')?>" style="width:90px;" type="text" value="" data-date-format="dd-mm-yyyy">
							</div>
							<div class="col-xs-3" style="padding: 0 5px 0 0">
								<div class="col-xs-5" style="padding: 0 5px 0 0">
									<button type="submit" class="btn btn-sm btn-danger" >Filter <i class="fa fa-info-circle"></i></button>
								</div>
								<div class="col-xs-5">
									<a class="btn btn-sm btn-danger" Onclick="Clear();">Clear <i class="fa fa-times-circle"></i></a>
								</div>
							</div>
						</div>
						<div class="col-xs-3">
							<div class="input-group">
								<input type="text" class="form-control" name="search" value="<?=($this->input->get('search'))?$this->input->get('search'):''?>" placeholder="Search">
								<span class="input-group-addon bg-blue" style="cursor: pointer" onClick="$('#form-search').submit();">     
									<span class="arrow"></span><i class="fa fa-search"></i> 
								</span>
							</div>
						</div>
					</div>
				</form>
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="align-center">#</th>
							<th class="align-center">Phương thức</th>
							<th class="align-center">Người thực hiện</th>
							<th class="align-center">Người nhận</th>
							<th class="align-center">Số tiền</th>
							<th class="align-center">Kiểu</th>
							<th class="align-center">Ghi chú</th>
							<th class="align-center">Thời gian</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(isset($logs) && is_array($logs))
							foreach($logs as $key=>$log) {
						?>
						<tr>
							<td class="align-center"><?=$key+1?></td>
							<td class="align-center"><?=$log['method']['name'];?></td>
							<td class="align-center">
								<?php if($log['list_id']>0) echo '<span style="color: red">Hệ thống</span>'; else echo '<a target="_blank" href="'.URL.'/manager/users/item/'.$log['user']['id'].'">'.$log['user']['username'].'</a>';
								?>
							</td>
							<td class="align-center">
								<?php 
									if($log['other_id']>0 && $log['list_id']==0) echo '<a target="_blank" href="'.URL.'/manager/users/item/'.$log['other']['id'].'">'.$log['other']['username'].'</a>';
									elseif($log['other_id']==0 && $log['list_id']==0) echo '<span style="color: red">Hệ thống</span>';
									else echo $log['user']['username'];?>
							</td>
							<td class="align-center"><span class="user_money_icon"></span> <span class="user_money" style="color:<?=($log['real_money']>0)?'green':'red'?>"><?=number_format($log['money'],0,',','.')?></span></td>
							<td class="align-center">
								<?php
									if($log['set_type']=='event') echo 'Sự kiện';
									elseif($log['set_type']=='agent') echo 'Đại lý';
									elseif($log['set_type']=='delay') echo 'Trễ thẻ';
									else echo '';
								?>
								</td>
							<td class="align-center"><?=$log['note']?></td>
							<td class="align-center"><?=date(" h:i:s A - d/m/Y",$log['created'])?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<div class="col-xs-12" id="jquery_link" style="line-height: 24px;">
					<div class="col-sm-2">Tổng <?=$total?></div>
					<?=$this->payment_transactions_model->all_pages;?>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".pagination a").click(function() {
					var href = $(this).attr("href");
					$.get(href, function(data, status){
						$("#log_payment").html(data);
					});
					return false;
				});
				$("#form-search").submit(function() {
					var str = $("#form-search").serialize();
					var url = '<?=URL?>/manager/users/log_payment/<?=$uid?>';
					var href = url+'?'+str;
					$.get(href, function(data, status){
						$("#log_payment").html(data);
					});
					return false;
				});
			});
			function Clear(){
				var url = '<?=URL?>/manager/users/log_payment/<?=$uid?>';
				$.get(url, function(data, status){
					$("#log_payment").html(data);
				});
			}
		</script>
		<div class="clearfix"></div>
		<script src="<?=CDN?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script>
			$('#date-start').datepicker();
			$('#date-end').datepicker();
		</script>
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>