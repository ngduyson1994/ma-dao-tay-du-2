<?php
	if(permission('log_user_banned')) {
?>
<table class="table table-striped">
	<thead>
		<tr>
			<th class="align-center">#</th>
			<th class="align-center">Tài khoản Banned</th>
			<th class="align-center">Người Banned</th>
			<th class="align-center">Trạng thái</th>
			<th class="align-center">Ngày Banned</th>
			<th class="align-center">Ngày hết hạn</th>
			<th class="align-center">Ghi chú</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if(isset($logs) && is_array($logs))
			foreach($logs as $key=>$log) {
		?>
		<tr>
			<td class="align-center"><?=$key+1?></td>
			<td class="align-center"><?=$log['user']['username']?></td>
			<td class="align-center"><?=$log['list']['username']?></td>
			<td class="align-center" style="color:<?=($log['is_ban']=='0')?'green':'red'?>">
				<?php
					if($log['is_ban']=='1') echo 'Banned';
					else echo 'Unbaned';
				?>
			</td>
			<td class="align-center"><?=date(" h:i:s A - d/m/Y",$log['created'])?></td>
			<td class="align-center"><?=date(" h:i:s A - d/m/Y",$log['expried'])?></td>
			<td class="align-center"><?=$log['note']?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<div class="col-xs-12" id="jquery_link" style="line-height: 24px;">
	<div class="col-sm-2">Tổng <?=$total?></div>
	<?=$this->user_ban_model->all_pages;?>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$(".pagination a").click(function() {
			var href = $(this).attr("href");
			$.get(href, function(data, status){
				$("#log_user_banned").html(data);
			});
			return false;
		});
	});
</script>
<div class="clearfix"></div>
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>