<?php
	if(permission('users')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Chi tiết thành viên</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <div class="btn-group col-md-6" style="padding-left: 0">
                                        <a id="table-edit_new" class="btn btn-danger btn-sm" href="<?=URL.'/manager/users/'?>">
                                            <i class="fa fa-arrow-left"></i> Back
                                        </a>
                                    </div>
                                    <div class="btn-group col-md-6"></div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
									<div id="edit-content">
										<div class="row">
											<div class="col-md-1 col-sm-1 col-xs-1">
											</div>
											<div class="col-md-4 col-sm-4 col-xs-4">
												<div style="margin-bottom: 5px; padding: 0 5px">
													<div class="bold pull-left">Tài khoản</div>
													<div class="pull-right"><?=$data['username']?></div>
													<div class="clearfix"></div>
												</div>
												<div style="margin-bottom: 5px; padding: 0 5px">
													<div class="bold pull-left">Email</div>
													<div class="pull-right"><?=$data['email']?></div>
													<div class="clearfix"></div>
												</div>
												<div style="margin-bottom: 5px; padding: 0 5px">
													<div class="bold pull-left">SĐT</div>
													<div class="pull-right"><?=$data['phone']?></div>
													<div class="clearfix"></div>
												</div>
												<div style="margin-bottom: 5px; padding: 0 5px">
													<div class="bold pull-left">Trạng thái</div>
													<div class="pull-right"><?if($data['status']=='valid') echo 'Đã kích hoạt'; elseif($data['status']=='pending') echo 'Chưa kích hoạt'; else echo 'Banned'; ?></div>
													<div class="clearfix"></div>
												</div>
											</div>
											<div class="col-md-3 col-sm-3 col-xs-3">
												<div style="margin-bottom: 5px; padding: 0 5px">
													<div class="bold pull-left">GCash</div>
													<div class="pull-right"><?=number_format($data['money'])?></div>
													<div class="clearfix"></div>
												</div>
												<div style="margin-bottom: 5px; padding: 0 5px">
													<div class="bold pull-left">Tiền VIP</div>
													<div class="pull-right"><?=number_format($data['vip_money'])?></div>
													<div class="clearfix"></div>
												</div>
												<div style="margin-bottom: 5px; padding: 0 5px">
													<div class="bold pull-left">Tổng tiền nạp</div>
													<div class="pull-right"><?=0?></div>
													<div class="clearfix"></div>
												</div>
												<div style="margin-bottom: 5px; padding: 0 5px">
													<div class="bold pull-left">Tổng tiền rút</div>
													<div class="pull-right"><?=0?></div>
													<div class="clearfix"></div>
												</div>
											</div>
											<div class="col-md-3 col-sm-3 col-xs-3">
												<div style="margin-bottom: 5px; padding: 0 5px">
													<div class="bold pull-left">Tên đầy đủ</div>
													<div class="pull-right"><?=$data['name']?></div>
													<div class="clearfix"></div>
												</div>
												<div style="margin-bottom: 5px; padding: 0 5px">
													<div class="bold pull-left">Giới tính</div>
													<div class="pull-right"><?if($data['gender']=='male') echo 'Nam'; elseif($data['gender']=='female') echo 'Nữ'; else echo 'Chưa rõ'; ?></div>
													<div class="clearfix"></div>
												</div>
												<div style="margin-bottom: 5px; padding: 0 5px">
													<div class="bold pull-left">IP đăng ký</div>
													<div class="pull-right"><a target="_blank" href="<?=URL?>/manager/ip_info?ip=<?=$data['ipaddress']?>"><?=$data['ipaddress']?></a></div>
													<div class="clearfix"></div>
												</div>
												<div style="margin-bottom: 5px; padding: 0 5px">
													<div class="bold pull-left">Ngày đăng ký</div>
													<div class="pull-right"><?=date("d/m/Y", $data['created'])?></div>
													<div class="clearfix"></div>
												</div>
											</div>
											<br/>
											<br/>
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="tabcordion">
													<ul id="myTab" class="nav nav-tabs">
														<li class="active"><a href="#log_login" data-toggle="tab" onclick="log_login();"><i class="fa fa-history"></i> Lịch sử đăng nhập</a></li>
														<li class=""><a href="#log_user_banned" data-toggle="tab" onclick="log_user_banned();"><i class="fa fa-history"></i> Lịch sử banned</a></li>
														<li class=""><a href="#log_payment" data-toggle="tab" onclick="log_payment();"><i class="fa fa-history"></i> Lịch sử giao dịch</a></li>
														<li class=""><a href="#log_betting" data-toggle="tab" onclick="log_betting();"><i class="fa fa-history"></i> Lịch sử cá cược</a></li>
														<!--li class=""><a href="#log_gift" data-toggle="tab"><i class="fa fa-history"></i> Lịch sử đổi thưởng</a></li-->
													</ul>
													<div id="myTabContent" class="tab-content">
														<div class="tab-pane fade active in" id="log_login"></div>
														<div class="tab-pane fade" id="log_payment"></div>
														<div class="tab-pane fade" id="log_user_banned"></div>
														<div class="tab-pane fade" id="log_betting"></div>
														<div class="tab-pane fade" id="log_gift">
															<p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		<!-- END MAIN CONTENT -->
		<script type="text/javascript">
			function log_login() {
				$.get("<?=URL?>/manager/users/log_login/<?=$data['id']?>",
				function(data, status){
					$("#log_login").html(data);
				});
			}
			function log_payment() {
				$.get("<?=URL?>/manager/users/log_payment/<?=$data['id']?>",
				function(data, status){
					$("#log_payment").html(data);
				});
			}
			function log_betting() {
				$.get("<?=URL?>/manager/users/log_betting/<?=$data['id']?>",
				function(data, status){
					$("#log_betting").html(data);
				});
			}
			function log_user_banned() {
				$.get("<?=URL?>/manager/users/log_user_banned/<?=$data['id']?>",
				function(data, status){
					$("#log_user_banned").html(data);
				});
			}
			$(document).ready(function() {
				log_login();
				log_payment();
				log_betting();
				log_user_banned();
			});
		</script>
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>