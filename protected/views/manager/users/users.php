<?php
	if(permission('users')) {
?>
		<!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-red">
                            <h3 class="panel-title"><strong>Users </strong> manager</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
								<?php if(permission('add_user')) { ?>
                                <div class="col-md-12 m-b-20">
                                    <div class="pull-left">
                                        <a id="table-edit_new" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#item-edit" href="javascript:;" onClick="render();">
                                            Add New <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
								<?php } ?>
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red">
									<form id="form-search" action="<?=URL.'/manager/users';?>" method="GET">
										<div class="row">
											<div class="col-xs-9">
												<div class="col-xs-2" style="padding: 0 5px 0 0">
													<select id="status" name="status" class="form-control" data-style="input-sm btn-default" onChange="$('#form-search').submit();">
														<? $s = $this->input->get('status') ?>
														<option value="" >Status</option>
														<option value="valid" <?=($s=='valid')?'selected':''?> data-icon="fa-check-circle">Valid</option>
														<option value="pending"<?=($s=='pending')?'selected':''?> data-icon="fa-clock-o">Pending</option>
														<option value="banned" <?=($s=='banned')?'selected':''?> data-icon="fa-minus-circle">Banned</option>
													</select>
												</div>
												<div class="col-xs-4" style="padding: 0 5px 0 0">
													<input name="date-start" id="date-start" class="input-s-sm input-s datepicker-input form-control" value="<?=$this->input->get('date-start')?>" style="width:90px;" type="text" value="" data-date-format="dd-mm-yyyy">
													<input name="date-end" id="date-end" class="input-s-sm input-s datepicker-input form-control" value="<?=$this->input->get('date-end')?>" style="width:90px;" type="text" value="" data-date-format="dd-mm-yyyy">
												</div>
												<div class="col-xs-3" style="padding: 0 5px 0 0">
													<div class="col-xs-5" style="padding: 0 5px 0 0">
														<button type="button" class="btn btn-sm btn-danger" onClick="$('#form-search').submit();">Filter <i class="fa fa-info-circle"></i></button>
													</div>
													<div class="col-xs-5">
														<a class="btn btn-sm btn-danger" href="<?=URL?>/manager/users">Clear <i class="fa fa-times-circle"></i></a>
													</div>
												</div>
												<div class="col-xs-2"></div>
											</div>
											<div class="col-xs-3">
												<div class="input-group">
													<input type="text" class="form-control" name="search" value="<?=($this->input->get('search'))?$this->input->get('search'):''?>" placeholder="Search">
													<span class="input-group-addon bg-blue" style="cursor: pointer" onClick="$('#form-search').submit();">     
														<span class="arrow"></span><i class="fa fa-search"></i> 
													</span>
												</div>
											</div>
										</div>
									</form>
                                    <table class="table table-striped table-hover dataTable" id="table-editable">
                                        <thead>
                                            <tr role="row">
                                                <th class="align-center">ID</th>
                                                <th class="align-center">Tài khoản</th>
                                                <!--th class="align-center">Email</th-->
                                                <th class="align-center">SĐT</th>
												<?php
													if($this->input->get('order') == 'desc') $order = 'asc';
													else $order = 'desc';
												?>
                                                <th class="align-center">Money <a href="<?=URL?>/manager/users?order_by=money&order=<?=$order?>"><i class="fa fa-sort"></i></a></th>
                                                <th class="align-center">Trạng thái</th>
                                                <th class="align-center">Online <a href="<?=URL?>/manager/users?order_by=last_activity&order=<?=$order?>"><i class="fa fa-sort"></i></a></th>
                                                <th class="align-center"></th>
                                                <th class="align-center"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
										if(isset($users) && is_array($users)) {
											foreach($users as $user) {
												$online = ($user['token'] && ($user['last_activity'] && (time()-$user['last_activity'])<1800))?'<span class="status_online"></span>':'<span class="status_offline"></span>';
										?>
                                            <tr id="user-<?=$user['id']?>">
                                                <td class="align-center"><?=$user['id']?></td>
                                                <td><a class="user_style" href="<?=URL?>/manager/users/item/<?=$user['id']?>"><?=$user['username']?></a></td>
                                                <!--td class="align-center"><?=$user['email']?></td-->
                                                <td class="align-center"><?=$user['phone']?></td>
                                                <td class="align-center"><span class="user_money_icon"></span> <span class="user_money"><?=number_format($user['money'],0,',','.')?></span></td>
                                                <td class="align-center"><?
												if($user['status'] == 'valid') echo '<i class="fa fa-check-circle"></i> Valid';
												elseif($user['status'] == 'pending') echo '<i class="fa fa-clock-o"></i> Pending';
												else echo '<i class="fa fa-minus-circle"></i> Banned';
												?></td>
                                                <td class="align-center"><?=$online?></td>
                                                <td class="align-center"><?=date("d/m/Y",$user['created'])?></td>
                                                <td class="align-center">
													<?php if(permission('set_money')) { ?>
														<a class="action-button" href="javascript:;" onclick="set_money(<?=$user['id']?>, '<?=$user['username']?>')" data-toggle="modal" data-target="#set-money">
															<span class="user_money_icon" style="margin-top: 2px;vertical-align: top; background: transparent url(<?=CDN?>/images/coin.png) no-repeat;"></span>
														</a>
													<?php } ?>
													<?php if(permission('ban_user')) { ?>
														<a class="action-button" href="javascript:;" onclick="ban_user(<?=$user['id']?>, '<?=$user['username']?>', '<?=$user['status']?>')" data-toggle="modal" data-target="#ban-user"><i class="fa fa-ban"></i>
														</a>
													<?php } ?>
													<?php if(permission('edit_user')) { ?>
														<a class="action-button" href="javascript:;" data-toggle="modal" data-target="#item-edit" onclick="render(<?=$user['id']?>)"><i class="fa fa-pencil-square-o"></i>
														</a>
													<?php } ?>
													<?php if(permission('delete_user')) { ?>
														<button class="delete btn btn-sm btn-danger" href="javascript:;" onclick="remove(<?=$user['id']?>);"><i class="fa fa-times-circle"></i></button>
													<?php } ?>
												</td>
                                            </tr>
										<?php 
											}
										} else echo '<tr><td colspan="10" style="text-align: center">No results</td></tr>';
										?>
                                        </tbody>
                                    </table>
									<div class="col-xs-12" style="line-height: 24px;">
										<div class="col-sm-2">Have <?=$total?> users</div>
										<div class="col-sm-2"><?=$users_online?> users online</div>
										<?=$this->user_model->all_pages;?>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="modal fade" id="set-money" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
				<form method="POST">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel"><strong>Edit</strong></h4>
                        </div>
                        <div class="modal-body">
							<form method="POST" id="set_money_form">
								<div id="edit-content">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="hidden" name="action" value="set_money" />
											<input type="hidden" name="id" id="set_money_id" />
											<div class="form-group">
												<label for="money" class="control-label bold">Set tiền: <span id="set_money_username"></span></label>
											</div>
											<div class="form-group">
												<label for="money" class="control-label bold">Số tiền</label>
												<div class="controls">
													<input type="text" name="money" id="money" required="" autocomplete="off" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label for="password" class="control-label bold">Thực hiện</label>
												<div class="controls">
													<div class="col-md-3">
														<input type="radio" value="add" id="add" name="type" checked='checked'>
														<label for="add">Cộng tiền</label>
													</div>
													<div class="col-md-3">
														<input type="radio" value="sub" id="sub" name="type">
														<label for="sub">Trừ tiền</label>
													</div>
													<div class="col-md-6"></div>
													<div class="clearfix"></div>
												</div>
											</div>
											<div class="form-group on_hide">
												<label for="password" class="control-label bold">Loại</label>
												<div class="controls">
													<div class="col-md-3">
														<input type="radio" value="other" id="type_1" name="set_type" checked='checked'>
														<label for="type_1">Khác</label>
													</div>
													<div class="col-md-3">
														<input type="radio" value="delay" id="type_2" name="set_type">
														<label for="type_2">Trễ thẻ</label>
													</div>
													<div class="col-md-3">
														<input type="radio" value="agent" id="type_3" name="set_type"> 
														<label for="type_3">Đại lý</label>
													</div>
													<div class="col-md-3">
														<input type="radio" value="event" id="type_4" name="set_type">
														<label for="type_4">Sự kiện</label>
													</div>
												</div>
											</div>
											<div class="form-group on_hide">
												<label for="password" class="control-label bold">Cộng VIP</label>
												<div class="controls">
													<div class="col-md-3">
														<input type="radio" value="3" id="vip_yes" name="add_vip" checked='checked'>
														<label for="vip_yes">Có</label>
													</div>
													<div class="col-md-3">
														<input type="radio" value="3" id="vip_no" name="add_vip">
														<label for="vip_no">Không</label>
													</div>
													<div class="col-md-6"></div>
													<div class="clearfix"></div>
												</div>
											</div>
											<div class="form-group">
												<label for="note" class="control-label bold">Lý do Set Tiền</label>
												<div class="controls">
													<textarea name="note" id="note" required="" class="form-control" rows="4"></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Set Tiền">
                        </div>
                    </div>
					<script type="text/javascript">
						$("#money").keyup(function(event) {
							transfer_value = removeCommas($("#money").val());
							$("#money").val(addCommas(transfer_value));
						});
						function set_money(id, username) {
							$("#set_money_id").val(id);
							$("#set_money_username").html(username);
						}
						$('input[name="type"]').on('change', function(e) {
							if(e.target.defaultValue=='add') $(".on_hide").show();
							else $(".on_hide").hide();
						});
					</script>
				</form>
                </div>
            </div>
			<div class="modal fade" id="ban-user" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
				<form method="POST">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel"><strong>Edit</strong></h4>
                        </div>
                        <div class="modal-body">
							<form method="POST" enctype="multipart/form-data" class="col-sm-12" autocomplete="on" accept-charset="utf-8">
								<div id="edit-content">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="hidden" name="id" id="ban_id" />
											<input type="hidden" name="action" value="ban_user" />
											<input type="hidden" name="is_ban" id="is_ban" value="1" checked='checked'>
											<div class="form-group">
												<label for="note" class="control-label bold">Lý do <span class="text_ban">Ban</span></label><span class="tips" id="ban_username"></span>
												<div class="controls">
													<textarea name="ban_note" id="ban_note" required="" class="form-control" rows="4"></textarea>
												</div>
											</div>
											<div class="form-group on_hiden">
												<label for="password" class="control-label bold">Thời gian</label>
												<div class="controls">
													<div class="col-md-6">
														<input type="radio" value="day" id="day" name="ban_time" checked='checked'>
														<label for="day">1 ngày</label>
													</div>
													<div class="col-md-6">
														<input type="radio" value="week" id="week" name="ban_time"> 
														<label for="week">1 tuần</label>
													</div>
													<div class="col-md-6">
														<input type="radio" value="month" id="month" name="ban_time">
														<label for="month">1 tháng</label>
													</div>
													<div class="col-md-6">
														<input type="radio" value="forever" id="forever" name="ban_time">
														<label for="forever">Vĩnh viễn</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Thực hiện">
                        </div>
                    </div>
					<script type="text/javascript">
						function ban_user(id, username, status) {
							$("#ban_id").val(id);
							$("#ban_username").html(username);
							if(status=='valid') {
								$(".on_hiden").show();
								$(".text_ban").html('Ban');
								$("#is_ban").val(1);
							} else {
								$(".on_hiden").hide();
								$(".text_ban").html('Unban');
								$("#is_ban").val(0);
							}
						}
					</script>
				</form>
                </div>
            </div>
			<div class="modal fade" id="item-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
				<form method="POST">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel"><strong>Edit</strong></h4>
                        </div>
                        <div class="modal-body">
							<form method="POST" enctype="multipart/form-data" class="col-sm-12" autocomplete="on" accept-charset="utf-8">
								<div id="edit-content">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 0">
												<input type="hidden" name="id" />
												<div class="form-group">
													<label for="username" class="control-label bold">Tài khoản</label><span class="tips">3 - 16 ký tự</span>
													<div class="controls"><input type="text" name="username" id="username" label="Username" required="" class="form-control" /> </div>
												</div>
												<div class="form-group">
													<label for="password" class="control-label bold">Mật khẩu</label><span class="tips">Để trống không thay đổi</span>
													<div class="controls"><input type="password" name="password" id="password" label="Password" class="form-control" /> </div>
												</div>
												<div class="form-group">
													<label for="password_second" class="control-label bold">Mật khẩu lớp 2</label><span class="tips">Để trống không thay đổi</span>
													<div class="controls"><input type="password" name="password_second" id="password_second" label="password_second" class="form-control" /> </div>
												</div>
												<div class="form-group">
													<label for="email" class="control-label bold">Email</label><span class="tips">Email thật để reset mật khẩu</span>
													<div class="controls"><input type="email" name="email" id="email" label="Email" required="" class="form-control" /> </div>
												</div>
												<div class="form-group">
													<label for="phone" class="control-label bold">SĐT</label><span class="tips"></span>
													<div class="controls"><input type="phone" name="phone" id="phone" label="Phone" class="form-control" /> </div>
												</div>
												<div class="form-group">
													<label for="identification" class="control-label bold">Số CMT</label>
													<div class="controls"><input type="text" name="identification" id="identification" label="identification" class="form-control" /> </div>
												</div>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 0">
												<div class="form-group">
													<label for="name" class="control-label bold">Tên đầy đủ</label>
													<div class="controls"><input type="text" name="name" id="name" label="Name" class="form-control" /> </div>
												</div>
												<div class="col-md-6 col-sm-6 col-xs-6">
													<div class="form-group">
														<label for="gender" class="control-label bold">Giới tính</label>
														<div class="controls">
															<select name="gender" id="gender" label="Gender" required="" class="valid form-control">
																<option value="other">Không xác định</option>
																<option value="male">Nam</option>
																<option value="female">Nữ</option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-md-6 col-sm-6 col-xs-6">
													<div class="form-group">
														<label for="status" class="control-label bold">Trạng thái</label>
														<div class="controls">
															<select name="status" id="status1" label="Status" required="" class="valid form-control">
																<option value="valid">Kích hoạt</option>
																<option value="pending">Pending</option>
																<option value="banned">Banned</option> </select>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label for="birthday" class="control-label bold">Ngày sinh</label><span class="tips">Tháng/Ngày/Năm</span>
													<div class="controls">
														<input type="text" name="birthday" id="birthday" label="Birthday" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label for="chatroom_color" class="control-label bold">Màu chat</label>
													<div class="controls">
														<input type="text" name="chatroom_color" id="chatroom_color" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label for="disable_chatroom" class="control-label bold">Chat Rom</label>
													<div class="controls">
														<select name="disable_chatroom" id="disable_chatroom" label="disable_chatroom" required="" class="valid form-control">
															<option value="0">Kích hoạt</option>
															<option value="1">Dừng Chat</option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-6">
												<div class="form-group">
													<label for="avatar" style="width: 100%;" class="control-label bold">URL</label>
													<div class="controls"><input type="text" name="avatar" id="avatar" label="URL" class="form-control" /> </div>
													<div class="controls" style="overflow: hidden;position: absolute;right: 0;top: 31px;width: 30px;">
														<input type="file" name="uploadavatar" id="uploadavatar" label="Upload image" class="form-control filestyle" data-size="sm" data-buttonText="" />
													</div>
												</div>
											</div>
											<div id="avatar"></div>
										</div>
									</div>
								</div>
							</form>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Save changes">
                        </div>
                    </div>
				</form>
                </div>
            </div>
			<script src="<?=CDN?>/cpanel/plugins/datepicker/bootstrap-datepicker.js"></script>
			<script src="<?=CDN?>/cpanel/plugins/filestyle/bootstrap-filestyle.min.js"></script>
			<script type="text/javascript">
				$('#birthday').datepicker();
				$('#date-start').datepicker();
				$('#date-end').datepicker();
				function remove(id) {
					if (confirm("Are you want to delete this tag permanent?")) {
						$.post("<?=URL?>/manager/users/ajax/del", {id:id},
						function(data,status){
							$("#user-"+id).hide();
						});
					}
				}
				function render(id) {
					if(id) {
						$.post("<?=URL?>/manager/users/ajax/get", {id:id},
						function(data,status){
							var data = jQuery.parseJSON(data);
							console.log(data);
							$("input[name=id]").val(data.id);
							$("#username").val(data.username);
							$("#password").val('');
							$("#email").val(data.email);
							$("#name").val(data.name);
							$("#phone").val(data.phone);
							$("#identification").val(data.identification);
							$("#gender").val(data.gender).change();
							$("#status1").val(data.status).change();
							$("#chatroom_color").val(data.chatroom_color);
							$("#disable_chatroom").val(data.disable_chatroom).change();
							if(data.birthday) {
								var date = data.birthday.split('-');
								$("#birthday").val(date[1]+'/'+date[2]+'/'+date[0]);
							}
						});
					} else {
						$("input[name=id]").val('');
						$("#username").val('');
						$("#password").val('');
						$("#email").val('');
						$("#name").val('');
						$("#phone").val('');
						$("#identification").val('');
						$("#gender").val('');
						$("#status").val('');
						$("#disable_chatroom").val('');
					}
				}
			</script>
            <!-- the overlay element -->
		</div>
		<!-- END MAIN CONTENT -->
<?
	} else {
?>
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="error-container">
                <div class="error-main">
                    <h1 style="color: red;">Denied</h1>
                    <h3> You don't have permit in this area. </h3>
                    <h4> Go back to our  <a href="<?=URL?>">site</a> or  contact us about the problem. </h4>
                </div>
            </div>
        </div>
    </div>
<?php
	}
?>