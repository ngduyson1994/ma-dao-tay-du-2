<div class="main_wrapper">
    <div class="main_section">
        <div class="big_news">
            <div class="big_news_info">
                <h1 class="title-404">Oops! 404</h1>
                <div class="content-404">
                    <p style="font-size:25px">Trang này không khả dụng</p>
                    <p>Truy cập của bạn có thể bị lỗi hoặc không tìm thấy nội dung</p>
                </div>
                <div class="gotohome-404">
                    <a href="<?=URL?>" class="btn btn-default"><i class="fa fa-home"></i> Quay trở về trang chủ</a>
                </div>
            </div>
        </div>
        </div>
    </div>