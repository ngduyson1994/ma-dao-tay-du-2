<div class="main_wrapper">
	<div class="banner">
		<?php widget('ads', 13); ?>
	</div>
    <div class="main_section">
        <?php if (isset($posts) && is_array($posts)) { ?>
            <div class="big_news">
                <div class="big_news_img">
                    <a href="<?php echo getPostURL($posts[0]); ?>">
                        <img src="<?php echo getThumb($posts[0], 760, 410); ?>"/>
                    </a>
                </div>
                <div class="big_news_info">
                    <a href="<?php echo getPostURL($posts[0]); ?>">
                        <h2><?php echo cutOf(clearString($posts[0]['title']), 100); ?></h2></a>
                </div>
            </div>
            <div class="news-list">
                <?php foreach ($posts as $key => $post) { ?>
                    <?php if ($key > 0 && $key < 5) { ?>
                        <div class="news">
                            <div class="news_img">
                                <a href="<?php echo getPostURL($post); ?>">
                                    <img src="<?php echo getThumb($post, 760, 410); ?>"/>
                                </a>
                            </div>
                            <div class="news_info">
                                <a href="<?php echo getPostURL($post); ?>"><?php echo cutOf(clearString($post['title']), 100); ?></a>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>
        <div class="game_section">
            <? widget('mobile/top_game'); ?>
        </div>
		<div class="banner">
			<?php widget('ads', 14); ?>
		</div>
        <div class="hot_news_section">
            <div class="section_title">
                <h5>Tin mới cập nhật</h5>
            </div>
            <div id="list" class="news-game">
                <?php
                if (isset($posts) && is_array($posts)) {
                    foreach ($posts as $key => $post) {
                        if ($key > 4 && $key < 17) {
                            ?>
                            <div class="li-news-game">
                                <a title="<?= isset($post['name']) ?>" class="li"
                                   href="<?php echo getPostURL($post); ?>">
                                    <h2 class="title"><?php echo clearString($post['title']); ?></h2>
                                </a>
                                <a title="<?= isset($post['name']) ?>" class="img"
                                   href="<?php echo getPostURL($post); ?>">
                                    <img class="lazy" src="<?php echo getThumb($post, 760, 410); ?>" loaded="true">
                                </a>
                                <div class="right">
                                    <div class="sapo"><?php echo clearString($post['title']); ?></div>
                                    <div class="time">
                                        <?php
                                        if (isset($post['categories']) && $post['categories']) {
                                            $cat = reset($post['categories']);
                                            ?>
                                            <a href="<?= URL ?>/<?= $cat['slug'] ?>"><?= $cat['name'] ?></a> |
                                        <?php } ?>
                                        <?php echo showDate($post['published']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php }
                    }
                } ?>
            </div>
        </div>
        <div id="paginate">
            <center>
                <?= $this->post_model->all_pages; ?>
            </center>
        </div>
		<div class="banner">
			<?php widget('ads', 15); ?>
		</div>
    </div>
</div>