
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?=(isset($title))?$title:$settings['site_title']?></title>
        <link rel="stylesheet" href="<?=CDN?>/core/css/bootstrap.min.css?v=<?=time()?>">
        <link rel="stylesheet" href="<?=CDN?>/core/css/style-mobile.css?v=<?=time()?>">
        <link rel="stylesheet" href="<?=CDN?>/core/css/fontawesome.min.css?v=<?=time()?>">
        <link rel="stylesheet" href="<?=CDN?>/core/css/animate.min.css">
        <link rel="stylesheet" href="<?=CDN?>/core/css/slick.css?v=<?=time()?>" />
        <link rel="icon" type="image/png" href="<?=CDN?>/core/images/favicon.png" sizes="16x16"/>
        <script src="<?=CDN?>/core/js/jquery-3.3.1.min.js?v=<?=time()?>"></script>
        <script src="<?=CDN?>/core/js/jquery-migrate-1.2.1.min.js?v=<?=time()?>"></script>
        <script src="<?=CDN?>/core/js/bootstrap.min.js?v=<?=time()?>"></script>
        <script src="<?=CDN?>/core/js/custom.js?v=<?=time()?>"></script>
        <script src="<?=CDN?>/core/js/slick.min.js?v=<?=time()?>"></script>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-53578032-1', 'auto');
			ga('send', 'pageview');
		</script>
    </head>
    <body>
    <script type="text/javascript">
        $(document).ready(function() {
            var url_s = '<?=URL?>';
            $("#search_input").keyup(function() {
                var search_input = $(this).val();
                var dataString = 'search='+ search_input;
                if(search_input.length>3)
                {
                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: url_s+'/ajax/searchPost',
                        data: dataString,
                        beforeSend:  function() {
                            $('input#search_input').addClass('loading');
                        },
                        success: function(res)
                        {
                            var html = '';
                            $(res.data).each(function(index, element){
                                html += '<li><a href="' + element.url + '"><img src="'+ element.thumb +'"/><p>'+element.title+'</p></a></li>';
                            });
                            $("#finalResult").html(html);
                        }

                    });
                }return false;
            });

        });
    </script>
    <div class="wrapper">
        <div class="cf wrap_header">
            <div id="header_menu">
                <div class="empty"></div>
                <div id="header">
                    <div class="header">
                        <div class="o_h home_main">
                            <a class="home_tab fl" href="/">
                                <h1 title="Kênh tin tức Game hay dành cho Game Thủ">
                                    <span class="btn_home"><i class="sprite-icons"></i></span>
                                </h1>
                            </a>
                            <a class="expand_tab fr" style="border-right: none" href="javascript:void(0)">
                                <span class="btn_menu"><i class="sprite-icons"></i></span>
                                <span class="txt">Menu</span>
                            </a>
                            <a class="search fr" href="javascript:;">
                                <span class="btn_search"><i class="sprite-icons"></i></span>
                                <span>Tìm kiếm</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-icon">
            <form id="search-form" method="GET" action="<?=URL?>/search" style="display: none;">
                <input class="fl" autocomplete="off" type="text" id="search_input" name="s" placeholder="Tìm kiếm..." /><input class="search fl" type="submit" value="Tìm" />
            </form>
                <ul id="finalResult" class="search_ajx"></ul>
            </div>
            <div class="mobile-menu">
                <ul class="mobile-menu-list">
                    <? widget('mobile/main_menu_mobile');?>
                </ul>
            </div>

        </div>