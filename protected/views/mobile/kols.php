<div class="main_wrapper">
    <div class="main_section">
        <div class="player-kols">
            <?php if (isset($kol['stream_code']) && $kol['stream_code']) { ?>
                <iframe class="player-header"
                        src="<?= $kol['stream_code'] ?>?autoplay=1&showinfo=0&controls=0&enablejsapi=1"
                        frameborder="0"></iframe>
            <?php } else { ?>
                <div class='check-live'><h4 class='live-warning-message'>Hiện chưa có kênh trực tiếp!</h4></div>
            <?php } ?>
        </div>
        <div class="info-kols">
            <div class="section_title">
                <h5>Thông tin</h5>
            </div>
            <div class="kols-avatar">
                <img src="<?= $kol['thumb'] ?>"/>
            </div>
            <div class="kols-name">
                <h3><?= $kol['name'] ?></h3>
                <p>- <?= $kol['team'] ?> -</p>
                <div class="kols-social">
                    <a href="<?= $kol['facebook'] ?>">
                        <img src="<?= CDN ?>/core/images/facebook-kol.png"/> <?= str_replace('https://www.facebook.com/', 'fb.com/', $kol['facebook']) ?>
                    </a> <br>
                    <a href="<?= $kol['youtube'] ?>">
                        <img src="<?= CDN ?>/core/images/youtube-kol.png"/> <?= str_replace('https://www.youtube.com/', 'youtube.com/', $kol['youtube']) ?>
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <?php if (isset($posts) && is_array($posts)) { ?>
            <div class="hot_news_section">
                <div class="section_title">
                    <h5>Tin tức liên quan</h5>
                </div>
                <div id="list" class="news-game">
                    <?php
                    foreach ($posts as $key => $post) {
                        ?>
                        <div class="li-news-game">
                            <a title="<?= isset($post['name']) ?>" class="li" href="<?php echo getPostURL($post); ?>">
                                <h2 class="title"><?php echo clearString($post['title']); ?></h2>
                            </a>
                            <a title="<?= isset($post['name']) ?>" class="img" href="<?php echo getPostURL($post); ?>">
                                <img class="lazy" src="<?php echo getThumb($post, 760, 410); ?>" loaded="true">
                            </a>
                            <div class="right">
                                <div class="sapo"><?php echo clearString($post['title']); ?></div>
                                <div class="time">
                                    <?php
                                    if (isset($post['categories']) && $post['categories']) {
                                        $cat = reset($post['categories']);
                                        ?>
                                        <a href="<?= URL ?>/<?= $cat['slug'] ?>"><?= $cat['name'] ?></a> |
                                    <?php } ?>
                                    <?php echo showDate($post['published']) ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>