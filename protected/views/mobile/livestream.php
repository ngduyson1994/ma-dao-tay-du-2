<div class="main_wrapper">
    <div class="player_section">
        <div class="player-header" id="video-filter"></div>
    </div>
    <script>
        var token = '<?=md5('C1Q394MAsS')?>';
        $(document).ready(function () {
            getFeatureHome();
            getListChannel();
        });

        function getListChannel() {
            $.ajax({
                type: 'GET',
                url: '<?=URL?>/api/getListSchedule',
                data: {token: token},
                crossDomain: true,
                dataType: 'jsonp',
                encode: true,
                success: function (res) {
                    var html = '';
                    if (res.status == false) {
                        $("#list_match").html("<div class='check-live'><h4 class='live-warning-message'>" + res.message + "</h4></div>");
                    } else {
                        $(res.data.schedules).each(function (index, element) {
                            html += '<div class="col-md-4 row-col"><div class="video-item"><a href="javascript:;" onclick="stream_home(' + element.id + ');"><img src="' + element.avatar + '" /><p>' + element.name + '</p></a></div></div>';
                        });
                        $("#list_match").html(html);
                    }
                }
            });
        }

        function getFeatureHome() {
            $.ajax({
                type: 'GET',
                url: '<?=URL?>/api/getFeatureHomeSchedule',
                data: {token: token},
                crossDomain: true,
                dataType: 'jsonp',
                encode: true,
                success: function (res) {
                    if (res.status == false) {
                        $("#video-filter").html("<div class='check-live'><h4 class='live-warning-message'>" + res.message + "</h4></div>");
                    } else {
                        if (res.data.status == 0) {
                            $("#video-filter").html("<div class='check-live'><h4 class='live-warning-message'>Kênh offline!</h4></div>");
                        } else {
                            if (res.data.stream_code[0]) {
                                $("#video-filter").html("<iframe width='100%' height='100%' src='" + res.data.stream_code[0] + "/?autoplay=1' frameborder='0' allowfullscreen></iframe>");
                            } else {
                                $("#video-filter").html("<div class='check-live'><h4 class='live-warning-message'>Xin lỗi! Hiện tại chưa có kênh trực tiếp</h4></div>");
                            }
                        }
                    }
                }
            });
        }

        function stream_home(id) {
            $.ajax({
                type: 'GET',
                url: '<?=URL?>/api/getDetailSchedule',
                data: {
                    token: token,
                    id: id
                },
                crossDomain: true,
                dataType: 'jsonp',
                encode: true,
                success: function (res) {
                    if (res.status == false) {
                        $("#video-filter").html("<div class='check-live'><h4 class='live-warning-message'>" + res.message + "</h4></div>");
                    } else {
                        if (res.data.status == 0) {
                            $("#video-filter").html("<div class='check-live'><h4 class='live-warning-message'>Kênh offline!</h4></div>");
                        } else {
                            if (res.data.stream_code[0]) {
                                $("#video-filter").html("<iframe width='100%' height='100%' src='" + res.data.stream_code[0] + "/?autoplay=1' frameborder='0' allowfullscreen></iframe>");
                            } else {
                                $("#video-filter").html("<div class='check-live'><h4 class='live-warning-message'>Xin lỗi! Hiện tại chưa có kênh trực tiếp</h4></div>");
                            }
                        }
                    }
                }
            });
        }

        var page = 1;

        function ShowMore() {
            page++;
            $.ajax({
                type: 'GET',
                url: '<?=URL?>/api/getListSchedule',
                data: {token: token, page: page},
                crossDomain: true,
                dataType: 'jsonp',
                encode: true,
                success: function (res) {
                    var html = '';
                    if (res.status == false) {
                        $("#list_match").html("<div class='check-live'><h4 class='live-warning-message'>" + res.message + "</h4></div>");
                    } else {
                        $(res.data.schedules).each(function (index, element) {
                            html += '<div class="col-md-4 row-col"><div class="video-item"><a href="javascript:;" onclick="stream_home(' + element.id + ');"><img src="' + element.avatar + '" /><p>' + element.name + '</p></a></div></div>';
                        });
                        $("#list_match").append(html);
                    }
                }
            });
        }

        function FilterLive() {
            var page = 1;
            var game = $("#game").val();
            $.ajax({
                type: 'GET',
                url: '<?=URL?>/api/getListSchedule',
                data: {token: token, page: page, game: game},
                crossDomain: true,
                dataType: 'jsonp',
                encode: true,
                success: function (res) {
                    var html = '';
                    if (res.status == false) {
                        $("#list_match").html("<div class='check-live'><h4 class='live-warning-message'>" + res.message + "</h4></div>");
                    } else {
                        $(res.data.schedules).each(function (index, element) {
                            html += '<div class="col-md-4 row-col"><div class="video-item"><a href="javascript:;" onclick="stream_home(' + element.id + ');"><img src="' + element.avatar + '" /><p>' + element.name + '</p></a></div></div>';
                        });
                        $("#list_match").html(html);
                    }
                }
            });
        }
    </script>
    <div class="player_section">
        <div class="main_section">
            <div class="video-section">
                <div class="core-section">
                    <div class="video-type-name">
                        <h4>Live esports stream</h4>
                    </div>
                    <div class="video-filter">
                        <div class="filter-selection type-filter">
                            <p class="filter-item">Type</p>
                            <select id="game" name="game" class="filter-item">
                                <option value="">All</option>
                                <option value="aoe">AOE</option>
                                <option value="cs">CS</option>
                                <option value="lol">Liên Minh</option>
                                <option value="dota">Dota</option>
                                <option value="pes">Pes</option>
                                <option value="lienquan">Liên Quân</option>
                            </select>
                        </div>
                        <button type="submit" name="submit" id="video-filter" onclick="FilterLive();"/>
                        Lọc</button>
                    </div>
                    <div id="list_match" class="video-list"></div>
                    <div class="row show-more">
                        <h5><a href="javascript:;" onclick="ShowMore();">Xem thêm</a></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>