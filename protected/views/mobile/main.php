<div class="main_wrapper">
    <div class="main_section">
		<div class="banner">
			<?php widget('ads', 13); ?>
		</div>
        <? widget('mobile/news_hot'); ?>
        <div class="game_section">
            <? widget('mobile/top_game'); ?>
        </div>
		<div class="banner">
			<?php widget('ads', 14); ?>
		</div>
		<div class="hot_news_section">
			<div class="section_title">
				<h5>Tin mới </h5>
			</div>
			<div id="list" class="news-game"></div>
			<div class="row show-more">
				<h5><a href="javascript:;" onclick="ShowMorePost();">Xem thêm</a></h5>
			</div>
			<script>
				var token = '<?=md5('C1Q394MAsS')?>';
				$(document).ready(function() {
					getListPost();
				});
				function getListPost() {
					$.ajax({
						type: 'GET',
						url: '<?=URL?>/api/getListPosts',
						data: {token: token},
						crossDomain: true,
						dataType: 'jsonp',
						encode: true,
						success : function(res) {
							var html = '';
							if(res.status==false) {
								$("#list").html("<div class='li-news-game'></div>");
							} else {
								$(res.data.posts).each(function(index, element){
									html += '<a href="'+element.url+'" class="li"><h2 class="title">'+element.title+'</h2></a>' +
										'<a class="img" href="'+element.url+'"><img class="lazy" src="'+element.thumb+'" ></a>' +
										'<div class="right">' + '<div class="sapo">'+ element.summary+'</div>' +
										'<div class="time">' +'<a href="">'+ element.user +'</a>' + element.published +'</div></div>'
								});
								$("#list").html(html);
							}
						}
					});
				}
				var page = 1;
				function ShowMorePost() {
					page++;
					$.ajax({
						type: 'GET',
						url: '<?=URL?>/api/getListPosts',
						data: {token: token, page: page },
						crossDomain: true,
						dataType: 'jsonp',
						encode: true,
						success : function(res) {
							var html = '';
							if(res.status==false) {
								$("#list").html("<div class='li-news-game'></div>");
							} else {
								$(res.data.posts).each(function(index, element){
									html += '<a href="'+element.url+'" class="li"><h2 class="title">'+element.title+'</h2></a>' +
										'<a class="img" href="'+element.url+'"><img class="lazy" src="'+element.thumb+'" ></a>' +
										'<div class="right">' + '<div class="sapo">'+ element.summary+'</div>' +
										'<div class="time">' +'<a href="">'+element.user+'</a>' +element.published+'</div></div>'
								});
								$("#list").append(html);
							}
						}
					});
				}
			</script> 
		</div>
		<div class="banner">
			<?php widget('ads', 15); ?>
		</div>
	</div>
</div>
