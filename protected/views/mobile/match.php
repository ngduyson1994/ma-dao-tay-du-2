<div class="main_wrapper">
	<div class="main_section">
		<div class="player-kols">
			<?php if(isset($match['stream_code']) && $match['stream_code']) { ?>
				<iframe class="player-header" src="<?=$match['stream_code']?>?autoplay=1&showinfo=0&controls=0&enablejsapi=1" frameborder="0"></iframe>
			<?php } else { ?>
				<div class='check-live'><h4 class='live-warning-message'>Hiện chưa có kênh trực tiếp!</h4></div>
			<?php }  ?>
		</div>
		<div class="match-title">
			<h5><?=isset($match['tournament']['name'])?$match['tournament']['name']:$match['name']?></h5>
		</div>
		<div class="match-date">
			<p><?=date('H:i d/m/Y',$match['start_time'])?></p>
		</div>
		<div class="match-section">
			<div class="match-section-item match-team">
				<div class="match-team-img">
					<img src="<?=$match['away']['avatar']?>" width="90" height="90">
				</div>
				<div class="match-team-name">
					<p><?=$match['home']['name']?> </p>
				</div>
			</div>
			<div class="match-section-item match-detail">
				<h5>VS</h5>
			</div>
			<div class="match-section-item match-team">
				<div class="match-team-img">
					<img src="<?=$match['away']['avatar']?>" width="90" height="90">
				</div>
				<div class="match-team-name">
					<p><?=$match['away']['name']?></p>
				</div>
			</div>
		</div>
		<?php  if(isset($posts) && is_array($posts)) { ?>
		<div class="hot_news_section">
			<div class="section_title">
				<h5>Tin tức liên quan</h5>
			</div>
			<div id="list" class="news-game">
				<?php
				foreach($posts as $key=>$post) {
				?>
				<div class="li-news-game">
					<a title="<?=isset($post['name'])?>" class="li" href="<?php echo getPostURL($post);?>">
						<h2 class="title"><?php echo clearString($post['title']);?></h2>
					</a>
					<a title="<?=isset($post['name'])?>" class="img" href="<?php echo getPostURL($post);?>">
						<img class="lazy" src="<?php echo getThumb($post, 760, 410);?>"  loaded="true">
					</a>
					<div class="right">
						<div class="sapo"><?php echo clearString($post['title']);?></div>
						<div class="time">
							<?php
								if(isset($post['categories']) && $post['categories']) {
								$cat = reset($post['categories']);
							?>
							<a href="<?=URL?>/<?=$cat['slug']?>"><?=$cat['name']?></a> | 
							<?php } ?>
							<?php echo showDate($post['published'])?>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>
	</div>
</div>