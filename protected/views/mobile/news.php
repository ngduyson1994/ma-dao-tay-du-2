<div class="hot_news_section">
    <div class="section_title">
        <h5>TIN TỨC</h5>
    </div>

    <div id="list" class="news-game">
        <?php
        if(isset($posts) && is_array($posts)) {
            foreach($posts as $post) {
                ?>
                <div class="li-news-game">
                    <a title="<?=isset($post['name'])?>" class="li" href="<?php echo getPostURL($post);?>">
                        <h2 class="title"><?php echo clearString($post['title']);?></h2>
                    </a>
                    <a title="<?=isset($post['name'])?>" class="img" href="<?php echo getPostURL($post);?>">
                        <img class="lazy" src="<?php echo getThumb($post, 760, 410);?>"  loaded="true">
                    </a>
                    <div class="right">
                        <div class="sapo"><?php echo clearString($post['title']);?></div>
                        <div class="time">
                            <?php
                            if(isset($post['categories']) && $post['categories']) {
                                $cat = reset($post['categories']);
                                ?>
                                <a href="<?=URL?>/<?=$cat['slug']?>"><?=$cat['name']?></a> |
                            <?php } ?> <?php echo showDate($post['published'])?>
                        </div>
                    </div>
                </div>
            <?php } } ?>
            <div id="paginate">
                <center>
                    <?=$this->post_model->all_pages;?>
                </center>
            </div>
    </div>
</div>
