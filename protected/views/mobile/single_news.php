<?php
$post_url = getPostURL($post);
?>
<div class="main_wrapper">
    <div class="banner">
		<?php widget('ads', 13); ?>
	</div>
    <div class="newstop o_h">
        <?php
        if (isset($post['categories']) && $post['categories']) {
            $cat = reset($post['categories']);
            ?>
            <a class="fl catinfo" href="<?= URL ?>/<?= $cat['slug'] ?>"><?= $cat['name'] ?></a>
        <?php } ?>
        <a class="fr mgr2 back" href="javascript: history.go(-1)"><i class="back-icon"></i>Quay lại</a>
    </div>
    <div class="detail-game o_h">
        <h1><?php echo clearString($post['title']); ?></h1>
        <div class="meta-info">
            <span class="meta-author"><?= (isset($post['list']['username']) && $post['list']['username']) ? $post['list']['username'] : 'GTV News' ?></span>
            -
            <a class="meta-source" target="_blank" rel="nofollow"
               href="<?= URL ?>"><?php echo showDate($post['published']); ?></a>
        </div>

        <div class="o_h social">
            <div class="fb-like" data-href="<?= $post_url ?>" data-layout="standard" data-action="like"
                 data-size="small" data-show-faces="true" data-share="true"></div>
        </div>
        <div class="detailct-game o_h">
            <h2 class="sapo">
                <?php echo clearString($post['title']); ?>
            </h2>

            <ul class="relation-news">
                <?php
                if (isset($post_realted) && is_array($post_realted)) {
                    foreach ($post_realted as $key => $tag) {
                        if ($key > 0 && $key < 4) {
                            ?>
                            <li><h3 class="title"><a
                                            href="<?php echo getPostURL($tag); ?>"><?php echo clearString($tag['title']); ?></a>
                                </h3></li>
                            <?php
                        }
                    }
                }
                ?>
            </ul>
            <div class="single_content">
                <?php
					//$ad_code = '<center><div style="padding: 6px 0;"><a href="https://gametvplus.com/" target="_blank"><img src="https://2.bp.blogspot.com/-myTYhL-YcSE/XGaagZhCXzI/AAAAAAAAAEU/nAXTmbTmbuYt7VMLSIyA1KKeQm8DoyKsQCLcBGAs/s0/336x280-Plus.png"></a></div></center>';
					$ad_code = '';
					$content = clearString($post['content'], false);
					$content = str_replace(array('&lt;', '&gt;'), array('<', '>'), $content);
					echo ads_content( $ad_code,6,$content);
                ?>
            </div>
            <?php if (count($post['tags'])) { ?>
                <div class="tagnew">
                    <strong>
                        Xem thêm:<?php
                        foreach ($post['tags'] as $tag) {
                            ?>
                            <a href="<?= URL . '/tag/' . trim($tag['slug']) ?>"
                               title="<?= isset($post['name']) ?>"><?= trim($tag['name']) ?></a>
                            <?php
                        }
                        ?>
                    </strong>
                </div>
            <?php } ?>
        </div>
        <div id="related-embed-box" class="related-embed-box type1 ">
            <div class="box-header">
                <h4 class="box-header-title">Tin liên quan</h4>
            </div>

            <ul class="list-news">
                <?php
                if (isset($post['tags']) && $post['tags']) {
                $tag_id = reset($post['tags']);
                ?>
                <? widget('mobile/cat_news_top',$tag_id); ?>
                <?php } ?>
            </ul>
            
           
        </div>
        <div class="game_section">
            <? widget('mobile/top_game'); ?>
        </div>
		<div class="banner">
			<?php widget('ads', 14); ?>
		</div>
        <div class="hot_news_section">
            <div class="section_title">
                <h5>Tin Cùng Chuyên Mục </h5>
            </div>
            <div id="list" class="news-game">

                <? widget('mobile/cat_news'); ?>
            </div>
<!--            Comment-->
            <p class="top_p">
                Bình luận về bài viết
            </p>
            <div class="fb-comments" data-href="<?=$post_url?>" data-numposts="10"></div>

        </div>
        <div class="clearfix ovh" id="end-post-content" style="width: 100%;"></div>
		
		<div class="banner">
			<?php widget('ads', 15); ?>
		</div>
    </div>
</div>

