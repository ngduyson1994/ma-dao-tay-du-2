<div class="main_wrapper">
	<div class="main_section">
		<div class="video-section">
			<div class="video-type-name">
				<h4>VIDEO</h4>
			</div>
			<div id="list_match" class="video-list">
                <?php
                if(isset($posts) && is_array($posts)) {
                foreach($posts as $post) {
                ?>
				<div class="video-item">
                    <a href="<?php echo getPostURL($post);?>">
                        <i class="icon-play"></i>
                        <img src="<?php echo getThumb($post, 760, 410);?>" />
                        <p><?php echo cutOf(clearString($post['title']),180);?></p>
					</a>
				</div>
                <?php }}?>
			</div>
		</div>
	</div>
</div>