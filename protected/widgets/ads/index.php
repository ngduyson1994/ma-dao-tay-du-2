 <?php
class Ads extends Widgets{
    public function __construct() {
        parent::widgets();
    }
    function run($zid=null) {
		$this->load->model('adlink_model');
		$parameter['arg'][] = array('zid', $zid);
		$parameter['with'] = array('code');
		$data['adlinks'] = $this->xcache->model('adlink_model', 'get_all', array($parameter));
        $dir = dirname(__FILE__);
        return $this->build($dir, $data);
    }
}
?> 