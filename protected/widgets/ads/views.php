<?php
if(isset($adlinks) && is_array($adlinks)) {
	$ads = array();
	$default = array();
	foreach($adlinks as $adlink) {
		$adlink['expired'] = intval($adlink['expired']);
		if(($adlink['expired'] > 0 && $adlink['expired'] > time()) || $adlink['expired']==0) {
			if($adlink['weight']==0) $default[] = $adlink['code'];
			else for($j=0;$j<$adlink['weight']; $j++) {
				$ads[] = $adlink['code']['code'];
			}
		}
	}
	if(count($ads)==0 && count($default)) $code = $default[rand(0, count($default)-1)]['code'];
	elseif(count($ads)>0) $code = $ads[rand(0, count($ads)-1)];
	else $code = '';
	echo ($code)?base64_decode($code):'';
}
?>