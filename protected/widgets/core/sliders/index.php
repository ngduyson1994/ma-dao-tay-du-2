 <?php
class Sliders extends Widgets{
    public function __construct() {
        parent::widgets();
    }
    function run() {
		$parameter = array();
		$parameter['limit'] = 5;
		$parameter['order'] = array('order'=>'ASC', 'order_by'=>'order');
		$data['sliders'] = $this->xcache->model('slider_model', 'get_all', array($parameter));
        $dir = dirname(__FILE__);
        return $this->build($dir, $data);
    }
}
?> 