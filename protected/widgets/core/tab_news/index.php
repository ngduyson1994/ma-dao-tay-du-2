 <?php
class Tab_news extends Widgets{
    public function __construct() {
        parent::widgets();
    }
    function run() {
        /* Tin tức*/
        $parameter = array();
        $parameter['arg'][] = array('type', 'news');
        $parameter['arg'][] = array('category', 1);
        $parameter['limit'] = 6;
        $parameter['with'] = array('relationships');
        $parameter['order'] = array('order'=>'DESC', 'order_by'=>'id');
        $data['post_1'] = $this->xcache->model('post_model', 'get_all', array($parameter));
        /* Tin tức*/
        $parameter = array();
        $parameter['arg'][] = array('type', 'news');
        $parameter['arg'][] = array('category', 2);
        $parameter['limit'] = 6;
        $parameter['with'] = array('relationships');
        $parameter['order'] = array('order'=>'DESC', 'order_by'=>'id');
        $data['post_2'] = $this->xcache->model('post_model', 'get_all', array($parameter));
        /* Tin tức*/
        $parameter = array();
        $parameter['arg'][] = array('type', 'news');
        $parameter['arg'][] = array('category', 3);
        $parameter['limit'] = 6;
        $parameter['with'] = array('relationships');
        $parameter['order'] = array('order'=>'DESC', 'order_by'=>'id');
        $data['post_3'] = $this->xcache->model('post_model', 'get_all', array($parameter));

        /* Sự kiện*/
        $parameter = array();
        $parameter['arg'][] = array('type', 'news');
        $parameter['arg'][] = array('category', 4);
        $parameter['limit'] = 6;
        $parameter['with'] = array('relationships');
        $parameter['order'] = array('order'=>'DESC', 'order_by'=>'id');
        $data['post_4'] = $this->xcache->model('post_model', 'get_all', array($parameter));
        $dir = dirname(__FILE__);
        return $this->build($dir, $data);
    }
}
?> 