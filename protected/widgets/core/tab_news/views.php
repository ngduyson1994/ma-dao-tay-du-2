<div class="tabTxt tabBox">
    <div class="tab_h newsTab clearfix">
        <ul>
            <li class="cur">Tin tức</li>
            <li>Sự kiện</li>
            <li>Hướng dẫn</li>
            <li>Hệ thống</li>
        </ul>
    </div>
    <a href="/tin-tuc" target="_blank" class="more">Xem thêm</a>
    <div class="newsTag tabGroup">
        <div class="tab_c tabUnit">
            <ul class="public newsL">
                <?php if(isset($post_1) && is_array($post_1)) { ?>
					<?php foreach($post_1 as $post) { ?>
					   <li>
						   <b>Tin tức</b>
						   <a href="<?php echo getPostURL($post);?>" target="_blank" title="<?=isset($post['name'])?>"><?php echo clearString($post['title'],1);?></a>
						   <span><?=date('d/m',$post['published'])?></span>
					   </li>
					<?php } ?> 
				<?php } ?>
            </ul>
            <p class="news-title"><a title="Xem thêm" target="_blank" href="/tin-tuc"><i></i>Xem thêm</a> </p>
        </div>
        <div class="tab_c tabUnit">
            <ul class="public newsL">
                <?php if(isset($post_2) && is_array($post_2)) { ?>
					<?php foreach($post_2 as $post) { ?>
					   <li>
						   <b>Sự kiện</b>
						   <a href="<?php echo getPostURL($post);?>" target="_blank" title="<?=isset($post['name'])?>"><?php echo clearString($post['title'],20);?></a>
						   <span><?=date('d/m',$post['published'])?></span>
					   </li>
					<?php } ?>
				<?php } ?>
            </ul><!--1-->
            <p class="news-title"><a title="Xem thêm" target="_blank" href="/su-kien"><i></i>Xem thêm</a> </p>
        </div>
        <div class="tab_c tabUnit">
            <ul class="public newsL">
                <?php if(isset($post_3) && is_array($post_3)) { ?>
					<?php foreach($post_3 as $post) { ?>
					   <li>
						   <b>Hướng dẫn</b>
						   <a href="<?php echo getPostURL($post);?>" target="_blank" title="<?=isset($post['name'])?>"><?php echo clearString($post['title'],20);?></a>
						   <span><?=date('d/m',$post['published'])?></span>
					   </li>
					<?php } ?> 
				<?php } ?>
            </ul><!--2-->
            <p class="news-title"><a title="Xem thêm" target="_blank" href="/huong-dan"><i></i>Xem thêm</a> </p>
        </div>
        <div class="tab_c tabUnit">
            <ul class="public newsL">
                <?php if(isset($post_4) && is_array($post_4)) { ?>
					<?php foreach($post_4 as $post) { ?>
					   <li>
						   <b>Hệ thống</b>
						   <a href="<?php echo getPostURL($post);?>" target="_blank" title="<?=isset($post['name'])?>"><?php echo clearString($post['title'],20);?></a>
						   <span><?=date('d/m',$post['published'])?></span>
					   </li>
					<?php } ?> 
				<?php } ?>
            </ul><!--3-->
            <p class="news-title"><a title="Xem thêm" target="_blank" href="/lich-mo-sv"><i></i>Xem thêm</a> </p>
        </div>
    </div>
</div>