 <?php
class Cat_news extends Widgets{
    public function __construct() {
        parent::widgets();
    }
    function run() {
        $cat = null;
        $data = init_data();
        $parameter = array();
        $parameter['arg'][] = array('type', 'news');
        $parameter['arg'][] = array('status', 'publish');
        $data['post'] = $this->xcache->model('post_model', 'get', array($parameter));
        if($cat['id']) {
            $parameter['arg'][] = array('category', $cat['id']);
        }
        $parameter['exclude'][] = $data['post']['id'];
        $parameter['limit'] = 6;
        $parameter['with'] = array('relationships');
        $parameter['order'] = array('order'=>'DESC', 'order_by'=>'id');
        $data['posts'] = $this->xcache->model('post_model', 'get_all', array($parameter));
        $dir = dirname(__FILE__);
        return $this->build($dir, $data);
    }
}
?>

