
<?php
if (isset($posts) && is_array($posts)) {
    foreach ($posts as $post) {
        if (isset($post['categories']) && $post['categories']) {
            $cat = reset($post['categories']);

        ?>
        <div class="li-news-game">
            <a class="li" href="<?= URL ?>/<?= $cat['slug'] ?>" title="<?= isset($post['name']) ?>">
                <h2 class="title"
                    style="font-weight: bold;"><?php echo clearString($post['title']); ?></h2>
            </a>
            <a href="<?php echo getPostURL($post); ?>" class="img">
                <img src="<?php echo getThumb($post, 277, 156); ?> "/>
            </a>
            <div class="right">
                <div class="sapo"><?php echo clearString($post['title']); ?></div>
                <div class="time">
                    <a href="<?php echo getPostURL($post); ?>"><?= $cat['slug'] ?>--<?php echo showDate($post['published']); ?></a>
                </div>
            </div>
        </div>

    <?php } } }  ?>