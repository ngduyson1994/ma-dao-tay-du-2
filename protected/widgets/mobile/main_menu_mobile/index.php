 <?php
class Main_menu_mobile extends Widgets{
    public function __construct() {
        parent::widgets();
    }
    function run(){
		$this->load->model('menu_data_model');
		$data['menu_main'] = $this->xcache->model('menu_data_model', 'getList', array(1));
		$parameter = array();
		$parameter['arg'][] = array('parent', 29);
		$parameter['order'] = array('order_by'=>'order', 'order'=>'ASC');
		$data['gameshows'] = $this->xcache->model('menu_data_model', 'get_all', array($parameter));
        $dir = dirname(__FILE__);
        return $this->build($dir, $data);
    }
}
?> 