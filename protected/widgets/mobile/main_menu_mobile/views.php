
<?php
if(isset($menu_main)) foreach($menu_main as $key=>$menu) {
    if(isset($menu['slug'])) $menu_link = getCatURL($menu['slug']); else $menu_link = $menu['value'];
    $class = '';
    if(isset($menu['slug']) && $menu['slug'] == $this->uri->segment(1)) $class .= " active";
    elseif(str_replace('/', '', $menu['value']) == $this->uri->segment(1)) $class .= " active";
    $sub = '';
    if($menu['child'] && count($menu['child'])>0) {
        $sub = '<a href="javascript:;" class="menu-toggle"><i class="fas fa-angle-down"></i></a> <ul class="sub-menu-list">';
        foreach($menu['child'] as $cmenu) {
			if(isset($cmenu['slug'])) $cmenu_link = getCatURL($cmenu['slug']); else $cmenu_link = $cmenu['value'];
            $sub .= '<li  class="menu-item" ><a href="'.$cmenu_link.'" class="menu-toggle">'.clearString($cmenu['title'], false).'</a>'.'</li>';
        }
        $sub .= '</ul>';
        echo '<li  class="menu-item '.$class.' menu-has-children"><a class="menu-item-name" id="menu-'.$menu['id'].'" href="'.$menu_link.'">'.clearString($menu['icon'], false).' '.clearString($menu['title'], false).' </a>'.$sub.'</li>';
    } else echo '<li class="menu-item'.$class.'"><a class="menu-item-name" id="menu-'.$menu['id'].'" href="'.$menu_link.'">'.clearString($menu['icon'], false).' '.clearString($menu['title'], false).'</a></li>';
}
?>