<?php
class News_hot extends Widgets{
    public function __construct() {
        parent::widgets();
    }
    function run() {
        $parameter = array();
        $parameter['arg'][] = array('type', 'news');
        $parameter['limit'] = 5;
        $parameter['with'] = array('relationships');
        $parameter['order'] = array('order'=>'DESC', 'order_by'=>'id');
        $data['posts'] = $this->xcache->model('post_model', 'get_all', array($parameter));
        $dir = dirname(__FILE__);
        return $this->build($dir, $data);
    }
}
?> 