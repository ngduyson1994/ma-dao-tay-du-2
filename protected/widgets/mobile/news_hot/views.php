<?php if(isset($posts) && is_array($posts)) { ?>
    <div class="big_news">
        <div class="big_news_img">
            <a href="<?php echo getPostURL($posts[0]);?>">
                <img src="<?php echo getThumb($posts[0], 760, 410);?>" />
            </a>
        </div>
        <div class="big_news_info">
            <a href="<?php echo getPostURL($posts[0]);?>"><h2><?php echo cutOf(clearString($posts[0]['title']),100);?></h2></a>
        </div>
    </div>
    <div class="news-list">
        <?php foreach($posts as $key=>$post) { ?>
            <?php if ($key > 0){ ?>
                <div class="news">
                    <div class="news_img">
                        <a href="<?php echo getPostURL($post);?>">
                            <img src="<?php echo getThumb($post, 760, 410);?>" />
                        </a>
                    </div>
                    <div class="news_info">
                        <a href="<?php echo getPostURL($post);?>"><?php echo cutOf(clearString($post['title']),100);?></a>
                    </div>

                </div>
            <?php } ?>
        <?php } ?>
    </div>
<?php } ?>

