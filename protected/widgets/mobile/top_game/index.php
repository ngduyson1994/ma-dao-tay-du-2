 <?php
class Top_game extends Widgets
{
    public function __construct()
    {
        parent::widgets();
    }

    function run()
    {
        $parameter = array();
        $parameter['arg'][] = array('feature', 1);
        $parameter['limit'] = 5;
        $parameter['with'] = array('relationships');
        $parameter['order'] = array('order' => 'ASC', 'order_by' => 'rank');
        $data['game'] = $this->xcache->model('game_model', 'get_all', array($parameter));
        $dir = dirname(__FILE__);
        return $this->build($dir, $data);
    }
}
?>

