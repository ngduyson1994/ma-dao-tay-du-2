<div class="section_title">
    <h5>Top game </h5>
</div>
<div class="game_list">
    <?php if(isset($game) && is_array($game)) { ?>
    <?php   foreach($game as $key=>$item)
    {

    ?>
    <div class="game_list_item">
        <div class="game_img">
            <a href="<?=$item['link']?>">
                <img src="<?=$item['thumb']?>" />
            </a>
        </div>
        <div class="game_name">
            <a href="<?=$item['link']?>"><?=$item['name']?></a>
        </div>
    </div>
    <?php } } ?>
</div>