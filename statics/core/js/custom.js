function myShow(n) {
    function e(n) {
        index = n;
        var e = 0;
        for (d = 0; d < f.length; d++) f[d].className = "";
        for (f[index].className = "current", clearInterval(c), d = 0; d < s.length; d++) s[d].style.opacity = 0, s[d].style.filter = "alpha(opacity=0)", s[d].style.display = "none";
        if (c = setInterval(function() {
                e += 15, e > 100 && (e = 100), s[index].style.opacity = e / 100, s[index].style.filter = "alpha(opacity = " + e + ")", s[index].style.display = "block"
            }, 70), !window.XMLHttpRequest) {
            for (d = 0; d < s.length; d++) s[d].style.display = "none";
            s[index].style.display = "block"
        }
    }

    function i() {
        play = setInterval(function() {
            index++, index >= s.length && (index = 0), e(index)
        }, 3500)
    }
    var t = document.getElementById(n),
        a = t.getElementsByTagName("span")[0],
        l = t.getElementsByTagName("span")[1],
        o = t.getElementsByTagName("ul")[0],
        s = o.getElementsByTagName("li"),
        c = play = null,
        d = index = 0,
        u = [],
        f = null;
    for (d = 0; d < s.length; d++) u.push("<li>" + (d + 1) + "</li>");
    var r = document.createElement("ul");
    for (r.className = "count", r.innerHTML = u.join(""), t.appendChild(r), f = t.getElementsByTagName("ul")[1].getElementsByTagName("li"), e(index), d = 0; d < f.length; d++) f[d].index = d, f[d].onmouseover = function() {
        e(this.index)
    };
    i(), t.onmouseover = function() {
        clearInterval(play)
    }, t.onmouseout = function() {
        i()
    }, l && (a.onclick = function() {
        index--, index < 0 && (index = s.length - 1), e(index)
    }, l.onclick = function() {
        index++, index >= s.length && (index = 0), e(index)
    })
}

function selectList() {
    var n = document.getElementById("search"),
        e = (n.getElementsByTagName("span")[0], n.getElementsByTagName("ul")[0]),
        i = e.getElementsByTagName("li"),
        t = 0;
    for (n.onclick = function(n) {
            var i = e.style;
            i.display = "block" == i.display ? "none" : "block", (n || window.event).cancelBubble = !0
        }, t = 0; t < i.length; t++) i[t].onmouseover = function() {
        this.className = "hover"
    }, i[t].onmouseout = function() {
        this.className = ""
    };
    document.onclick = function() {
        e.style.display = "none"
    }
}
$(function() {
        $.fn.hc_proTabChange = function(n) {
            var n = n || "click",
                e = $(this);
            e.find(".tab_h li").each(function(i) {
                $(this)[n](function() {
                    $(this).addClass("cur").siblings("li").removeClass("cur"), e.find(".tab_c").eq(i).show().siblings(".tab_c").hide()
                })
            })
        }
    }),
    function() {
        $backToTopEle = $('<div class="back_top"></div>').appendTo($("body")).click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, 200)
        }), $backToTopFun = function() {
            var n = $(document).scrollTop();
            $(window).height();
            n > 0 ? $backToTopEle.show() : $backToTopEle.hide()
        }, $(window).bind("scroll", $backToTopFun), $(function() {
            $backToTopFun()
        })
    }(), $(function() {
        $(".zy-tab a").click(function() {
            var n = $(".zy-tab a").index(this);
            $(this).addClass("cur").siblings().removeClass("cur"), $(".zy-tac").eq(n).fadeIn(500).siblings().fadeOut(500)
        }), $(".data-con li").hover(function() {
            $(this).find(".data-list").fadeIn(500)
        }, function() {
            $(this).find(".data-list").fadeOut(500)
        }), $(".yh .video-tab a").click(function() {
            var n = $(".yh .video-tab a").index(this);
            $(this).addClass("cur").siblings().removeClass("cur"), $(".yh-con .tab_c").eq(n).show().siblings().hide()
        })
    });
