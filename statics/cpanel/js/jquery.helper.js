jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
    this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
    return this;
}

jQuery.fn.onlyNumbers = function () {
	this.keydown(function(event) {
        if ( event.keyCode == 46 || event.keyCode == 8 ) {
        }
        else {
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
}

$(function() {
    $("a#gamebai_start").on('click', function() {
        jQuery("#thongbaoModal1").modal('show');
        return  false;
    });
});

function xoGameStar() {
    jQuery('#thongbaoModal1').modal('hide');
    var formData = {
        'ajax': 'ajax'
    };
    var base_url = "http://trading.gametv.vn";
    $.ajax({
        url: base_url + "/login/gameBaiLogin",
        type: "GET",
        data: formData, // our data object
        crossDomain: true,
        dataType: 'jsonp', // what type of data do we expect back from the server
        encode: true,
        beforeSend: function(xhr) {

        },
        timeout: 5000,
        error: function(xhr, status) {
            switch (status) {
                case "timeout":

                    break;

                default:

                    break;
            }
        }, 
        success: function(response) {
            if(response.Error_Code == 0) {
                var href = response.url;
//                    window.open(href);
//                    window.open(response.url, "", "width=1000,height=500,left=100,top=100,resizable=yes,scrollbars=no");
                window.location.href = response.url;
                return false;
            } else {
                alert(response.Error_Message);
                return false;
            }
        }
    });
    return false;
}